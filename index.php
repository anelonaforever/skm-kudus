<!DOCTYPE html>
<html lang="id-ID" ng-app="portalApp">
  <head>
    <meta charset="utf-8">
    <base href="/skm-kudus/index.php">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Bappeda Kota Semarang, SIMPERDA Kota Semarang">
    <meta name="keyword" content="simperda semarang, perencanaan semarang, penganggaran semarang, musrenbang semarang, simperda pemkot semarang">
    <meta name="author" content="BAPPEDA Kota Semarang, dinustek">
    <!-- browser color -->
    <meta name="theme-color" content="#003687">
    <meta name="msapplication-navbutton-color" content="#003687">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <link rel="shortcut icon" href="assets/img/favicon.png">
    <title>AKUPuas! - Aplikasi Pengukuran Kepuasan Masyarakat Kab.Kudus</title>
    <link rel="stylesheet" type="text/css" href="assets/css/bundle.css" media="all">
    <link rel="stylesheet" type="text/css" href="assets/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" media="all">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,600,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->    
  </head>
  <body ng-controller="mainController">
    <button id="btnLogout" ng-click="logoutsurvey()" class="btn btn-light float-right" ng-show="logged_user.id_opd"><i>Logout</i></button>
    <div class="container">
      <div class="row pt-5">
        <div class="col-md-12">
          <div class="d-flex bd-highlight mb-3">
            <div class="p-2 bd-highlight"><img src="assets/img/aku-puas.png" alt="Aku Puas" class="img-fluid"></div>
            <div class="p-2 bd-highlight align-self-end d-none d-sm-none d-md-block">
              <p class="text-nowrap" style="font-size: 17px; margin-bottom: 5px !important;">Aplikasi Pengukuran Kepuasan Masyarakat Kab. Kudus</p>
            </div>
            <div class="ml-auto p-2 bd-highlight"><img src="assets/img/kudus-taste-of-java.png" alt="Kudus Taste of Java" class="img-fluid"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="container" ng-show="!logged_user.nama_opd">
      <nav class="navbar navbar-expand-lg navbar-dark bg-primary mb-4">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarMenu">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="[[baseUrl]]"><i class="fas fa-home"></i> Beranda<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="[[baseUrl]]/berita/1"><i class="far fa-newspaper"></i> Berita</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="[[baseUrl]]/kontak"><i class="fas fa-phone"></i> Kontak</a>
            </li>
          </ul>
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="[[baseUrl]]/login_survey"><i class="fas fa-key"></i> Login Survey</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="[[baseUrl]]/admin" target="_blank"><i class="fas fa-sign-in-alt"></i> Login Admin</a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
    <main id="isi" ng-view></main>
    <section class="col-sm-12 text-center px-5 pt-5">
      <b style="font-size: 1.3rem;"><span id="hari"></span> <span id="tanggal"></span> &nbsp; <span id="waktu"></span></b>
    </section>
    <footer class="col-sm-12 text-center px-5 py-5">
      <p>&copy; 2018 <a href="http://kuduskab.go.id">Aku Puas! Kabupaten Kudus</a>&nbsp; &bull; &nbsp; dikembangkan oleh <a target="_blank" href="http://dinustek.com">dinustek</a>.
    </footer>
    <script type="text/javascript" src="assets/js/bundle.js"></script>
    <script type="text/javascript" src="assets/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="assets/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <!-- angularjs app -->
    <script src="assets/js/app.js"></script>
  </body>
</html>
