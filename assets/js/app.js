var portalApp = angular.module('portalApp', ['ngRoute', 'ngAnimate', 'ngSanitize', 'angular-loading-bar', 'ui.bootstrap']);
portalApp.config(function($interpolateProvider, $routeProvider, $locationProvider, cfpLoadingBarProvider, $httpProvider) {
  $locationProvider.hashPrefix('');
  $interpolateProvider.startSymbol('[[');
  $interpolateProvider.endSymbol(']]');
  $locationProvider.html5Mode(true);

  $routeProvider
  .when('/', {
    templateUrl: 'view/beranda.html',
    controller: 'navCtrl'
  })
  .when('/detail_ikm/:id', {
    templateUrl: 'view/detail_ikm.html',
    controller: 'navCtrl'
  })
  .when('/detail_ikm/:id/:id_layanan', {
    templateUrl: 'view/detail_ikm.html',
    controller: 'navCtrl'
  })
  .when('/berita/:hal', {
    templateUrl: 'view/berita.html',
    controller: 'navCtrl'
  })
  .when('/baca-berita/:judul', {
    templateUrl: 'view/baca-berita.html',
    controller: 'navCtrl'
  })
  .when('/kontak', {
    templateUrl: 'view/kontak.html',
    controller: 'navCtrl'
  })
  .when('/login_survey', {
    templateUrl: 'view/login_survey.html',
    controller: 'navCtrl'
  })
  .when('/beranda_survey', {
    templateUrl: 'view/beranda_survey.html',
    controller: 'navCtrl'
  })
  .when('/isi_profil', {
    templateUrl: 'view/isi_profil.html',
    controller: 'navCtrl'
  })
  .when('/pilih_layanan', {
    templateUrl: 'view/pilih_layanan.html',
    controller: 'navCtrl'
  })
  .when('/kuesioner', {
    templateUrl: 'view/kuesioner.html',
    controller: 'navCtrl'
  })
  .when('/saran', {
    templateUrl: 'view/saran.html',
    controller: 'navCtrl'
  })
  .when('/finish', {
    templateUrl: 'view/finish.html',
    controller: 'navCtrl'
  })
  .when('/404', {
    templateUrl: 'view/404.html',
    controller: 'navCtrl'
  })
  .otherwise({redirectTo: '/404'});
  cfpLoadingBarProvider.includeSpinner = false;
  cfpLoadingBarProvider.parentSelector = '.loading-bar-container';
});

portalApp.run( function($rootScope, $location) {
  // register listener to watch route changes
  var location = $location.url().split('/');
  var history = [];
  $rootScope.$on( "$routeChangeStart", function(event, next, current) {
    history.push($location.$$path);

    if ( localStorage.getItem("logged_user") == null) {
      // no logged user, we should be going to #login
      if(next.templateUrl == 'view/isi_profil.html' || next.templateUrl == 'view/pilih_layanan.html' || next.templateUrl == 'view/kuesioner.html' || next.templateUrl == 'view/finish.html' || next.templateUrl == 'view/beranda_survey.html'){
        if ( next.templateUrl != "view/login_survey.html" ) {
          $location.path( "/login_survey" );
        }
      }            
    }else{
      if(next.templateUrl == "view/login_survey.html" || next.templateUrl == "view/beranda.html" || next.templateUrl == "view/berita.html" || next.templateUrl == "view/baca-berita.html" || next.templateUrl == "view/kontak.html"){
        $location.path( "/beranda_survey" );
      }

      if(next.templateUrl == "view/isi_profil.html"){
        var datalayanan = JSON.parse(localStorage.getItem("datalayanan"));
        if(datalayanan.length == 0){
          $location.path( "/beranda_survey" );
        }
      }

      if(next.templateUrl == "view/pilih_layanan.html"){
        var profil = JSON.parse(localStorage.getItem("profil"));
        if(profil == null){
          $location.path( "/isi_profil" );
        }
      }

      if(next.templateUrl == "view/kuesioner.html"){
        if(localStorage.getItem("jenis_layanan") == null){
          $location.path( "/pilih_layanan" );
        }
      }

      if(next.templateUrl == "view/saran.html"){
        var check = JSON.parse(localStorage.getItem("kuesioner"));
        if(check[8].jawaban == undefined){
          $location.path( "/kuesioner" );
        }
      }
    }

  });
  $rootScope.back = function () {
    var prevUrl = history.length > 1 ? history.splice(-2)[0] : "/";
    console.log(prevUrl);
    if(prevUrl == '/beranda_survey' && localStorage.getItem("logged_user") == null){
      $location.path('/');
    }else{
      $location.path(prevUrl);
    }
  };
});

portalApp.controller('navCtrl', ['$http', '$rootScope', '$location', '$log', function($http, $rootScope, $location, $log, $routeParams){
  $rootScope.location = $location;
  var location = $location.url().split('/');

  
}]);

portalApp.controller('mainController', function($scope, $rootScope, $location, $http, $routeParams) {
  var split = $location.absUrl().split('/');
  $rootScope.baseUrl = split[0]+split[1]+'//'+split[2]+'/'+split[3];
  $scope.currentUrl = $location.absUrl().split('?')[0];
  var adminUrl = $scope.baseUrl+'/admin/';
  $rootScope.logged_user = {};
  $rootScope.profil = {};

  if(localStorage.getItem("profil") != null){
    $rootScope.profil = JSON.parse(localStorage.getItem("profil"));
  }

  if(localStorage.getItem("logged_user") != null) {
    $rootScope.logged_user = JSON.parse(localStorage.getItem("logged_user"));
  }

  if(localStorage.getItem("jenis_layanan") != null){
    $rootScope.profil.jenis_layanan = localStorage.getItem("jenis_layanan");
  }

  $http({
    url: $rootScope.baseUrl+"/admin/refdata", 
    method: "GET",
    params: {'id_opd' : $rootScope.logged_user.id_opd}
  }).then(function (response) {
    $rootScope.refdata = response.data;
    $rootScope.datalayanan = response.data.jenis_layanan;
  })

  $scope.submitprofil = function(){
    if($rootScope.profil.jenis_kelamin && $rootScope.profil.usia && $rootScope.profil.jenjang_pendidikan && $rootScope.profil.pekerjaan){
      localStorage.setItem("profil",  JSON.stringify($rootScope.profil));
      $location.path('/pilih_layanan');
    }
  }

  $scope.submitlayanan = function(){
    localStorage.setItem("jenis_layanan", $rootScope.profil.jenis_layanan);
    $location.path('/kuesioner');
  }

  $scope.logoutsurvey = function(){
    localStorage.clear();
    $rootScope.logged_user = {};
    $rootScope.profil = {};
    $rootScope.kuesioner = {};
    $rootScope.jawaban = {};
    $rootScope.progress = 0;
    $location.path('/login_survey');
  }

});

portalApp.controller('loginController', function($scope, $rootScope, $location, $http, $routeParams) {
  var split = $location.absUrl().split('/');;
  $rootScope.baseUrl = split[0]+split[1]+'//'+split[2]+'/'+split[3];
  $scope.currentUrl = $location.absUrl().split('?')[0];
  var adminUrl = $scope.baseUrl+'/admin/';

  $scope.dologin = function(){
    $http({
      url: $rootScope.baseUrl+"/admin/login-survey", 
      method: "POST",
      data: $scope.formData
    }).then(function (response) {
      if(response.data.result == 'success'){
        localStorage.setItem("logged_user", JSON.stringify(response.data.logged_user));
        localStorage.setItem("datalayanan", JSON.stringify(response.data.layanan));
        $rootScope.logged_user = response.data.logged_user;
        $rootScope.datalayanan = response.data.layanan;
        $location.path( "/beranda_survey" );
      }
    })
  }
});

portalApp.controller('detailikmController', function($scope, $rootScope, $location, $http, $routeParams, $route, $timeout) {
  $scope.dataikm = [];
  $scope.availability = true;

  $http({
    url: $rootScope.baseUrl+"/admin/dataopd", 
    method: "GET",
    params: {'id_opd' : $routeParams.id}
  }).then(function (response) {

    $scope.dataopd = response.data.opd;
    $scope.layanans = response.data.layanan;

    $scope.tgl_awal = response.data.tgl_awal;
    $scope.tgl_akhir = response.data.tgl_akhir;

    if($routeParams.id_layanan != undefined){
      $scope.id_layanan = parseInt($routeParams.id_layanan);
    }

    $scope.getnilai = function(id_opd, id_layanan){
      $http({
        url: $rootScope.baseUrl+"/admin/nilaiikm", 
        method: "GET",
        params: {'id_opd' : id_opd, 'id_layanan' : id_layanan, 'tgl_awal' : $scope.tgl_awal, 'tgl_akhir' : $scope.tgl_akhir}
      }).then(function (response) {
        $scope.availability = response.data.res;
        $scope.dataikm = response.data.nilai;
      })
    }

    $scope.$watch('id_layanan', function(newValue) {
      if(newValue == undefined){
        $scope.getnilai($routeParams.id);
      }else{
        $scope.getnilai($routeParams.id, newValue);
      }
    });

  });

  $scope.reload = function(){
    $http({
      url: $rootScope.baseUrl+"/admin/nilaiikm", 
      method: "GET",
      params: {'id_opd' : $routeParams.id, 'id_layanan' : $scope.id_layanan, 'tgl_awal' : $scope.tgl_awal, 'tgl_akhir' : $scope.tgl_akhir}
    }).then(function (response) {
      $scope.availability = response.data.res;
      $scope.dataikm = response.data.nilai;
    })
  }

});

portalApp.controller('kuesionerController', function($scope, $rootScope, $location, $http, $routeParams) {
  $scope.logged_user = JSON.parse(localStorage.getItem("logged_user"));
  $rootScope.kuesioner = {};
  $rootScope.jawaban = {};
  $rootScope.progress = 0;

  if(localStorage.getItem("kuesioner") == null){
    $http({
      url: $rootScope.baseUrl+"/admin/datakuesioner", 
      method: "GET",
      params: {'id_opd' : $scope.logged_user.id_opd, 'id_layanan' : localStorage.getItem("jenis_layanan")}
    }).then(function (response) {
      $rootScope.kuesioner = response.data;
      localStorage.setItem("kuesioner", JSON.stringify(response.data));
      localStorage.setItem("progress", 0);
    })
  }else{
    $rootScope.kuesioner = JSON.parse(localStorage.getItem("kuesioner"));
    $rootScope.progress = localStorage.getItem("progress");
  }

  $scope.lanjut = function(progress){
    if($rootScope.kuesioner[progress].jawaban != undefined){
      localStorage.setItem("kuesioner", JSON.stringify($rootScope.kuesioner));
      
      if(progress < 8){
        $rootScope.progress++;
        localStorage.setItem("progress", $rootScope.progress);
        $scope.noprogress = parseInt($rootScope.progress)+1;
      }

      if(progress == 8){
        $location.path("/saran"); 
      }
    }
  }

  $scope.kembali = function(progress){
    if(progress > 0){
      $rootScope.progress--;
      localStorage.setItem("progress", $rootScope.progress);
      $scope.noprogress = parseInt($rootScope.progress)+1;
    }else{
      $location.path("/pilih_layanan");
    }
  }

  $scope.noprogress = parseInt($rootScope.progress)+1;
});

portalApp.controller('finishingController', function($scope, $rootScope, $location, $http) {
  $scope.isiansaran = '';
  $scope.selesai = function(){
    var data = {
      profil : JSON.parse(localStorage.getItem("profil")),
      logged_user : JSON.parse(localStorage.getItem("logged_user")),
      kuesioner : JSON.parse(localStorage.getItem("kuesioner")),
      saran : $scope.isiansaran,
      jenis_layanan : localStorage.getItem("jenis_layanan")
    };

    $http({
      url: $rootScope.baseUrl+"/admin/simpansurvey", 
      method: "POST",
      data: data
    }).then(function (response) {
      $rootScope.kuesioner = {};
      $rootScope.jawaban = {};
      $rootScope.profil = {};
      $rootScope.progress = 0;
      localStorage.removeItem("jenis_layanan");
      localStorage.removeItem("kuesioner");
      localStorage.removeItem("profil");
      localStorage.removeItem("progress");
      $location.path("/finish");
    })
  }
});

portalApp.controller('berandaController', function($scope, $rootScope, $location, $http) {
  $http({
    url: $rootScope.baseUrl+"/admin/databeranda", 
    method: "GET",
  }).then(function (response) {
    $scope.daftaropd = response.data.daftaropd;
    $scope.pengantar = response.data.pengantar;
    $scope.berita = response.data.berita;
  })
});

portalApp.controller('beritaController', function ($scope, $window, $rootScope, $location, $http, $route, $routeParams, $timeout) {
  $timeout(function () {
    $scope.hal = $routeParams.hal;
  }, 300);

  $http({
    url: $rootScope.baseUrl + "/admin/berita",
    method: "GET",
    params: {
      page: $routeParams.hal,
    }
  }).then(function (response) {
    $scope.totalItems = response.data.total;
    $scope.Records = response.data.data;
  })

  $scope.pageChanged = function(){
    $route.updateParams({
      hal: $scope.hal
    });
  }

});

portalApp.controller('bacaberitaController', function ($scope, $rootScope, $location, $http, $routeParams) {
  $http({
    url: $rootScope.baseUrl + "/admin/isiberita",
    method: "GET",
    params: {
      'url': $routeParams.judul
    }
  }).then(function (response) {
    $scope.berita = response.data;
  })
});

portalApp.directive('onlyNumbers', function () {
  return  {
    restrict: 'A',
    link: function (scope, elm, attrs, ctrl) {
      elm.on('keydown', function (event) {
          if(event.shiftKey){event.preventDefault(); return false;}
          //console.log(event.which);
          if ([8, 13, 27, 37, 38, 39, 40].indexOf(event.which) > -1) {
              // backspace, enter, escape, arrows
              return true;
          } else if (event.which >= 48 && event.which <= 57) {
              // numbers 0 to 9
              return true;
          } else if (event.which >= 96 && event.which <= 105) {
              // numpad number
              return true;
          } 
          // else if ([110, 190].indexOf(event.which) > -1) {
          //     // dot and numpad dot
          //     return true;
          // }
          else {
              event.preventDefault();
              return false;
          }
      });
    }
  }
});

portalApp.directive('materialDatepicker', function() {
  return {
    restrict: 'A',
    scope: {
      'model': '='
    },
    link: function(scope, element, attrs) {
      $(element).bootstrapMaterialDatePicker({ format : 'DD-MM-YYYY', time: false, clearButton: true, nowButton: true });
    }
  };
});

portalApp.directive('chartJenkel', function ($timeout, $http) {
   return {
        restrict: 'E',
        replace:true,
        scope: { 
            layanan: '=layanan',
            laki_laki:'=lakiLaki',
            perempuan:'=perempuan'
        },
        template: '<div id="chartdivB" style="min-width: 310px; height: 380px; margin: 0 auto"></div>',
        link: function (scope, element, attrs) {

            function initPie(laki_laki, perempuan){
              var chart = false;
              var initChart = function() {                     
                  if (chart) chart.destroy();
                  var config = scope.config || {};

                  var chart = AmCharts.makeChart( "chartdivB", {
                    "type": "pie",
                    "theme": "dark",
                    "titles": [{
                      "text": "Jenis Kelamin"
                    }],
                    "radius": 100,
                    "fontSize": 12,
                    "autoMargins": false,
                    "marginTop": 50,
                    "marginBottom": 50,
                    "startEffect": "easeOutSine",
                    "legend":{
                      "position":"bottom",
                      // "marginbottom":50,
                      "autoMargins":true
                    },
                    "responsive": {
                      "enabled": true
                    },
                    "dataProvider": [ {
                      "jenis_kelamin": "Laki-laki",
                      "litres": parseInt(laki_laki)
                    }, {
                      "jenis_kelamin": "Perempuan",
                      "litres": parseInt(perempuan)
                    }],
                    "valueField": "litres",
                    "titleField": "jenis_kelamin",
                     "balloon":{
                     "fixedPosition":true
                    },
                    "export": {
                      "enabled": false
                    }
                  } );

                  chart.addListener("init", handleInit);

                  function handleInit(){
                    chart.legend.addListener("rollOverItem", handleRollOver);
                  }
              };
              initChart();
            }

            scope.$watchGroup(['laki_laki', 'perempuan'], function() {
              initPie(scope.laki_laki, scope.perempuan);
            });
          
        }  
    }
});

portalApp.directive('chartJenjang', function ($timeout, $http) {
   return {
        restrict: 'E',
        replace:true,
        scope: { 
            layanan: '=layanan',
            sd:'=sd',
            smp:'=smp',
            sma:'=sma',
            s1:'=s1',
            s2:'=s2',
            s3:'=s3'
        },
        template: '<div id="chartdivA" style="min-width: 310px; height: 410px; margin: 0 auto"></div>',
        link: function (scope, element, attrs) {

            function initPie(sd, smp, sma, s1, s2, s3){
              var initChart = function() { 
                var chart = false;          
                if (chart) chart.destroy();
                var config = scope.config || {};

                var chart = AmCharts.makeChart( "chartdivA", {
                  "type": "pie",
                  "theme": "dark",
                  "titles": [{
                    "text": "Jenjang Pendidikan"
                  }],
                  "radius": 100,
                  "fontSize": 12,
                  "autoMargins": false,
                  "marginTop": 50,
                  "marginBottom": 50,
                  "startEffect": "easeOutSine",
                  "legend":{
                    "position":"bottom",
                    // "marginRight":100,
                    "autoMargins":true
                  },
                  "responsive": {
                    "enabled": true
                  },
                  "dataProvider": [ {
                    "jenjang": "SD",
                    "litres": parseInt(sd)
                  }, {
                    "jenjang": "SMP",
                    "litres": parseInt(smp)
                  }, {
                    "jenjang": "SMA",
                    "litres": parseInt(sma)
                  }, {
                    "jenjang": "S1",
                    "litres": parseInt(s1)
                  }, {
                    "jenjang": "S2",
                    "litres": parseInt(s2)
                  }, {
                    "jenjang": "S3",
                    "litres": parseInt(s3)
                  }],
                  "valueField": "litres",
                  "titleField": "jenjang",
                   "balloon":{
                   "fixedPosition":true
                  },
                  "export": {
                    "enabled": false
                  }
                } );

                chart.addListener("init", handleInit);

                function handleInit(){
                  chart.legend.addListener("rollOverItem", handleRollOver);
                }
              };
              initChart();
            }

            scope.$watchGroup(['sd', 'smp', 'sma', 's1', 's2' ,'s3'], function() {
              initPie(scope.sd, scope.smp, scope.sma, scope.s1, scope.s2, scope.s3);
            });
          
        }      
    }
});

portalApp.directive('fancyBox', function($window) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.find('.fancybox').fancybox();
        }
    };
});