/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.0.5
 Source Server Type    : MySQL
 Source Server Version : 50560
 Source Host           : 192.168.0.5:3306
 Source Schema         : skm-kudus

 Target Server Type    : MySQL
 Target Server Version : 50560
 File Encoding         : 65001

 Date: 09/01/2019 13:05:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for application_log
-- ----------------------------
DROP TABLE IF EXISTS `application_log`;
CREATE TABLE `application_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `aksi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `module` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user` bigint(20) NULL DEFAULT NULL,
  `waktu` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for configurations
-- ----------------------------
DROP TABLE IF EXISTS `configurations`;
CREATE TABLE `configurations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of configurations
-- ----------------------------
INSERT INTO `configurations` VALUES (1, 'site-name', 'AKUPuas! - Administrator', '2018-12-18 08:57:47', '2018-12-18 01:57:47');
INSERT INTO `configurations` VALUES (2, 'list-limit', '20', '2014-02-19 00:00:00', '2016-11-18 04:00:03');
INSERT INTO `configurations` VALUES (3, 'Framework Version', '5.1', '2015-05-01 13:46:29', '2016-06-09 03:19:42');
INSERT INTO `configurations` VALUES (8, 'fixed', '1', '2016-09-02 03:47:35', '2017-03-23 06:43:21');
INSERT INTO `configurations` VALUES (9, 'mini-sidebar', '1', '2016-09-02 03:47:53', '2017-03-23 06:43:26');
INSERT INTO `configurations` VALUES (10, 'skin', 'skin-black', '2016-09-02 03:49:03', '2016-09-02 03:49:24');

-- ----------------------------
-- Table structure for contexts
-- ----------------------------
DROP TABLE IF EXISTS `contexts`;
CREATE TABLE `contexts`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `module_path` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `uses` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `flag` int(11) NULL DEFAULT NULL,
  `is_nav_bar` int(11) NULL DEFAULT NULL,
  `icons` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `order` int(11) NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of contexts
-- ----------------------------
INSERT INTO `contexts` VALUES (1, 0, 'Dashboard', '/dashboard', 'HomeController@dashboard', 1, 1, 'fa fa-dashboard', 1, '2016-06-19 00:00:00', '2017-11-16 03:27:59');
INSERT INTO `contexts` VALUES (2, 0, 'Developer', '', '', 1, 1, 'fa fa-code', 99, '2016-06-19 00:00:00', '2016-06-19 00:00:00');
INSERT INTO `contexts` VALUES (3, 0, 'Settings', '', '', 1, 1, 'fa fa-gears', 100, '2016-06-19 00:00:00', '2016-06-21 07:54:29');
INSERT INTO `contexts` VALUES (4, 1, 'Master Data', '', '', 1, 1, 'fa fa-archive', 3, '2018-10-07 15:58:38', '2018-10-07 15:58:38');
INSERT INTO `contexts` VALUES (5, 1, 'Respon Masyarakat', '', '', 1, 1, 'fa fa-comments-o', 2, '2018-10-07 16:24:37', '2018-10-07 16:24:37');
INSERT INTO `contexts` VALUES (6, 1, 'Pengaturan', '', '', 1, 1, 'fa fa-wrench', 5, '2018-11-25 21:42:23', '2018-10-26 06:47:00');
INSERT INTO `contexts` VALUES (7, 1, 'Portal', '', '', 1, 1, 'fa fa-circle-o', 4, '2018-11-25 21:42:23', '2018-11-25 14:42:17');

-- ----------------------------
-- Table structure for font-awesome
-- ----------------------------
DROP TABLE IF EXISTS `font-awesome`;
CREATE TABLE `font-awesome`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `icon` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 786 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of font-awesome
-- ----------------------------
INSERT INTO `font-awesome` VALUES (1, 'fa-address-book');
INSERT INTO `font-awesome` VALUES (2, 'fa-address-book-o');
INSERT INTO `font-awesome` VALUES (3, 'fa-address-card');
INSERT INTO `font-awesome` VALUES (4, 'fa-address-card-o');
INSERT INTO `font-awesome` VALUES (5, 'fa-adjust');
INSERT INTO `font-awesome` VALUES (6, 'fa-adn');
INSERT INTO `font-awesome` VALUES (7, 'fa-align-center');
INSERT INTO `font-awesome` VALUES (8, 'fa-align-justify');
INSERT INTO `font-awesome` VALUES (9, 'fa-align-left');
INSERT INTO `font-awesome` VALUES (10, 'fa-align-right');
INSERT INTO `font-awesome` VALUES (11, 'fa-amazon');
INSERT INTO `font-awesome` VALUES (12, 'fa-ambulance');
INSERT INTO `font-awesome` VALUES (13, 'fa-american-sign-language-inte');
INSERT INTO `font-awesome` VALUES (14, 'fa-anchor');
INSERT INTO `font-awesome` VALUES (15, 'fa-android');
INSERT INTO `font-awesome` VALUES (16, 'fa-angellist');
INSERT INTO `font-awesome` VALUES (17, 'fa-angle-double-down');
INSERT INTO `font-awesome` VALUES (18, 'fa-angle-double-left');
INSERT INTO `font-awesome` VALUES (19, 'fa-angle-double-right');
INSERT INTO `font-awesome` VALUES (20, 'fa-angle-double-up');
INSERT INTO `font-awesome` VALUES (21, 'fa-angle-down');
INSERT INTO `font-awesome` VALUES (22, 'fa-angle-left');
INSERT INTO `font-awesome` VALUES (23, 'fa-angle-right');
INSERT INTO `font-awesome` VALUES (24, 'fa-angle-up');
INSERT INTO `font-awesome` VALUES (25, 'fa-apple');
INSERT INTO `font-awesome` VALUES (26, 'fa-archive');
INSERT INTO `font-awesome` VALUES (27, 'fa-area-chart');
INSERT INTO `font-awesome` VALUES (28, 'fa-arrow-circle-down');
INSERT INTO `font-awesome` VALUES (29, 'fa-arrow-circle-left');
INSERT INTO `font-awesome` VALUES (30, 'fa-arrow-circle-o-down');
INSERT INTO `font-awesome` VALUES (31, 'fa-arrow-circle-o-left');
INSERT INTO `font-awesome` VALUES (32, 'fa-arrow-circle-o-right');
INSERT INTO `font-awesome` VALUES (33, 'fa-arrow-circle-o-up');
INSERT INTO `font-awesome` VALUES (34, 'fa-arrow-circle-right');
INSERT INTO `font-awesome` VALUES (35, 'fa-arrow-circle-up');
INSERT INTO `font-awesome` VALUES (36, 'fa-arrow-down');
INSERT INTO `font-awesome` VALUES (37, 'fa-arrow-left');
INSERT INTO `font-awesome` VALUES (38, 'fa-arrow-right');
INSERT INTO `font-awesome` VALUES (39, 'fa-arrows');
INSERT INTO `font-awesome` VALUES (40, 'fa-arrows-alt');
INSERT INTO `font-awesome` VALUES (41, 'fa-arrows-h');
INSERT INTO `font-awesome` VALUES (42, 'fa-arrows-v');
INSERT INTO `font-awesome` VALUES (43, 'fa-arrow-up');
INSERT INTO `font-awesome` VALUES (44, 'fa-asl-interpreting');
INSERT INTO `font-awesome` VALUES (45, 'fa-assistive-listening-systems');
INSERT INTO `font-awesome` VALUES (46, 'fa-asterisk');
INSERT INTO `font-awesome` VALUES (47, 'fa-at');
INSERT INTO `font-awesome` VALUES (48, 'fa-audio-description');
INSERT INTO `font-awesome` VALUES (49, 'fa-automobile');
INSERT INTO `font-awesome` VALUES (50, 'fa-backward');
INSERT INTO `font-awesome` VALUES (51, 'fa-balance-scale');
INSERT INTO `font-awesome` VALUES (52, 'fa-ban');
INSERT INTO `font-awesome` VALUES (53, 'fa-bandcamp');
INSERT INTO `font-awesome` VALUES (54, 'fa-bank');
INSERT INTO `font-awesome` VALUES (55, 'fa-bar-chart');
INSERT INTO `font-awesome` VALUES (56, 'fa-bar-chart-o');
INSERT INTO `font-awesome` VALUES (57, 'fa-barcode');
INSERT INTO `font-awesome` VALUES (58, 'fa-bars');
INSERT INTO `font-awesome` VALUES (59, 'fa-bath');
INSERT INTO `font-awesome` VALUES (60, 'fa-bathtub');
INSERT INTO `font-awesome` VALUES (61, 'fa-battery');
INSERT INTO `font-awesome` VALUES (62, 'fa-battery-0');
INSERT INTO `font-awesome` VALUES (63, 'fa-battery-1');
INSERT INTO `font-awesome` VALUES (64, 'fa-battery-2');
INSERT INTO `font-awesome` VALUES (65, 'fa-battery-3');
INSERT INTO `font-awesome` VALUES (66, 'fa-battery-4');
INSERT INTO `font-awesome` VALUES (67, 'fa-battery-empty');
INSERT INTO `font-awesome` VALUES (68, 'fa-battery-full');
INSERT INTO `font-awesome` VALUES (69, 'fa-battery-half');
INSERT INTO `font-awesome` VALUES (70, 'fa-battery-quarter');
INSERT INTO `font-awesome` VALUES (71, 'fa-battery-three-quarters');
INSERT INTO `font-awesome` VALUES (72, 'fa-bed');
INSERT INTO `font-awesome` VALUES (73, 'fa-beer');
INSERT INTO `font-awesome` VALUES (74, 'fa-behance');
INSERT INTO `font-awesome` VALUES (75, 'fa-behance-square');
INSERT INTO `font-awesome` VALUES (76, 'fa-bell');
INSERT INTO `font-awesome` VALUES (77, 'fa-bell-o');
INSERT INTO `font-awesome` VALUES (78, 'fa-bell-slash');
INSERT INTO `font-awesome` VALUES (79, 'fa-bell-slash-o');
INSERT INTO `font-awesome` VALUES (80, 'fa-bicycle');
INSERT INTO `font-awesome` VALUES (81, 'fa-binoculars');
INSERT INTO `font-awesome` VALUES (82, 'fa-birthday-cake');
INSERT INTO `font-awesome` VALUES (83, 'fa-bitbucket');
INSERT INTO `font-awesome` VALUES (84, 'fa-bitbucket-square');
INSERT INTO `font-awesome` VALUES (85, 'fa-bitcoin');
INSERT INTO `font-awesome` VALUES (86, 'fa-black-tie');
INSERT INTO `font-awesome` VALUES (87, 'fa-blind');
INSERT INTO `font-awesome` VALUES (88, 'fa-bluetooth');
INSERT INTO `font-awesome` VALUES (89, 'fa-bluetooth-b');
INSERT INTO `font-awesome` VALUES (90, 'fa-bold');
INSERT INTO `font-awesome` VALUES (91, 'fa-bolt');
INSERT INTO `font-awesome` VALUES (92, 'fa-bomb');
INSERT INTO `font-awesome` VALUES (93, 'fa-book');
INSERT INTO `font-awesome` VALUES (94, 'fa-bookmark');
INSERT INTO `font-awesome` VALUES (95, 'fa-bookmark-o');
INSERT INTO `font-awesome` VALUES (96, 'fa-braille');
INSERT INTO `font-awesome` VALUES (97, 'fa-briefcase');
INSERT INTO `font-awesome` VALUES (98, 'fa-btc');
INSERT INTO `font-awesome` VALUES (99, 'fa-bug');
INSERT INTO `font-awesome` VALUES (100, 'fa-building');
INSERT INTO `font-awesome` VALUES (101, 'fa-building-o');
INSERT INTO `font-awesome` VALUES (102, 'fa-bullhorn');
INSERT INTO `font-awesome` VALUES (103, 'fa-bullseye');
INSERT INTO `font-awesome` VALUES (104, 'fa-bus');
INSERT INTO `font-awesome` VALUES (105, 'fa-buysellads');
INSERT INTO `font-awesome` VALUES (106, 'fa-cab');
INSERT INTO `font-awesome` VALUES (107, 'fa-calculator');
INSERT INTO `font-awesome` VALUES (108, 'fa-calendar');
INSERT INTO `font-awesome` VALUES (109, 'fa-calendar-check-o');
INSERT INTO `font-awesome` VALUES (110, 'fa-calendar-minus-o');
INSERT INTO `font-awesome` VALUES (111, 'fa-calendar-o');
INSERT INTO `font-awesome` VALUES (112, 'fa-calendar-plus-o');
INSERT INTO `font-awesome` VALUES (113, 'fa-calendar-times-o');
INSERT INTO `font-awesome` VALUES (114, 'fa-camera');
INSERT INTO `font-awesome` VALUES (115, 'fa-camera-retro');
INSERT INTO `font-awesome` VALUES (116, 'fa-car');
INSERT INTO `font-awesome` VALUES (117, 'fa-caret-down');
INSERT INTO `font-awesome` VALUES (118, 'fa-caret-left');
INSERT INTO `font-awesome` VALUES (119, 'fa-caret-right');
INSERT INTO `font-awesome` VALUES (120, 'fa-caret-square-o-down');
INSERT INTO `font-awesome` VALUES (121, 'fa-caret-square-o-left');
INSERT INTO `font-awesome` VALUES (122, 'fa-caret-square-o-right');
INSERT INTO `font-awesome` VALUES (123, 'fa-caret-square-o-up');
INSERT INTO `font-awesome` VALUES (124, 'fa-caret-up');
INSERT INTO `font-awesome` VALUES (125, 'fa-cart-arrow-down');
INSERT INTO `font-awesome` VALUES (126, 'fa-cart-plus');
INSERT INTO `font-awesome` VALUES (127, 'fa-cc');
INSERT INTO `font-awesome` VALUES (128, 'fa-cc-amex');
INSERT INTO `font-awesome` VALUES (129, 'fa-cc-diners-club');
INSERT INTO `font-awesome` VALUES (130, 'fa-cc-discover');
INSERT INTO `font-awesome` VALUES (131, 'fa-cc-jcb');
INSERT INTO `font-awesome` VALUES (132, 'fa-cc-mastercard');
INSERT INTO `font-awesome` VALUES (133, 'fa-cc-paypal');
INSERT INTO `font-awesome` VALUES (134, 'fa-cc-stripe');
INSERT INTO `font-awesome` VALUES (135, 'fa-cc-visa');
INSERT INTO `font-awesome` VALUES (136, 'fa-certificate');
INSERT INTO `font-awesome` VALUES (137, 'fa-chain');
INSERT INTO `font-awesome` VALUES (138, 'fa-chain-broken');
INSERT INTO `font-awesome` VALUES (139, 'fa-check');
INSERT INTO `font-awesome` VALUES (140, 'fa-check-circle');
INSERT INTO `font-awesome` VALUES (141, 'fa-check-circle-o');
INSERT INTO `font-awesome` VALUES (142, 'fa-check-square');
INSERT INTO `font-awesome` VALUES (143, 'fa-check-square-o');
INSERT INTO `font-awesome` VALUES (144, 'fa-chevron-circle-down');
INSERT INTO `font-awesome` VALUES (145, 'fa-chevron-circle-left');
INSERT INTO `font-awesome` VALUES (146, 'fa-chevron-circle-right');
INSERT INTO `font-awesome` VALUES (147, 'fa-chevron-circle-up');
INSERT INTO `font-awesome` VALUES (148, 'fa-chevron-down');
INSERT INTO `font-awesome` VALUES (149, 'fa-chevron-left');
INSERT INTO `font-awesome` VALUES (150, 'fa-chevron-right');
INSERT INTO `font-awesome` VALUES (151, 'fa-chevron-up');
INSERT INTO `font-awesome` VALUES (152, 'fa-child');
INSERT INTO `font-awesome` VALUES (153, 'fa-chrome');
INSERT INTO `font-awesome` VALUES (154, 'fa-circle');
INSERT INTO `font-awesome` VALUES (155, 'fa-circle-o');
INSERT INTO `font-awesome` VALUES (156, 'fa-circle-o-notch');
INSERT INTO `font-awesome` VALUES (157, 'fa-circle-thin');
INSERT INTO `font-awesome` VALUES (158, 'fa-clipboard');
INSERT INTO `font-awesome` VALUES (159, 'fa-clock-o');
INSERT INTO `font-awesome` VALUES (160, 'fa-clone');
INSERT INTO `font-awesome` VALUES (161, 'fa-close');
INSERT INTO `font-awesome` VALUES (162, 'fa-cloud');
INSERT INTO `font-awesome` VALUES (163, 'fa-cloud-download');
INSERT INTO `font-awesome` VALUES (164, 'fa-cloud-upload');
INSERT INTO `font-awesome` VALUES (165, 'fa-cny');
INSERT INTO `font-awesome` VALUES (166, 'fa-code');
INSERT INTO `font-awesome` VALUES (167, 'fa-code-fork');
INSERT INTO `font-awesome` VALUES (168, 'fa-codepen');
INSERT INTO `font-awesome` VALUES (169, 'fa-codiepie');
INSERT INTO `font-awesome` VALUES (170, 'fa-coffee');
INSERT INTO `font-awesome` VALUES (171, 'fa-cog');
INSERT INTO `font-awesome` VALUES (172, 'fa-cogs');
INSERT INTO `font-awesome` VALUES (173, 'fa-columns');
INSERT INTO `font-awesome` VALUES (174, 'fa-comment');
INSERT INTO `font-awesome` VALUES (175, 'fa-commenting');
INSERT INTO `font-awesome` VALUES (176, 'fa-commenting-o');
INSERT INTO `font-awesome` VALUES (177, 'fa-comment-o');
INSERT INTO `font-awesome` VALUES (178, 'fa-comments');
INSERT INTO `font-awesome` VALUES (179, 'fa-comments-o');
INSERT INTO `font-awesome` VALUES (180, 'fa-compass');
INSERT INTO `font-awesome` VALUES (181, 'fa-compress');
INSERT INTO `font-awesome` VALUES (182, 'fa-connectdevelop');
INSERT INTO `font-awesome` VALUES (183, 'fa-contao');
INSERT INTO `font-awesome` VALUES (184, 'fa-copy');
INSERT INTO `font-awesome` VALUES (185, 'fa-copyright');
INSERT INTO `font-awesome` VALUES (186, 'fa-creative-commons');
INSERT INTO `font-awesome` VALUES (187, 'fa-credit-card');
INSERT INTO `font-awesome` VALUES (188, 'fa-credit-card-alt');
INSERT INTO `font-awesome` VALUES (189, 'fa-crop');
INSERT INTO `font-awesome` VALUES (190, 'fa-crosshairs');
INSERT INTO `font-awesome` VALUES (191, 'fa-css3');
INSERT INTO `font-awesome` VALUES (192, 'fa-cube');
INSERT INTO `font-awesome` VALUES (193, 'fa-cubes');
INSERT INTO `font-awesome` VALUES (194, 'fa-cut');
INSERT INTO `font-awesome` VALUES (195, 'fa-cutlery');
INSERT INTO `font-awesome` VALUES (196, 'fa-dashboard');
INSERT INTO `font-awesome` VALUES (197, 'fa-dashcube');
INSERT INTO `font-awesome` VALUES (198, 'fa-database');
INSERT INTO `font-awesome` VALUES (199, 'fa-deaf');
INSERT INTO `font-awesome` VALUES (200, 'fa-deafness');
INSERT INTO `font-awesome` VALUES (201, 'fa-dedent');
INSERT INTO `font-awesome` VALUES (202, 'fa-delicious');
INSERT INTO `font-awesome` VALUES (203, 'fa-desktop');
INSERT INTO `font-awesome` VALUES (204, 'fa-deviantart');
INSERT INTO `font-awesome` VALUES (205, 'fa-diamond');
INSERT INTO `font-awesome` VALUES (206, 'fa-digg');
INSERT INTO `font-awesome` VALUES (207, 'fa-dollar');
INSERT INTO `font-awesome` VALUES (208, 'fa-dot-circle-o');
INSERT INTO `font-awesome` VALUES (209, 'fa-download');
INSERT INTO `font-awesome` VALUES (210, 'fa-dribbble');
INSERT INTO `font-awesome` VALUES (211, 'fa-drivers-license');
INSERT INTO `font-awesome` VALUES (212, 'fa-drivers-license-o');
INSERT INTO `font-awesome` VALUES (213, 'fa-dropbox');
INSERT INTO `font-awesome` VALUES (214, 'fa-drupal');
INSERT INTO `font-awesome` VALUES (215, 'fa-edge');
INSERT INTO `font-awesome` VALUES (216, 'fa-edit');
INSERT INTO `font-awesome` VALUES (217, 'fa-eercast');
INSERT INTO `font-awesome` VALUES (218, 'fa-eject');
INSERT INTO `font-awesome` VALUES (219, 'fa-ellipsis-h');
INSERT INTO `font-awesome` VALUES (220, 'fa-ellipsis-v');
INSERT INTO `font-awesome` VALUES (221, 'fa-empire');
INSERT INTO `font-awesome` VALUES (222, 'fa-envelope');
INSERT INTO `font-awesome` VALUES (223, 'fa-envelope-o');
INSERT INTO `font-awesome` VALUES (224, 'fa-envelope-open');
INSERT INTO `font-awesome` VALUES (225, 'fa-envelope-open-o');
INSERT INTO `font-awesome` VALUES (226, 'fa-envelope-square');
INSERT INTO `font-awesome` VALUES (227, 'fa-envira');
INSERT INTO `font-awesome` VALUES (228, 'fa-eraser');
INSERT INTO `font-awesome` VALUES (229, 'fa-etsy');
INSERT INTO `font-awesome` VALUES (230, 'fa-eur');
INSERT INTO `font-awesome` VALUES (231, 'fa-euro');
INSERT INTO `font-awesome` VALUES (232, 'fa-exchange');
INSERT INTO `font-awesome` VALUES (233, 'fa-exclamation');
INSERT INTO `font-awesome` VALUES (234, 'fa-exclamation-circle');
INSERT INTO `font-awesome` VALUES (235, 'fa-exclamation-triangle');
INSERT INTO `font-awesome` VALUES (236, 'fa-expand');
INSERT INTO `font-awesome` VALUES (237, 'fa-expeditedssl');
INSERT INTO `font-awesome` VALUES (238, 'fa-external-link');
INSERT INTO `font-awesome` VALUES (239, 'fa-external-link-square');
INSERT INTO `font-awesome` VALUES (240, 'fa-eye');
INSERT INTO `font-awesome` VALUES (241, 'fa-eyedropper');
INSERT INTO `font-awesome` VALUES (242, 'fa-eye-slash');
INSERT INTO `font-awesome` VALUES (243, 'fa-fa');
INSERT INTO `font-awesome` VALUES (244, 'fa-facebook');
INSERT INTO `font-awesome` VALUES (245, 'fa-facebook-f');
INSERT INTO `font-awesome` VALUES (246, 'fa-facebook-official');
INSERT INTO `font-awesome` VALUES (247, 'fa-facebook-square');
INSERT INTO `font-awesome` VALUES (248, 'fa-fast-backward');
INSERT INTO `font-awesome` VALUES (249, 'fa-fast-forward');
INSERT INTO `font-awesome` VALUES (250, 'fa-fax');
INSERT INTO `font-awesome` VALUES (251, 'fa-feed');
INSERT INTO `font-awesome` VALUES (252, 'fa-female');
INSERT INTO `font-awesome` VALUES (253, 'fa-fighter-jet');
INSERT INTO `font-awesome` VALUES (254, 'fa-file');
INSERT INTO `font-awesome` VALUES (255, 'fa-file-archive-o');
INSERT INTO `font-awesome` VALUES (256, 'fa-file-audio-o');
INSERT INTO `font-awesome` VALUES (257, 'fa-file-code-o');
INSERT INTO `font-awesome` VALUES (258, 'fa-file-excel-o');
INSERT INTO `font-awesome` VALUES (259, 'fa-file-image-o');
INSERT INTO `font-awesome` VALUES (260, 'fa-file-movie-o');
INSERT INTO `font-awesome` VALUES (261, 'fa-file-o');
INSERT INTO `font-awesome` VALUES (262, 'fa-file-pdf-o');
INSERT INTO `font-awesome` VALUES (263, 'fa-file-photo-o');
INSERT INTO `font-awesome` VALUES (264, 'fa-file-picture-o');
INSERT INTO `font-awesome` VALUES (265, 'fa-file-powerpoint-o');
INSERT INTO `font-awesome` VALUES (266, 'fa-files-o');
INSERT INTO `font-awesome` VALUES (267, 'fa-file-sound-o');
INSERT INTO `font-awesome` VALUES (268, 'fa-file-text');
INSERT INTO `font-awesome` VALUES (269, 'fa-file-text-o');
INSERT INTO `font-awesome` VALUES (270, 'fa-file-video-o');
INSERT INTO `font-awesome` VALUES (271, 'fa-file-word-o');
INSERT INTO `font-awesome` VALUES (272, 'fa-file-zip-o');
INSERT INTO `font-awesome` VALUES (273, 'fa-film');
INSERT INTO `font-awesome` VALUES (274, 'fa-filter');
INSERT INTO `font-awesome` VALUES (275, 'fa-fire');
INSERT INTO `font-awesome` VALUES (276, 'fa-fire-extinguisher');
INSERT INTO `font-awesome` VALUES (277, 'fa-firefox');
INSERT INTO `font-awesome` VALUES (278, 'fa-first-order');
INSERT INTO `font-awesome` VALUES (279, 'fa-flag');
INSERT INTO `font-awesome` VALUES (280, 'fa-flag-checkered');
INSERT INTO `font-awesome` VALUES (281, 'fa-flag-o');
INSERT INTO `font-awesome` VALUES (282, 'fa-flash');
INSERT INTO `font-awesome` VALUES (283, 'fa-flask');
INSERT INTO `font-awesome` VALUES (284, 'fa-flickr');
INSERT INTO `font-awesome` VALUES (285, 'fa-floppy-o');
INSERT INTO `font-awesome` VALUES (286, 'fa-folder');
INSERT INTO `font-awesome` VALUES (287, 'fa-folder-o');
INSERT INTO `font-awesome` VALUES (288, 'fa-folder-open');
INSERT INTO `font-awesome` VALUES (289, 'fa-folder-open-o');
INSERT INTO `font-awesome` VALUES (290, 'fa-font');
INSERT INTO `font-awesome` VALUES (291, 'fa-font-awesome');
INSERT INTO `font-awesome` VALUES (292, 'fa-fonticons');
INSERT INTO `font-awesome` VALUES (293, 'fa-fort-awesome');
INSERT INTO `font-awesome` VALUES (294, 'fa-forumbee');
INSERT INTO `font-awesome` VALUES (295, 'fa-forward');
INSERT INTO `font-awesome` VALUES (296, 'fa-foursquare');
INSERT INTO `font-awesome` VALUES (297, 'fa-free-code-camp');
INSERT INTO `font-awesome` VALUES (298, 'fa-frown-o');
INSERT INTO `font-awesome` VALUES (299, 'fa-futbol-o');
INSERT INTO `font-awesome` VALUES (300, 'fa-gamepad');
INSERT INTO `font-awesome` VALUES (301, 'fa-gavel');
INSERT INTO `font-awesome` VALUES (302, 'fa-gbp');
INSERT INTO `font-awesome` VALUES (303, 'fa-ge');
INSERT INTO `font-awesome` VALUES (304, 'fa-gear');
INSERT INTO `font-awesome` VALUES (305, 'fa-gears');
INSERT INTO `font-awesome` VALUES (306, 'fa-genderless');
INSERT INTO `font-awesome` VALUES (307, 'fa-get-pocket');
INSERT INTO `font-awesome` VALUES (308, 'fa-gg');
INSERT INTO `font-awesome` VALUES (309, 'fa-gg-circle');
INSERT INTO `font-awesome` VALUES (310, 'fa-gift');
INSERT INTO `font-awesome` VALUES (311, 'fa-git');
INSERT INTO `font-awesome` VALUES (312, 'fa-github');
INSERT INTO `font-awesome` VALUES (313, 'fa-github-alt');
INSERT INTO `font-awesome` VALUES (314, 'fa-github-square');
INSERT INTO `font-awesome` VALUES (315, 'fa-gitlab');
INSERT INTO `font-awesome` VALUES (316, 'fa-git-square');
INSERT INTO `font-awesome` VALUES (317, 'fa-gittip');
INSERT INTO `font-awesome` VALUES (318, 'fa-glass');
INSERT INTO `font-awesome` VALUES (319, 'fa-glide');
INSERT INTO `font-awesome` VALUES (320, 'fa-glide-g');
INSERT INTO `font-awesome` VALUES (321, 'fa-globe');
INSERT INTO `font-awesome` VALUES (322, 'fa-google');
INSERT INTO `font-awesome` VALUES (323, 'fa-google-plus');
INSERT INTO `font-awesome` VALUES (324, 'fa-google-plus-circle');
INSERT INTO `font-awesome` VALUES (325, 'fa-google-plus-official');
INSERT INTO `font-awesome` VALUES (326, 'fa-google-plus-square');
INSERT INTO `font-awesome` VALUES (327, 'fa-google-wallet');
INSERT INTO `font-awesome` VALUES (328, 'fa-graduation-cap');
INSERT INTO `font-awesome` VALUES (329, 'fa-gratipay');
INSERT INTO `font-awesome` VALUES (330, 'fa-grav');
INSERT INTO `font-awesome` VALUES (331, 'fa-group');
INSERT INTO `font-awesome` VALUES (332, 'fa-hacker-news');
INSERT INTO `font-awesome` VALUES (333, 'fa-hand-grab-o');
INSERT INTO `font-awesome` VALUES (334, 'fa-hand-lizard-o');
INSERT INTO `font-awesome` VALUES (335, 'fa-hand-o-down');
INSERT INTO `font-awesome` VALUES (336, 'fa-hand-o-left');
INSERT INTO `font-awesome` VALUES (337, 'fa-hand-o-right');
INSERT INTO `font-awesome` VALUES (338, 'fa-hand-o-up');
INSERT INTO `font-awesome` VALUES (339, 'fa-hand-paper-o');
INSERT INTO `font-awesome` VALUES (340, 'fa-hand-peace-o');
INSERT INTO `font-awesome` VALUES (341, 'fa-hand-pointer-o');
INSERT INTO `font-awesome` VALUES (342, 'fa-hand-rock-o');
INSERT INTO `font-awesome` VALUES (343, 'fa-hand-scissors-o');
INSERT INTO `font-awesome` VALUES (344, 'fa-handshake-o');
INSERT INTO `font-awesome` VALUES (345, 'fa-hand-spock-o');
INSERT INTO `font-awesome` VALUES (346, 'fa-hand-stop-o');
INSERT INTO `font-awesome` VALUES (347, 'fa-hard-of-hearing');
INSERT INTO `font-awesome` VALUES (348, 'fa-hashtag');
INSERT INTO `font-awesome` VALUES (349, 'fa-hdd-o');
INSERT INTO `font-awesome` VALUES (350, 'fa-header');
INSERT INTO `font-awesome` VALUES (351, 'fa-headphones');
INSERT INTO `font-awesome` VALUES (352, 'fa-heart');
INSERT INTO `font-awesome` VALUES (353, 'fa-heartbeat');
INSERT INTO `font-awesome` VALUES (354, 'fa-heart-o');
INSERT INTO `font-awesome` VALUES (355, 'fa-history');
INSERT INTO `font-awesome` VALUES (356, 'fa-home');
INSERT INTO `font-awesome` VALUES (357, 'fa-hospital-o');
INSERT INTO `font-awesome` VALUES (358, 'fa-hotel');
INSERT INTO `font-awesome` VALUES (359, 'fa-hourglass');
INSERT INTO `font-awesome` VALUES (360, 'fa-hourglass-1');
INSERT INTO `font-awesome` VALUES (361, 'fa-hourglass-2');
INSERT INTO `font-awesome` VALUES (362, 'fa-hourglass-3');
INSERT INTO `font-awesome` VALUES (363, 'fa-hourglass-end');
INSERT INTO `font-awesome` VALUES (364, 'fa-hourglass-half');
INSERT INTO `font-awesome` VALUES (365, 'fa-hourglass-o');
INSERT INTO `font-awesome` VALUES (366, 'fa-hourglass-start');
INSERT INTO `font-awesome` VALUES (367, 'fa-houzz');
INSERT INTO `font-awesome` VALUES (368, 'fa-h-square');
INSERT INTO `font-awesome` VALUES (369, 'fa-html5');
INSERT INTO `font-awesome` VALUES (370, 'fa-i-cursor');
INSERT INTO `font-awesome` VALUES (371, 'fa-id-badge');
INSERT INTO `font-awesome` VALUES (372, 'fa-id-card');
INSERT INTO `font-awesome` VALUES (373, 'fa-id-card-o');
INSERT INTO `font-awesome` VALUES (374, 'fa-ils');
INSERT INTO `font-awesome` VALUES (375, 'fa-image');
INSERT INTO `font-awesome` VALUES (376, 'fa-imdb');
INSERT INTO `font-awesome` VALUES (377, 'fa-inbox');
INSERT INTO `font-awesome` VALUES (378, 'fa-indent');
INSERT INTO `font-awesome` VALUES (379, 'fa-industry');
INSERT INTO `font-awesome` VALUES (380, 'fa-info');
INSERT INTO `font-awesome` VALUES (381, 'fa-info-circle');
INSERT INTO `font-awesome` VALUES (382, 'fa-inr');
INSERT INTO `font-awesome` VALUES (383, 'fa-instagram');
INSERT INTO `font-awesome` VALUES (384, 'fa-institution');
INSERT INTO `font-awesome` VALUES (385, 'fa-internet-explorer');
INSERT INTO `font-awesome` VALUES (386, 'fa-intersex');
INSERT INTO `font-awesome` VALUES (387, 'fa-ioxhost');
INSERT INTO `font-awesome` VALUES (388, 'fa-italic');
INSERT INTO `font-awesome` VALUES (389, 'fa-joomla');
INSERT INTO `font-awesome` VALUES (390, 'fa-jpy');
INSERT INTO `font-awesome` VALUES (391, 'fa-jsfiddle');
INSERT INTO `font-awesome` VALUES (392, 'fa-key');
INSERT INTO `font-awesome` VALUES (393, 'fa-keyboard-o');
INSERT INTO `font-awesome` VALUES (394, 'fa-krw');
INSERT INTO `font-awesome` VALUES (395, 'fa-language');
INSERT INTO `font-awesome` VALUES (396, 'fa-laptop');
INSERT INTO `font-awesome` VALUES (397, 'fa-lastfm');
INSERT INTO `font-awesome` VALUES (398, 'fa-lastfm-square');
INSERT INTO `font-awesome` VALUES (399, 'fa-leaf');
INSERT INTO `font-awesome` VALUES (400, 'fa-leanpub');
INSERT INTO `font-awesome` VALUES (401, 'fa-legal');
INSERT INTO `font-awesome` VALUES (402, 'fa-lemon-o');
INSERT INTO `font-awesome` VALUES (403, 'fa-level-down');
INSERT INTO `font-awesome` VALUES (404, 'fa-level-up');
INSERT INTO `font-awesome` VALUES (405, 'fa-life-bouy');
INSERT INTO `font-awesome` VALUES (406, 'fa-life-buoy');
INSERT INTO `font-awesome` VALUES (407, 'fa-life-ring');
INSERT INTO `font-awesome` VALUES (408, 'fa-life-saver');
INSERT INTO `font-awesome` VALUES (409, 'fa-lightbulb-o');
INSERT INTO `font-awesome` VALUES (410, 'fa-line-chart');
INSERT INTO `font-awesome` VALUES (411, 'fa-link');
INSERT INTO `font-awesome` VALUES (412, 'fa-linkedin');
INSERT INTO `font-awesome` VALUES (413, 'fa-linkedin-square');
INSERT INTO `font-awesome` VALUES (414, 'fa-linode');
INSERT INTO `font-awesome` VALUES (415, 'fa-linux');
INSERT INTO `font-awesome` VALUES (416, 'fa-list');
INSERT INTO `font-awesome` VALUES (417, 'fa-list-alt');
INSERT INTO `font-awesome` VALUES (418, 'fa-list-ol');
INSERT INTO `font-awesome` VALUES (419, 'fa-list-ul');
INSERT INTO `font-awesome` VALUES (420, 'fa-location-arrow');
INSERT INTO `font-awesome` VALUES (421, 'fa-lock');
INSERT INTO `font-awesome` VALUES (422, 'fa-long-arrow-down');
INSERT INTO `font-awesome` VALUES (423, 'fa-long-arrow-left');
INSERT INTO `font-awesome` VALUES (424, 'fa-long-arrow-right');
INSERT INTO `font-awesome` VALUES (425, 'fa-long-arrow-up');
INSERT INTO `font-awesome` VALUES (426, 'fa-low-vision');
INSERT INTO `font-awesome` VALUES (427, 'fa-magic');
INSERT INTO `font-awesome` VALUES (428, 'fa-magnet');
INSERT INTO `font-awesome` VALUES (429, 'fa-mail-forward');
INSERT INTO `font-awesome` VALUES (430, 'fa-mail-reply');
INSERT INTO `font-awesome` VALUES (431, 'fa-mail-reply-all');
INSERT INTO `font-awesome` VALUES (432, 'fa-male');
INSERT INTO `font-awesome` VALUES (433, 'fa-map');
INSERT INTO `font-awesome` VALUES (434, 'fa-map-marker');
INSERT INTO `font-awesome` VALUES (435, 'fa-map-o');
INSERT INTO `font-awesome` VALUES (436, 'fa-map-pin');
INSERT INTO `font-awesome` VALUES (437, 'fa-map-signs');
INSERT INTO `font-awesome` VALUES (438, 'fa-mars');
INSERT INTO `font-awesome` VALUES (439, 'fa-mars-double');
INSERT INTO `font-awesome` VALUES (440, 'fa-mars-stroke');
INSERT INTO `font-awesome` VALUES (441, 'fa-mars-stroke-h');
INSERT INTO `font-awesome` VALUES (442, 'fa-mars-stroke-v');
INSERT INTO `font-awesome` VALUES (443, 'fa-maxcdn');
INSERT INTO `font-awesome` VALUES (444, 'fa-meanpath');
INSERT INTO `font-awesome` VALUES (445, 'fa-medium');
INSERT INTO `font-awesome` VALUES (446, 'fa-medkit');
INSERT INTO `font-awesome` VALUES (447, 'fa-meetup');
INSERT INTO `font-awesome` VALUES (448, 'fa-meh-o');
INSERT INTO `font-awesome` VALUES (449, 'fa-mercury');
INSERT INTO `font-awesome` VALUES (450, 'fa-microchip');
INSERT INTO `font-awesome` VALUES (451, 'fa-microphone');
INSERT INTO `font-awesome` VALUES (452, 'fa-microphone-slash');
INSERT INTO `font-awesome` VALUES (453, 'fa-minus');
INSERT INTO `font-awesome` VALUES (454, 'fa-minus-circle');
INSERT INTO `font-awesome` VALUES (455, 'fa-minus-square');
INSERT INTO `font-awesome` VALUES (456, 'fa-minus-square-o');
INSERT INTO `font-awesome` VALUES (457, 'fa-mixcloud');
INSERT INTO `font-awesome` VALUES (458, 'fa-mobile');
INSERT INTO `font-awesome` VALUES (459, 'fa-mobile-phone');
INSERT INTO `font-awesome` VALUES (460, 'fa-modx');
INSERT INTO `font-awesome` VALUES (461, 'fa-money');
INSERT INTO `font-awesome` VALUES (462, 'fa-moon-o');
INSERT INTO `font-awesome` VALUES (463, 'fa-mortar-board');
INSERT INTO `font-awesome` VALUES (464, 'fa-motorcycle');
INSERT INTO `font-awesome` VALUES (465, 'fa-mouse-pointer');
INSERT INTO `font-awesome` VALUES (466, 'fa-music');
INSERT INTO `font-awesome` VALUES (467, 'fa-navicon');
INSERT INTO `font-awesome` VALUES (468, 'fa-neuter');
INSERT INTO `font-awesome` VALUES (469, 'fa-newspaper-o');
INSERT INTO `font-awesome` VALUES (470, 'fa-object-group');
INSERT INTO `font-awesome` VALUES (471, 'fa-object-ungroup');
INSERT INTO `font-awesome` VALUES (472, 'fa-odnoklassniki');
INSERT INTO `font-awesome` VALUES (473, 'fa-odnoklassniki-square');
INSERT INTO `font-awesome` VALUES (474, 'fa-opencart');
INSERT INTO `font-awesome` VALUES (475, 'fa-openid');
INSERT INTO `font-awesome` VALUES (476, 'fa-opera');
INSERT INTO `font-awesome` VALUES (477, 'fa-optin-monster');
INSERT INTO `font-awesome` VALUES (478, 'fa-outdent');
INSERT INTO `font-awesome` VALUES (479, 'fa-pagelines');
INSERT INTO `font-awesome` VALUES (480, 'fa-paint-brush');
INSERT INTO `font-awesome` VALUES (481, 'fa-paperclip');
INSERT INTO `font-awesome` VALUES (482, 'fa-paper-plane');
INSERT INTO `font-awesome` VALUES (483, 'fa-paper-plane-o');
INSERT INTO `font-awesome` VALUES (484, 'fa-paragraph');
INSERT INTO `font-awesome` VALUES (485, 'fa-paste');
INSERT INTO `font-awesome` VALUES (486, 'fa-pause');
INSERT INTO `font-awesome` VALUES (487, 'fa-pause-circle');
INSERT INTO `font-awesome` VALUES (488, 'fa-pause-circle-o');
INSERT INTO `font-awesome` VALUES (489, 'fa-paw');
INSERT INTO `font-awesome` VALUES (490, 'fa-paypal');
INSERT INTO `font-awesome` VALUES (491, 'fa-pencil');
INSERT INTO `font-awesome` VALUES (492, 'fa-pencil-square');
INSERT INTO `font-awesome` VALUES (493, 'fa-pencil-square-o');
INSERT INTO `font-awesome` VALUES (494, 'fa-percent');
INSERT INTO `font-awesome` VALUES (495, 'fa-phone');
INSERT INTO `font-awesome` VALUES (496, 'fa-phone-square');
INSERT INTO `font-awesome` VALUES (497, 'fa-photo');
INSERT INTO `font-awesome` VALUES (498, 'fa-picture-o');
INSERT INTO `font-awesome` VALUES (499, 'fa-pie-chart');
INSERT INTO `font-awesome` VALUES (500, 'fa-pied-piper');
INSERT INTO `font-awesome` VALUES (501, 'fa-pied-piper-alt');
INSERT INTO `font-awesome` VALUES (502, 'fa-pied-piper-pp');
INSERT INTO `font-awesome` VALUES (503, 'fa-pinterest');
INSERT INTO `font-awesome` VALUES (504, 'fa-pinterest-p');
INSERT INTO `font-awesome` VALUES (505, 'fa-pinterest-square');
INSERT INTO `font-awesome` VALUES (506, 'fa-plane');
INSERT INTO `font-awesome` VALUES (507, 'fa-play');
INSERT INTO `font-awesome` VALUES (508, 'fa-play-circle');
INSERT INTO `font-awesome` VALUES (509, 'fa-play-circle-o');
INSERT INTO `font-awesome` VALUES (510, 'fa-plug');
INSERT INTO `font-awesome` VALUES (511, 'fa-plus');
INSERT INTO `font-awesome` VALUES (512, 'fa-plus-circle');
INSERT INTO `font-awesome` VALUES (513, 'fa-plus-square');
INSERT INTO `font-awesome` VALUES (514, 'fa-plus-square-o');
INSERT INTO `font-awesome` VALUES (515, 'fa-podcast');
INSERT INTO `font-awesome` VALUES (516, 'fa-power-off');
INSERT INTO `font-awesome` VALUES (517, 'fa-print');
INSERT INTO `font-awesome` VALUES (518, 'fa-product-hunt');
INSERT INTO `font-awesome` VALUES (519, 'fa-puzzle-piece');
INSERT INTO `font-awesome` VALUES (520, 'fa-qq');
INSERT INTO `font-awesome` VALUES (521, 'fa-qrcode');
INSERT INTO `font-awesome` VALUES (522, 'fa-question');
INSERT INTO `font-awesome` VALUES (523, 'fa-question-circle');
INSERT INTO `font-awesome` VALUES (524, 'fa-question-circle-o');
INSERT INTO `font-awesome` VALUES (525, 'fa-quora');
INSERT INTO `font-awesome` VALUES (526, 'fa-quote-left');
INSERT INTO `font-awesome` VALUES (527, 'fa-quote-right');
INSERT INTO `font-awesome` VALUES (528, 'fa-ra');
INSERT INTO `font-awesome` VALUES (529, 'fa-random');
INSERT INTO `font-awesome` VALUES (530, 'fa-ravelry');
INSERT INTO `font-awesome` VALUES (531, 'fa-rebel');
INSERT INTO `font-awesome` VALUES (532, 'fa-recycle');
INSERT INTO `font-awesome` VALUES (533, 'fa-reddit');
INSERT INTO `font-awesome` VALUES (534, 'fa-reddit-alien');
INSERT INTO `font-awesome` VALUES (535, 'fa-reddit-square');
INSERT INTO `font-awesome` VALUES (536, 'fa-refresh');
INSERT INTO `font-awesome` VALUES (537, 'fa-registered');
INSERT INTO `font-awesome` VALUES (538, 'fa-remove');
INSERT INTO `font-awesome` VALUES (539, 'fa-renren');
INSERT INTO `font-awesome` VALUES (540, 'fa-reorder');
INSERT INTO `font-awesome` VALUES (541, 'fa-repeat');
INSERT INTO `font-awesome` VALUES (542, 'fa-reply');
INSERT INTO `font-awesome` VALUES (543, 'fa-reply-all');
INSERT INTO `font-awesome` VALUES (544, 'fa-resistance');
INSERT INTO `font-awesome` VALUES (545, 'fa-retweet');
INSERT INTO `font-awesome` VALUES (546, 'fa-rmb');
INSERT INTO `font-awesome` VALUES (547, 'fa-road');
INSERT INTO `font-awesome` VALUES (548, 'fa-rocket');
INSERT INTO `font-awesome` VALUES (549, 'fa-rotate-left');
INSERT INTO `font-awesome` VALUES (550, 'fa-rotate-right');
INSERT INTO `font-awesome` VALUES (551, 'fa-rouble');
INSERT INTO `font-awesome` VALUES (552, 'fa-rss');
INSERT INTO `font-awesome` VALUES (553, 'fa-rss-square');
INSERT INTO `font-awesome` VALUES (554, 'fa-rub');
INSERT INTO `font-awesome` VALUES (555, 'fa-ruble');
INSERT INTO `font-awesome` VALUES (556, 'fa-rupee');
INSERT INTO `font-awesome` VALUES (557, 'fa-s15');
INSERT INTO `font-awesome` VALUES (558, 'fa-safari');
INSERT INTO `font-awesome` VALUES (559, 'fa-save');
INSERT INTO `font-awesome` VALUES (560, 'fa-scissors');
INSERT INTO `font-awesome` VALUES (561, 'fa-scribd');
INSERT INTO `font-awesome` VALUES (562, 'fa-search');
INSERT INTO `font-awesome` VALUES (563, 'fa-search-minus');
INSERT INTO `font-awesome` VALUES (564, 'fa-search-plus');
INSERT INTO `font-awesome` VALUES (565, 'fa-sellsy');
INSERT INTO `font-awesome` VALUES (566, 'fa-send');
INSERT INTO `font-awesome` VALUES (567, 'fa-send-o');
INSERT INTO `font-awesome` VALUES (568, 'fa-server');
INSERT INTO `font-awesome` VALUES (569, 'fa-share');
INSERT INTO `font-awesome` VALUES (570, 'fa-share-alt');
INSERT INTO `font-awesome` VALUES (571, 'fa-share-alt-square');
INSERT INTO `font-awesome` VALUES (572, 'fa-share-square');
INSERT INTO `font-awesome` VALUES (573, 'fa-share-square-o');
INSERT INTO `font-awesome` VALUES (574, 'fa-shekel');
INSERT INTO `font-awesome` VALUES (575, 'fa-sheqel');
INSERT INTO `font-awesome` VALUES (576, 'fa-shield');
INSERT INTO `font-awesome` VALUES (577, 'fa-ship');
INSERT INTO `font-awesome` VALUES (578, 'fa-shirtsinbulk');
INSERT INTO `font-awesome` VALUES (579, 'fa-shopping-bag');
INSERT INTO `font-awesome` VALUES (580, 'fa-shopping-basket');
INSERT INTO `font-awesome` VALUES (581, 'fa-shopping-cart');
INSERT INTO `font-awesome` VALUES (582, 'fa-shower');
INSERT INTO `font-awesome` VALUES (583, 'fa-signal');
INSERT INTO `font-awesome` VALUES (584, 'fa-sign-in');
INSERT INTO `font-awesome` VALUES (585, 'fa-signing');
INSERT INTO `font-awesome` VALUES (586, 'fa-sign-language');
INSERT INTO `font-awesome` VALUES (587, 'fa-sign-out');
INSERT INTO `font-awesome` VALUES (588, 'fa-simplybuilt');
INSERT INTO `font-awesome` VALUES (589, 'fa-sitemap');
INSERT INTO `font-awesome` VALUES (590, 'fa-skyatlas');
INSERT INTO `font-awesome` VALUES (591, 'fa-skype');
INSERT INTO `font-awesome` VALUES (592, 'fa-slack');
INSERT INTO `font-awesome` VALUES (593, 'fa-sliders');
INSERT INTO `font-awesome` VALUES (594, 'fa-slideshare');
INSERT INTO `font-awesome` VALUES (595, 'fa-smile-o');
INSERT INTO `font-awesome` VALUES (596, 'fa-snapchat');
INSERT INTO `font-awesome` VALUES (597, 'fa-snapchat-ghost');
INSERT INTO `font-awesome` VALUES (598, 'fa-snapchat-square');
INSERT INTO `font-awesome` VALUES (599, 'fa-snowflake-o');
INSERT INTO `font-awesome` VALUES (600, 'fa-soccer-ball-o');
INSERT INTO `font-awesome` VALUES (601, 'fa-sort');
INSERT INTO `font-awesome` VALUES (602, 'fa-sort-alpha-asc');
INSERT INTO `font-awesome` VALUES (603, 'fa-sort-alpha-desc');
INSERT INTO `font-awesome` VALUES (604, 'fa-sort-amount-asc');
INSERT INTO `font-awesome` VALUES (605, 'fa-sort-amount-desc');
INSERT INTO `font-awesome` VALUES (606, 'fa-sort-asc');
INSERT INTO `font-awesome` VALUES (607, 'fa-sort-desc');
INSERT INTO `font-awesome` VALUES (608, 'fa-sort-down');
INSERT INTO `font-awesome` VALUES (609, 'fa-sort-numeric-asc');
INSERT INTO `font-awesome` VALUES (610, 'fa-sort-numeric-desc');
INSERT INTO `font-awesome` VALUES (611, 'fa-sort-up');
INSERT INTO `font-awesome` VALUES (612, 'fa-soundcloud');
INSERT INTO `font-awesome` VALUES (613, 'fa-space-shuttle');
INSERT INTO `font-awesome` VALUES (614, 'fa-spinner');
INSERT INTO `font-awesome` VALUES (615, 'fa-spoon');
INSERT INTO `font-awesome` VALUES (616, 'fa-spotify');
INSERT INTO `font-awesome` VALUES (617, 'fa-square');
INSERT INTO `font-awesome` VALUES (618, 'fa-square-o');
INSERT INTO `font-awesome` VALUES (619, 'fa-stack-exchange');
INSERT INTO `font-awesome` VALUES (620, 'fa-stack-overflow');
INSERT INTO `font-awesome` VALUES (621, 'fa-star');
INSERT INTO `font-awesome` VALUES (622, 'fa-star-half');
INSERT INTO `font-awesome` VALUES (623, 'fa-star-half-empty');
INSERT INTO `font-awesome` VALUES (624, 'fa-star-half-full');
INSERT INTO `font-awesome` VALUES (625, 'fa-star-half-o');
INSERT INTO `font-awesome` VALUES (626, 'fa-star-o');
INSERT INTO `font-awesome` VALUES (627, 'fa-steam');
INSERT INTO `font-awesome` VALUES (628, 'fa-steam-square');
INSERT INTO `font-awesome` VALUES (629, 'fa-step-backward');
INSERT INTO `font-awesome` VALUES (630, 'fa-step-forward');
INSERT INTO `font-awesome` VALUES (631, 'fa-stethoscope');
INSERT INTO `font-awesome` VALUES (632, 'fa-sticky-note');
INSERT INTO `font-awesome` VALUES (633, 'fa-sticky-note-o');
INSERT INTO `font-awesome` VALUES (634, 'fa-stop');
INSERT INTO `font-awesome` VALUES (635, 'fa-stop-circle');
INSERT INTO `font-awesome` VALUES (636, 'fa-stop-circle-o');
INSERT INTO `font-awesome` VALUES (637, 'fa-street-view');
INSERT INTO `font-awesome` VALUES (638, 'fa-strikethrough');
INSERT INTO `font-awesome` VALUES (639, 'fa-stumbleupon');
INSERT INTO `font-awesome` VALUES (640, 'fa-stumbleupon-circle');
INSERT INTO `font-awesome` VALUES (641, 'fa-subscript');
INSERT INTO `font-awesome` VALUES (642, 'fa-subway');
INSERT INTO `font-awesome` VALUES (643, 'fa-suitcase');
INSERT INTO `font-awesome` VALUES (644, 'fa-sun-o');
INSERT INTO `font-awesome` VALUES (645, 'fa-superpowers');
INSERT INTO `font-awesome` VALUES (646, 'fa-superscript');
INSERT INTO `font-awesome` VALUES (647, 'fa-support');
INSERT INTO `font-awesome` VALUES (648, 'fa-table');
INSERT INTO `font-awesome` VALUES (649, 'fa-tablet');
INSERT INTO `font-awesome` VALUES (650, 'fa-tachometer');
INSERT INTO `font-awesome` VALUES (651, 'fa-tag');
INSERT INTO `font-awesome` VALUES (652, 'fa-tags');
INSERT INTO `font-awesome` VALUES (653, 'fa-tasks');
INSERT INTO `font-awesome` VALUES (654, 'fa-taxi');
INSERT INTO `font-awesome` VALUES (655, 'fa-telegram');
INSERT INTO `font-awesome` VALUES (656, 'fa-television');
INSERT INTO `font-awesome` VALUES (657, 'fa-tencent-weibo');
INSERT INTO `font-awesome` VALUES (658, 'fa-terminal');
INSERT INTO `font-awesome` VALUES (659, 'fa-text-height');
INSERT INTO `font-awesome` VALUES (660, 'fa-text-width');
INSERT INTO `font-awesome` VALUES (661, 'fa-th');
INSERT INTO `font-awesome` VALUES (662, 'fa-themeisle');
INSERT INTO `font-awesome` VALUES (663, 'fa-thermometer');
INSERT INTO `font-awesome` VALUES (664, 'fa-thermometer-0');
INSERT INTO `font-awesome` VALUES (665, 'fa-thermometer-1');
INSERT INTO `font-awesome` VALUES (666, 'fa-thermometer-2');
INSERT INTO `font-awesome` VALUES (667, 'fa-thermometer-3');
INSERT INTO `font-awesome` VALUES (668, 'fa-thermometer-4');
INSERT INTO `font-awesome` VALUES (669, 'fa-thermometer-empty');
INSERT INTO `font-awesome` VALUES (670, 'fa-thermometer-full');
INSERT INTO `font-awesome` VALUES (671, 'fa-thermometer-half');
INSERT INTO `font-awesome` VALUES (672, 'fa-thermometer-quarter');
INSERT INTO `font-awesome` VALUES (673, 'fa-thermometer-three-quarters');
INSERT INTO `font-awesome` VALUES (674, 'fa-th-large');
INSERT INTO `font-awesome` VALUES (675, 'fa-th-list');
INSERT INTO `font-awesome` VALUES (676, 'fa-thumbs-down');
INSERT INTO `font-awesome` VALUES (677, 'fa-thumbs-o-down');
INSERT INTO `font-awesome` VALUES (678, 'fa-thumbs-o-up');
INSERT INTO `font-awesome` VALUES (679, 'fa-thumbs-up');
INSERT INTO `font-awesome` VALUES (680, 'fa-thumb-tack');
INSERT INTO `font-awesome` VALUES (681, 'fa-ticket');
INSERT INTO `font-awesome` VALUES (682, 'fa-times');
INSERT INTO `font-awesome` VALUES (683, 'fa-times-circle');
INSERT INTO `font-awesome` VALUES (684, 'fa-times-circle-o');
INSERT INTO `font-awesome` VALUES (685, 'fa-times-rectangle');
INSERT INTO `font-awesome` VALUES (686, 'fa-times-rectangle-o');
INSERT INTO `font-awesome` VALUES (687, 'fa-tint');
INSERT INTO `font-awesome` VALUES (688, 'fa-toggle-down');
INSERT INTO `font-awesome` VALUES (689, 'fa-toggle-left');
INSERT INTO `font-awesome` VALUES (690, 'fa-toggle-off');
INSERT INTO `font-awesome` VALUES (691, 'fa-toggle-on');
INSERT INTO `font-awesome` VALUES (692, 'fa-toggle-right');
INSERT INTO `font-awesome` VALUES (693, 'fa-toggle-up');
INSERT INTO `font-awesome` VALUES (694, 'fa-trademark');
INSERT INTO `font-awesome` VALUES (695, 'fa-train');
INSERT INTO `font-awesome` VALUES (696, 'fa-transgender');
INSERT INTO `font-awesome` VALUES (697, 'fa-transgender-alt');
INSERT INTO `font-awesome` VALUES (698, 'fa-trash');
INSERT INTO `font-awesome` VALUES (699, 'fa-trash-o');
INSERT INTO `font-awesome` VALUES (700, 'fa-tree');
INSERT INTO `font-awesome` VALUES (701, 'fa-trello');
INSERT INTO `font-awesome` VALUES (702, 'fa-tripadvisor');
INSERT INTO `font-awesome` VALUES (703, 'fa-trophy');
INSERT INTO `font-awesome` VALUES (704, 'fa-truck');
INSERT INTO `font-awesome` VALUES (705, 'fa-try');
INSERT INTO `font-awesome` VALUES (706, 'fa-tty');
INSERT INTO `font-awesome` VALUES (707, 'fa-tumblr');
INSERT INTO `font-awesome` VALUES (708, 'fa-tumblr-square');
INSERT INTO `font-awesome` VALUES (709, 'fa-turkish-lira');
INSERT INTO `font-awesome` VALUES (710, 'fa-tv');
INSERT INTO `font-awesome` VALUES (711, 'fa-twitch');
INSERT INTO `font-awesome` VALUES (712, 'fa-twitter');
INSERT INTO `font-awesome` VALUES (713, 'fa-twitter-square');
INSERT INTO `font-awesome` VALUES (714, 'fa-umbrella');
INSERT INTO `font-awesome` VALUES (715, 'fa-underline');
INSERT INTO `font-awesome` VALUES (716, 'fa-undo');
INSERT INTO `font-awesome` VALUES (717, 'fa-universal-access');
INSERT INTO `font-awesome` VALUES (718, 'fa-university');
INSERT INTO `font-awesome` VALUES (719, 'fa-unlink');
INSERT INTO `font-awesome` VALUES (720, 'fa-unlock');
INSERT INTO `font-awesome` VALUES (721, 'fa-unlock-alt');
INSERT INTO `font-awesome` VALUES (722, 'fa-unsorted');
INSERT INTO `font-awesome` VALUES (723, 'fa-upload');
INSERT INTO `font-awesome` VALUES (724, 'fa-usb');
INSERT INTO `font-awesome` VALUES (725, 'fa-usd');
INSERT INTO `font-awesome` VALUES (726, 'fa-user');
INSERT INTO `font-awesome` VALUES (727, 'fa-user-circle');
INSERT INTO `font-awesome` VALUES (728, 'fa-user-circle-o');
INSERT INTO `font-awesome` VALUES (729, 'fa-user-md');
INSERT INTO `font-awesome` VALUES (730, 'fa-user-o');
INSERT INTO `font-awesome` VALUES (731, 'fa-user-plus');
INSERT INTO `font-awesome` VALUES (732, 'fa-users');
INSERT INTO `font-awesome` VALUES (733, 'fa-user-secret');
INSERT INTO `font-awesome` VALUES (734, 'fa-user-times');
INSERT INTO `font-awesome` VALUES (735, 'fa-vcard');
INSERT INTO `font-awesome` VALUES (736, 'fa-vcard-o');
INSERT INTO `font-awesome` VALUES (737, 'fa-venus');
INSERT INTO `font-awesome` VALUES (738, 'fa-venus-double');
INSERT INTO `font-awesome` VALUES (739, 'fa-venus-mars');
INSERT INTO `font-awesome` VALUES (740, 'fa-viacoin');
INSERT INTO `font-awesome` VALUES (741, 'fa-viadeo');
INSERT INTO `font-awesome` VALUES (742, 'fa-viadeo-square');
INSERT INTO `font-awesome` VALUES (743, 'fa-video-camera');
INSERT INTO `font-awesome` VALUES (744, 'fa-vimeo');
INSERT INTO `font-awesome` VALUES (745, 'fa-vimeo-square');
INSERT INTO `font-awesome` VALUES (746, 'fa-vine');
INSERT INTO `font-awesome` VALUES (747, 'fa-vk');
INSERT INTO `font-awesome` VALUES (748, 'fa-volume-control-phone');
INSERT INTO `font-awesome` VALUES (749, 'fa-volume-down');
INSERT INTO `font-awesome` VALUES (750, 'fa-volume-off');
INSERT INTO `font-awesome` VALUES (751, 'fa-volume-up');
INSERT INTO `font-awesome` VALUES (752, 'fa-warning');
INSERT INTO `font-awesome` VALUES (753, 'fa-wechat');
INSERT INTO `font-awesome` VALUES (754, 'fa-weibo');
INSERT INTO `font-awesome` VALUES (755, 'fa-weixin');
INSERT INTO `font-awesome` VALUES (756, 'fa-whatsapp');
INSERT INTO `font-awesome` VALUES (757, 'fa-wheelchair');
INSERT INTO `font-awesome` VALUES (758, 'fa-wheelchair-alt');
INSERT INTO `font-awesome` VALUES (759, 'fa-wifi');
INSERT INTO `font-awesome` VALUES (760, 'fa-wikipedia-w');
INSERT INTO `font-awesome` VALUES (761, 'fa-window-close');
INSERT INTO `font-awesome` VALUES (762, 'fa-window-close-o');
INSERT INTO `font-awesome` VALUES (763, 'fa-window-maximize');
INSERT INTO `font-awesome` VALUES (764, 'fa-window-minimize');
INSERT INTO `font-awesome` VALUES (765, 'fa-window-restore');
INSERT INTO `font-awesome` VALUES (766, 'fa-windows');
INSERT INTO `font-awesome` VALUES (767, 'fa-won');
INSERT INTO `font-awesome` VALUES (768, 'fa-wordpress');
INSERT INTO `font-awesome` VALUES (769, 'fa-wpbeginner');
INSERT INTO `font-awesome` VALUES (770, 'fa-wpexplorer');
INSERT INTO `font-awesome` VALUES (771, 'fa-wpforms');
INSERT INTO `font-awesome` VALUES (772, 'fa-wrench');
INSERT INTO `font-awesome` VALUES (773, 'fa-xing');
INSERT INTO `font-awesome` VALUES (774, 'fa-xing-square');
INSERT INTO `font-awesome` VALUES (775, 'fa-yahoo');
INSERT INTO `font-awesome` VALUES (776, 'fa-yc');
INSERT INTO `font-awesome` VALUES (777, 'fa-y-combinator');
INSERT INTO `font-awesome` VALUES (778, 'fa-y-combinator-square');
INSERT INTO `font-awesome` VALUES (779, 'fa-yc-square');
INSERT INTO `font-awesome` VALUES (780, 'fa-yelp');
INSERT INTO `font-awesome` VALUES (781, 'fa-yen');
INSERT INTO `font-awesome` VALUES (782, 'fa-yoast');
INSERT INTO `font-awesome` VALUES (783, 'fa-youtube');
INSERT INTO `font-awesome` VALUES (784, 'fa-youtube-play');
INSERT INTO `font-awesome` VALUES (785, 'fa-youtube-square');

-- ----------------------------
-- Table structure for icons
-- ----------------------------
DROP TABLE IF EXISTS `icons`;
CREATE TABLE `icons`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `icon` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 787 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of icons
-- ----------------------------
INSERT INTO `icons` VALUES (1, 'fa-500px', '2017-11-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (2, 'fa-address-book', '2017-11-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (3, 'fa-address-book-o', '2017-11-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (4, 'fa-address-card', '2017-11-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (5, 'fa-address-card-o', '2017-11-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (6, 'fa-adjust', '2017-11-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (7, 'fa-adn', '2017-11-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (8, 'fa-align-center', '2017-11-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (9, 'fa-align-justify', '2017-11-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (10, 'fa-align-left', '2017-11-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (11, 'fa-align-right', '2017-11-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (12, 'fa-amazon', '2017-11-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (13, 'fa-ambulance', '2017-11-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (14, 'fa-american-sign-language-interpreting', '2017-11-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (15, 'fa-anchor', '2017-11-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (16, 'fa-android', '2017-12-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (17, 'fa-angellist', '2017-12-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (18, 'fa-angle-double-down', '2017-12-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (19, 'fa-angle-double-left', '2017-12-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (20, 'fa-angle-double-right', '2017-12-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (21, 'fa-angle-double-up', '2017-12-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (22, 'fa-angle-down', '2017-12-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (23, 'fa-angle-left', '2017-12-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (24, 'fa-angle-right', '2017-12-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (25, 'fa-angle-up', '2017-12-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (26, 'fa-apple', '2017-12-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (27, 'fa-archive', '2017-12-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (28, 'fa-area-chart', '2017-12-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (29, 'fa-arrow-circle-down', '2017-12-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (30, 'fa-arrow-circle-left', '2017-12-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (31, 'fa-arrow-circle-o-down', '2017-12-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (32, 'fa-arrow-circle-o-left', '2017-12-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (33, 'fa-arrow-circle-o-right', '2017-12-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (34, 'fa-arrow-circle-o-up', '2017-12-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (35, 'fa-arrow-circle-right', '2017-12-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (36, 'fa-arrow-circle-up', '2017-12-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (37, 'fa-arrow-down', '2017-12-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (38, 'fa-arrow-left', '2017-12-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (39, 'fa-arrow-right', '2017-12-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (40, 'fa-arrow-up', '2017-12-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (41, 'fa-arrows', '2017-12-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (42, 'fa-arrows-alt', '2017-12-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (43, 'fa-arrows-h', '2017-12-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (44, 'fa-arrows-v', '2017-12-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (45, 'fa-asl-interpreting', '2017-12-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (46, 'fa-assistive-listening-systems', '2017-12-31 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (47, 'fa-asterisk', '2018-01-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (48, 'fa-at', '2018-01-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (49, 'fa-audio-description', '2018-01-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (50, 'fa-automobile', '2018-01-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (51, 'fa-backward', '2018-01-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (52, 'fa-balance-scale', '2018-01-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (53, 'fa-ban', '2018-01-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (54, 'fa-bandcamp', '2018-01-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (55, 'fa-bank', '2018-01-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (56, 'fa-bar-chart', '2018-01-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (57, 'fa-bar-chart-o', '2018-01-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (58, 'fa-barcode', '2018-01-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (59, 'fa-bars', '2018-01-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (60, 'fa-bath', '2018-01-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (61, 'fa-bathtub', '2018-01-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (62, 'fa-battery', '2018-01-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (63, 'fa-battery-0', '2018-01-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (64, 'fa-battery-1', '2018-01-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (65, 'fa-battery-2', '2018-01-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (66, 'fa-battery-3', '2018-01-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (67, 'fa-battery-4', '2018-01-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (68, 'fa-battery-empty', '2018-01-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (69, 'fa-battery-full', '2018-01-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (70, 'fa-battery-half', '2018-01-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (71, 'fa-battery-quarter', '2018-01-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (72, 'fa-battery-three-quarters', '2018-01-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (73, 'fa-bed', '2018-01-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (74, 'fa-beer', '2018-01-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (75, 'fa-behance', '2018-01-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (76, 'fa-behance-square', '2018-01-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (77, 'fa-bell', '2018-01-31 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (78, 'fa-bell-o', '2018-02-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (79, 'fa-bell-slash', '2018-02-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (80, 'fa-bell-slash-o', '2018-02-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (81, 'fa-bicycle', '2018-02-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (82, 'fa-binoculars', '2018-02-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (83, 'fa-birthday-cake', '2018-02-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (84, 'fa-bitbucket', '2018-02-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (85, 'fa-bitbucket-square', '2018-02-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (86, 'fa-bitcoin', '2018-02-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (87, 'fa-black-tie', '2018-02-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (88, 'fa-blind', '2018-02-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (89, 'fa-bluetooth', '2018-02-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (90, 'fa-bluetooth-b', '2018-02-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (91, 'fa-bold', '2018-02-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (92, 'fa-bolt', '2018-02-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (93, 'fa-bomb', '2018-02-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (94, 'fa-book', '2018-02-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (95, 'fa-bookmark', '2018-02-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (96, 'fa-bookmark-o', '2018-02-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (97, 'fa-braille', '2018-02-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (98, 'fa-briefcase', '2018-02-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (99, 'fa-btc', '2018-02-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (100, 'fa-bug', '2018-02-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (101, 'fa-building', '2018-02-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (102, 'fa-building-o', '2018-02-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (103, 'fa-bullhorn', '2018-02-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (104, 'fa-bullseye', '2018-02-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (105, 'fa-bus', '2018-02-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (106, 'fa-buysellads', '2018-03-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (107, 'fa-cab', '2018-03-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (108, 'fa-calculator', '2018-03-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (109, 'fa-calendar', '2018-03-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (110, 'fa-calendar-check-o', '2018-03-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (111, 'fa-calendar-minus-o', '2018-03-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (112, 'fa-calendar-o', '2018-03-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (113, 'fa-calendar-plus-o', '2018-03-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (114, 'fa-calendar-times-o', '2018-03-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (115, 'fa-camera', '2018-03-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (116, 'fa-camera-retro', '2018-03-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (117, 'fa-car', '2018-03-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (118, 'fa-caret-down', '2018-03-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (119, 'fa-caret-left', '2018-03-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (120, 'fa-caret-right', '2018-03-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (121, 'fa-caret-square-o-down', '2018-03-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (122, 'fa-caret-square-o-left', '2018-03-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (123, 'fa-caret-square-o-right', '2018-03-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (124, 'fa-caret-square-o-up', '2018-03-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (125, 'fa-caret-up', '2018-03-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (126, 'fa-cart-arrow-down', '2018-03-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (127, 'fa-cart-plus', '2018-03-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (128, 'fa-cc', '2018-03-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (129, 'fa-cc-amex', '2018-03-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (130, 'fa-cc-diners-club', '2018-03-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (131, 'fa-cc-discover', '2018-03-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (132, 'fa-cc-jcb', '2018-03-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (133, 'fa-cc-mastercard', '2018-03-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (134, 'fa-cc-paypal', '2018-03-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (135, 'fa-cc-stripe', '2018-03-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (136, 'fa-cc-visa', '2018-03-31 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (137, 'fa-certificate', '2018-04-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (138, 'fa-chain', '2018-04-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (139, 'fa-chain-broken', '2018-04-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (140, 'fa-check', '2018-04-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (141, 'fa-check-circle', '2018-04-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (142, 'fa-check-circle-o', '2018-04-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (143, 'fa-check-square', '2018-04-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (144, 'fa-check-square-o', '2018-04-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (145, 'fa-chevron-circle-down', '2018-04-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (146, 'fa-chevron-circle-left', '2018-04-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (147, 'fa-chevron-circle-right', '2018-04-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (148, 'fa-chevron-circle-up', '2018-04-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (149, 'fa-chevron-down', '2018-04-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (150, 'fa-chevron-left', '2018-04-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (151, 'fa-chevron-right', '2018-04-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (152, 'fa-chevron-up', '2018-04-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (153, 'fa-child', '2018-04-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (154, 'fa-chrome', '2018-04-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (155, 'fa-circle', '2018-04-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (156, 'fa-circle-o', '2018-04-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (157, 'fa-circle-o-notch', '2018-04-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (158, 'fa-circle-thin', '2018-04-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (159, 'fa-clipboard', '2018-04-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (160, 'fa-clock-o', '2018-04-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (161, 'fa-clone', '2018-04-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (162, 'fa-close', '2018-04-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (163, 'fa-cloud', '2018-04-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (164, 'fa-cloud-download', '2018-04-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (165, 'fa-cloud-upload', '2018-04-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (166, 'fa-cny', '2018-04-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (167, 'fa-code', '2018-05-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (168, 'fa-code-fork', '2018-05-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (169, 'fa-codepen', '2018-05-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (170, 'fa-codiepie', '2018-05-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (171, 'fa-coffee', '2018-05-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (172, 'fa-cog', '2018-05-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (173, 'fa-cogs', '2018-05-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (174, 'fa-columns', '2018-05-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (175, 'fa-comment', '2018-05-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (176, 'fa-comment-o', '2018-05-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (177, 'fa-commenting', '2018-05-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (178, 'fa-commenting-o', '2018-05-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (179, 'fa-comments', '2018-05-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (180, 'fa-comments-o', '2018-05-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (181, 'fa-compass', '2018-05-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (182, 'fa-compress', '2018-05-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (183, 'fa-connectdevelop', '2018-05-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (184, 'fa-contao', '2018-05-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (185, 'fa-copy', '2018-05-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (186, 'fa-copyright', '2018-05-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (187, 'fa-creative-commons', '2018-05-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (188, 'fa-credit-card', '2018-05-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (189, 'fa-credit-card-alt', '2018-05-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (190, 'fa-crop', '2018-05-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (191, 'fa-crosshairs', '2018-05-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (192, 'fa-css3', '2018-05-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (193, 'fa-cube', '2018-05-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (194, 'fa-cubes', '2018-05-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (195, 'fa-cut', '2018-05-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (196, 'fa-cutlery', '2018-05-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (197, 'fa-dashboard', '2018-05-31 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (198, 'fa-dashcube', '2018-06-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (199, 'fa-database', '2018-06-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (200, 'fa-deaf', '2018-06-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (201, 'fa-deafness', '2018-06-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (202, 'fa-dedent', '2018-06-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (203, 'fa-delicious', '2018-06-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (204, 'fa-desktop', '2018-06-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (205, 'fa-deviantart', '2018-06-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (206, 'fa-diamond', '2018-06-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (207, 'fa-digg', '2018-06-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (208, 'fa-dollar', '2018-06-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (209, 'fa-dot-circle-o', '2018-06-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (210, 'fa-download', '2018-06-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (211, 'fa-dribbble', '2018-06-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (212, 'fa-drivers-license', '2018-06-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (213, 'fa-drivers-license-o', '2018-06-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (214, 'fa-dropbox', '2018-06-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (215, 'fa-drupal', '2018-06-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (216, 'fa-edge', '2018-06-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (217, 'fa-edit', '2018-06-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (218, 'fa-eercast', '2018-06-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (219, 'fa-eject', '2018-06-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (220, 'fa-ellipsis-h', '2018-06-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (221, 'fa-ellipsis-v', '2018-06-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (222, 'fa-empire', '2018-06-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (223, 'fa-envelope', '2018-06-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (224, 'fa-envelope-o', '2018-06-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (225, 'fa-envelope-open', '2018-06-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (226, 'fa-envelope-open-o', '2018-06-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (227, 'fa-envelope-square', '2018-06-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (228, 'fa-envira', '2018-07-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (229, 'fa-eraser', '2018-07-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (230, 'fa-etsy', '2018-07-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (231, 'fa-eur', '2018-07-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (232, 'fa-euro', '2018-07-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (233, 'fa-exchange', '2018-07-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (234, 'fa-exclamation', '2018-07-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (235, 'fa-exclamation-circle', '2018-07-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (236, 'fa-exclamation-triangle', '2018-07-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (237, 'fa-expand', '2018-07-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (238, 'fa-expeditedssl', '2018-07-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (239, 'fa-external-link', '2018-07-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (240, 'fa-external-link-square', '2018-07-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (241, 'fa-eye', '2018-07-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (242, 'fa-eye-slash', '2018-07-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (243, 'fa-eyedropper', '2018-07-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (244, 'fa-fa', '2018-07-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (245, 'fa-facebook', '2018-07-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (246, 'fa-facebook-f', '2018-07-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (247, 'fa-facebook-official', '2018-07-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (248, 'fa-facebook-square', '2018-07-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (249, 'fa-fast-backward', '2018-07-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (250, 'fa-fast-forward', '2018-07-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (251, 'fa-fax', '2018-07-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (252, 'fa-feed', '2018-07-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (253, 'fa-female', '2018-07-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (254, 'fa-fighter-jet', '2018-07-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (255, 'fa-file', '2018-07-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (256, 'fa-file-archive-o', '2018-07-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (257, 'fa-file-audio-o', '2018-07-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (258, 'fa-file-code-o', '2018-07-31 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (259, 'fa-file-excel-o', '2018-08-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (260, 'fa-file-image-o', '2018-08-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (261, 'fa-file-movie-o', '2018-08-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (262, 'fa-file-o', '2018-08-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (263, 'fa-file-pdf-o', '2018-08-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (264, 'fa-file-photo-o', '2018-08-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (265, 'fa-file-picture-o', '2018-08-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (266, 'fa-file-powerpoint-o', '2018-08-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (267, 'fa-file-sound-o', '2018-08-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (268, 'fa-file-text', '2018-08-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (269, 'fa-file-text-o', '2018-08-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (270, 'fa-file-video-o', '2018-08-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (271, 'fa-file-word-o', '2018-08-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (272, 'fa-file-zip-o', '2018-08-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (273, 'fa-files-o', '2018-08-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (274, 'fa-film', '2018-08-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (275, 'fa-filter', '2018-08-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (276, 'fa-fire', '2018-08-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (277, 'fa-fire-extinguisher', '2018-08-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (278, 'fa-firefox', '2018-08-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (279, 'fa-first-order', '2018-08-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (280, 'fa-flag', '2018-08-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (281, 'fa-flag-checkered', '2018-08-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (282, 'fa-flag-o', '2018-08-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (283, 'fa-flash', '2018-08-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (284, 'fa-flask', '2018-08-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (285, 'fa-flickr', '2018-08-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (286, 'fa-floppy-o', '2018-08-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (287, 'fa-folder', '2018-08-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (288, 'fa-folder-o', '2018-08-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (289, 'fa-folder-open', '2018-08-31 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (290, 'fa-folder-open-o', '2018-09-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (291, 'fa-font', '2018-09-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (292, 'fa-font-awesome', '2018-09-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (293, 'fa-fonticons', '2018-09-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (294, 'fa-fort-awesome', '2018-09-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (295, 'fa-forumbee', '2018-09-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (296, 'fa-forward', '2018-09-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (297, 'fa-foursquare', '2018-09-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (298, 'fa-free-code-camp', '2018-09-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (299, 'fa-frown-o', '2018-09-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (300, 'fa-futbol-o', '2018-09-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (301, 'fa-gamepad', '2018-09-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (302, 'fa-gavel', '2018-09-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (303, 'fa-gbp', '2018-09-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (304, 'fa-ge', '2018-09-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (305, 'fa-gear', '2018-09-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (306, 'fa-gears', '2018-09-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (307, 'fa-genderless', '2018-09-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (308, 'fa-get-pocket', '2018-09-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (309, 'fa-gg', '2018-09-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (310, 'fa-gg-circle', '2018-09-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (311, 'fa-gift', '2018-09-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (312, 'fa-git', '2018-09-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (313, 'fa-git-square', '2018-09-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (314, 'fa-github', '2018-09-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (315, 'fa-github-alt', '2018-09-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (316, 'fa-github-square', '2018-09-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (317, 'fa-gitlab', '2018-09-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (318, 'fa-gittip', '2018-09-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (319, 'fa-glass', '2018-09-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (320, 'fa-glide', '2018-10-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (321, 'fa-glide-g', '2018-10-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (322, 'fa-globe', '2018-10-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (323, 'fa-google', '2018-10-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (324, 'fa-google-plus', '2018-10-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (325, 'fa-google-plus-circle', '2018-10-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (326, 'fa-google-plus-official', '2018-10-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (327, 'fa-google-plus-square', '2018-10-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (328, 'fa-google-wallet', '2018-10-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (329, 'fa-graduation-cap', '2018-10-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (330, 'fa-gratipay', '2018-10-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (331, 'fa-grav', '2018-10-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (332, 'fa-group', '2018-10-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (333, 'fa-h-square', '2018-10-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (334, 'fa-hacker-news', '2018-10-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (335, 'fa-hand-grab-o', '2018-10-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (336, 'fa-hand-lizard-o', '2018-10-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (337, 'fa-hand-o-down', '2018-10-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (338, 'fa-hand-o-left', '2018-10-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (339, 'fa-hand-o-right', '2018-10-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (340, 'fa-hand-o-up', '2018-10-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (341, 'fa-hand-paper-o', '2018-10-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (342, 'fa-hand-peace-o', '2018-10-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (343, 'fa-hand-pointer-o', '2018-10-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (344, 'fa-hand-rock-o', '2018-10-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (345, 'fa-hand-scissors-o', '2018-10-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (346, 'fa-hand-spock-o', '2018-10-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (347, 'fa-hand-stop-o', '2018-10-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (348, 'fa-handshake-o', '2018-10-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (349, 'fa-hard-of-hearing', '2018-10-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (350, 'fa-hashtag', '2018-10-31 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (351, 'fa-hdd-o', '2018-11-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (352, 'fa-header', '2018-11-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (353, 'fa-headphones', '2018-11-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (354, 'fa-heart', '2018-11-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (355, 'fa-heart-o', '2018-11-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (356, 'fa-heartbeat', '2018-11-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (357, 'fa-history', '2018-11-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (358, 'fa-home', '2018-11-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (359, 'fa-hospital-o', '2018-11-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (360, 'fa-hotel', '2018-11-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (361, 'fa-hourglass', '2018-11-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (362, 'fa-hourglass-1', '2018-11-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (363, 'fa-hourglass-2', '2018-11-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (364, 'fa-hourglass-3', '2018-11-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (365, 'fa-hourglass-end', '2018-11-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (366, 'fa-hourglass-half', '2018-11-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (367, 'fa-hourglass-o', '2018-11-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (368, 'fa-hourglass-start', '2018-11-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (369, 'fa-houzz', '2018-11-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (370, 'fa-html5', '2018-11-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (371, 'fa-i-cursor', '2018-11-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (372, 'fa-id-badge', '2018-11-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (373, 'fa-id-card', '2018-11-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (374, 'fa-id-card-o', '2018-11-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (375, 'fa-ils', '2018-11-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (376, 'fa-image', '2018-11-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (377, 'fa-imdb', '2018-11-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (378, 'fa-inbox', '2018-11-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (379, 'fa-indent', '2018-11-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (380, 'fa-industry', '2018-11-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (381, 'fa-info', '2018-12-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (382, 'fa-info-circle', '2018-12-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (383, 'fa-inr', '2018-12-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (384, 'fa-instagram', '2018-12-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (385, 'fa-institution', '2018-12-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (386, 'fa-internet-explorer', '2018-12-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (387, 'fa-intersex', '2018-12-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (388, 'fa-ioxhost', '2018-12-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (389, 'fa-italic', '2018-12-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (390, 'fa-joomla', '2018-12-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (391, 'fa-jpy', '2018-12-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (392, 'fa-jsfiddle', '2018-12-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (393, 'fa-key', '2018-12-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (394, 'fa-keyboard-o', '2018-12-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (395, 'fa-krw', '2018-12-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (396, 'fa-language', '2018-12-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (397, 'fa-laptop', '2018-12-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (398, 'fa-lastfm', '2018-12-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (399, 'fa-lastfm-square', '2018-12-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (400, 'fa-leaf', '2018-12-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (401, 'fa-leanpub', '2018-12-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (402, 'fa-legal', '2018-12-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (403, 'fa-lemon-o', '2018-12-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (404, 'fa-level-down', '2018-12-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (405, 'fa-level-up', '2018-12-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (406, 'fa-life-bouy', '2018-12-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (407, 'fa-life-buoy', '2018-12-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (408, 'fa-life-ring', '2018-12-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (409, 'fa-life-saver', '2018-12-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (410, 'fa-lightbulb-o', '2018-12-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (411, 'fa-line-chart', '2018-12-31 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (412, 'fa-link', '2019-01-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (413, 'fa-linkedin', '2019-01-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (414, 'fa-linkedin-square', '2019-01-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (415, 'fa-linode', '2019-01-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (416, 'fa-linux', '2019-01-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (417, 'fa-list', '2019-01-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (418, 'fa-list-alt', '2019-01-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (419, 'fa-list-ol', '2019-01-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (420, 'fa-list-ul', '2019-01-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (421, 'fa-location-arrow', '2019-01-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (422, 'fa-lock', '2019-01-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (423, 'fa-long-arrow-down', '2019-01-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (424, 'fa-long-arrow-left', '2019-01-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (425, 'fa-long-arrow-right', '2019-01-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (426, 'fa-long-arrow-up', '2019-01-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (427, 'fa-low-vision', '2019-01-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (428, 'fa-magic', '2019-01-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (429, 'fa-magnet', '2019-01-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (430, 'fa-mail-forward', '2019-01-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (431, 'fa-mail-reply', '2019-01-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (432, 'fa-mail-reply-all', '2019-01-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (433, 'fa-male', '2019-01-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (434, 'fa-map', '2019-01-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (435, 'fa-map-marker', '2019-01-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (436, 'fa-map-o', '2019-01-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (437, 'fa-map-pin', '2019-01-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (438, 'fa-map-signs', '2019-01-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (439, 'fa-mars', '2019-01-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (440, 'fa-mars-double', '2019-01-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (441, 'fa-mars-stroke', '2019-01-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (442, 'fa-mars-stroke-h', '2019-01-31 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (443, 'fa-mars-stroke-v', '2019-02-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (444, 'fa-maxcdn', '2019-02-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (445, 'fa-meanpath', '2019-02-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (446, 'fa-medium', '2019-02-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (447, 'fa-medkit', '2019-02-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (448, 'fa-meetup', '2019-02-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (449, 'fa-meh-o', '2019-02-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (450, 'fa-mercury', '2019-02-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (451, 'fa-microchip', '2019-02-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (452, 'fa-microphone', '2019-02-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (453, 'fa-microphone-slash', '2019-02-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (454, 'fa-minus', '2019-02-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (455, 'fa-minus-circle', '2019-02-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (456, 'fa-minus-square', '2019-02-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (457, 'fa-minus-square-o', '2019-02-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (458, 'fa-mixcloud', '2019-02-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (459, 'fa-mobile', '2019-02-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (460, 'fa-mobile-phone', '2019-02-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (461, 'fa-modx', '2019-02-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (462, 'fa-money', '2019-02-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (463, 'fa-moon-o', '2019-02-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (464, 'fa-mortar-board', '2019-02-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (465, 'fa-motorcycle', '2019-02-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (466, 'fa-mouse-pointer', '2019-02-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (467, 'fa-music', '2019-02-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (468, 'fa-navicon', '2019-02-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (469, 'fa-neuter', '2019-02-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (470, 'fa-newspaper-o', '2019-02-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (471, 'fa-object-group', '2019-03-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (472, 'fa-object-ungroup', '2019-03-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (473, 'fa-odnoklassniki', '2019-03-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (474, 'fa-odnoklassniki-square', '2019-03-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (475, 'fa-opencart', '2019-03-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (476, 'fa-openid', '2019-03-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (477, 'fa-opera', '2019-03-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (478, 'fa-optin-monster', '2019-03-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (479, 'fa-outdent', '2019-03-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (480, 'fa-pagelines', '2019-03-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (481, 'fa-paint-brush', '2019-03-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (482, 'fa-paper-plane', '2019-03-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (483, 'fa-paper-plane-o', '2019-03-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (484, 'fa-paperclip', '2019-03-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (485, 'fa-paragraph', '2019-03-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (486, 'fa-paste', '2019-03-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (487, 'fa-pause', '2019-03-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (488, 'fa-pause-circle', '2019-03-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (489, 'fa-pause-circle-o', '2019-03-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (490, 'fa-paw', '2019-03-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (491, 'fa-paypal', '2019-03-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (492, 'fa-pencil', '2019-03-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (493, 'fa-pencil-square', '2019-03-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (494, 'fa-pencil-square-o', '2019-03-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (495, 'fa-percent', '2019-03-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (496, 'fa-phone', '2019-03-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (497, 'fa-phone-square', '2019-03-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (498, 'fa-photo', '2019-03-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (499, 'fa-picture-o', '2019-03-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (500, 'fa-pie-chart', '2019-03-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (501, 'fa-pied-piper', '2019-03-31 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (502, 'fa-pied-piper-alt', '2019-04-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (503, 'fa-pied-piper-pp', '2019-04-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (504, 'fa-pinterest', '2019-04-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (505, 'fa-pinterest-p', '2019-04-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (506, 'fa-pinterest-square', '2019-04-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (507, 'fa-plane', '2019-04-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (508, 'fa-play', '2019-04-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (509, 'fa-play-circle', '2019-04-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (510, 'fa-play-circle-o', '2019-04-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (511, 'fa-plug', '2019-04-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (512, 'fa-plus', '2019-04-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (513, 'fa-plus-circle', '2019-04-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (514, 'fa-plus-square', '2019-04-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (515, 'fa-plus-square-o', '2019-04-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (516, 'fa-podcast', '2019-04-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (517, 'fa-power-off', '2019-04-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (518, 'fa-print', '2019-04-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (519, 'fa-product-hunt', '2019-04-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (520, 'fa-puzzle-piece', '2019-04-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (521, 'fa-qq', '2019-04-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (522, 'fa-qrcode', '2019-04-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (523, 'fa-question', '2019-04-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (524, 'fa-question-circle', '2019-04-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (525, 'fa-question-circle-o', '2019-04-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (526, 'fa-quora', '2019-04-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (527, 'fa-quote-left', '2019-04-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (528, 'fa-quote-right', '2019-04-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (529, 'fa-ra', '2019-04-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (530, 'fa-random', '2019-04-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (531, 'fa-ravelry', '2019-04-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (532, 'fa-rebel', '2019-05-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (533, 'fa-recycle', '2019-05-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (534, 'fa-reddit', '2019-05-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (535, 'fa-reddit-alien', '2019-05-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (536, 'fa-reddit-square', '2019-05-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (537, 'fa-refresh', '2019-05-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (538, 'fa-registered', '2019-05-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (539, 'fa-remove', '2019-05-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (540, 'fa-renren', '2019-05-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (541, 'fa-reorder', '2019-05-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (542, 'fa-repeat', '2019-05-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (543, 'fa-reply', '2019-05-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (544, 'fa-reply-all', '2019-05-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (545, 'fa-resistance', '2019-05-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (546, 'fa-retweet', '2019-05-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (547, 'fa-rmb', '2019-05-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (548, 'fa-road', '2019-05-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (549, 'fa-rocket', '2019-05-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (550, 'fa-rotate-left', '2019-05-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (551, 'fa-rotate-right', '2019-05-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (552, 'fa-rouble', '2019-05-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (553, 'fa-rss', '2019-05-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (554, 'fa-rss-square', '2019-05-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (555, 'fa-rub', '2019-05-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (556, 'fa-ruble', '2019-05-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (557, 'fa-rupee', '2019-05-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (558, 'fa-s15', '2019-05-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (559, 'fa-safari', '2019-05-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (560, 'fa-save', '2019-05-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (561, 'fa-scissors', '2019-05-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (562, 'fa-scribd', '2019-05-31 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (563, 'fa-search', '2019-06-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (564, 'fa-search-minus', '2019-06-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (565, 'fa-search-plus', '2019-06-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (566, 'fa-sellsy', '2019-06-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (567, 'fa-send', '2019-06-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (568, 'fa-send-o', '2019-06-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (569, 'fa-server', '2019-06-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (570, 'fa-share', '2019-06-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (571, 'fa-share-alt', '2019-06-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (572, 'fa-share-alt-square', '2019-06-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (573, 'fa-share-square', '2019-06-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (574, 'fa-share-square-o', '2019-06-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (575, 'fa-shekel', '2019-06-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (576, 'fa-sheqel', '2019-06-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (577, 'fa-shield', '2019-06-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (578, 'fa-ship', '2019-06-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (579, 'fa-shirtsinbulk', '2019-06-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (580, 'fa-shopping-bag', '2019-06-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (581, 'fa-shopping-basket', '2019-06-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (582, 'fa-shopping-cart', '2019-06-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (583, 'fa-shower', '2019-06-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (584, 'fa-sign-in', '2019-06-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (585, 'fa-sign-language', '2019-06-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (586, 'fa-sign-out', '2019-06-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (587, 'fa-signal', '2019-06-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (588, 'fa-signing', '2019-06-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (589, 'fa-simplybuilt', '2019-06-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (590, 'fa-sitemap', '2019-06-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (591, 'fa-skyatlas', '2019-06-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (592, 'fa-skype', '2019-06-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (593, 'fa-slack', '2019-07-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (594, 'fa-sliders', '2019-07-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (595, 'fa-slideshare', '2019-07-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (596, 'fa-smile-o', '2019-07-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (597, 'fa-snapchat', '2019-07-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (598, 'fa-snapchat-ghost', '2019-07-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (599, 'fa-snapchat-square', '2019-07-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (600, 'fa-snowflake-o', '2019-07-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (601, 'fa-soccer-ball-o', '2019-07-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (602, 'fa-sort', '2019-07-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (603, 'fa-sort-alpha-asc', '2019-07-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (604, 'fa-sort-alpha-desc', '2019-07-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (605, 'fa-sort-amount-asc', '2019-07-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (606, 'fa-sort-amount-desc', '2019-07-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (607, 'fa-sort-asc', '2019-07-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (608, 'fa-sort-desc', '2019-07-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (609, 'fa-sort-down', '2019-07-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (610, 'fa-sort-numeric-asc', '2019-07-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (611, 'fa-sort-numeric-desc', '2019-07-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (612, 'fa-sort-up', '2019-07-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (613, 'fa-soundcloud', '2019-07-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (614, 'fa-space-shuttle', '2019-07-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (615, 'fa-spinner', '2019-07-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (616, 'fa-spoon', '2019-07-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (617, 'fa-spotify', '2019-07-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (618, 'fa-square', '2019-07-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (619, 'fa-square-o', '2019-07-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (620, 'fa-stack-exchange', '2019-07-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (621, 'fa-stack-overflow', '2019-07-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (622, 'fa-star', '2019-07-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (623, 'fa-star-half', '2019-07-31 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (624, 'fa-star-half-empty', '2019-08-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (625, 'fa-star-half-full', '2019-08-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (626, 'fa-star-half-o', '2019-08-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (627, 'fa-star-o', '2019-08-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (628, 'fa-steam', '2019-08-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (629, 'fa-steam-square', '2019-08-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (630, 'fa-step-backward', '2019-08-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (631, 'fa-step-forward', '2019-08-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (632, 'fa-stethoscope', '2019-08-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (633, 'fa-sticky-note', '2019-08-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (634, 'fa-sticky-note-o', '2019-08-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (635, 'fa-stop', '2019-08-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (636, 'fa-stop-circle', '2019-08-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (637, 'fa-stop-circle-o', '2019-08-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (638, 'fa-street-view', '2019-08-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (639, 'fa-strikethrough', '2019-08-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (640, 'fa-stumbleupon', '2019-08-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (641, 'fa-stumbleupon-circle', '2019-08-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (642, 'fa-subscript', '2019-08-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (643, 'fa-subway', '2019-08-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (644, 'fa-suitcase', '2019-08-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (645, 'fa-sun-o', '2019-08-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (646, 'fa-superpowers', '2019-08-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (647, 'fa-superscript', '2019-08-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (648, 'fa-support', '2019-08-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (649, 'fa-table', '2019-08-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (650, 'fa-tablet', '2019-08-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (651, 'fa-tachometer', '2019-08-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (652, 'fa-tag', '2019-08-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (653, 'fa-tags', '2019-08-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (654, 'fa-tasks', '2019-08-31 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (655, 'fa-taxi', '2019-09-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (656, 'fa-telegram', '2019-09-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (657, 'fa-television', '2019-09-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (658, 'fa-tencent-weibo', '2019-09-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (659, 'fa-terminal', '2019-09-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (660, 'fa-text-height', '2019-09-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (661, 'fa-text-width', '2019-09-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (662, 'fa-th', '2019-09-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (663, 'fa-th-large', '2019-09-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (664, 'fa-th-list', '2019-09-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (665, 'fa-themeisle', '2019-09-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (666, 'fa-thermometer', '2019-09-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (667, 'fa-thermometer-0', '2019-09-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (668, 'fa-thermometer-1', '2019-09-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (669, 'fa-thermometer-2', '2019-09-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (670, 'fa-thermometer-3', '2019-09-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (671, 'fa-thermometer-4', '2019-09-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (672, 'fa-thermometer-empty', '2019-09-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (673, 'fa-thermometer-full', '2019-09-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (674, 'fa-thermometer-half', '2019-09-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (675, 'fa-thermometer-quarter', '2019-09-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (676, 'fa-thermometer-three-quarters', '2019-09-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (677, 'fa-thumb-tack', '2019-09-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (678, 'fa-thumbs-down', '2019-09-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (679, 'fa-thumbs-o-down', '2019-09-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (680, 'fa-thumbs-o-up', '2019-09-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (681, 'fa-thumbs-up', '2019-09-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (682, 'fa-ticket', '2019-09-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (683, 'fa-times', '2019-09-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (684, 'fa-times-circle', '2019-09-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (685, 'fa-times-circle-o', '2019-10-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (686, 'fa-times-rectangle', '2019-10-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (687, 'fa-times-rectangle-o', '2019-10-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (688, 'fa-tint', '2019-10-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (689, 'fa-toggle-down', '2019-10-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (690, 'fa-toggle-left', '2019-10-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (691, 'fa-toggle-off', '2019-10-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (692, 'fa-toggle-on', '2019-10-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (693, 'fa-toggle-right', '2019-10-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (694, 'fa-toggle-up', '2019-10-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (695, 'fa-trademark', '2019-10-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (696, 'fa-train', '2019-10-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (697, 'fa-transgender', '2019-10-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (698, 'fa-transgender-alt', '2019-10-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (699, 'fa-trash', '2019-10-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (700, 'fa-trash-o', '2019-10-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (701, 'fa-tree', '2019-10-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (702, 'fa-trello', '2019-10-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (703, 'fa-tripadvisor', '2019-10-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (704, 'fa-trophy', '2019-10-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (705, 'fa-truck', '2019-10-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (706, 'fa-try', '2019-10-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (707, 'fa-tty', '2019-10-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (708, 'fa-tumblr', '2019-10-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (709, 'fa-tumblr-square', '2019-10-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (710, 'fa-turkish-lira', '2019-10-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (711, 'fa-tv', '2019-10-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (712, 'fa-twitch', '2019-10-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (713, 'fa-twitter', '2019-10-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (714, 'fa-twitter-square', '2019-10-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (715, 'fa-umbrella', '2019-10-31 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (716, 'fa-underline', '2019-11-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (717, 'fa-undo', '2019-11-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (718, 'fa-universal-access', '2019-11-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (719, 'fa-university', '2019-11-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (720, 'fa-unlink', '2019-11-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (721, 'fa-unlock', '2019-11-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (722, 'fa-unlock-alt', '2019-11-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (723, 'fa-unsorted', '2019-11-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (724, 'fa-upload', '2019-11-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (725, 'fa-usb', '2019-11-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (726, 'fa-usd', '2019-11-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (727, 'fa-user', '2019-11-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (728, 'fa-user-circle', '2019-11-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (729, 'fa-user-circle-o', '2019-11-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (730, 'fa-user-md', '2019-11-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (731, 'fa-user-o', '2019-11-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (732, 'fa-user-plus', '2019-11-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (733, 'fa-user-secret', '2019-11-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (734, 'fa-user-times', '2019-11-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (735, 'fa-users', '2019-11-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (736, 'fa-vcard', '2019-11-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (737, 'fa-vcard-o', '2019-11-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (738, 'fa-venus', '2019-11-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (739, 'fa-venus-double', '2019-11-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (740, 'fa-venus-mars', '2019-11-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (741, 'fa-viacoin', '2019-11-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (742, 'fa-viadeo', '2019-11-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (743, 'fa-viadeo-square', '2019-11-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (744, 'fa-video-camera', '2019-11-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (745, 'fa-vimeo', '2019-11-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (746, 'fa-vimeo-square', '2019-12-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (747, 'fa-vine', '2019-12-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (748, 'fa-vk', '2019-12-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (749, 'fa-volume-control-phone', '2019-12-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (750, 'fa-volume-down', '2019-12-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (751, 'fa-volume-off', '2019-12-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (752, 'fa-volume-up', '2019-12-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (753, 'fa-warning', '2019-12-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (754, 'fa-wechat', '2019-12-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (755, 'fa-weibo', '2019-12-10 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (756, 'fa-weixin', '2019-12-11 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (757, 'fa-whatsapp', '2019-12-12 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (758, 'fa-wheelchair', '2019-12-13 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (759, 'fa-wheelchair-alt', '2019-12-14 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (760, 'fa-wifi', '2019-12-15 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (761, 'fa-wikipedia-w', '2019-12-16 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (762, 'fa-window-close', '2019-12-17 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (763, 'fa-window-close-o', '2019-12-18 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (764, 'fa-window-maximize', '2019-12-19 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (765, 'fa-window-minimize', '2019-12-20 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (766, 'fa-window-restore', '2019-12-21 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (767, 'fa-windows', '2019-12-22 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (768, 'fa-won', '2019-12-23 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (769, 'fa-wordpress', '2019-12-24 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (770, 'fa-wpbeginner', '2019-12-25 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (771, 'fa-wpexplorer', '2019-12-26 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (772, 'fa-wpforms', '2019-12-27 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (773, 'fa-wrench', '2019-12-28 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (774, 'fa-xing', '2019-12-29 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (775, 'fa-xing-square', '2019-12-30 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (776, 'fa-y-combinator', '2019-12-31 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (777, 'fa-y-combinator-square', '2020-01-01 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (778, 'fa-yahoo', '2020-01-02 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (779, 'fa-yc', '2020-01-03 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (780, 'fa-yc-square', '2020-01-04 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (781, 'fa-yelp', '2020-01-05 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (782, 'fa-yen', '2020-01-06 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (783, 'fa-yoast', '2020-01-07 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (784, 'fa-youtube', '2020-01-08 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (785, 'fa-youtube-play', '2020-01-09 10:04:13', '2017-11-16 10:07:10');
INSERT INTO `icons` VALUES (786, 'fa-youtube-square', '2020-01-10 10:04:13', '2017-11-16 10:07:10');

-- ----------------------------
-- Table structure for layanan_opd
-- ----------------------------
DROP TABLE IF EXISTS `layanan_opd`;
CREATE TABLE `layanan_opd`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_opd` int(11) NULL DEFAULT NULL,
  `id_layanan` int(11) NOT NULL,
  `id_kuesioner` int(11) NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`, `id_layanan`) USING BTREE,
  INDEX `idx`(`id_layanan`, `id_opd`, `id_kuesioner`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of layanan_opd
-- ----------------------------
INSERT INTO `layanan_opd` VALUES (1, 42, 7, 1, 1, 65, '2018-10-30 02:58:37', '2018-10-30 02:58:12');
INSERT INTO `layanan_opd` VALUES (2, 42, 8, 1, 1, 65, '2018-10-30 02:58:40', '2018-10-30 02:58:13');
INSERT INTO `layanan_opd` VALUES (3, 42, 9, 1, 1, 65, '2018-10-30 02:58:42', '2018-10-30 02:58:14');
INSERT INTO `layanan_opd` VALUES (4, 42, 10, 1, 1, 65, '2018-10-30 02:58:44', '2018-10-30 02:58:15');
INSERT INTO `layanan_opd` VALUES (5, 42, 11, 1, 1, 65, '2018-10-30 02:58:46', '2018-10-30 02:58:16');
INSERT INTO `layanan_opd` VALUES (6, 42, 12, 1, 1, 65, '2018-10-30 02:58:49', '2018-10-30 02:58:17');
INSERT INTO `layanan_opd` VALUES (8, 42, 14, 1, 1, 65, '2018-10-30 02:58:53', '2018-10-30 02:58:18');
INSERT INTO `layanan_opd` VALUES (9, 12, 15, 1, 2, 82, NULL, '2018-11-07 09:59:36');
INSERT INTO `layanan_opd` VALUES (11, 16, 19, 1, 2, 82, NULL, '2018-11-07 10:01:12');
INSERT INTO `layanan_opd` VALUES (16, 14, 22, 1, 2, 82, NULL, '2018-11-07 10:05:12');
INSERT INTO `layanan_opd` VALUES (19, 18, 20, 1, 2, 82, NULL, '2018-11-07 13:23:46');
INSERT INTO `layanan_opd` VALUES (20, 18, 24, 1, 2, 82, NULL, '2018-11-07 13:23:46');
INSERT INTO `layanan_opd` VALUES (21, 38, 18, 1, 2, 82, NULL, '2018-11-07 13:24:02');
INSERT INTO `layanan_opd` VALUES (24, 17, 17, 1, 2, 82, NULL, '2018-11-07 13:25:16');
INSERT INTO `layanan_opd` VALUES (25, 30, 27, 1, 2, 82, NULL, '2018-11-07 13:26:01');
INSERT INTO `layanan_opd` VALUES (26, 30, 28, 1, 2, 82, NULL, '2018-11-07 13:26:01');
INSERT INTO `layanan_opd` VALUES (29, 59, 29, 1, 1, 65, NULL, '2018-12-07 14:00:27');
INSERT INTO `layanan_opd` VALUES (30, 59, 31, 1, 1, 65, NULL, '2018-12-07 14:00:27');
INSERT INTO `layanan_opd` VALUES (31, 61, 29, 1, 1, 65, NULL, '2018-12-07 14:00:57');
INSERT INTO `layanan_opd` VALUES (32, 61, 31, 1, 1, 65, NULL, '2018-12-07 14:00:57');

-- ----------------------------
-- Table structure for login_log
-- ----------------------------
DROP TABLE IF EXISTS `login_log`;
CREATE TABLE `login_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `login_time` datetime NOT NULL,
  `ip` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `browser` varchar(400) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 413 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of login_log
-- ----------------------------
INSERT INTO `login_log` VALUES (284, 65, '2018-05-28 06:44:35', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0');
INSERT INTO `login_log` VALUES (285, 65, '2018-05-28 06:45:50', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0');
INSERT INTO `login_log` VALUES (286, 65, '2018-05-29 06:18:43', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0');
INSERT INTO `login_log` VALUES (287, 65, '2018-06-02 15:23:49', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0');
INSERT INTO `login_log` VALUES (288, 65, '2018-06-02 15:24:55', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0');
INSERT INTO `login_log` VALUES (289, 65, '2018-06-27 22:42:44', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0');
INSERT INTO `login_log` VALUES (290, 65, '2018-06-28 18:26:07', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0');
INSERT INTO `login_log` VALUES (291, 65, '2018-06-30 03:48:57', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0');
INSERT INTO `login_log` VALUES (292, 65, '2018-06-30 14:33:54', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36');
INSERT INTO `login_log` VALUES (293, 65, '2018-08-15 03:37:40', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0');
INSERT INTO `login_log` VALUES (294, 65, '2018-08-29 07:24:03', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0');
INSERT INTO `login_log` VALUES (295, 65, '2018-08-29 14:04:44', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36');
INSERT INTO `login_log` VALUES (296, 65, '2018-09-26 13:38:49', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
INSERT INTO `login_log` VALUES (297, 65, '2018-10-03 04:08:02', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
INSERT INTO `login_log` VALUES (298, 65, '2018-10-03 05:29:26', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
INSERT INTO `login_log` VALUES (299, 65, '2018-10-03 05:29:46', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
INSERT INTO `login_log` VALUES (300, 65, '2018-10-04 04:10:54', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
INSERT INTO `login_log` VALUES (301, 65, '2018-10-04 04:39:55', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
INSERT INTO `login_log` VALUES (302, 65, '2018-10-04 05:01:01', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
INSERT INTO `login_log` VALUES (303, 65, '2018-10-07 15:58:09', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
INSERT INTO `login_log` VALUES (304, 82, '2018-10-07 16:42:39', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
INSERT INTO `login_log` VALUES (305, 65, '2018-10-08 06:25:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
INSERT INTO `login_log` VALUES (306, 65, '2018-10-08 06:34:27', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
INSERT INTO `login_log` VALUES (307, 65, '2018-10-11 04:32:11', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
INSERT INTO `login_log` VALUES (308, 65, '2018-10-25 23:42:18', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (309, 65, '2018-10-26 06:11:19', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (310, 65, '2018-10-28 16:28:56', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (311, 65, '2018-10-28 17:45:14', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (312, 65, '2018-10-29 05:14:05', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (313, 65, '2018-10-29 19:56:33', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (314, 65, '2018-10-30 13:09:33', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (315, 65, '2018-10-31 03:43:18', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (316, 65, '2018-10-31 04:08:27', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (317, 65, '2018-10-31 04:10:37', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (318, 65, '2018-10-31 06:18:39', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (319, 65, '2018-10-31 07:53:56', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (320, 65, '2018-10-31 18:15:22', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (321, 125, '2018-11-01 06:21:10', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (322, 65, '2018-11-01 06:21:34', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (323, 65, '2018-11-01 13:58:14', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (324, 65, '2018-11-01 22:20:09', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (325, 65, '2018-11-02 01:42:04', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (326, 65, '2018-11-02 02:09:17', '192.168.0.72', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (327, 82, '2018-11-02 02:09:53', '192.168.0.72', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (328, 82, '2018-11-02 02:12:52', '192.168.0.72', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36');
INSERT INTO `login_log` VALUES (329, 82, '2018-11-03 05:09:46', '222.124.162.170', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
INSERT INTO `login_log` VALUES (330, 82, '2018-11-05 06:14:49', '222.124.162.170', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
INSERT INTO `login_log` VALUES (331, 82, '2018-11-05 06:28:57', '222.124.162.170', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
INSERT INTO `login_log` VALUES (332, 82, '2018-11-05 07:48:48', '114.124.134.160', 'Mozilla/5.0 (Linux; Android 5.1.1; HUAWEI SCL-L21 Build/HuaweiSCL-L21) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.84 Mobile Safari/537.36');
INSERT INTO `login_log` VALUES (333, 65, '2018-11-06 03:15:22', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (334, 124, '2018-11-06 07:12:17', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (335, 82, '2018-11-06 07:17:19', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (336, 124, '2018-11-06 07:18:32', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (337, 124, '2018-11-06 12:17:56', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (338, 82, '2018-11-06 14:38:58', '182.1.66.131', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
INSERT INTO `login_log` VALUES (339, 82, '2018-11-06 14:44:26', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (340, 65, '2018-11-06 15:00:32', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36');
INSERT INTO `login_log` VALUES (341, 82, '2018-11-06 15:43:12', '182.1.65.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
INSERT INTO `login_log` VALUES (342, 82, '2018-11-06 23:07:27', '114.125.93.27', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
INSERT INTO `login_log` VALUES (343, 65, '2018-11-06 23:47:22', '120.188.92.80', 'Mozilla/5.0 (Linux; Android 8.1.0; Mi A1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.80 Mobile Safari/537.36');
INSERT INTO `login_log` VALUES (344, 82, '2018-11-07 00:27:49', '222.124.162.170', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
INSERT INTO `login_log` VALUES (345, 65, '2018-11-07 00:44:20', '120.188.32.132', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (346, 65, '2018-11-07 01:06:10', '114.4.214.22', 'Mozilla/5.0 (Linux; Android 8.1.0; Mi A1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.80 Mobile Safari/537.36');
INSERT INTO `login_log` VALUES (347, 65, '2018-11-07 01:24:42', '114.4.214.22', 'Mozilla/5.0 (Linux; Android 8.1.0; Mi A1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.80 Mobile Safari/537.36');
INSERT INTO `login_log` VALUES (348, 65, '2018-11-07 01:25:28', '114.4.214.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (349, 82, '2018-11-07 01:27:18', '114.4.214.22', 'Mozilla/5.0 (Linux; Android 8.1.0; Mi A1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.80 Mobile Safari/537.36');
INSERT INTO `login_log` VALUES (350, 82, '2018-11-07 01:39:43', '182.1.82.147', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36');
INSERT INTO `login_log` VALUES (351, 82, '2018-11-07 02:49:38', '182.1.83.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/600.1.25 (KHTML, like Gecko) Version/8.0 Safari/600.1.25');
INSERT INTO `login_log` VALUES (352, 82, '2018-11-07 02:52:42', '182.1.83.146', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
INSERT INTO `login_log` VALUES (353, 82, '2018-11-07 02:57:38', '182.1.65.191', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36');
INSERT INTO `login_log` VALUES (354, 82, '2018-11-07 06:15:45', '222.124.162.170', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36');
INSERT INTO `login_log` VALUES (355, 65, '2018-11-08 15:26:45', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (356, 65, '2018-11-09 06:22:08', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (357, 65, '2018-11-09 14:03:12', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (358, 65, '2018-11-11 00:38:07', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (359, 65, '2018-11-11 07:06:47', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (360, 65, '2018-11-14 07:18:24', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (361, 65, '2018-11-15 04:03:49', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (362, 65, '2018-11-25 14:37:30', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (363, 65, '2018-11-26 06:19:27', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (364, 65, '2018-12-03 03:54:22', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (365, 65, '2018-12-03 06:17:40', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (366, 65, '2018-12-03 21:46:07', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (367, 65, '2018-12-04 04:28:06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (368, 65, '2018-12-05 14:37:27', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (369, 65, '2018-12-06 02:22:50', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (370, 65, '2018-12-06 02:27:49', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (371, 65, '2018-12-06 06:40:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0');
INSERT INTO `login_log` VALUES (372, 65, '2018-12-07 03:22:20', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (373, 65, '2018-12-07 06:31:26', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (374, 65, '2018-12-10 04:19:07', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (375, 65, '2018-12-10 07:56:01', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (376, 65, '2018-12-11 07:17:19', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (377, 65, '2018-12-12 07:51:22', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (378, 65, '2018-12-17 03:33:12', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (379, 65, '2018-12-18 01:57:13', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (380, 65, '2018-12-18 04:18:53', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (381, 65, '2018-12-18 04:20:06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (382, 65, '2018-12-19 04:02:30', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (383, 65, '2018-12-19 06:53:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (384, 65, '2018-12-20 06:30:45', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (385, 65, '2018-12-21 05:54:25', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (386, 82, '2018-12-21 06:32:16', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (387, 65, '2018-12-21 08:53:26', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (388, 65, '2018-12-24 13:22:46', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (389, 65, '2018-12-24 16:01:51', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (390, 65, '2018-12-25 12:39:51', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (391, 65, '2018-12-25 13:16:46', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (392, 65, '2018-12-25 13:19:49', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (393, 65, '2018-12-26 00:17:19', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (394, 65, '2018-12-26 04:05:29', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (395, 65, '2018-12-26 04:29:13', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (396, 65, '2018-12-26 04:49:55', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (397, 65, '2018-12-26 05:23:42', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (398, 65, '2018-12-27 04:03:23', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (399, 65, '2018-12-28 04:14:27', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (400, 65, '2018-12-30 07:16:27', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36');
INSERT INTO `login_log` VALUES (401, 65, '2018-12-30 13:06:14', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (402, 65, '2019-01-01 16:21:14', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (403, 98, '2019-01-01 20:06:07', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36');
INSERT INTO `login_log` VALUES (404, 98, '2019-01-02 00:54:30', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36');
INSERT INTO `login_log` VALUES (405, 65, '2019-01-02 00:55:55', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (406, 82, '2019-01-02 00:56:23', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (407, 65, '2019-01-02 08:28:07', '114.4.82.12', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (408, 82, '2019-01-04 01:07:13', '222.124.162.170', 'Mozilla/5.0 (Linux; U;Android 4.4.2; LG-D690 Build/KOT49I.A1420806068) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/34.0.1847.118 Mobile Safari/537.36');
INSERT INTO `login_log` VALUES (409, 65, '2019-01-08 06:41:21', '192.168.0.249', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36');
INSERT INTO `login_log` VALUES (410, 65, '2019-01-09 04:41:22', '192.168.0.249', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (411, 156, '2019-01-09 04:42:55', '192.168.0.249', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0');
INSERT INTO `login_log` VALUES (412, 156, '2019-01-09 04:44:40', '192.168.0.249', 'Mozilla/5.0 (Linux; Android 7.0; Redmi Note 4 Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `migration` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('2014_02_25_015844_create_claravel_database', 1);
INSERT INTO `migrations` VALUES ('2014_02_26_022537_visi', 2);
INSERT INTO `migrations` VALUES ('2014_02_26_022632_misi', 3);
INSERT INTO `migrations` VALUES ('2014_02_26_040858_kota', 4);
INSERT INTO `migrations` VALUES ('2014_02_26_041510_skpd', 5);
INSERT INTO `migrations` VALUES ('2014_02_26_042247_kelurahan', 6);
INSERT INTO `migrations` VALUES ('2014_02_26_042538_rw', 7);
INSERT INTO `migrations` VALUES ('2014_02_26_042736_rt', 8);
INSERT INTO `migrations` VALUES ('2014_03_06_122915_manajemenurusan', 9);
INSERT INTO `migrations` VALUES ('2014_03_06_131236_manajemenprogram', 10);
INSERT INTO `migrations` VALUES ('2014_03_06_132930_urusan', 11);
INSERT INTO `migrations` VALUES ('2014_03_06_151708_kegiatan', 12);
INSERT INTO `migrations` VALUES ('2014_03_07_094838_kewenanganskpd', 13);
INSERT INTO `migrations` VALUES ('2014_03_08_101557_jenispelatihan', 14);
INSERT INTO `migrations` VALUES ('2014_03_08_103119_kategoribelanja', 15);
INSERT INTO `migrations` VALUES ('2014_03_08_103933_jenisbelanja', 16);
INSERT INTO `migrations` VALUES ('2014_03_08_132350_jenisssh', 17);
INSERT INTO `migrations` VALUES ('2014_03_08_132517_subjenisssh', 18);
INSERT INTO `migrations` VALUES ('2014_03_08_132813_daftarhargassh', 19);
INSERT INTO `migrations` VALUES ('2014_03_08_143606_periodetahun', 20);
INSERT INTO `migrations` VALUES ('2014_03_14_161036_settingbatasmaxanggaran', 21);
INSERT INTO `migrations` VALUES ('2014_03_17_083848_jenisbantuan', 22);
INSERT INTO `migrations` VALUES ('2014_03_17_085823_usulanbantuansarprasdanhibahkelompokmasyarakat', 23);
INSERT INTO `migrations` VALUES ('2014_03_17_103335_jenisdana', 24);
INSERT INTO `migrations` VALUES ('2014_03_18_160800_jadwalmusrenbang', 25);
INSERT INTO `migrations` VALUES ('2014_03_19_081512_jenismusrenkecamatan', 26);
INSERT INTO `migrations` VALUES ('2014_03_19_083424_prioritasmusren', 27);
INSERT INTO `migrations` VALUES ('2014_03_19_083717_prioritasmusren', 28);
INSERT INTO `migrations` VALUES ('2014_03_19_085812_hasilmusrenkecamatan', 29);
INSERT INTO `migrations` VALUES ('2014_03_19_130356_visiskpd', 30);
INSERT INTO `migrations` VALUES ('2014_03_19_135750_misiskpd', 31);
INSERT INTO `migrations` VALUES ('2014_03_19_140029_misiskpd', 32);
INSERT INTO `migrations` VALUES ('2014_03_20_104910_masterindikatorcapaian', 33);
INSERT INTO `migrations` VALUES ('2014_03_20_105630_masterindikatorcapaian', 34);
INSERT INTO `migrations` VALUES ('2014_03_20_112454_coba', 35);
INSERT INTO `migrations` VALUES ('2014_03_20_112757_cobalagi', 36);
INSERT INTO `migrations` VALUES ('2014_03_20_114930_verifikasiprogram', 37);
INSERT INTO `migrations` VALUES ('2014_03_20_115122_masterindikatorkegiatan', 38);
INSERT INTO `migrations` VALUES ('2014_03_20_115553_masterindikatorkegiatan', 39);
INSERT INTO `migrations` VALUES ('2014_03_20_120417_verifikasiprogram', 40);
INSERT INTO `migrations` VALUES ('2014_03_20_122910_verifikasiprogram', 41);
INSERT INTO `migrations` VALUES ('2014_03_20_125428_penyesuaiandanperubahanprogram', 42);
INSERT INTO `migrations` VALUES ('2014_03_20_130441_listprogram', 43);
INSERT INTO `migrations` VALUES ('2014_03_20_130632_kesimpulanhasilverifikasiolehpetugas', 44);
INSERT INTO `migrations` VALUES ('2014_03_20_130826_cetakprogram', 45);
INSERT INTO `migrations` VALUES ('2014_03_20_144509_kuappa', 46);
INSERT INTO `migrations` VALUES ('2014_03_21_110604_matrikrenjaskpd', 47);
INSERT INTO `migrations` VALUES ('2014_03_21_110848_rekapitulasibelanjadanplafonrenjaskpd', 48);
INSERT INTO `migrations` VALUES ('2014_03_21_114339_indikator', 49);
INSERT INTO `migrations` VALUES ('2014_03_21_114519_indicator', 50);
INSERT INTO `migrations` VALUES ('2014_03_21_202940_jeniskewirausahaan', 51);
INSERT INTO `migrations` VALUES ('2014_03_22_120154_usulanbantuanwargamiskin', 52);
INSERT INTO `migrations` VALUES ('2014_03_23_004908_datahasilmusrenbangkelurahan', 53);
INSERT INTO `migrations` VALUES ('2014_03_24_125357_indikatorcapaianprogram', 54);
INSERT INTO `migrations` VALUES ('2014_03_25_140750_indikatorcapaiankegiatan', 55);
INSERT INTO `migrations` VALUES ('2014_03_25_164128_penentuanprioritasrenjaawal', 56);
INSERT INTO `migrations` VALUES ('2014_03_28_101213_validasimusrenbangkelurahan', 57);
INSERT INTO `migrations` VALUES ('2014_03_28_143315_penentuandsphasilmusrenbang', 58);
INSERT INTO `migrations` VALUES ('2014_03_30_002828_usulankecamatan', 59);
INSERT INTO `migrations` VALUES ('2014_04_08_150823_tespage', 60);
INSERT INTO `migrations` VALUES ('2014_04_08_173322_kelurahanpageview', 61);
INSERT INTO `migrations` VALUES ('2014_04_10_084950_settingpagunilaiusulan', 62);
INSERT INTO `migrations` VALUES ('2014_04_14_125550_sinkronisasiforumskpd', 63);
INSERT INTO `migrations` VALUES ('2014_04_14_143108_prarkamurni', 64);
INSERT INTO `migrations` VALUES ('2014_04_17_165947_scoringdanpenilaianusulan', 65);
INSERT INTO `migrations` VALUES ('2014_04_23_085503_downloadrpjmd', 66);
INSERT INTO `migrations` VALUES ('2014_04_24_041308_aturusernamedanpassword', 67);

-- ----------------------------
-- Table structure for modules
-- ----------------------------
DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `module_path` int(11) NOT NULL,
  `id_context` int(11) NOT NULL,
  `id_parent` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(350) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `uses` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `flag` int(11) NOT NULL,
  `is_nav_bar` int(11) NOT NULL,
  `icons` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `table_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of modules
-- ----------------------------
INSERT INTO `modules` VALUES (1, 0, 2, 0, 'Context', 'Contexts Moduls', '/developer/context', 'ContextController', 1, 1, 'fa fa-align-left', 14, 'contexts', '2018-12-17 10:40:20', '2016-06-21 02:24:56');
INSERT INTO `modules` VALUES (2, 0, 2, 0, 'Modules', 'Modules', '/developer/modules', 'ModulesController', 1, 1, 'fa fa-codepen', 15, 'modules', '2018-12-17 10:40:20', '2016-06-21 02:25:24');
INSERT INTO `modules` VALUES (3, 0, 3, 0, 'Permissions', '', '/settings/permissions', 'PermissionsController', 1, 1, 'fa fa-check-square-o', 17, 'permissions', '2018-12-17 10:40:20', '2014-02-22 00:00:00');
INSERT INTO `modules` VALUES (4, 0, 3, 0, 'Permissions Matrix', '', '/settings/permissionsmatrix', 'PermissionsmatrixController', 1, 1, 'fa fa-check-square-o', 18, 'role_permission', '2018-12-17 10:40:20', '2014-02-22 00:00:00');
INSERT INTO `modules` VALUES (5, 0, 3, 0, 'Roles', '', '/settings/roles', 'RolesController', 1, 1, 'fa fa-child', 19, 'roles', '2018-12-17 10:40:20', '2014-02-24 00:00:00');
INSERT INTO `modules` VALUES (6, 0, 3, 0, 'Configurations', '', '/settings/configurations', 'ConfigurationsController', 1, 1, 'fa fa-wrench', 16, 'configurations', '2018-12-17 10:40:20', '2017-11-16 03:45:49');
INSERT INTO `modules` VALUES (7, 0, 3, 0, 'Users', '', '/settings/users', 'UsersController', 1, 1, 'fa fa-users', 20, 'users', '2018-12-17 10:40:20', '2014-02-24 00:00:00');
INSERT INTO `modules` VALUES (9, 1, 4, 0, 'Jenjang Pendidikan', '', '/masterdata/jenjangpendidikan', 'JenjangpendidikanController', 1, 1, 'fa fa-circle-o', 7, 'mst_jenjang_pendidikan', '2018-12-17 10:35:13', '2018-10-07 16:14:40');
INSERT INTO `modules` VALUES (10, 1, 4, 0, 'Pekerjaan', '', '/masterdata/pekerjaan', 'PekerjaanController', 1, 1, 'fa fa-circle-o', 6, 'mst_pekerjaan', '2018-12-17 10:35:08', '2018-10-07 16:15:10');
INSERT INTO `modules` VALUES (16, 1, 4, 0, 'OPD', '', '/masterdata/opd', 'OpdController', 1, 1, 'fa fa-circle-o', 4, 'mst_opd', '2018-12-17 10:35:08', '2018-10-25 23:43:11');
INSERT INTO `modules` VALUES (17, 1, 4, 0, 'Layanan', '', '/masterdata/layanan', 'LayananController', 1, 1, 'fa fa-circle-o', 2, 'mst_layanan', '2018-12-17 10:35:08', '2018-10-25 23:45:22');
INSERT INTO `modules` VALUES (19, 1, 6, 0, 'Layanan OPD', '', '/pengaturan/layananopd', 'LayananopdController', 1, 1, 'fa fa-bolt', 12, 'layanan_opd', '2018-12-17 10:35:08', '2018-10-26 06:48:13');
INSERT INTO `modules` VALUES (21, 1, 4, 0, 'Unsur SKM', '', '/masterdata/unsurskm', 'UnsurskmController', 1, 1, 'fa fa-circle-o', 5, 'mst_unsur_skm', '2018-12-17 10:35:08', '2018-11-08 16:40:13');
INSERT INTO `modules` VALUES (22, 1, 4, 0, 'Rentang IKM', '', '/masterdata/rentangikm', 'RentangikmController', 1, 1, 'fa fa-circle-o', 3, 'mst_rentang_ikm', '2018-12-17 10:35:08', '2018-11-08 16:46:07');
INSERT INTO `modules` VALUES (23, 1, 6, 0, 'Pengguna', '', '/pengaturan/pengguna', 'PenggunaController', 1, 1, 'fa fa-user', 11, 'users', '2018-12-17 10:35:08', '2018-11-08 17:38:39');
INSERT INTO `modules` VALUES (24, 1, 7, 0, 'Beranda', '', '/portal/beranda', 'BerandaController', 1, 1, 'fa fa-home', 10, '', '2018-12-17 10:35:08', '2018-11-25 14:43:02');
INSERT INTO `modules` VALUES (25, 1, 7, 0, 'Berita', '', '/portal/berita', 'BeritaController', 1, 1, 'fa fa-newspaper-o', 9, 'mst_berita', '2018-12-17 10:35:08', '2018-11-25 14:47:25');
INSERT INTO `modules` VALUES (26, 1, 4, 0, 'Kuesioner', '', '/masterdata/kuesioner', 'KuesionerController', 1, 1, 'fa fa-circle-o', 8, 'mst_kuesioner', '2018-12-17 10:35:13', '2018-12-17 03:34:54');
INSERT INTO `modules` VALUES (27, 1, 6, 0, 'Kuesioner Layanan', '', '/pengaturan/kuesionerlayanan', 'KuesionerlayananController', 1, 1, 'fa fa-circle-o', 13, 'mst_layanan', '2018-12-17 10:40:20', '2018-12-17 03:40:14');
INSERT INTO `modules` VALUES (30, 1, 5, 0, 'Respon Masuk', '', '/responmasyarakat/responmasuk', 'ResponmasukController', 1, 1, 'fa fa-circle-o', 1, 'tr_respon', '2018-12-27 04:10:01', '2018-12-27 04:10:01');
INSERT INTO `modules` VALUES (31, 1, 5, 0, 'Nilai IKM', '', '/responmasyarakat/nilaiikm', 'NilaiikmController', 1, 1, 'fa fa-circle-o', 1, 'mst_opd', '2018-12-27 04:37:25', '2018-12-27 04:37:25');
INSERT INTO `modules` VALUES (32, 1, 5, 0, 'Nilai per Unsur', '', '/responmasyarakat/nilaiperunsur', 'NilaiperunsurController', 1, 1, 'fa fa-circle-o', 1, 'tr_respon', '2019-01-01 18:40:45', '2019-01-01 18:40:45');

-- ----------------------------
-- Table structure for mst_berita
-- ----------------------------
DROP TABLE IF EXISTS `mst_berita`;
CREATE TABLE `mst_berita`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul_berita` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `url_berita` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `isi_berita` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `thumbnail` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of mst_berita
-- ----------------------------
INSERT INTO `mst_berita` VALUES (2, 'Desa Berinovasi, Jangan Takut !', 'Desa-Berinovasi-Jangan-Takut-', '<p>Kudus (8/11) - Menciptakan suatu produk atau karya memang tidak mudah. Diperlukan suatu inovasi agar produk tersebut memiliki ciri khas dan menjadi lebih baru. Tak terkecuali, produk atau karya dari inovasi desa di kabupaten Kudus yang dipamerkan pada Bursa Inovasi Desa di Taman Budaya Bae, pagi tadi.</p>\n<p>Bupati Kudus, H.M. Tamzil mengapresiasi terselenggaranya acara tersebut dan mendorong supaya pemerintah desa lebih dapat mengoptimalkan dana desa, \"Saya apresiasi kegiatan ini. Untuk para kepala desa, manfaatkan dana desa ini tidak hanya untuk membangun secara fisik saja tetapi kegiatan inovasi desa juga didukung,\" ungkapnya. Bagi Tamzil, inovasi desa yang dihasilkan oleh masyarakat akan membantu perekonomian mereka.</p>\n<p>Selain itu, pihaknya mengakui bahwa masih ada kendala dalam memunculkan potensi desa utamanya mengenai sumber daya manusia, \"SDM di desa harus kita bimbing, supaya kendala-kendala yang dialami dapat segera dicarikan solusi, kampus siap masuk ke desa,\" imbuhnya. Meski begitu, Tamzil tetap optimis bahwa masyarakat desa mampu untuk berkreasi dan berinovasi lebih baik lagi, \"Jangan takut berinovasi, kami dorong untuk para kepala desa terus mencari potensi desanya, digali, dirembug, dan diwujudkan secara nyata,\" ucapnya.</p>\n<p>Sebagai tambahan, Bupati Tamzil juga memiliki tiga menu pokok untuk dapat diaplikasikan di desa agar desa tersebut menjadi lebih sejahter dan maju, diantaranya posyandu, sedekah sampah dan kampung IT.</p>\n<p>Sementara itu, Kepala Dinas Pemberdayaan Masyarakat Desa, Adhy Sadhono berujar bahwa pemanfaatan dana desa harus lebih berkualitas, utamanya dalam rangka meningkatkan ekonomi masyarakat desa, \"Kalau bisa kegiatan inovasi desa masuk Musrenbangdes dan APBDes, sehingga dapat membantu ekonomi rakyat,\" ujarnya.</p>\n<p>Hal senada juga diungkapkan oleh Edi Pranoto yang mewakili Dispermadesdukcapil Jawa Tengah, Edi Pranoto yang menggaris bawahi pentingnya dana desa dioptimalkan untuk membantu ekonomi rakyat, \"Harusnya kebutuhan mendasar masyarakat juga dipenuhi, selain membangun infrastruktur, sehingga ekonomi rakyat juga tambah bagus,\' ujarnya.</p>', 'Desa-Berinovasi-Jangan-Takut--3J54g.jpg', 65, 1, '2018-12-27 05:03:01', '2018-12-30 07:39:28', '2018-12-30 07:39:28');
INSERT INTO `mst_berita` VALUES (6, 'Wabup Dorong RSUD dr.Loekmono Hadi untuk Terus Tingkatkan Pelayanan', 'Wabup-Dorong-RSUD-drLoekmono-Hadi-untuk-Terus-Tingkatkan-Pelayanan', '<p>KUDUS Wakil Bupati H.M. Hartopo berpesan kepada pihak RSUD dr.Loekmono Hadi untuk selalu meningkatkan pelayanan dan fasilitas sesuai standar nasional. Disampaikannya dalam acara Survey Verifikasi ke-2 Akreditasi Rumah Sakit RSUD dr. Loekmono Hadi pada Kamis (8/11). Acara tersebut juga dihadiri, Direktur RSUD, Dr. Abdul Aziz Achyar, M.Kes dan Surveyor, Enny Hermawati, SKM.</p>\r\n<p>Hartopo menyampaikan apresiasi kepada tim surveyor yang telah melaksanakan survey verifikasi ke-2. Dia berharap hasil survey dapat menjadi acuan dalam peningkatan kualitas pelayanan RSUD. \"Atas nama Pemkab dan pribadi menyampaikan terima kasih kepada yang telah melakukan survey, mudah-mudahan apa yang disampaikan menjadi perbaikan bagi RS,\" ujarnya.</p>\r\n<p>Dirinya juga menyampaikan langsung kepada pihak RSUD untuk meningkatkan semua pelayanan mulai dari kelas eksekutif sampai kelas 3. Mengenai fasilitas harus sesuai dengan apa yang telah di verifikasi. \"Peningkatan juga harus dilakukan pada pelayanan, alat dan fasilitas standar nasional, jangan hanya gedung saja,\" pungkasnya.</p>\r\n<p>Enny menyampaikan 3 hal yang perlu dipertahankan yaitu, semangat perubahan yang kontinu, semangat terakreditasi paripurna dan dukungan pemilik, direksi, stakeholder. Adapun hal yang perlu diperhatikan yaitu pemahaman, pelaksanaan, kepatuhan dalam tugas sehari-hari sesuai standar akreditasi. \"Penuhi fasilitas dan prasarana sesuai dengan standar akreditasi,\" ujarnya. Dia menegaskan untuk melakukan semua tugas dan kegiatan harus dilakukan sesuai standar setiap hari, jangan hanya ketika akan diverifikasi.</p>\r\n<p>Abdul Aziz turut menyampaikan bahwa akreditasi merupakan kebutuhan oleh karena itu semua yang ada di RSUD tersebut harus sesuai standar. Jika hal tersebut dapat terlaksana dengan baik maka dapat memberikan kepuasan kepada masyarakat. \"Semua yang ada di RS ini harus berstandar sehingga masyarakat merasa nyaman dan teman-teman pelaksana merasa nyaman pula,\" sampainya.</p>', 'Wabup-Dorong-RSUD-drLoekmono-Hadi-untuk-Terus-Tingkatkan-Pelayanan-ZgTxu.jpg', 65, 1, '2018-12-27 05:07:43', '2018-12-30 07:39:32', '2018-12-30 07:39:32');
INSERT INTO `mst_berita` VALUES (7, 'Asisten Sekda Tutup Pelatihan Teknis Perencanaan Pembangunan', 'Asisten-Sekda-Tutup-Pelatihan-Teknis-Perencanaan-Pembangunan', '<p>KUDUS- Bertempat di gedung Setda lantai 4, Asisten Ekonomi, Pembangunan, dan Kesejahteraan Rakyat Sekda Kudus, Ali Rifai, SH., MH, menutup pelatihan teknis perencanaan pembangunan daerah pada Kamis (8/11). Penutupan tersebut juga dihadiri Kepala BKPP Kudus Yuli Tri Nugroho, perwakilan OPD dan peserta diklat.</p>\r\n<p>Yuli melaporkan bahwa pelatihan yang berlangsung dari 15 Oktober - 8 November telah berjalan lancar berkat pertisipasi aktif seluruh pihak terkait. 40 PNS dari perangkat daerah telah mengikuti 130 jam pelajaran oleh Bappelitbangda Kudus di Balai Diklat, Menawan, Gebog. \"40 PNS telah lulus, kriteria sangat memuaskan untuk 15 PNS, memuaskan untuk 22 PNS dan cukup memuaskan untuk 3 PNS,\" ungkapnya.</p>\r\n<p>Menyampaikan sambutan Bupati, Ali menyampaikan untuk mengaplikasikan ilmu yang diperoleh dari pelatihan dalam perancangan renstra dari masing-masing OPD. Sehingga ikut mendukung peningkatan pembangunan untuk terwujudnya Kudus yang religius, modern, cerdas dan sejahtera. \"Peserta diklat harus dapat berperan dalam peningkatan pembangunan daerah, khususnya terhadap penyusunan renstra pada masing-masing OPD,\" ujarnya.</p>\r\n<p>Ali berpesan kepada peserta diklat untuk terus mengembangkan potensi yang diperoleh dari pelatihan tersebut. Pola pikir peserta diklat juga harus lebih inovatif dan mampu menularkan ilmu yang di dapat di lingkungan kerja masing-masing. \"Sudah di diklat, sudah punya kemampuan lebih sebagai bekal di unit kerja masing-masing,\" pungkasnya.</p>', 'Asisten-Sekda-Tutup-Pelatihan-Teknis-Perencanaan-Pembangunan-bj1zA.jpg', 65, 1, '2018-12-27 05:08:30', '2018-12-30 07:39:36', '2018-12-30 07:39:36');
INSERT INTO `mst_berita` VALUES (8, 'Lorem Ipsum - What is Lorem Ipsum?', 'Lorem-Ipsum-What-is-Lorem-Ipsum', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<p><strong>Why do we use it?</strong><br />It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', 'Lorem-Ipsum-ngu3P.jpg', 65, 1, '2018-12-28 04:16:49', '2018-12-28 04:20:47', NULL);
INSERT INTO `mst_berita` VALUES (9, 'Lorem Ipsum -  Where does it come from?', 'Lorem-Ipsum-Where-does-it-come-from', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.<br /><br />The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 'Lorem-Ipsum-Where-does-it-come-from-eLr3d.jpg', 65, 1, '2018-12-28 04:18:55', '2018-12-28 04:19:56', NULL);
INSERT INTO `mst_berita` VALUES (10, 'Lorem Ipsum - Where can I get some?', 'Lorem-Ipsum-Where-can-I-get-some', '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', 'Lorem-Ipsum-Where-can-I-get-some-bPeZe.jpg', 65, 1, '2018-12-28 04:19:28', '2018-12-28 04:20:05', NULL);

-- ----------------------------
-- Table structure for mst_jenjang_pendidikan
-- ----------------------------
DROP TABLE IF EXISTS `mst_jenjang_pendidikan`;
CREATE TABLE `mst_jenjang_pendidikan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenjang` char(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of mst_jenjang_pendidikan
-- ----------------------------
INSERT INTO `mst_jenjang_pendidikan` VALUES (1, 'SD', 65, 1, '2018-10-04 12:50:25', '2018-10-04 12:50:27', NULL);
INSERT INTO `mst_jenjang_pendidikan` VALUES (2, 'SMP', 65, 1, '2018-10-04 12:50:28', '2018-10-04 12:50:30', NULL);
INSERT INTO `mst_jenjang_pendidikan` VALUES (3, 'SMA', 65, 1, '2018-10-04 12:50:31', '2018-10-04 12:50:32', NULL);
INSERT INTO `mst_jenjang_pendidikan` VALUES (4, 'S1', 65, 1, '2018-10-04 12:50:33', '2018-10-04 12:50:34', NULL);
INSERT INTO `mst_jenjang_pendidikan` VALUES (5, 'S2', 65, 1, '2018-10-04 12:50:35', '2018-10-04 12:50:37', NULL);
INSERT INTO `mst_jenjang_pendidikan` VALUES (6, 'S3', 65, 1, '2018-10-04 12:50:37', '2018-10-04 12:50:40', NULL);
INSERT INTO `mst_jenjang_pendidikan` VALUES (7, 'fsdf', 65, 1, '2018-12-11 07:53:06', '2018-12-11 07:53:09', '2018-12-11 07:53:09');

-- ----------------------------
-- Table structure for mst_konten
-- ----------------------------
DROP TABLE IF EXISTS `mst_konten`;
CREATE TABLE `mst_konten`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `isi` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of mst_konten
-- ----------------------------
INSERT INTO `mst_konten` VALUES (1, 'beranda', '<h4><strong>Tentang SKM</strong></h4>\n<p>Berdasarkan Permenpan No 14 Tahun 2017 Survei Kepuasan Masyarakat adalah pengukuran secara komprehensif kegiatan tentang tingkat kepuasan masyarakat yang diperoleh dari hasil pengukuran atas pendapat masyarakat. Melalui survei ini diharapkan mendorong partisipasi masyarakat sebagai pengguna layanan dalam menilai kinerja penyelenggara pelayanan serta mendorong penyelenggara pelayanan publik untuk meningkatkan kualitas pelayanan dan melakukan pengembangan melalui inovasi-inovasi pelayanan publik.</p>\n<p>&nbsp;</p>\n<h4><strong>Manfaat SKM</strong></h4>\n<p>Maksud dan Tujuan penyusunan SKM untuk mendapatkan feedback secara berkala atas kinerja/kualitas pelayanan yang diberikan pemerintah / pelayanan publik kepada masyarakat sebagai bahan untuk menetapkan kebijakan dalam rangka peningkatan kualitas pelayanan publik yang selanjutnya dilakukan secara berkesinambungan.<br />Pelaksanaan Survei Kepuasan Masyarakat terhadap penyelenggaraan pelayanan publik dapat dilaksanakan melalui tahapan perencanaan, persiapan, pelaksanaan, pengolahan dan penyajian hasil survei.</p>', 1, 65, NULL, '2018-12-27 12:18:10');

-- ----------------------------
-- Table structure for mst_kuesioner
-- ----------------------------
DROP TABLE IF EXISTS `mst_kuesioner`;
CREATE TABLE `mst_kuesioner`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kuesioner` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of mst_kuesioner
-- ----------------------------
INSERT INTO `mst_kuesioner` VALUES (1, 'Kuesioner A', 'Kuesioner Default', 1, 65, '2018-10-30 03:16:06', '2018-12-18 06:21:08', NULL);
INSERT INTO `mst_kuesioner` VALUES (2, 'aaaa', 'bbbb', 1, 65, '2018-12-19 04:20:06', '2018-12-19 04:24:50', '2018-12-19 04:24:50');

-- ----------------------------
-- Table structure for mst_layanan
-- ----------------------------
DROP TABLE IF EXISTS `mst_layanan`;
CREATE TABLE `mst_layanan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_layanan` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_kuesioner` int(11) NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of mst_layanan
-- ----------------------------
INSERT INTO `mst_layanan` VALUES (15, 'Pelayanan Perizinan Kesehatan', 1, 2, 82, '2018-11-07 02:38:55', '2018-12-19 15:35:31', NULL);
INSERT INTO `mst_layanan` VALUES (17, 'Pelayanan Perizinan Terpadu', 1, 2, 82, '2018-11-07 02:41:33', '2018-12-19 15:35:32', NULL);
INSERT INTO `mst_layanan` VALUES (18, 'Pelayanan PATEN', 1, 2, 82, '2018-11-07 02:46:21', '2018-12-19 15:35:42', NULL);
INSERT INTO `mst_layanan` VALUES (19, 'Pelayanan Administrasi Kependudukan', 1, 2, 82, '2018-11-07 02:46:53', '2018-12-17 10:38:03', NULL);
INSERT INTO `mst_layanan` VALUES (20, 'Pelayanan Pendaftaran Pelatihan', 1, 2, 82, '2018-11-07 02:48:10', '2018-12-17 10:38:03', NULL);
INSERT INTO `mst_layanan` VALUES (22, 'Pelayanan Dinas Sosial P3AP2KB', 1, 2, 82, '2018-11-07 03:05:00', '2018-12-17 10:38:03', NULL);
INSERT INTO `mst_layanan` VALUES (24, 'Pelayanan Pembuatan Kartu Kuning/AK.1', 1, 2, 82, '2018-11-07 03:23:33', '2018-12-17 10:38:05', NULL);
INSERT INTO `mst_layanan` VALUES (29, 'Pelayanan Rawat Jalan Puskesmas', 1, 1, 65, '2018-12-07 06:58:20', '2018-12-17 10:38:04', NULL);
INSERT INTO `mst_layanan` VALUES (30, 'Pelayanan Rawat Jalan RS', 1, 1, 65, '2018-12-07 06:58:34', '2018-12-17 10:38:06', NULL);
INSERT INTO `mst_layanan` VALUES (31, 'Pelayanan Rawat Inap Puskesmas', 1, 1, 65, '2018-12-07 06:59:51', '2018-12-17 10:38:04', NULL);
INSERT INTO `mst_layanan` VALUES (32, 'Pelayanan Rawat Inap RS', 1, 1, 65, '2018-12-07 06:59:56', '2018-12-17 10:38:06', NULL);

-- ----------------------------
-- Table structure for mst_opd
-- ----------------------------
DROP TABLE IF EXISTS `mst_opd`;
CREATE TABLE `mst_opd`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_opd` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pwd_default` char(6) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 71 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of mst_opd
-- ----------------------------
INSERT INTO `mst_opd` VALUES (1, 'Bagian Tata Pemerintahan', 'Z6MJVJ', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (2, 'Bagian Hukum', 'HX5LGG', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (3, 'Bagian Perekonomian dan Administrasi Pembangunan', 'ZYKCMT', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (4, 'Bagian Pengadaan Barang/Jasa', 'FMIWZY', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (5, 'Bagian Kesra', 'G3XERR', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (6, 'Bagian Organisasi', 'RLP2BT', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (7, 'Bagian Umum', 'KUMWFL', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (8, 'Bagian Perlengkapan dan Keuangan', 'I9TXWG', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (9, 'Sekretariat DPRD', 'AW0WNP', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (10, 'Inspektorat Daerah', 'N42DWN', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (11, 'Dinas Pendidikan, Kepemudaan dan Olahraga', 'LHL583', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (12, 'Dinas Kesehatan', 'YVUQ2Q', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (13, 'Dinas Kebudayaan dan Pariwisata', 'ZMUVWX', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (14, 'Dinas Sosial, Pemberdayaan Perempuan, Perlindungan Anak, Pengendalian Penduduk dan KB (Dinsos P3AP2KB)', 'FUSFME', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (15, 'Dinas Pemberdayaan Masyarakat Desa (PMD)', 'T09N97', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (16, 'Dinas Kependudukan dan Pencatatan Sipil', 'EZB0HI', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (17, 'Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu ( DPMPTSP )', '9XJBM1', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (18, 'Dinas Tenaga Kerja, Perindustrian, Koperasi dan UKM', 'HZYJ8R', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (19, 'Dinas Perdagangan', 'YCR8UO', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (20, 'Dinas Komunikasi dan Informatika', 'JPETYB', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (21, 'Dinas Perhubungan', 'D578EF', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (22, 'Dinas Pekerjaan Umum dan Penataan Ruang (PUPR)', 'XCDODD', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (23, 'Dinas Perumahan, Kawasan Permukiman dan Lingkungan Hidup (PKPLH)', 'W6EEEN', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (24, 'Dinas Pertanian dan Pangan', 'X02S1N', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (25, 'Dinas Kearsipan dan Perpustakaan', 'XSTSEG', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (26, 'Satuan Polisi Pamong Praja (Satpol PP)', 'MXKGXD', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (27, 'Badan Perencanaan Pembangunan, Penelitian dan Pengembangan Daerah (BAPPELITBANGDA)', '5UYEMJ', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (28, 'Badan Kepegawaian, Pendidikan dan Pelatihan (BKPP)', 'SNZTPO', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (29, 'BPPKAD', 'HXQZXB', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (30, 'RSUD dr Loekmono Hadi', 'KTFF4Q', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (31, 'Kantor Kesbangpol', 'JRN7SL', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (32, 'BPBD', 'CPWOKO', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (33, 'Kecamatan Mejobo', 'HE7QT6', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (34, 'Kecamatan Dawe', 'CGUAOZ', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (35, 'Kecamatan Kota', '6JMII6', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (36, 'Kecamatan Undaan', 'YLXIOR', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (37, 'Kecamatan Jekulo', 'NSONJT', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (38, 'Kecamatan Kaliwungu', 'IBLYOE', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (39, 'Kecamatan Jati', 'BBJPHG', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (40, 'Kecamatan Bae', 'R1OUP4', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (41, 'Kecamatan Gebog', '1Z4KOF', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (42, 'Kelurahan Panjunan', 'NBD05D', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (43, 'Kelurahan Mlatinorowito', 'BPCEZU', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (44, 'Kelurahan Kajeksan', 'DISBH4', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (45, 'Kelurahan Kerjasan', '6UV8IP', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (46, 'Kelurahan Wergu Wetan', 'QLY1LB', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (47, 'Kelurahan Wergu Kulon', 'D4ZTIT', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (48, 'Kelurahan Mlati Kidul', 'NDXBMD', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (49, 'Kelurahan Sunggingan', 'DFOHSY', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (50, 'Kelurahan Purwosasri', 'LJBQET', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (51, 'Puskesmas Bae', 'DYH54N', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (52, 'Puskesmas Dawe', 'BRL4RY', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (53, 'Puskesmas Dersalam', 'TEWZKT', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (54, 'Puskesmas Gondosari', 'RYHSXC', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (55, 'Puskesmas Gribig', 'IXP5YT', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (56, 'Puskesmas Jati', 'QHEVO3', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (57, 'Puskesmas Jekulo', 'J5ZXMW', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (58, 'Puskesmas Jepang', 'EKJR87', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (59, 'Puskesmas Kaliwungu', 'YPNKUV', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (60, 'Puskesmas Mejobo', '4ZMZ5Y', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (61, 'Puskesmas Ngembal Kulon', 'J025IT', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (62, 'Puskesmas Ngemplak', 'DGANWG', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (63, 'Puskesmas Purwosari', 'YXWUDZ', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (64, 'Puskesmas Rejosari', 'TH3NPY', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (65, 'Puskesmas Rendeng', 'S3YYPW', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (66, 'Puskesmas Sidorekso', '0N2WG9', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (67, 'Puskesmas Tanjungrejo', 'VDJJ2K', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (68, 'Puskesmas Undaan', '6YFKLM', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');
INSERT INTO `mst_opd` VALUES (69, 'Puskesmas Wergu Wetan', 'FKCQN3', 1, 65, '2018-10-26 06:41:31', '2018-12-06 10:56:41');
INSERT INTO `mst_opd` VALUES (70, 'Laboratorium Kesehatan', '91HIPR', 1, 65, '2018-10-26 06:41:31', '2018-10-29 01:22:28');

-- ----------------------------
-- Table structure for mst_pekerjaan
-- ----------------------------
DROP TABLE IF EXISTS `mst_pekerjaan`;
CREATE TABLE `mst_pekerjaan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pekerjaan` char(35) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of mst_pekerjaan
-- ----------------------------
INSERT INTO `mst_pekerjaan` VALUES (1, 'PNS', 65, 1, '2018-10-04 12:53:09', '2018-10-04 12:53:11', NULL);
INSERT INTO `mst_pekerjaan` VALUES (2, 'TNI', 65, 1, '2018-10-04 12:53:13', '2018-10-04 12:53:14', NULL);
INSERT INTO `mst_pekerjaan` VALUES (3, 'POLRI', 65, 1, '2018-10-04 12:53:15', '2018-10-04 12:53:17', NULL);
INSERT INTO `mst_pekerjaan` VALUES (4, 'SWASTA', 65, 1, '2018-10-04 12:53:18', '2018-10-04 12:53:19', NULL);
INSERT INTO `mst_pekerjaan` VALUES (5, 'WIRAUSAHA', 65, 1, '2018-10-04 12:53:20', '2018-10-04 12:53:22', NULL);

-- ----------------------------
-- Table structure for mst_pertanyaan
-- ----------------------------
DROP TABLE IF EXISTS `mst_pertanyaan`;
CREATE TABLE `mst_pertanyaan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pertanyaan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `id_kuesioner` int(11) NULL DEFAULT NULL,
  `id_unsur_skm` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of mst_pertanyaan
-- ----------------------------
INSERT INTO `mst_pertanyaan` VALUES (1, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 1, 65, 1, '2018-10-30 03:17:10', '2018-12-18 13:21:08');
INSERT INTO `mst_pertanyaan` VALUES (2, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 1, 2, 65, 1, '2018-10-30 03:22:21', '2018-11-09 02:13:40');
INSERT INTO `mst_pertanyaan` VALUES (3, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 1, 3, 65, 1, '2018-10-30 03:22:23', '2018-11-09 02:13:40');
INSERT INTO `mst_pertanyaan` VALUES (4, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 1, 4, 65, 1, '2018-10-30 03:22:26', '2018-11-09 02:13:42');
INSERT INTO `mst_pertanyaan` VALUES (5, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 1, 5, 65, 1, '2018-10-30 03:22:28', '2018-11-09 02:13:41');
INSERT INTO `mst_pertanyaan` VALUES (6, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 1, 6, 65, 1, '2018-10-30 03:22:31', '2018-11-09 02:13:43');
INSERT INTO `mst_pertanyaan` VALUES (7, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 1, 7, 65, 1, '2018-10-30 03:22:33', '2018-11-09 02:13:44');
INSERT INTO `mst_pertanyaan` VALUES (8, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 1, 8, 65, 1, '2018-10-30 03:22:36', '2018-11-09 02:13:44');
INSERT INTO `mst_pertanyaan` VALUES (9, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 1, 9, 65, 1, '2018-10-30 03:22:38', '2018-11-09 02:13:45');
INSERT INTO `mst_pertanyaan` VALUES (10, 'ajoijiojoi', 2, 1, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pertanyaan` VALUES (11, 'ioj', 2, 2, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pertanyaan` VALUES (12, 'ioj', 2, 3, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pertanyaan` VALUES (13, 'ioj', 2, 4, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pertanyaan` VALUES (14, 'ij', 2, 5, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pertanyaan` VALUES (15, 'ioj', 2, 6, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pertanyaan` VALUES (16, 'ub', 2, 7, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pertanyaan` VALUES (17, 'bkyu', 2, 8, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pertanyaan` VALUES (18, 'byu', 2, 9, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');

-- ----------------------------
-- Table structure for mst_pilihan
-- ----------------------------
DROP TABLE IF EXISTS `mst_pilihan`;
CREATE TABLE `mst_pilihan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pertanyaan` int(11) NULL DEFAULT NULL,
  `pilihan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `poin` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 73 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of mst_pilihan
-- ----------------------------
INSERT INTO `mst_pilihan` VALUES (1, 1, 'Tidak sesuai', 1, 66, 1, '2018-10-30 03:27:56', '2018-12-18 13:21:08');
INSERT INTO `mst_pilihan` VALUES (2, 1, 'Kurang sesuai', 2, 66, 1, '2018-10-30 03:27:56', '2018-12-18 13:21:08');
INSERT INTO `mst_pilihan` VALUES (3, 1, 'Sesuai', 3, 66, 1, '2018-10-30 03:27:56', '2018-12-18 13:21:08');
INSERT INTO `mst_pilihan` VALUES (4, 1, 'Sangat Sesuai', 4, 66, 1, '2018-10-30 03:27:56', '2018-12-18 13:21:08');
INSERT INTO `mst_pilihan` VALUES (5, 2, 'Tidak mudah', 1, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (6, 2, 'Kurang mudah', 2, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (7, 2, 'Mudah', 3, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (8, 2, 'Sangat Mudah', 4, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (9, 3, 'Tidak Cepat', 1, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (10, 3, 'Kurang Cepat', 2, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (11, 3, 'Cepat', 3, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (12, 3, 'Sangat Cepat', 4, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (13, 4, 'Sangat Mahal', 1, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (14, 4, 'Cukup Mahal', 2, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (15, 4, 'Murah', 3, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (16, 4, 'Gratis', 4, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (17, 5, 'Tidak Sesuai', 1, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (18, 5, 'Kurang Sesuai', 2, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (19, 5, 'Sesuai', 3, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (20, 5, 'Sangat Sesuai', 4, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (21, 6, 'Tidak Kompeten', 1, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (22, 6, 'Kurang Kompeten', 2, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (23, 6, 'Kompeten', 3, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (24, 6, 'Sangat Kompeten', 4, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (25, 7, 'Tidak Sopan dan ramah', 1, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (26, 7, 'Kurang Sopan dan ramah', 2, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (27, 7, 'Sopan dan ramah', 3, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (28, 7, 'Sangat sopan dan ramah', 4, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (29, 8, 'Buruk', 1, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (30, 8, 'Cukup', 2, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (31, 8, 'Baik', 3, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (32, 8, 'Sangat Baik', 4, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (33, 9, 'Tidak ada', 1, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (34, 9, 'Ada tetapi tidak berfungsi', 2, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (35, 9, 'Berfungsi kurang maksimal', 3, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (36, 9, 'Dikelola dengan baik', 4, 66, 1, '2018-10-30 03:27:56', '2018-10-30 03:27:56');
INSERT INTO `mst_pilihan` VALUES (37, 10, 'jioj', 1, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (38, 10, 'iojio', 2, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (39, 10, 'jij', 3, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (40, 10, 'oij', 4, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (41, 11, 'ij', 1, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (42, 11, 'ioj', 2, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (43, 11, 'ij', 3, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (44, 11, 'ioj', 4, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (45, 12, 'ij', 1, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (46, 12, 'ioj', 2, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (47, 12, 'ioj', 3, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (48, 12, 'ioj', 4, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (49, 13, 'iojio', 1, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (50, 13, 'jioj', 2, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (51, 13, 'io', 3, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (52, 13, 'jiojioj', 4, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (53, 14, 'ioj', 1, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (54, 14, 'ioj', 2, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (55, 14, 'ioj', 3, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (56, 14, 'ioj', 4, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (57, 15, 'ioj', 1, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (58, 15, 'ioj', 2, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (59, 15, 'iojhbyug', 3, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (60, 15, 'uy', 4, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (61, 16, 'u', 1, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (62, 16, 'bnuiguvbh', 2, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (63, 16, 'bjhk', 3, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (64, 16, 'bu', 4, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (65, 17, 'ytvmv', 1, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (66, 17, 'k', 2, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (67, 17, 'byuk', 3, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (68, 17, 'kuy', 4, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (69, 18, 'bkyu', 1, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (70, 18, 'vk', 2, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (71, 18, 'vy', 3, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');
INSERT INTO `mst_pilihan` VALUES (72, 18, 'gvkh', 4, 65, 1, '2018-12-19 04:20:06', '2018-12-19 11:20:06');

-- ----------------------------
-- Table structure for mst_rentang_ikm
-- ----------------------------
DROP TABLE IF EXISTS `mst_rentang_ikm`;
CREATE TABLE `mst_rentang_ikm`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` char(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `range1` float NULL DEFAULT NULL,
  `range2` float NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of mst_rentang_ikm
-- ----------------------------
INSERT INTO `mst_rentang_ikm` VALUES (1, 'A', 'Sangat Baik', 88.31, 100, 65, 1, '2018-10-04 13:34:51', '2018-10-04 13:34:53');
INSERT INTO `mst_rentang_ikm` VALUES (2, 'B', 'Baik', 76.61, 88.3, 65, 1, '2018-10-04 13:34:54', '2018-10-04 13:34:55');
INSERT INTO `mst_rentang_ikm` VALUES (3, 'C', 'Kurang Baik', 65, 76.6, 65, 1, '2018-10-04 13:34:56', '2018-10-04 13:34:58');
INSERT INTO `mst_rentang_ikm` VALUES (4, 'D', 'Tidak Baik', 25, 64.99, 65, 1, '2018-10-04 13:34:58', '2018-10-04 13:35:01');

-- ----------------------------
-- Table structure for mst_unsur_skm
-- ----------------------------
DROP TABLE IF EXISTS `mst_unsur_skm`;
CREATE TABLE `mst_unsur_skm`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_unsur` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of mst_unsur_skm
-- ----------------------------
INSERT INTO `mst_unsur_skm` VALUES (1, 'Persyaratan', 'A', 1, 65, '2018-11-08 23:38:24', '2018-11-08 23:38:25');
INSERT INTO `mst_unsur_skm` VALUES (2, 'Sistem, Mekanisme, dan Prosedur', 'B', 1, 65, '2018-11-08 23:38:26', '2018-11-08 23:38:27');
INSERT INTO `mst_unsur_skm` VALUES (3, 'Waktu Penyelesaian', 'C', 1, 65, '2018-11-08 23:38:28', '2018-11-08 23:38:29');
INSERT INTO `mst_unsur_skm` VALUES (4, 'Biaya/Tarif', 'D', 1, 65, '2018-11-08 23:38:33', '2018-11-08 23:38:34');
INSERT INTO `mst_unsur_skm` VALUES (5, 'Produk Spesifikasi Jenis Pelayanan', 'E', 1, 65, '2018-11-08 23:38:35', '2018-11-08 23:38:36');
INSERT INTO `mst_unsur_skm` VALUES (6, 'Kompetensi Pelaksana', 'F', 1, 65, '2018-11-08 23:38:37', '2018-11-08 23:38:39');
INSERT INTO `mst_unsur_skm` VALUES (7, 'Perilaku Pelaksana', 'G', 1, 65, '2018-11-08 23:38:39', '2018-11-08 23:38:41');
INSERT INTO `mst_unsur_skm` VALUES (8, 'Penganganan Pengaduan, Saran dan Masukan', 'H', 1, 65, '2018-11-08 23:38:41', '2018-11-08 23:38:43');
INSERT INTO `mst_unsur_skm` VALUES (9, 'Sarana dan Prasarana', 'I', 1, 65, '2018-11-08 23:38:50', '2018-11-08 23:38:52');

-- ----------------------------
-- Table structure for password_change_log
-- ----------------------------
DROP TABLE IF EXISTS `password_change_log`;
CREATE TABLE `password_change_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ip` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `browser` varchar(400) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of password_change_log
-- ----------------------------
INSERT INTO `password_change_log` VALUES (3, 65, '192.168.1.66', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0', '2016-09-06 13:59:09');
INSERT INTO `password_change_log` VALUES (4, 65, '192.168.1.66', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0', '2016-09-06 13:59:56');
INSERT INTO `password_change_log` VALUES (5, 65, '192.168.1.66', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0', '2016-09-06 14:01:30');
INSERT INTO `password_change_log` VALUES (6, 65, '192.168.1.66', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0', '2016-09-06 14:01:50');
INSERT INTO `password_change_log` VALUES (7, 65, '192.168.1.93', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-11-18 09:35:21');
INSERT INTO `password_change_log` VALUES (8, 65, '192.168.1.93', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-11-18 09:35:50');
INSERT INTO `password_change_log` VALUES (9, 65, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2017-11-15 16:20:48');
INSERT INTO `password_change_log` VALUES (10, 65, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2017-11-15 16:21:02');
INSERT INTO `password_change_log` VALUES (11, 65, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2017-11-16 17:35:42');
INSERT INTO `password_change_log` VALUES (12, 65, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2017-11-16 17:35:58');
INSERT INTO `password_change_log` VALUES (13, 65, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2017-11-20 22:00:35');
INSERT INTO `password_change_log` VALUES (14, 65, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2017-11-20 22:00:52');
INSERT INTO `password_change_log` VALUES (15, 65, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2017-11-20 23:41:46');
INSERT INTO `password_change_log` VALUES (16, 65, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2017-11-20 23:41:55');
INSERT INTO `password_change_log` VALUES (17, 65, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '2018-05-28 12:43:50');
INSERT INTO `password_change_log` VALUES (18, 65, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '2018-05-28 12:45:14');
INSERT INTO `password_change_log` VALUES (19, 65, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '2018-05-28 13:28:25');
INSERT INTO `password_change_log` VALUES (20, 65, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '2018-05-28 13:37:21');
INSERT INTO `password_change_log` VALUES (21, 65, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '2018-05-28 13:37:37');

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','Inactive') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 139 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES (1, 'site-login', 'Allow User to login', 'Active', '2014-02-12 00:00:00', '2016-06-26 01:28:44');
INSERT INTO `permissions` VALUES (2, 'context-dashboard', 'Allow View Dashboard', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (3, 'context-developer', 'Allow View Developer', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (4, 'context-settings', 'Allow View Settings', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (5, 'mod-context-index', 'Allow Access Context Index', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (6, 'mod-context-create', 'Allow Access Context Create', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (7, 'mod-context-edit', 'Allow Access Context Edit', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (8, 'mod-context-delete', 'Allow Access Context Delete', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (9, 'mod-modules-index', 'Allow Access Modules Index', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (10, 'mod-modules-create', 'Allow Access Modules Create', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (11, 'mod-modules-edit', 'Allow Access Modules Edit', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (12, 'mod-modules-delete', 'Allow Access Modules Delete', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (13, 'mod-permissions-index', 'Allow Access Permissions Index', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (14, 'mod-permissions-create', 'Allow Access Permissions Create', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (15, 'mod-permissions-edit', 'Allow Access Permissions Edit', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (16, 'mod-permissions-delete', 'Allow Access Permissions Delete', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (17, 'mod-roles-index', 'Allow Access Roles Index', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (18, 'mod-roles-create', 'Allow Access Roles Create', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (19, 'mod-roles-edit', 'Allow Access Roles Edit', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (20, 'mod-roles-delete', 'Allow Access Roles Delete', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (21, 'mod-permissionsmatrix-index', 'Allow Access Permissions Matrix Index', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (22, 'mod-permissionsmatrix-create', 'Allow Access Permissions Matrix Create', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (23, 'mod-permissionsmatrix-edit', 'Allow Access Permissions Matrix Edit', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (24, 'mod-permissionsmatrix-delete', 'Allow Access Permissions Matrix Delete', 'Active', '2014-02-12 00:00:00', '2014-02-12 00:00:00');
INSERT INTO `permissions` VALUES (25, 'mod-users-index', 'Allow Access Users Index', 'Active', '2014-02-18 00:00:00', '2014-02-18 00:00:00');
INSERT INTO `permissions` VALUES (26, 'mod-users-create', 'Allow Access Users Create', 'Active', '2014-02-18 00:00:00', '2014-02-18 00:00:00');
INSERT INTO `permissions` VALUES (27, 'mod-users-edit', 'Allow Access Users Edit', 'Active', '2014-02-18 00:00:00', '2014-02-18 00:00:00');
INSERT INTO `permissions` VALUES (28, 'mod-users-delete', 'Allow Access Users Delete', 'Active', '2014-02-18 00:00:00', '2014-02-18 00:00:00');
INSERT INTO `permissions` VALUES (30, 'mod-configurations-index', 'Allow Access Configurations Index', 'Active', '2014-02-19 00:00:00', '2014-02-19 00:00:00');
INSERT INTO `permissions` VALUES (31, 'mod-configurations-create', 'Allow Access Configurations Create', 'Active', '2014-02-19 00:00:00', '2014-02-19 00:00:00');
INSERT INTO `permissions` VALUES (32, 'mod-configurations-edit', 'Allow Access Configurations Edit', 'Active', '2014-02-19 00:00:00', '2014-02-19 00:00:00');
INSERT INTO `permissions` VALUES (33, 'mod-configurations-delete', 'Allow Access Configurations Delete', 'Active', '2014-02-19 00:00:00', '2014-02-19 00:00:00');
INSERT INTO `permissions` VALUES (34, 'context-masterdata', 'Allow Access masterdata', 'Active', '2018-10-07 15:58:38', '2018-10-07 15:58:38');
INSERT INTO `permissions` VALUES (40, 'mod-jenjangpendidikan-index', 'Allow Access Jenjang Pendidikan Index', 'Active', '2018-10-07 16:14:40', '2018-10-07 16:14:40');
INSERT INTO `permissions` VALUES (41, 'mod-jenjangpendidikan-listall', 'Allow Access Jenjang Pendidikan Listall', 'Active', '2018-10-07 16:14:40', '2018-10-07 16:14:40');
INSERT INTO `permissions` VALUES (42, 'mod-jenjangpendidikan-create', 'Allow Access Jenjang Pendidikan Create', 'Active', '2018-10-07 16:14:40', '2018-10-07 16:14:40');
INSERT INTO `permissions` VALUES (43, 'mod-jenjangpendidikan-edit', 'Allow Access Jenjang Pendidikan Edit', 'Active', '2018-10-07 16:14:40', '2018-10-07 16:14:40');
INSERT INTO `permissions` VALUES (44, 'mod-jenjangpendidikan-delete', 'Allow Access Jenjang Pendidikan Delete', 'Active', '2018-10-07 16:14:40', '2018-10-07 16:14:40');
INSERT INTO `permissions` VALUES (45, 'mod-pekerjaan-index', 'Allow Access Pekerjaan Index', 'Active', '2018-10-07 16:15:10', '2018-10-07 16:15:10');
INSERT INTO `permissions` VALUES (46, 'mod-pekerjaan-listall', 'Allow Access Pekerjaan Listall', 'Active', '2018-10-07 16:15:10', '2018-10-07 16:15:10');
INSERT INTO `permissions` VALUES (47, 'mod-pekerjaan-create', 'Allow Access Pekerjaan Create', 'Active', '2018-10-07 16:15:10', '2018-10-07 16:15:10');
INSERT INTO `permissions` VALUES (48, 'mod-pekerjaan-edit', 'Allow Access Pekerjaan Edit', 'Active', '2018-10-07 16:15:10', '2018-10-07 16:15:10');
INSERT INTO `permissions` VALUES (49, 'mod-pekerjaan-delete', 'Allow Access Pekerjaan Delete', 'Active', '2018-10-07 16:15:10', '2018-10-07 16:15:10');
INSERT INTO `permissions` VALUES (65, 'context-responmasyarakat', 'Allow Access responmasyarakat', 'Active', '2018-10-07 16:24:37', '2018-10-07 16:24:37');
INSERT INTO `permissions` VALUES (76, 'mod-opd-index', 'Allow Access OPD Index', 'Active', '2018-10-25 23:43:10', '2018-10-25 23:43:10');
INSERT INTO `permissions` VALUES (77, 'mod-opd-listall', 'Allow Access OPD Listall', 'Active', '2018-10-25 23:43:11', '2018-10-25 23:43:11');
INSERT INTO `permissions` VALUES (78, 'mod-opd-create', 'Allow Access OPD Create', 'Active', '2018-10-25 23:43:11', '2018-10-25 23:43:11');
INSERT INTO `permissions` VALUES (79, 'mod-opd-edit', 'Allow Access OPD Edit', 'Active', '2018-10-25 23:43:11', '2018-10-25 23:43:11');
INSERT INTO `permissions` VALUES (80, 'mod-opd-delete', 'Allow Access OPD Delete', 'Active', '2018-10-25 23:43:11', '2018-10-25 23:43:11');
INSERT INTO `permissions` VALUES (81, 'mod-layanan-index', 'Allow Access Layanan Index', 'Active', '2018-10-25 23:45:22', '2018-10-25 23:45:22');
INSERT INTO `permissions` VALUES (82, 'mod-layanan-listall', 'Allow Access Layanan Listall', 'Active', '2018-10-25 23:45:22', '2018-10-25 23:45:22');
INSERT INTO `permissions` VALUES (83, 'mod-layanan-create', 'Allow Access Layanan Create', 'Active', '2018-10-25 23:45:22', '2018-10-25 23:45:22');
INSERT INTO `permissions` VALUES (84, 'mod-layanan-edit', 'Allow Access Layanan Edit', 'Active', '2018-10-25 23:45:22', '2018-10-25 23:45:22');
INSERT INTO `permissions` VALUES (85, 'mod-layanan-delete', 'Allow Access Layanan Delete', 'Active', '2018-10-25 23:45:22', '2018-10-25 23:45:22');
INSERT INTO `permissions` VALUES (86, 'context-pengaturan', 'Allow Access pengaturan', 'Active', '2018-10-26 06:32:26', '2018-10-26 06:32:26');
INSERT INTO `permissions` VALUES (92, 'mod-layananopd-index', 'Allow Access Layanan OPD Index', 'Active', '2018-10-26 06:45:01', '2018-10-26 06:45:01');
INSERT INTO `permissions` VALUES (93, 'mod-layananopd-listall', 'Allow Access Layanan OPD Listall', 'Active', '2018-10-26 06:45:01', '2018-10-26 06:45:01');
INSERT INTO `permissions` VALUES (94, 'mod-layananopd-create', 'Allow Access Layanan OPD Create', 'Active', '2018-10-26 06:45:01', '2018-10-26 06:45:01');
INSERT INTO `permissions` VALUES (95, 'mod-layananopd-edit', 'Allow Access Layanan OPD Edit', 'Active', '2018-10-26 06:45:01', '2018-10-26 06:45:01');
INSERT INTO `permissions` VALUES (96, 'mod-layananopd-delete', 'Allow Access Layanan OPD Delete', 'Active', '2018-10-26 06:45:01', '2018-10-26 06:45:01');
INSERT INTO `permissions` VALUES (102, 'mod-unsurskm-index', 'Allow Access Unsur SKM Index', 'Active', '2018-11-08 16:40:13', '2018-11-08 16:40:13');
INSERT INTO `permissions` VALUES (103, 'mod-unsurskm-listall', 'Allow Access Unsur SKM Listall', 'Active', '2018-11-08 16:40:13', '2018-11-08 16:40:13');
INSERT INTO `permissions` VALUES (104, 'mod-rentangikm-index', 'Allow Access Rentang IKM Index', 'Active', '2018-11-08 16:46:07', '2018-11-08 16:46:07');
INSERT INTO `permissions` VALUES (105, 'mod-rentangikm-listall', 'Allow Access Rentang IKM Listall', 'Active', '2018-11-08 16:46:07', '2018-11-08 16:46:07');
INSERT INTO `permissions` VALUES (106, 'mod-rentangikm-edit', 'Allow Access Rentang IKM Edit', 'Active', '2018-11-08 16:46:07', '2018-11-08 16:46:07');
INSERT INTO `permissions` VALUES (107, 'mod-pengguna-index', 'Allow Access Pengguna Index', 'Active', '2018-11-08 17:38:39', '2018-11-08 17:38:39');
INSERT INTO `permissions` VALUES (108, 'mod-pengguna-listall', 'Allow Access Pengguna Listall', 'Active', '2018-11-08 17:38:39', '2018-11-08 17:38:39');
INSERT INTO `permissions` VALUES (109, 'mod-pengguna-create', 'Allow Access Pengguna Create', 'Active', '2018-11-08 17:38:39', '2018-11-08 17:38:39');
INSERT INTO `permissions` VALUES (110, 'mod-pengguna-edit', 'Allow Access Pengguna Edit', 'Active', '2018-11-08 17:38:39', '2018-11-08 17:38:39');
INSERT INTO `permissions` VALUES (111, 'mod-pengguna-delete', 'Allow Access Pengguna Delete', 'Active', '2018-11-08 17:38:39', '2018-11-08 17:38:39');
INSERT INTO `permissions` VALUES (112, 'context-portal', 'Allow Access portal', 'Active', '2018-11-25 14:42:17', '2018-11-25 14:42:17');
INSERT INTO `permissions` VALUES (113, 'mod-beranda-index', 'Allow Access Beranda Index', 'Active', '2018-11-25 14:43:02', '2018-11-25 14:43:02');
INSERT INTO `permissions` VALUES (114, 'mod-berita-index', 'Allow Access Berita Index', 'Active', '2018-11-25 14:47:25', '2018-11-25 14:47:25');
INSERT INTO `permissions` VALUES (115, 'mod-berita-listall', 'Allow Access Berita Listall', 'Active', '2018-11-25 14:47:25', '2018-11-25 14:47:25');
INSERT INTO `permissions` VALUES (116, 'mod-berita-create', 'Allow Access Berita Create', 'Active', '2018-11-25 14:47:25', '2018-11-25 14:47:25');
INSERT INTO `permissions` VALUES (117, 'mod-berita-edit', 'Allow Access Berita Edit', 'Active', '2018-11-25 14:47:25', '2018-11-25 14:47:25');
INSERT INTO `permissions` VALUES (118, 'mod-berita-delete', 'Allow Access Berita Delete', 'Active', '2018-11-25 14:47:25', '2018-11-25 14:47:25');
INSERT INTO `permissions` VALUES (119, 'mod-kuesioner-index', 'Allow Access Kuesioner Index', 'Active', '2018-12-17 03:34:54', '2018-12-17 03:34:54');
INSERT INTO `permissions` VALUES (120, 'mod-kuesioner-listall', 'Allow Access Kuesioner Listall', 'Active', '2018-12-17 03:34:54', '2018-12-17 03:34:54');
INSERT INTO `permissions` VALUES (121, 'mod-kuesioner-create', 'Allow Access Kuesioner Create', 'Active', '2018-12-17 03:34:54', '2018-12-17 03:34:54');
INSERT INTO `permissions` VALUES (122, 'mod-kuesioner-edit', 'Allow Access Kuesioner Edit', 'Active', '2018-12-17 03:34:54', '2018-12-17 03:34:54');
INSERT INTO `permissions` VALUES (123, 'mod-kuesioner-delete', 'Allow Access Kuesioner Delete', 'Active', '2018-12-17 03:34:54', '2018-12-17 03:34:54');
INSERT INTO `permissions` VALUES (124, 'mod-kuesionerlayanan-index', 'Allow Access Kuesioner Layanan Index', 'Active', '2018-12-17 03:40:14', '2018-12-17 03:40:14');
INSERT INTO `permissions` VALUES (125, 'mod-kuesionerlayanan-listall', 'Allow Access Kuesioner Layanan Listall', 'Active', '2018-12-17 03:40:14', '2018-12-17 03:40:14');
INSERT INTO `permissions` VALUES (133, 'mod-responmasuk-index', 'Allow Access Respon Masuk Index', 'Active', '2018-12-27 04:10:01', '2018-12-27 04:10:01');
INSERT INTO `permissions` VALUES (134, 'mod-responmasuk-listall', 'Allow Access Respon Masuk Listall', 'Active', '2018-12-27 04:10:01', '2018-12-27 04:10:01');
INSERT INTO `permissions` VALUES (135, 'mod-nilaiikm-index', 'Allow Access Nilai IKM Index', 'Active', '2018-12-27 04:37:25', '2018-12-27 04:37:25');
INSERT INTO `permissions` VALUES (136, 'mod-nilaiikm-listall', 'Allow Access Nilai IKM Listall', 'Active', '2018-12-27 04:37:25', '2018-12-27 04:37:25');
INSERT INTO `permissions` VALUES (137, 'mod-nilaiperunsur-index', 'Allow Access Nilai per Unsur Index', 'Active', '2019-01-01 18:40:45', '2019-01-01 18:40:45');
INSERT INTO `permissions` VALUES (138, 'mod-nilaiperunsur-listall', 'Allow Access Nilai per Unsur Listall', 'Active', '2019-01-01 18:40:45', '2019-01-01 18:40:45');

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL,
  `role_parent` bigint(20) NOT NULL,
  `permission_id` bigint(20) NOT NULL,
  `start_date` datetime NULL DEFAULT NULL,
  `end_date` datetime NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1408 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of role_permission
-- ----------------------------
INSERT INTO `role_permission` VALUES (406, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (407, 1, 0, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (408, 1, 0, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (409, 1, 0, 33, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (410, 1, 0, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (411, 1, 0, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (412, 1, 0, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (413, 1, 0, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (417, 1, 0, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (418, 1, 0, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (419, 1, 0, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (420, 1, 0, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (422, 1, 0, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (423, 1, 0, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (424, 1, 0, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (425, 1, 0, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (426, 1, 0, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (427, 1, 0, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (428, 1, 0, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (429, 1, 0, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (430, 1, 0, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (432, 1, 0, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (433, 1, 0, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (434, 1, 0, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (435, 1, 0, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (436, 1, 0, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (437, 1, 0, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (438, 1, 0, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (439, 1, 0, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (440, 1, 0, 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (446, 1, 0, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-06-30 21:34:19', '2016-06-30 21:34:19');
INSERT INTO `role_permission` VALUES (483, 1, 0, 125, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-07-18 22:21:15', '2016-07-18 22:21:15');
INSERT INTO `role_permission` VALUES (534, 1, 0, 176, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-07-18 23:31:42', '2016-07-18 23:31:42');
INSERT INTO `role_permission` VALUES (535, 1, 0, 177, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-07-18 23:33:13', '2016-07-18 23:33:13');
INSERT INTO `role_permission` VALUES (536, 1, 0, 178, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-07-18 23:33:13', '2016-07-18 23:33:13');
INSERT INTO `role_permission` VALUES (537, 1, 0, 179, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-07-18 23:33:13', '2016-07-18 23:33:13');
INSERT INTO `role_permission` VALUES (538, 1, 0, 180, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-07-18 23:33:13', '2016-07-18 23:33:13');
INSERT INTO `role_permission` VALUES (679, 3, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-18 19:31:55', '2016-08-18 19:31:55');
INSERT INTO `role_permission` VALUES (683, 3, 0, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-18 19:31:55', '2016-08-18 19:31:55');
INSERT INTO `role_permission` VALUES (700, 6, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-09-01 02:04:03', '2016-09-01 02:04:03');
INSERT INTO `role_permission` VALUES (702, 6, 0, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-09-01 02:04:03', '2016-09-01 02:04:03');
INSERT INTO `role_permission` VALUES (703, 6, 0, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-09-01 02:04:03', '2016-09-01 02:04:03');
INSERT INTO `role_permission` VALUES (704, 6, 0, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-09-01 02:04:03', '2016-09-01 02:04:03');
INSERT INTO `role_permission` VALUES (705, 6, 0, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-09-01 02:04:03', '2016-09-01 02:04:03');
INSERT INTO `role_permission` VALUES (708, 6, 0, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-09-01 02:04:03', '2016-09-01 02:04:03');
INSERT INTO `role_permission` VALUES (709, 6, 0, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-09-01 02:04:03', '2016-09-01 02:04:03');
INSERT INTO `role_permission` VALUES (710, 6, 0, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-09-01 02:04:03', '2016-09-01 02:04:03');
INSERT INTO `role_permission` VALUES (711, 6, 0, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-09-01 02:04:03', '2016-09-01 02:04:03');
INSERT INTO `role_permission` VALUES (715, 6, 0, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-09-01 02:04:03', '2016-09-01 02:04:03');
INSERT INTO `role_permission` VALUES (716, 6, 0, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-09-01 02:04:03', '2016-09-01 02:04:03');
INSERT INTO `role_permission` VALUES (1113, 2, 0, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-11-16 14:50:37', '2017-11-16 14:50:37');
INSERT INTO `role_permission` VALUES (1124, 1, 0, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-11-16 14:55:32', '2017-11-16 14:55:32');
INSERT INTO `role_permission` VALUES (1147, 1, 0, 34, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 15:58:38', '2018-10-07 15:58:38');
INSERT INTO `role_permission` VALUES (1153, 1, 0, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:14:40', '2018-10-07 16:14:40');
INSERT INTO `role_permission` VALUES (1154, 1, 0, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:14:40', '2018-10-07 16:14:40');
INSERT INTO `role_permission` VALUES (1155, 1, 0, 42, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:14:40', '2018-10-07 16:14:40');
INSERT INTO `role_permission` VALUES (1156, 1, 0, 43, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:14:40', '2018-10-07 16:14:40');
INSERT INTO `role_permission` VALUES (1157, 1, 0, 44, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:14:40', '2018-10-07 16:14:40');
INSERT INTO `role_permission` VALUES (1158, 1, 0, 45, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:15:10', '2018-10-07 16:15:10');
INSERT INTO `role_permission` VALUES (1159, 1, 0, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:15:10', '2018-10-07 16:15:10');
INSERT INTO `role_permission` VALUES (1160, 1, 0, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:15:10', '2018-10-07 16:15:10');
INSERT INTO `role_permission` VALUES (1161, 1, 0, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:15:10', '2018-10-07 16:15:10');
INSERT INTO `role_permission` VALUES (1162, 1, 0, 49, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:15:10', '2018-10-07 16:15:10');
INSERT INTO `role_permission` VALUES (1178, 1, 0, 65, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:24:37', '2018-10-07 16:24:37');
INSERT INTO `role_permission` VALUES (1190, 2, 0, 34, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:37:32', '2018-10-07 16:37:32');
INSERT INTO `role_permission` VALUES (1192, 2, 0, 65, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:37:54', '2018-10-07 16:37:54');
INSERT INTO `role_permission` VALUES (1225, 2, 0, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:40:50', '2018-10-07 16:40:50');
INSERT INTO `role_permission` VALUES (1226, 2, 0, 49, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:40:51', '2018-10-07 16:40:51');
INSERT INTO `role_permission` VALUES (1227, 2, 0, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:40:52', '2018-10-07 16:40:52');
INSERT INTO `role_permission` VALUES (1228, 2, 0, 45, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:40:53', '2018-10-07 16:40:53');
INSERT INTO `role_permission` VALUES (1229, 2, 0, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:40:54', '2018-10-07 16:40:54');
INSERT INTO `role_permission` VALUES (1232, 2, 0, 42, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:41:09', '2018-10-07 16:41:09');
INSERT INTO `role_permission` VALUES (1233, 2, 0, 44, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:41:10', '2018-10-07 16:41:10');
INSERT INTO `role_permission` VALUES (1234, 2, 0, 43, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:41:11', '2018-10-07 16:41:11');
INSERT INTO `role_permission` VALUES (1235, 2, 0, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:41:12', '2018-10-07 16:41:12');
INSERT INTO `role_permission` VALUES (1236, 2, 0, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-07 16:41:14', '2018-10-07 16:41:14');
INSERT INTO `role_permission` VALUES (1244, 1, 0, 76, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-25 23:43:10', '2018-10-25 23:43:10');
INSERT INTO `role_permission` VALUES (1245, 1, 0, 77, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-25 23:43:11', '2018-10-25 23:43:11');
INSERT INTO `role_permission` VALUES (1246, 1, 0, 78, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-25 23:43:11', '2018-10-25 23:43:11');
INSERT INTO `role_permission` VALUES (1247, 1, 0, 79, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-25 23:43:11', '2018-10-25 23:43:11');
INSERT INTO `role_permission` VALUES (1248, 1, 0, 80, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-25 23:43:11', '2018-10-25 23:43:11');
INSERT INTO `role_permission` VALUES (1249, 1, 0, 81, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-25 23:45:22', '2018-10-25 23:45:22');
INSERT INTO `role_permission` VALUES (1250, 1, 0, 82, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-25 23:45:22', '2018-10-25 23:45:22');
INSERT INTO `role_permission` VALUES (1251, 1, 0, 83, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-25 23:45:22', '2018-10-25 23:45:22');
INSERT INTO `role_permission` VALUES (1252, 1, 0, 84, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-25 23:45:22', '2018-10-25 23:45:22');
INSERT INTO `role_permission` VALUES (1253, 1, 0, 85, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-25 23:45:22', '2018-10-25 23:45:22');
INSERT INTO `role_permission` VALUES (1254, 1, 0, 86, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:32:26', '2018-10-26 06:32:26');
INSERT INTO `role_permission` VALUES (1255, 4, 0, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:41:58', '2018-10-26 06:41:58');
INSERT INTO `role_permission` VALUES (1261, 4, 0, 65, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:41:58', '2018-10-26 06:41:58');
INSERT INTO `role_permission` VALUES (1274, 1, 0, 92, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:45:01', '2018-10-26 06:45:01');
INSERT INTO `role_permission` VALUES (1275, 1, 0, 93, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:45:01', '2018-10-26 06:45:01');
INSERT INTO `role_permission` VALUES (1277, 1, 0, 95, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:45:01', '2018-10-26 06:45:01');
INSERT INTO `role_permission` VALUES (1284, 2, 0, 86, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:50:43', '2018-10-26 06:50:43');
INSERT INTO `role_permission` VALUES (1300, 2, 0, 95, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:51:50', '2018-10-26 06:51:50');
INSERT INTO `role_permission` VALUES (1302, 2, 0, 92, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:51:52', '2018-10-26 06:51:52');
INSERT INTO `role_permission` VALUES (1304, 2, 0, 93, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:51:54', '2018-10-26 06:51:54');
INSERT INTO `role_permission` VALUES (1305, 4, 0, 93, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:51:56', '2018-10-26 06:51:56');
INSERT INTO `role_permission` VALUES (1316, 2, 0, 78, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:52:39', '2018-10-26 06:52:39');
INSERT INTO `role_permission` VALUES (1318, 2, 0, 80, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:52:43', '2018-10-26 06:52:43');
INSERT INTO `role_permission` VALUES (1320, 2, 0, 79, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:52:45', '2018-10-26 06:52:45');
INSERT INTO `role_permission` VALUES (1322, 2, 0, 76, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:52:47', '2018-10-26 06:52:47');
INSERT INTO `role_permission` VALUES (1324, 2, 0, 77, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:52:49', '2018-10-26 06:52:49');
INSERT INTO `role_permission` VALUES (1326, 2, 0, 83, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:52:59', '2018-10-26 06:52:59');
INSERT INTO `role_permission` VALUES (1328, 2, 0, 85, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:53:01', '2018-10-26 06:53:01');
INSERT INTO `role_permission` VALUES (1330, 2, 0, 84, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:53:03', '2018-10-26 06:53:03');
INSERT INTO `role_permission` VALUES (1332, 2, 0, 81, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:53:05', '2018-10-26 06:53:05');
INSERT INTO `role_permission` VALUES (1334, 2, 0, 82, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-10-26 06:53:08', '2018-10-26 06:53:08');
INSERT INTO `role_permission` VALUES (1339, 1, 0, 102, NULL, NULL, '2018-11-08 16:40:13', '2018-11-08 16:40:13');
INSERT INTO `role_permission` VALUES (1340, 1, 0, 103, NULL, NULL, '2018-11-08 16:40:13', '2018-11-08 16:40:13');
INSERT INTO `role_permission` VALUES (1341, 1, 0, 104, NULL, NULL, '2018-11-08 16:46:07', '2018-11-08 16:46:07');
INSERT INTO `role_permission` VALUES (1342, 1, 0, 105, NULL, NULL, '2018-11-08 16:46:07', '2018-11-08 16:46:07');
INSERT INTO `role_permission` VALUES (1343, 1, 0, 106, NULL, NULL, '2018-11-08 16:46:07', '2018-11-08 16:46:07');
INSERT INTO `role_permission` VALUES (1344, 1, 0, 107, NULL, NULL, '2018-11-08 17:38:39', '2018-11-08 17:38:39');
INSERT INTO `role_permission` VALUES (1345, 1, 0, 108, NULL, NULL, '2018-11-08 17:38:39', '2018-11-08 17:38:39');
INSERT INTO `role_permission` VALUES (1346, 1, 0, 109, NULL, NULL, '2018-11-08 17:38:39', '2018-11-08 17:38:39');
INSERT INTO `role_permission` VALUES (1347, 1, 0, 110, NULL, NULL, '2018-11-08 17:38:39', '2018-11-08 17:38:39');
INSERT INTO `role_permission` VALUES (1348, 1, 0, 111, NULL, NULL, '2018-11-08 17:38:39', '2018-11-08 17:38:39');
INSERT INTO `role_permission` VALUES (1349, 1, 0, 112, NULL, NULL, '2018-11-25 14:42:17', '2018-11-25 14:42:17');
INSERT INTO `role_permission` VALUES (1350, 1, 0, 113, NULL, NULL, '2018-11-25 14:43:02', '2018-11-25 14:43:02');
INSERT INTO `role_permission` VALUES (1351, 1, 0, 114, NULL, NULL, '2018-11-25 14:47:25', '2018-11-25 14:47:25');
INSERT INTO `role_permission` VALUES (1352, 1, 0, 115, NULL, NULL, '2018-11-25 14:47:25', '2018-11-25 14:47:25');
INSERT INTO `role_permission` VALUES (1353, 1, 0, 116, NULL, NULL, '2018-11-25 14:47:25', '2018-11-25 14:47:25');
INSERT INTO `role_permission` VALUES (1354, 1, 0, 117, NULL, NULL, '2018-11-25 14:47:25', '2018-11-25 14:47:25');
INSERT INTO `role_permission` VALUES (1355, 1, 0, 118, NULL, NULL, '2018-11-25 14:47:25', '2018-11-25 14:47:25');
INSERT INTO `role_permission` VALUES (1356, 1, 0, 119, NULL, NULL, '2018-12-17 03:34:54', '2018-12-17 03:34:54');
INSERT INTO `role_permission` VALUES (1357, 1, 0, 120, NULL, NULL, '2018-12-17 03:34:54', '2018-12-17 03:34:54');
INSERT INTO `role_permission` VALUES (1358, 1, 0, 121, NULL, NULL, '2018-12-17 03:34:54', '2018-12-17 03:34:54');
INSERT INTO `role_permission` VALUES (1359, 1, 0, 122, NULL, NULL, '2018-12-17 03:34:54', '2018-12-17 03:34:54');
INSERT INTO `role_permission` VALUES (1360, 1, 0, 123, NULL, NULL, '2018-12-17 03:34:54', '2018-12-17 03:34:54');
INSERT INTO `role_permission` VALUES (1361, 1, 0, 124, NULL, NULL, '2018-12-17 03:40:14', '2018-12-17 03:40:14');
INSERT INTO `role_permission` VALUES (1362, 1, 0, 125, NULL, NULL, '2018-12-17 03:40:14', '2018-12-17 03:40:14');
INSERT INTO `role_permission` VALUES (1370, 1, 0, 133, NULL, NULL, '2018-12-27 04:10:01', '2018-12-27 04:10:01');
INSERT INTO `role_permission` VALUES (1371, 1, 0, 134, NULL, NULL, '2018-12-27 04:10:01', '2018-12-27 04:10:01');
INSERT INTO `role_permission` VALUES (1374, 1, 0, 137, NULL, NULL, '2019-01-01 18:40:45', '2019-01-01 18:40:45');
INSERT INTO `role_permission` VALUES (1375, 1, 0, 138, NULL, NULL, '2019-01-01 18:40:45', '2019-01-01 18:40:45');
INSERT INTO `role_permission` VALUES (1376, 2, 0, 134, NULL, NULL, '2019-01-01 20:06:48', '2019-01-01 20:06:48');
INSERT INTO `role_permission` VALUES (1377, 4, 0, 134, NULL, NULL, '2019-01-01 20:06:49', '2019-01-01 20:06:49');
INSERT INTO `role_permission` VALUES (1378, 5, 0, 134, NULL, NULL, '2019-01-01 20:06:50', '2019-01-01 20:06:50');
INSERT INTO `role_permission` VALUES (1379, 2, 0, 133, NULL, NULL, '2019-01-01 20:06:54', '2019-01-01 20:06:54');
INSERT INTO `role_permission` VALUES (1380, 4, 0, 133, NULL, NULL, '2019-01-01 20:06:56', '2019-01-01 20:06:56');
INSERT INTO `role_permission` VALUES (1381, 5, 0, 133, NULL, NULL, '2019-01-01 20:06:57', '2019-01-01 20:06:57');
INSERT INTO `role_permission` VALUES (1382, 2, 0, 137, NULL, NULL, '2019-01-01 20:07:24', '2019-01-01 20:07:24');
INSERT INTO `role_permission` VALUES (1383, 4, 0, 137, NULL, NULL, '2019-01-01 20:07:25', '2019-01-01 20:07:25');
INSERT INTO `role_permission` VALUES (1384, 5, 0, 137, NULL, NULL, '2019-01-01 20:07:26', '2019-01-01 20:07:26');
INSERT INTO `role_permission` VALUES (1385, 5, 0, 138, NULL, NULL, '2019-01-01 20:07:27', '2019-01-01 20:07:27');
INSERT INTO `role_permission` VALUES (1386, 4, 0, 138, NULL, NULL, '2019-01-01 20:07:29', '2019-01-01 20:07:29');
INSERT INTO `role_permission` VALUES (1387, 2, 0, 138, NULL, NULL, '2019-01-01 20:07:30', '2019-01-01 20:07:30');
INSERT INTO `role_permission` VALUES (1388, 5, 0, 34, NULL, NULL, '2019-01-01 20:10:24', '2019-01-01 20:10:24');
INSERT INTO `role_permission` VALUES (1389, 2, 0, 109, NULL, NULL, '2019-01-02 00:57:19', '2019-01-02 00:57:19');
INSERT INTO `role_permission` VALUES (1390, 2, 0, 111, NULL, NULL, '2019-01-02 00:57:20', '2019-01-02 00:57:20');
INSERT INTO `role_permission` VALUES (1391, 2, 0, 110, NULL, NULL, '2019-01-02 00:57:21', '2019-01-02 00:57:21');
INSERT INTO `role_permission` VALUES (1392, 2, 0, 107, NULL, NULL, '2019-01-02 00:57:22', '2019-01-02 00:57:22');
INSERT INTO `role_permission` VALUES (1393, 2, 0, 108, NULL, NULL, '2019-01-02 00:57:24', '2019-01-02 00:57:24');
INSERT INTO `role_permission` VALUES (1394, 2, 0, 124, NULL, NULL, '2019-01-02 00:58:09', '2019-01-02 00:58:09');
INSERT INTO `role_permission` VALUES (1395, 2, 0, 125, NULL, NULL, '2019-01-02 00:58:11', '2019-01-02 00:58:11');
INSERT INTO `role_permission` VALUES (1396, 5, 0, 124, NULL, NULL, '2019-01-02 00:58:29', '2019-01-02 00:58:29');
INSERT INTO `role_permission` VALUES (1397, 5, 0, 125, NULL, NULL, '2019-01-02 00:58:30', '2019-01-02 00:58:30');
INSERT INTO `role_permission` VALUES (1398, 2, 0, 106, NULL, NULL, '2019-01-02 00:59:32', '2019-01-02 00:59:32');
INSERT INTO `role_permission` VALUES (1399, 2, 0, 104, NULL, NULL, '2019-01-02 00:59:33', '2019-01-02 00:59:33');
INSERT INTO `role_permission` VALUES (1400, 2, 0, 105, NULL, NULL, '2019-01-02 00:59:34', '2019-01-02 00:59:34');
INSERT INTO `role_permission` VALUES (1401, 2, 0, 102, NULL, NULL, '2019-01-02 00:59:54', '2019-01-02 00:59:54');
INSERT INTO `role_permission` VALUES (1402, 2, 0, 103, NULL, NULL, '2019-01-02 00:59:55', '2019-01-02 00:59:55');
INSERT INTO `role_permission` VALUES (1403, 2, 0, 121, NULL, NULL, '2019-01-02 01:00:14', '2019-01-02 01:00:14');
INSERT INTO `role_permission` VALUES (1404, 2, 0, 123, NULL, NULL, '2019-01-02 01:00:17', '2019-01-02 01:00:17');
INSERT INTO `role_permission` VALUES (1405, 2, 0, 122, NULL, NULL, '2019-01-02 01:00:18', '2019-01-02 01:00:18');
INSERT INTO `role_permission` VALUES (1406, 2, 0, 119, NULL, NULL, '2019-01-02 01:00:20', '2019-01-02 01:00:20');
INSERT INTO `role_permission` VALUES (1407, 2, 0, 120, NULL, NULL, '2019-01-02 01:00:22', '2019-01-02 01:00:22');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent` bigint(20) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `login_destination` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','In Active') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, 0, 'Developer', 'Developer Role', 'dashboard', 'Active', '2016-07-20 07:00:00', '2016-07-01 04:34:09');
INSERT INTO `roles` VALUES (2, 0, 'Super Admin', 'Untuk super admin', 'dashboard', 'Active', '2016-07-20 07:00:00', '2017-09-29 10:07:35');
INSERT INTO `roles` VALUES (4, 0, 'Admin OPD', '', 'dashboard', 'Active', '2018-10-07 16:37:05', '2018-10-26 01:41:48');
INSERT INTO `roles` VALUES (5, 0, 'Admin', '', 'dashboard', 'Active', '2018-12-20 06:33:50', '2018-12-20 06:33:50');

-- ----------------------------
-- Table structure for tr_respon
-- ----------------------------
DROP TABLE IF EXISTS `tr_respon`;
CREATE TABLE `tr_respon`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_layanan` int(11) NULL DEFAULT NULL,
  `id_opd` int(11) NULL DEFAULT NULL,
  `jenis_kelamin` int(11) NULL DEFAULT NULL,
  `id_jenjang_pendidikan` int(11) NULL DEFAULT NULL,
  `id_pekerjaan` int(11) NULL DEFAULT NULL,
  `usia` int(11) NULL DEFAULT NULL,
  `saran` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 184 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tr_respon
-- ----------------------------
INSERT INTO `tr_respon` VALUES (36, 19, 16, 1, 3, 4, 28, 'SAYA SANGAT PUAS', 16, 4, '2018-11-08 01:31:05', '2018-11-08 08:31:05');
INSERT INTO `tr_respon` VALUES (37, 19, 16, 2, 4, 1, 33, 'layanan yang sudah baik harus diertahankan', 16, 4, '2018-11-08 01:54:34', '2018-11-08 08:54:34');
INSERT INTO `tr_respon` VALUES (38, 26, 61, 2, 2, 4, 35, 'DITINGKATKAN PELAYANANNYA,LEBIH CEPAT LAGI PELAYANAN', 61, 4, '2018-11-08 01:57:27', '2018-11-08 08:57:27');
INSERT INTO `tr_respon` VALUES (39, 19, 16, 1, 4, 3, 45, 'dukcapil pelayanan nya sangat memuaskan', 16, 4, '2018-11-08 02:10:30', '2018-11-08 09:10:30');
INSERT INTO `tr_respon` VALUES (40, 17, 17, 1, 3, 4, 28, 'semoga kinerja semakin meningkat', 17, 4, '2018-11-08 02:11:49', '2018-11-08 09:11:49');
INSERT INTO `tr_respon` VALUES (41, 19, 16, 1, 3, 5, 19, 'sangat baik', 16, 4, '2018-11-08 02:23:41', '2018-11-08 09:23:41');
INSERT INTO `tr_respon` VALUES (42, 19, 16, 2, 3, 5, 18, 'semoga lebih baik', 16, 4, '2018-11-08 02:30:01', '2018-11-08 09:30:01');
INSERT INTO `tr_respon` VALUES (43, 19, 16, 1, 3, 5, 19, 'sangat baik', 16, 4, '2018-11-08 02:34:04', '2018-11-08 09:34:04');
INSERT INTO `tr_respon` VALUES (44, 26, 61, 2, 2, 5, 32, 'DAFTAR TUNGGU DIUSAHAKAN LEBIH CEPAT', 61, 4, '2018-11-08 02:41:33', '2018-11-08 09:41:33');
INSERT INTO `tr_respon` VALUES (45, 17, 17, 1, 4, 4, 39, 'ditambah petugas biar satu castamer satu petugas', 17, 4, '2018-11-08 02:43:50', '2018-11-08 09:43:50');
INSERT INTO `tr_respon` VALUES (46, 19, 16, 2, 4, 5, 42, 'pelayanan sudah bagus perlu dipertahankan', 16, 4, '2018-11-08 02:50:40', '2018-11-08 09:50:40');
INSERT INTO `tr_respon` VALUES (47, 26, 61, 2, 4, 4, 29, 'SUDAH BAIK PELAYANANNYA, PERTAHANKAN DAN TINGKATKAN PELAYANAN YANG SUDAH BAIK INI.', 61, 4, '2018-11-08 02:59:19', '2018-11-08 09:59:19');
INSERT INTO `tr_respon` VALUES (48, 19, 16, 1, 4, 5, 23, 'di perbaiki keterangan atau tulisan tulisan alur proses untuk orang baru.', 16, 4, '2018-11-08 03:05:44', '2018-11-08 10:05:44');
INSERT INTO `tr_respon` VALUES (49, 26, 61, 2, 3, 5, 36, 'SUDAH BAIK TAPI TETAP DI TNGKATKAN PELAYANANYA', 61, 4, '2018-11-08 03:26:12', '2018-11-08 10:26:12');
INSERT INTO `tr_respon` VALUES (50, 17, 17, 1, 4, 4, 26, 'Untuk sistem antrian agar dibuat lebih jelas sistemnya. Agar tidak saling tunggu dan tidak saling srobot', 17, 4, '2018-11-08 03:27:07', '2018-11-08 10:27:07');
INSERT INTO `tr_respon` VALUES (51, 19, 16, 1, 3, 4, 27, 'semangat kakak senyumu sll tersipu d hati masyarakat', 16, 4, '2018-11-08 03:32:41', '2018-11-08 10:32:41');
INSERT INTO `tr_respon` VALUES (52, 26, 61, 2, 3, 1, 52, 'ANTRIAN LAMA, MOHON BILA ADA PERBAIKAN SISTEM DI LAYANAN DI KASIH TAHU ( ADA  PENGUMUMAN YG TERTULIS, BIAR KITA TAK SALAH SANGKA, DAN MAKLUM ATAS KETERLAMBATAN ANTRIAN)', 61, 4, '2018-11-08 03:36:43', '2018-11-08 10:36:43');
INSERT INTO `tr_respon` VALUES (53, 19, 16, 1, 3, 1, 47, 'sarpras blanko ktp supaya dipenuhi.', 16, 4, '2018-11-08 03:40:15', '2018-11-08 10:40:15');
INSERT INTO `tr_respon` VALUES (54, 26, 61, 1, 3, 4, 43, 'AKIR AKHIR INI WAKTU TUNGGU DI  PELAYANAN KURANG CEPAT ,MOHON LEBIH DITINGKATKAN PELAYANANNYA.', 61, 4, '2018-11-08 03:42:31', '2018-11-08 10:42:31');
INSERT INTO `tr_respon` VALUES (55, 17, 17, 1, 4, 5, 42, 'prasarana laptop perlu ditambah', 17, 4, '2018-11-08 03:43:13', '2018-11-08 10:43:13');
INSERT INTO `tr_respon` VALUES (56, 17, 17, 1, 5, 5, 47, 'pelayanan sudah baik dan terus ditingkatkan', 17, 4, '2018-11-08 03:47:41', '2018-11-08 10:47:41');
INSERT INTO `tr_respon` VALUES (122, 19, 16, 2, 3, 4, 29, 'dan cukup puas dengan pelayanan capil kudus', 16, 4, '2018-11-08 04:09:56', '2018-11-08 11:09:56');
INSERT INTO `tr_respon` VALUES (167, 19, 16, 1, 3, 4, 22, 'bell drive di depan lebih memudahkan dalam proses pelayanan.terima kasih', 16, 4, '2018-11-08 04:19:58', '2018-11-08 11:19:58');
INSERT INTO `tr_respon` VALUES (170, 19, 16, 2, 3, 4, 27, 'untuk perbaikan kedepannya', 16, 4, '2018-11-08 04:44:54', '2018-11-08 11:44:54');
INSERT INTO `tr_respon` VALUES (172, 19, 16, 1, 5, 4, 23, 'mohon  ada standar kepastian waktu pelayanan....1 hari jadi maskimal', 16, 4, '2018-11-08 04:55:44', '2018-11-08 11:55:44');
INSERT INTO `tr_respon` VALUES (173, 19, 16, 2, 4, 4, 30, 'ditingkatkan lagi', 16, 4, '2018-11-08 05:02:02', '2018-11-08 12:02:02');
INSERT INTO `tr_respon` VALUES (174, 19, 16, 2, 4, 4, 30, 'ditingkatkan lagi', 16, 4, '2018-11-08 05:02:03', '2018-11-08 12:02:03');
INSERT INTO `tr_respon` VALUES (175, 19, 16, 2, 4, 4, 22, 'meningkatkan kinerja yg lebih baguss......', 16, 4, '2018-11-08 05:09:45', '2018-11-08 12:09:45');
INSERT INTO `tr_respon` VALUES (178, 19, 16, 1, 4, 4, 26, 'top', 16, 4, '2019-01-01 16:29:10', '2019-01-01 23:29:10');
INSERT INTO `tr_respon` VALUES (179, 19, 16, 1, 3, 5, 20, 'jos', 16, 4, '2019-01-02 16:32:51', '2019-01-02 09:09:56');
INSERT INTO `tr_respon` VALUES (180, 31, 59, 2, 3, 4, 31, '', 59, 4, '2019-01-05 02:57:38', '2019-01-05 09:57:39');
INSERT INTO `tr_respon` VALUES (181, 31, 59, 2, 1, 4, 80, 'Bagus', 59, 4, '2019-01-05 03:03:48', '2019-01-05 10:03:48');
INSERT INTO `tr_respon` VALUES (182, 29, 59, 1, 3, 4, 32, 'Sangat puas', 59, 4, '2019-01-09 01:57:55', '2019-01-09 08:57:58');

-- ----------------------------
-- Table structure for tr_respon_detail
-- ----------------------------
DROP TABLE IF EXISTS `tr_respon_detail`;
CREATE TABLE `tr_respon_detail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_respon` int(11) NULL DEFAULT NULL,
  `pertanyaan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `id_unsur_skm` int(11) NULL DEFAULT NULL,
  `jawaban` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `poin` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 703 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tr_respon_detail
-- ----------------------------
INSERT INTO `tr_respon_detail` VALUES (1, 1, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Kurang sesuai', 2, 42, 4, '2018-11-01 22:10:07', '2019-01-01 23:33:10');
INSERT INTO `tr_respon_detail` VALUES (2, 1, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 42, 4, '2018-11-01 22:10:07', '2019-01-01 23:33:11');
INSERT INTO `tr_respon_detail` VALUES (3, 1, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Kurang Cepat', 2, 42, 4, '2018-11-01 22:10:07', '2019-01-01 23:33:11');
INSERT INTO `tr_respon_detail` VALUES (4, 1, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Murah', 3, 42, 4, '2018-11-01 22:10:07', '2019-01-01 23:33:12');
INSERT INTO `tr_respon_detail` VALUES (5, 1, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Kurang Sesuai', 2, 42, 4, '2018-11-01 22:10:07', '2019-01-01 23:33:12');
INSERT INTO `tr_respon_detail` VALUES (6, 1, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 42, 4, '2018-11-01 22:10:07', '2019-01-01 23:33:13');
INSERT INTO `tr_respon_detail` VALUES (7, 1, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Kurang Sopan dan ramah', 2, 42, 4, '2018-11-01 22:10:07', '2019-01-01 23:33:13');
INSERT INTO `tr_respon_detail` VALUES (8, 1, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 42, 4, '2018-11-01 22:10:07', '2019-01-01 23:33:17');
INSERT INTO `tr_respon_detail` VALUES (9, 1, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Ada tetapi tidak berfungsi', 2, 42, 4, '2018-11-01 22:10:07', '2019-01-01 23:33:16');
INSERT INTO `tr_respon_detail` VALUES (10, 2, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Kurang sesuai', 2, 42, 4, '2018-11-01 22:14:34', '2019-01-01 23:33:37');
INSERT INTO `tr_respon_detail` VALUES (11, 2, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Tidak mudah', 1, 42, 4, '2018-11-01 22:14:34', '2019-01-01 23:33:37');
INSERT INTO `tr_respon_detail` VALUES (12, 2, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 42, 4, '2018-11-01 22:14:34', '2019-01-01 23:33:38');
INSERT INTO `tr_respon_detail` VALUES (13, 2, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 42, 4, '2018-11-01 22:14:34', '2019-01-01 23:33:38');
INSERT INTO `tr_respon_detail` VALUES (14, 2, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Kurang Sesuai', 2, 42, 4, '2018-11-01 22:14:34', '2019-01-01 23:33:39');
INSERT INTO `tr_respon_detail` VALUES (15, 2, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kurang Kompeten', 2, 42, 4, '2018-11-01 22:14:34', '2019-01-01 23:33:41');
INSERT INTO `tr_respon_detail` VALUES (16, 2, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 42, 4, '2018-11-01 22:14:34', '2019-01-01 23:33:40');
INSERT INTO `tr_respon_detail` VALUES (17, 2, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 42, 4, '2018-11-01 22:14:34', '2019-01-01 23:33:43');
INSERT INTO `tr_respon_detail` VALUES (18, 2, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Ada tetapi tidak berfungsi', 2, 42, 4, '2018-11-01 22:14:34', '2019-01-01 23:33:44');
INSERT INTO `tr_respon_detail` VALUES (19, 3, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 42, 4, '2018-11-01 22:44:08', '2019-01-01 23:33:45');
INSERT INTO `tr_respon_detail` VALUES (20, 3, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 42, 4, '2018-11-01 22:44:08', '2019-01-01 23:33:45');
INSERT INTO `tr_respon_detail` VALUES (21, 3, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 42, 4, '2018-11-01 22:44:08', '2019-01-01 23:33:46');
INSERT INTO `tr_respon_detail` VALUES (22, 3, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Murah', 3, 42, 4, '2018-11-01 22:44:08', '2019-01-01 23:33:47');
INSERT INTO `tr_respon_detail` VALUES (23, 3, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 42, 4, '2018-11-01 22:44:08', '2019-01-01 23:33:48');
INSERT INTO `tr_respon_detail` VALUES (24, 3, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 42, 4, '2018-11-01 22:44:08', '2019-01-01 23:33:48');
INSERT INTO `tr_respon_detail` VALUES (25, 3, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 42, 4, '2018-11-01 22:44:08', '2019-01-01 23:33:49');
INSERT INTO `tr_respon_detail` VALUES (26, 3, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 42, 4, '2018-11-01 22:44:08', '2019-01-01 23:33:51');
INSERT INTO `tr_respon_detail` VALUES (27, 3, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 42, 4, '2018-11-01 22:44:08', '2019-01-01 23:33:51');
INSERT INTO `tr_respon_detail` VALUES (28, 4, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Kurang sesuai', 2, 42, 4, '2018-11-02 00:52:35', '2019-01-01 23:33:52');
INSERT INTO `tr_respon_detail` VALUES (29, 4, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 42, 4, '2018-11-02 00:52:35', '2019-01-01 23:33:53');
INSERT INTO `tr_respon_detail` VALUES (30, 4, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 42, 4, '2018-11-02 00:52:35', '2019-01-01 23:33:54');
INSERT INTO `tr_respon_detail` VALUES (31, 4, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Murah', 3, 42, 4, '2018-11-02 00:52:35', '2019-01-01 23:33:56');
INSERT INTO `tr_respon_detail` VALUES (32, 4, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Kurang Sesuai', 2, 42, 4, '2018-11-02 00:52:35', '2019-01-01 23:33:56');
INSERT INTO `tr_respon_detail` VALUES (33, 4, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 42, 4, '2018-11-02 00:52:35', '2019-01-01 23:33:57');
INSERT INTO `tr_respon_detail` VALUES (34, 4, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 42, 4, '2018-11-02 00:52:35', '2019-01-01 23:33:58');
INSERT INTO `tr_respon_detail` VALUES (35, 4, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 42, 4, '2018-11-02 00:52:35', '2019-01-01 23:33:59');
INSERT INTO `tr_respon_detail` VALUES (36, 4, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Berfungsi kurang maksimal', 3, 42, 4, '2018-11-02 00:52:35', '2019-01-01 23:34:00');
INSERT INTO `tr_respon_detail` VALUES (37, 5, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sangat Sesuai', 4, 42, 4, '2018-11-02 02:07:26', '2019-01-01 23:34:07');
INSERT INTO `tr_respon_detail` VALUES (38, 5, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 42, 4, '2018-11-02 02:07:26', '2019-01-01 23:34:11');
INSERT INTO `tr_respon_detail` VALUES (39, 5, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 42, 4, '2018-11-02 02:07:26', '2019-01-01 23:34:12');
INSERT INTO `tr_respon_detail` VALUES (40, 5, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Murah', 3, 42, 4, '2018-11-02 02:07:26', '2019-01-01 23:34:12');
INSERT INTO `tr_respon_detail` VALUES (41, 5, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 42, 4, '2018-11-02 02:07:26', '2019-01-01 23:34:13');
INSERT INTO `tr_respon_detail` VALUES (42, 5, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 42, 4, '2018-11-02 02:07:26', '2019-01-01 23:34:14');
INSERT INTO `tr_respon_detail` VALUES (43, 5, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 42, 4, '2018-11-02 02:07:26', '2019-01-01 23:34:14');
INSERT INTO `tr_respon_detail` VALUES (44, 5, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 42, 4, '2018-11-02 02:07:26', '2019-01-01 23:34:15');
INSERT INTO `tr_respon_detail` VALUES (45, 5, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 42, 4, '2018-11-02 02:07:26', '2019-01-01 23:34:16');
INSERT INTO `tr_respon_detail` VALUES (46, 6, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 42, 4, '2018-11-02 02:15:37', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (47, 6, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 42, 4, '2018-11-02 02:15:37', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (48, 6, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 42, 4, '2018-11-02 02:15:37', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (49, 6, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Murah', 3, 42, 4, '2018-11-02 02:15:37', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (50, 6, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 42, 4, '2018-11-02 02:15:37', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (51, 6, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 42, 4, '2018-11-02 02:15:37', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (52, 6, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 42, 4, '2018-11-02 02:15:37', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (53, 6, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 42, 4, '2018-11-02 02:15:37', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (54, 6, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Ada tetapi tidak berfungsi', 2, 42, 4, '2018-11-02 02:15:37', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (55, 7, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 42, 4, '2018-11-05 06:10:18', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (56, 7, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 42, 4, '2018-11-05 06:10:18', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (57, 7, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 42, 4, '2018-11-05 06:10:18', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (58, 7, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 42, 4, '2018-11-05 06:10:18', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (59, 7, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 42, 4, '2018-11-05 06:10:18', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (60, 7, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 42, 4, '2018-11-05 06:10:18', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (61, 7, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 42, 4, '2018-11-05 06:10:18', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (62, 7, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 42, 4, '2018-11-05 06:10:18', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (63, 7, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 42, 4, '2018-11-05 06:10:18', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (64, 8, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sangat Sesuai', 4, 42, 4, '2018-11-06 16:55:30', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (65, 8, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 42, 4, '2018-11-06 16:55:30', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (66, 8, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 42, 4, '2018-11-06 16:55:30', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (67, 8, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Murah', 3, 42, 4, '2018-11-06 16:55:30', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (68, 8, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 42, 4, '2018-11-06 16:55:30', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (69, 8, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 42, 4, '2018-11-06 16:55:30', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (70, 8, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 42, 4, '2018-11-06 16:55:30', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (71, 8, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 42, 4, '2018-11-06 16:55:30', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (72, 8, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Berfungsi kurang maksimal', 3, 42, 4, '2018-11-06 16:55:30', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (73, 9, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 18, 4, '2018-11-07 03:13:07', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (74, 9, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 18, 4, '2018-11-07 03:13:07', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (75, 9, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 18, 4, '2018-11-07 03:13:07', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (76, 9, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 18, 4, '2018-11-07 03:13:07', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (77, 9, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 18, 4, '2018-11-07 03:13:07', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (78, 9, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 18, 4, '2018-11-07 03:13:07', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (79, 9, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 18, 4, '2018-11-07 03:13:07', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (80, 9, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 18, 4, '2018-11-07 03:13:07', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (81, 9, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 18, 4, '2018-11-07 03:13:07', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (82, 10, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 59, 4, '2018-11-07 03:18:13', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (83, 10, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 59, 4, '2018-11-07 03:18:13', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (84, 10, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 59, 4, '2018-11-07 03:18:13', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (85, 10, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-07 03:18:13', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (86, 10, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 59, 4, '2018-11-07 03:18:13', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (87, 10, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 59, 4, '2018-11-07 03:18:13', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (88, 10, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 59, 4, '2018-11-07 03:18:13', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (89, 10, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 59, 4, '2018-11-07 03:18:13', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (90, 10, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-07 03:18:13', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (91, 11, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 17, 4, '2018-11-07 03:19:04', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (92, 11, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 17, 4, '2018-11-07 03:19:04', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (93, 11, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Kurang Cepat', 2, 17, 4, '2018-11-07 03:19:04', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (94, 11, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 17, 4, '2018-11-07 03:19:04', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (95, 11, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 17, 4, '2018-11-07 03:19:04', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (96, 11, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 17, 4, '2018-11-07 03:19:04', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (97, 11, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 17, 4, '2018-11-07 03:19:04', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (98, 11, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 17, 4, '2018-11-07 03:19:04', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (99, 11, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Berfungsi kurang maksimal', 3, 17, 4, '2018-11-07 03:19:04', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (100, 12, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 16, 4, '2018-11-07 03:20:21', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (101, 12, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 16, 4, '2018-11-07 03:20:21', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (102, 12, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 16, 4, '2018-11-07 03:20:21', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (103, 12, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-07 03:20:21', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (104, 12, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 16, 4, '2018-11-07 03:20:21', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (105, 12, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 16, 4, '2018-11-07 03:20:21', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (106, 12, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 16, 4, '2018-11-07 03:20:21', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (107, 12, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 16, 4, '2018-11-07 03:20:21', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (108, 12, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2018-11-07 03:20:21', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (109, 13, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 16, 4, '2018-11-07 03:20:23', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (110, 13, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 16, 4, '2018-11-07 03:20:23', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (111, 13, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 16, 4, '2018-11-07 03:20:23', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (112, 13, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-07 03:20:23', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (113, 13, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 16, 4, '2018-11-07 03:20:23', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (114, 13, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 16, 4, '2018-11-07 03:20:23', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (115, 13, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 16, 4, '2018-11-07 03:20:23', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (116, 13, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 16, 4, '2018-11-07 03:20:23', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (117, 13, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2018-11-07 03:20:23', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (118, 14, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 61, 4, '2018-11-07 03:22:48', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (119, 14, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 61, 4, '2018-11-07 03:22:48', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (120, 14, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 61, 4, '2018-11-07 03:22:48', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (121, 14, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 61, 4, '2018-11-07 03:22:48', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (122, 14, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 61, 4, '2018-11-07 03:22:48', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (123, 14, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 61, 4, '2018-11-07 03:22:48', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (124, 14, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 61, 4, '2018-11-07 03:22:48', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (125, 14, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 61, 4, '2018-11-07 03:22:48', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (126, 14, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 61, 4, '2018-11-07 03:22:48', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (127, 15, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 14, 4, '2018-11-07 03:22:58', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (128, 15, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 14, 4, '2018-11-07 03:22:58', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (129, 15, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 14, 4, '2018-11-07 03:22:58', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (130, 15, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 14, 4, '2018-11-07 03:22:58', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (131, 15, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 14, 4, '2018-11-07 03:22:58', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (132, 15, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 14, 4, '2018-11-07 03:22:58', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (133, 15, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 14, 4, '2018-11-07 03:22:58', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (134, 15, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Cukup', 2, 14, 4, '2018-11-07 03:22:58', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (135, 15, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Berfungsi kurang maksimal', 3, 14, 4, '2018-11-07 03:22:58', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (136, 16, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sangat Sesuai', 4, 38, 4, '2018-11-07 03:23:35', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (137, 16, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 38, 4, '2018-11-07 03:23:35', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (138, 16, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 38, 4, '2018-11-07 03:23:35', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (139, 16, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 38, 4, '2018-11-07 03:23:35', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (140, 16, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 38, 4, '2018-11-07 03:23:35', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (141, 16, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 38, 4, '2018-11-07 03:23:35', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (142, 16, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 38, 4, '2018-11-07 03:23:35', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (143, 16, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 38, 4, '2018-11-07 03:23:35', '2019-01-01 23:39:12');
INSERT INTO `tr_respon_detail` VALUES (144, 16, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 38, 4, '2018-11-07 03:23:35', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (145, 17, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 16, 4, '2018-11-07 03:24:31', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (146, 17, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 16, 4, '2018-11-07 03:24:31', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (147, 17, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 16, 4, '2018-11-07 03:24:31', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (148, 17, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-07 03:24:31', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (149, 17, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 16, 4, '2018-11-07 03:24:31', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (150, 17, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 16, 4, '2018-11-07 03:24:31', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (151, 17, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 16, 4, '2018-11-07 03:24:31', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (152, 17, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 16, 4, '2018-11-07 03:24:31', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (153, 17, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2018-11-07 03:24:31', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (154, 18, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 61, 4, '2018-11-07 03:29:38', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (155, 18, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 61, 4, '2018-11-07 03:29:38', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (156, 18, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 61, 4, '2018-11-07 03:29:38', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (157, 18, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 61, 4, '2018-11-07 03:29:38', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (158, 18, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 61, 4, '2018-11-07 03:29:38', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (159, 18, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 61, 4, '2018-11-07 03:29:38', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (160, 18, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 61, 4, '2018-11-07 03:29:38', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (161, 18, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 61, 4, '2018-11-07 03:29:38', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (162, 18, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 61, 4, '2018-11-07 03:29:38', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (163, 19, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sangat Sesuai', 4, 59, 4, '2018-11-07 03:31:00', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (164, 19, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 59, 4, '2018-11-07 03:31:00', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (165, 19, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 59, 4, '2018-11-07 03:31:00', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (166, 19, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Murah', 3, 59, 4, '2018-11-07 03:31:00', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (167, 19, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 59, 4, '2018-11-07 03:31:00', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (168, 19, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 59, 4, '2018-11-07 03:31:00', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (169, 19, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 59, 4, '2018-11-07 03:31:00', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (170, 19, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 59, 4, '2018-11-07 03:31:00', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (171, 19, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-07 03:31:00', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (172, 20, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 59, 4, '2018-11-07 03:34:43', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (173, 20, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 59, 4, '2018-11-07 03:34:43', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (174, 20, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 59, 4, '2018-11-07 03:34:43', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (175, 20, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-07 03:34:43', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (176, 20, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 59, 4, '2018-11-07 03:34:43', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (177, 20, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 59, 4, '2018-11-07 03:34:43', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (178, 20, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 59, 4, '2018-11-07 03:34:43', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (179, 20, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 59, 4, '2018-11-07 03:34:43', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (180, 20, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-07 03:34:43', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (181, 21, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sangat Sesuai', 4, 18, 4, '2018-11-07 03:42:35', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (182, 21, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 18, 4, '2018-11-07 03:42:35', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (183, 21, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 18, 4, '2018-11-07 03:42:35', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (184, 21, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 18, 4, '2018-11-07 03:42:35', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (185, 21, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 18, 4, '2018-11-07 03:42:35', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (186, 21, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 18, 4, '2018-11-07 03:42:35', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (187, 21, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 18, 4, '2018-11-07 03:42:35', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (188, 21, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 18, 4, '2018-11-07 03:42:35', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (189, 21, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 18, 4, '2018-11-07 03:42:35', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (190, 22, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 16, 4, '2018-11-07 04:52:16', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (191, 22, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 16, 4, '2018-11-07 04:52:16', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (192, 22, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 16, 4, '2018-11-07 04:52:16', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (193, 22, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-07 04:52:16', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (194, 22, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 16, 4, '2018-11-07 04:52:16', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (195, 22, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 16, 4, '2018-11-07 04:52:16', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (196, 22, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 16, 4, '2018-11-07 04:52:16', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (197, 22, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 16, 4, '2018-11-07 04:52:16', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (198, 22, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2018-11-07 04:52:16', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (199, 23, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 59, 4, '2018-11-07 05:01:09', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (200, 23, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 59, 4, '2018-11-07 05:01:09', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (201, 23, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 59, 4, '2018-11-07 05:01:09', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (202, 23, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-07 05:01:09', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (203, 23, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 59, 4, '2018-11-07 05:01:09', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (204, 23, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 59, 4, '2018-11-07 05:01:09', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (205, 23, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 59, 4, '2018-11-07 05:01:09', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (206, 23, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 59, 4, '2018-11-07 05:01:09', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (207, 23, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-07 05:01:09', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (208, 25, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 59, 4, '2018-11-07 05:18:12', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (209, 25, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 59, 4, '2018-11-07 05:18:12', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (210, 25, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 59, 4, '2018-11-07 05:18:12', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (211, 25, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-07 05:18:12', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (212, 25, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 59, 4, '2018-11-07 05:18:12', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (213, 25, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 59, 4, '2018-11-07 05:18:12', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (214, 25, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 59, 4, '2018-11-07 05:18:12', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (215, 25, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 59, 4, '2018-11-07 05:18:12', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (216, 25, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-07 05:18:12', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (217, 26, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 38, 4, '2018-11-07 05:30:53', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (218, 26, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 38, 4, '2018-11-07 05:30:53', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (219, 26, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 38, 4, '2018-11-07 05:30:53', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (220, 26, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 38, 4, '2018-11-07 05:30:53', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (221, 26, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 38, 4, '2018-11-07 05:30:53', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (222, 26, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 38, 4, '2018-11-07 05:30:53', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (223, 26, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 38, 4, '2018-11-07 05:30:53', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (224, 26, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 38, 4, '2018-11-07 05:30:53', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (225, 26, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 38, 4, '2018-11-07 05:30:53', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (226, 27, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sangat Sesuai', 4, 16, 4, '2018-11-07 06:12:11', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (227, 27, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 16, 4, '2018-11-07 06:12:11', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (228, 27, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 16, 4, '2018-11-07 06:12:11', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (229, 27, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-07 06:12:11', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (230, 27, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 16, 4, '2018-11-07 06:12:11', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (231, 27, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 16, 4, '2018-11-07 06:12:11', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (232, 27, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 16, 4, '2018-11-07 06:12:11', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (233, 27, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 16, 4, '2018-11-07 06:12:11', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (234, 27, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2018-11-07 06:12:11', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (235, 28, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sangat Sesuai', 4, 59, 4, '2018-11-07 06:30:23', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (236, 28, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 59, 4, '2018-11-07 06:30:23', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (237, 28, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 59, 4, '2018-11-07 06:30:23', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (238, 28, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-07 06:30:23', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (239, 28, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 59, 4, '2018-11-07 06:30:23', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (240, 28, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 59, 4, '2018-11-07 06:30:23', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (241, 28, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 59, 4, '2018-11-07 06:30:23', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (242, 28, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 59, 4, '2018-11-07 06:30:23', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (243, 28, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-07 06:30:23', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (244, 29, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sangat Sesuai', 4, 59, 4, '2018-11-07 06:35:33', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (245, 29, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 59, 4, '2018-11-07 06:35:33', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (246, 29, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 59, 4, '2018-11-07 06:35:33', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (247, 29, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-07 06:35:33', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (248, 29, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 59, 4, '2018-11-07 06:35:33', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (249, 29, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 59, 4, '2018-11-07 06:35:33', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (250, 29, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 59, 4, '2018-11-07 06:35:33', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (251, 29, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 59, 4, '2018-11-07 06:35:33', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (252, 29, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-07 06:35:33', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (253, 30, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 00:41:22', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (254, 30, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 59, 4, '2018-11-08 00:41:22', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (255, 30, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 59, 4, '2018-11-08 00:41:22', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (256, 30, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-08 00:41:22', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (257, 30, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 00:41:22', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (258, 30, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 59, 4, '2018-11-08 00:41:22', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (259, 30, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 59, 4, '2018-11-08 00:41:22', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (260, 30, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 59, 4, '2018-11-08 00:41:22', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (261, 30, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 00:41:22', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (262, 31, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 00:43:46', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (263, 31, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 59, 4, '2018-11-08 00:43:46', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (264, 31, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 59, 4, '2018-11-08 00:43:46', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (265, 31, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-08 00:43:46', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (266, 31, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 00:43:46', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (267, 31, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 59, 4, '2018-11-08 00:43:46', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (268, 31, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 59, 4, '2018-11-08 00:43:46', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (269, 31, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 59, 4, '2018-11-08 00:43:46', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (270, 31, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 00:43:46', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (271, 32, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 59, 4, '2018-11-08 00:46:15', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (272, 32, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 59, 4, '2018-11-08 00:46:15', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (273, 32, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 59, 4, '2018-11-08 00:46:15', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (274, 32, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-08 00:46:15', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (275, 32, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 59, 4, '2018-11-08 00:46:15', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (276, 32, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 59, 4, '2018-11-08 00:46:15', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (277, 32, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 00:46:15', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (278, 32, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 59, 4, '2018-11-08 00:46:15', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (279, 32, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Berfungsi kurang maksimal', 3, 59, 4, '2018-11-08 00:46:15', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (280, 33, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 59, 4, '2018-11-08 00:48:46', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (281, 33, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 59, 4, '2018-11-08 00:48:46', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (282, 33, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 59, 4, '2018-11-08 00:48:46', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (283, 33, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-08 00:48:46', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (284, 33, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 59, 4, '2018-11-08 00:48:46', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (285, 33, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 59, 4, '2018-11-08 00:48:46', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (286, 33, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 00:48:46', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (287, 33, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 59, 4, '2018-11-08 00:48:46', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (288, 33, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 00:48:46', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (289, 34, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 59, 4, '2018-11-08 00:51:48', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (290, 34, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 59, 4, '2018-11-08 00:51:48', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (291, 34, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 59, 4, '2018-11-08 00:51:48', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (292, 34, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-08 00:51:48', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (293, 34, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 59, 4, '2018-11-08 00:51:48', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (294, 34, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 59, 4, '2018-11-08 00:51:48', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (295, 34, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 00:51:48', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (296, 34, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 59, 4, '2018-11-08 00:51:48', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (297, 34, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 00:51:48', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (298, 35, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 59, 4, '2018-11-08 00:54:54', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (299, 35, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 59, 4, '2018-11-08 00:54:54', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (300, 35, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 59, 4, '2018-11-08 00:54:54', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (301, 35, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-08 00:54:54', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (302, 35, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 59, 4, '2018-11-08 00:54:54', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (303, 35, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 59, 4, '2018-11-08 00:54:54', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (304, 35, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 00:54:54', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (305, 35, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 59, 4, '2018-11-08 00:54:54', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (306, 35, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 00:54:54', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (307, 36, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 16, 4, '2018-11-08 01:31:05', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (308, 36, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 16, 4, '2018-11-08 01:31:05', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (309, 36, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 16, 4, '2018-11-08 01:31:05', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (310, 36, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-08 01:31:05', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (311, 36, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 16, 4, '2018-11-08 01:31:05', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (312, 36, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 16, 4, '2018-11-08 01:31:05', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (313, 36, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 16, 4, '2018-11-08 01:31:05', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (314, 36, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 16, 4, '2018-11-08 01:31:05', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (315, 36, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 01:31:05', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (316, 37, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 16, 4, '2018-11-08 01:54:34', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (317, 37, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 16, 4, '2018-11-08 01:54:34', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (318, 37, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 16, 4, '2018-11-08 01:54:34', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (319, 37, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-08 01:54:34', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (320, 37, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 16, 4, '2018-11-08 01:54:34', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (321, 37, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 16, 4, '2018-11-08 01:54:34', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (322, 37, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 16, 4, '2018-11-08 01:54:34', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (323, 37, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 16, 4, '2018-11-08 01:54:34', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (324, 37, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 01:54:34', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (325, 38, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 61, 4, '2018-11-08 01:57:27', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (326, 38, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 61, 4, '2018-11-08 01:57:27', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (327, 38, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 61, 4, '2018-11-08 01:57:27', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (328, 38, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 61, 4, '2018-11-08 01:57:27', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (329, 38, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 61, 4, '2018-11-08 01:57:27', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (330, 38, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 61, 4, '2018-11-08 01:57:27', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (331, 38, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 61, 4, '2018-11-08 01:57:27', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (332, 38, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 61, 4, '2018-11-08 01:57:27', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (333, 38, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 61, 4, '2018-11-08 01:57:27', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (334, 39, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sangat Sesuai', 4, 16, 4, '2018-11-08 02:10:30', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (335, 39, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 16, 4, '2018-11-08 02:10:30', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (336, 39, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 16, 4, '2018-11-08 02:10:30', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (337, 39, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-08 02:10:30', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (338, 39, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 16, 4, '2018-11-08 02:10:30', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (339, 39, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 16, 4, '2018-11-08 02:10:30', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (340, 39, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 16, 4, '2018-11-08 02:10:30', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (341, 39, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 16, 4, '2018-11-08 02:10:30', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (342, 39, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 02:10:30', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (343, 40, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sangat Sesuai', 4, 17, 4, '2018-11-08 02:11:49', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (344, 40, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 17, 4, '2018-11-08 02:11:49', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (345, 40, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 17, 4, '2018-11-08 02:11:49', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (346, 40, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Murah', 3, 17, 4, '2018-11-08 02:11:49', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (347, 40, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 17, 4, '2018-11-08 02:11:49', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (348, 40, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 17, 4, '2018-11-08 02:11:49', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (349, 40, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 17, 4, '2018-11-08 02:11:49', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (350, 40, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Cukup', 2, 17, 4, '2018-11-08 02:11:49', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (351, 40, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Berfungsi kurang maksimal', 3, 17, 4, '2018-11-08 02:11:49', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (352, 41, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sangat Sesuai', 4, 16, 4, '2018-11-08 02:23:41', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (353, 41, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 16, 4, '2018-11-08 02:23:41', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (354, 41, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 16, 4, '2018-11-08 02:23:41', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (355, 41, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-08 02:23:41', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (356, 41, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 16, 4, '2018-11-08 02:23:41', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (357, 41, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 16, 4, '2018-11-08 02:23:41', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (358, 41, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 02:23:41', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (359, 41, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 16, 4, '2018-11-08 02:23:41', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (360, 41, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 02:23:41', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (361, 42, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Kurang sesuai', 2, 16, 4, '2018-11-08 02:30:01', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (362, 42, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 16, 4, '2018-11-08 02:30:01', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (363, 42, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 16, 4, '2018-11-08 02:30:01', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (364, 42, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-08 02:30:01', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (365, 42, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 16, 4, '2018-11-08 02:30:01', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (366, 42, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 16, 4, '2018-11-08 02:30:01', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (367, 42, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 02:30:01', '2019-01-01 23:39:13');
INSERT INTO `tr_respon_detail` VALUES (368, 42, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 16, 4, '2018-11-08 02:30:01', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (369, 42, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Berfungsi kurang maksimal', 3, 16, 4, '2018-11-08 02:30:01', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (370, 43, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 16, 4, '2018-11-08 02:34:04', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (371, 43, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 16, 4, '2018-11-08 02:34:04', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (372, 43, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 16, 4, '2018-11-08 02:34:04', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (373, 43, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-08 02:34:04', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (374, 43, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 16, 4, '2018-11-08 02:34:04', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (375, 43, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 16, 4, '2018-11-08 02:34:04', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (376, 43, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 02:34:04', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (377, 43, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 16, 4, '2018-11-08 02:34:04', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (378, 43, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 02:34:04', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (379, 44, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 61, 4, '2018-11-08 02:41:33', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (380, 44, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 61, 4, '2018-11-08 02:41:33', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (381, 44, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Kurang Cepat', 2, 61, 4, '2018-11-08 02:41:33', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (382, 44, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 61, 4, '2018-11-08 02:41:33', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (383, 44, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 61, 4, '2018-11-08 02:41:33', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (384, 44, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 61, 4, '2018-11-08 02:41:33', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (385, 44, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 61, 4, '2018-11-08 02:41:33', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (386, 44, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 61, 4, '2018-11-08 02:41:33', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (387, 44, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 61, 4, '2018-11-08 02:41:33', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (388, 45, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 17, 4, '2018-11-08 02:43:50', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (389, 45, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 17, 4, '2018-11-08 02:43:50', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (390, 45, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 17, 4, '2018-11-08 02:43:50', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (391, 45, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 17, 4, '2018-11-08 02:43:50', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (392, 45, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 17, 4, '2018-11-08 02:43:50', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (393, 45, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 17, 4, '2018-11-08 02:43:50', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (394, 45, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 17, 4, '2018-11-08 02:43:50', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (395, 45, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 17, 4, '2018-11-08 02:43:50', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (396, 45, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 17, 4, '2018-11-08 02:43:50', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (397, 46, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 16, 4, '2018-11-08 02:50:40', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (398, 46, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 16, 4, '2018-11-08 02:50:40', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (399, 46, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 16, 4, '2018-11-08 02:50:40', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (400, 46, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-08 02:50:40', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (401, 46, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 16, 4, '2018-11-08 02:50:40', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (402, 46, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 16, 4, '2018-11-08 02:50:40', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (403, 46, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 16, 4, '2018-11-08 02:50:40', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (404, 46, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 16, 4, '2018-11-08 02:50:40', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (405, 46, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 02:50:40', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (406, 47, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 61, 4, '2018-11-08 02:59:19', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (407, 47, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 61, 4, '2018-11-08 02:59:19', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (408, 47, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 61, 4, '2018-11-08 02:59:19', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (409, 47, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 61, 4, '2018-11-08 02:59:19', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (410, 47, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 61, 4, '2018-11-08 02:59:19', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (411, 47, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 61, 4, '2018-11-08 02:59:19', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (412, 47, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 61, 4, '2018-11-08 02:59:19', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (413, 47, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Cukup', 2, 61, 4, '2018-11-08 02:59:19', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (414, 47, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 61, 4, '2018-11-08 02:59:19', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (415, 48, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 16, 4, '2018-11-08 03:05:44', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (416, 48, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 16, 4, '2018-11-08 03:05:44', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (417, 48, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 16, 4, '2018-11-08 03:05:44', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (418, 48, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-08 03:05:44', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (419, 48, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 16, 4, '2018-11-08 03:05:44', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (420, 48, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 16, 4, '2018-11-08 03:05:44', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (421, 48, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 03:05:44', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (422, 48, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 16, 4, '2018-11-08 03:05:44', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (423, 48, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 03:05:44', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (424, 49, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 61, 4, '2018-11-08 03:26:12', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (425, 49, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 61, 4, '2018-11-08 03:26:12', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (426, 49, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 61, 4, '2018-11-08 03:26:12', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (427, 49, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 61, 4, '2018-11-08 03:26:12', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (428, 49, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 61, 4, '2018-11-08 03:26:12', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (429, 49, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 61, 4, '2018-11-08 03:26:12', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (430, 49, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 61, 4, '2018-11-08 03:26:12', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (431, 49, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 61, 4, '2018-11-08 03:26:12', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (432, 49, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 61, 4, '2018-11-08 03:26:12', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (433, 50, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 17, 4, '2018-11-08 03:27:07', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (434, 50, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 17, 4, '2018-11-08 03:27:07', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (435, 50, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 17, 4, '2018-11-08 03:27:07', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (436, 50, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 17, 4, '2018-11-08 03:27:07', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (437, 50, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 17, 4, '2018-11-08 03:27:07', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (438, 50, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 17, 4, '2018-11-08 03:27:07', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (439, 50, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 17, 4, '2018-11-08 03:27:07', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (440, 50, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 17, 4, '2018-11-08 03:27:07', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (441, 50, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 17, 4, '2018-11-08 03:27:07', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (442, 51, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 16, 4, '2018-11-08 03:32:41', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (443, 51, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 16, 4, '2018-11-08 03:32:41', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (444, 51, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Kurang Cepat', 2, 16, 4, '2018-11-08 03:32:41', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (445, 51, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-08 03:32:41', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (446, 51, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 16, 4, '2018-11-08 03:32:41', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (447, 51, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 16, 4, '2018-11-08 03:32:41', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (448, 51, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 03:32:41', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (449, 51, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 16, 4, '2018-11-08 03:32:41', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (450, 51, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 03:32:41', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (451, 52, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 61, 4, '2018-11-08 03:36:43', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (452, 52, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 61, 4, '2018-11-08 03:36:43', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (453, 52, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Kurang Cepat', 2, 61, 4, '2018-11-08 03:36:43', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (454, 52, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 61, 4, '2018-11-08 03:36:43', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (455, 52, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 61, 4, '2018-11-08 03:36:43', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (456, 52, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 61, 4, '2018-11-08 03:36:43', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (457, 52, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 61, 4, '2018-11-08 03:36:43', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (458, 52, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 61, 4, '2018-11-08 03:36:43', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (459, 52, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 61, 4, '2018-11-08 03:36:43', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (460, 53, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 16, 4, '2018-11-08 03:40:15', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (461, 53, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 16, 4, '2018-11-08 03:40:15', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (462, 53, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Kurang Cepat', 2, 16, 4, '2018-11-08 03:40:15', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (463, 53, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-08 03:40:15', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (464, 53, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 16, 4, '2018-11-08 03:40:15', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (465, 53, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 16, 4, '2018-11-08 03:40:15', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (466, 53, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 16, 4, '2018-11-08 03:40:15', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (467, 53, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Cukup', 2, 16, 4, '2018-11-08 03:40:15', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (468, 53, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Berfungsi kurang maksimal', 3, 16, 4, '2018-11-08 03:40:15', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (469, 54, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 61, 4, '2018-11-08 03:42:31', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (470, 54, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 61, 4, '2018-11-08 03:42:31', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (471, 54, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Kurang Cepat', 2, 61, 4, '2018-11-08 03:42:31', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (472, 54, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 61, 4, '2018-11-08 03:42:31', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (473, 54, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 61, 4, '2018-11-08 03:42:31', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (474, 54, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 61, 4, '2018-11-08 03:42:31', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (475, 54, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 61, 4, '2018-11-08 03:42:31', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (476, 54, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 61, 4, '2018-11-08 03:42:31', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (477, 54, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 61, 4, '2018-11-08 03:42:31', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (478, 55, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 17, 4, '2018-11-08 03:43:13', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (479, 55, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 17, 4, '2018-11-08 03:43:13', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (480, 55, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 17, 4, '2018-11-08 03:43:13', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (481, 55, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Murah', 3, 17, 4, '2018-11-08 03:43:13', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (482, 55, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 17, 4, '2018-11-08 03:43:13', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (483, 55, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 17, 4, '2018-11-08 03:43:13', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (484, 55, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 17, 4, '2018-11-08 03:43:13', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (485, 55, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Cukup', 2, 17, 4, '2018-11-08 03:43:13', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (486, 55, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 17, 4, '2018-11-08 03:43:13', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (487, 56, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 17, 4, '2018-11-08 03:47:41', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (488, 56, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 17, 4, '2018-11-08 03:47:41', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (489, 56, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 17, 4, '2018-11-08 03:47:41', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (490, 56, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Murah', 3, 17, 4, '2018-11-08 03:47:41', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (491, 56, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 17, 4, '2018-11-08 03:47:41', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (492, 56, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 17, 4, '2018-11-08 03:47:41', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (493, 56, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 17, 4, '2018-11-08 03:47:41', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (494, 56, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 17, 4, '2018-11-08 03:47:41', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (495, 56, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 17, 4, '2018-11-08 03:47:41', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (496, 59, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 04:04:43', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (497, 59, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 59, 4, '2018-11-08 04:04:43', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (498, 59, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 59, 4, '2018-11-08 04:04:43', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (499, 59, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-08 04:04:43', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (500, 59, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 04:04:43', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (501, 59, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 59, 4, '2018-11-08 04:04:43', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (502, 59, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 04:04:43', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (503, 59, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 59, 4, '2018-11-08 04:04:43', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (504, 59, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 04:04:43', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (505, 122, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 16, 4, '2018-11-08 04:09:56', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (506, 122, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 16, 4, '2018-11-08 04:09:56', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (507, 122, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 16, 4, '2018-11-08 04:09:56', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (508, 122, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-08 04:09:56', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (509, 122, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 16, 4, '2018-11-08 04:09:56', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (510, 122, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 16, 4, '2018-11-08 04:09:56', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (511, 122, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 04:09:56', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (512, 122, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 16, 4, '2018-11-08 04:09:56', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (513, 122, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 04:09:56', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (514, 162, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (515, 162, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (516, 162, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (517, 162, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (518, 162, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (519, 162, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (520, 162, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (521, 162, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (522, 162, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (523, 163, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (524, 163, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (525, 163, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (526, 163, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (527, 163, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (528, 163, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (529, 163, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (530, 163, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (531, 163, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (532, 163, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (533, 163, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (534, 163, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (535, 163, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (536, 163, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (537, 163, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (538, 163, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (539, 163, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (540, 163, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 04:12:46', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (541, 165, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 59, 4, '2018-11-08 04:12:47', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (542, 165, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 59, 4, '2018-11-08 04:12:47', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (543, 165, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 59, 4, '2018-11-08 04:12:47', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (544, 165, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-08 04:12:47', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (545, 165, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 04:12:47', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (546, 165, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 59, 4, '2018-11-08 04:12:47', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (547, 165, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 04:12:47', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (548, 165, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 59, 4, '2018-11-08 04:12:47', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (549, 165, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 04:12:47', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (550, 166, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 59, 4, '2018-11-08 04:12:53', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (551, 166, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 59, 4, '2018-11-08 04:12:53', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (552, 166, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 59, 4, '2018-11-08 04:12:53', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (553, 166, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-08 04:12:53', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (554, 166, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 04:12:53', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (555, 166, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 59, 4, '2018-11-08 04:12:53', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (556, 166, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 04:12:53', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (557, 166, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 59, 4, '2018-11-08 04:12:53', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (558, 166, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 04:12:53', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (559, 167, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sangat Sesuai', 4, 16, 4, '2018-11-08 04:19:58', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (560, 167, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 16, 4, '2018-11-08 04:19:58', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (561, 167, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 16, 4, '2018-11-08 04:19:58', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (562, 167, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-08 04:19:58', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (563, 167, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 16, 4, '2018-11-08 04:19:58', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (564, 167, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 16, 4, '2018-11-08 04:19:58', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (565, 167, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 04:19:58', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (566, 167, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 16, 4, '2018-11-08 04:19:58', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (567, 167, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 04:19:58', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (568, 168, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 59, 4, '2018-11-08 04:24:12', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (569, 168, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 59, 4, '2018-11-08 04:24:12', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (570, 168, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 59, 4, '2018-11-08 04:24:12', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (571, 168, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-08 04:24:12', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (572, 168, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 04:24:12', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (573, 168, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 59, 4, '2018-11-08 04:24:12', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (574, 168, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 59, 4, '2018-11-08 04:24:12', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (575, 168, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 59, 4, '2018-11-08 04:24:12', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (576, 168, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 04:24:12', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (577, 169, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 59, 4, '2018-11-08 04:26:18', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (578, 169, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 59, 4, '2018-11-08 04:26:18', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (579, 169, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 59, 4, '2018-11-08 04:26:18', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (580, 169, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-08 04:26:18', '2019-01-01 23:39:14');
INSERT INTO `tr_respon_detail` VALUES (581, 169, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 59, 4, '2018-11-08 04:26:18', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (582, 169, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 59, 4, '2018-11-08 04:26:18', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (583, 169, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 04:26:18', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (584, 169, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 59, 4, '2018-11-08 04:26:18', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (585, 169, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 04:26:18', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (586, 170, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 16, 4, '2018-11-08 04:44:54', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (587, 170, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 16, 4, '2018-11-08 04:44:54', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (588, 170, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 16, 4, '2018-11-08 04:44:54', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (589, 170, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-08 04:44:54', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (590, 170, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 16, 4, '2018-11-08 04:44:54', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (591, 170, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 16, 4, '2018-11-08 04:44:54', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (592, 170, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 04:44:54', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (593, 170, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 16, 4, '2018-11-08 04:44:54', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (594, 170, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 04:44:54', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (595, 171, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 04:49:57', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (596, 171, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 59, 4, '2018-11-08 04:49:57', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (597, 171, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 59, 4, '2018-11-08 04:49:57', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (598, 171, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2018-11-08 04:49:57', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (599, 171, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 59, 4, '2018-11-08 04:49:57', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (600, 171, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 59, 4, '2018-11-08 04:49:57', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (601, 171, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 04:49:57', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (602, 171, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 59, 4, '2018-11-08 04:49:57', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (603, 171, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 04:49:57', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (604, 172, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 16, 4, '2018-11-08 04:55:44', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (605, 172, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 16, 4, '2018-11-08 04:55:44', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (606, 172, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 16, 4, '2018-11-08 04:55:44', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (607, 172, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-08 04:55:44', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (608, 172, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 16, 4, '2018-11-08 04:55:44', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (609, 172, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 16, 4, '2018-11-08 04:55:44', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (610, 172, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 04:55:44', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (611, 172, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 16, 4, '2018-11-08 04:55:44', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (612, 172, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 04:55:44', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (613, 173, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 16, 4, '2018-11-08 05:02:02', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (614, 173, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 16, 4, '2018-11-08 05:02:02', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (615, 173, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 16, 4, '2018-11-08 05:02:02', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (616, 173, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-08 05:02:02', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (617, 173, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 16, 4, '2018-11-08 05:02:02', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (618, 173, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 16, 4, '2018-11-08 05:02:02', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (619, 173, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 05:02:02', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (620, 173, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 16, 4, '2018-11-08 05:02:02', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (621, 173, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 05:02:02', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (622, 174, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 16, 4, '2018-11-08 05:02:03', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (623, 174, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 16, 4, '2018-11-08 05:02:03', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (624, 174, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 16, 4, '2018-11-08 05:02:03', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (625, 174, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-08 05:02:03', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (626, 174, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 16, 4, '2018-11-08 05:02:03', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (627, 174, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 16, 4, '2018-11-08 05:02:03', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (628, 174, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 05:02:03', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (629, 174, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 16, 4, '2018-11-08 05:02:03', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (630, 174, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 05:02:03', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (631, 175, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sangat Sesuai', 4, 16, 4, '2018-11-08 05:09:45', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (632, 175, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 16, 4, '2018-11-08 05:09:45', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (633, 175, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 16, 4, '2018-11-08 05:09:45', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (634, 175, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2018-11-08 05:09:45', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (635, 175, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 16, 4, '2018-11-08 05:09:45', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (636, 175, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 16, 4, '2018-11-08 05:09:45', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (637, 175, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 05:09:45', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (638, 175, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 16, 4, '2018-11-08 05:09:45', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (639, 175, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 05:09:45', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (640, 176, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Kurang sesuai', 2, 59, 4, '2018-11-14 07:32:42', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (641, 176, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Kurang mudah', 2, 59, 4, '2018-11-14 07:32:42', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (642, 176, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 59, 4, '2018-11-14 07:32:42', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (643, 176, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Cukup Mahal', 2, 59, 4, '2018-11-14 07:32:42', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (644, 176, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 59, 4, '2018-11-14 07:32:42', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (645, 176, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 59, 4, '2018-11-14 07:32:42', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (646, 176, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 59, 4, '2018-11-14 07:32:42', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (647, 176, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Cukup', 2, 59, 4, '2018-11-14 07:32:42', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (648, 176, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Berfungsi kurang maksimal', 3, 59, 4, '2018-11-14 07:32:42', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (649, 177, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 59, 4, '2018-11-15 05:44:22', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (650, 177, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 59, 4, '2018-11-15 05:44:22', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (651, 177, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Kurang Cepat', 2, 59, 4, '2018-11-15 05:44:22', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (652, 177, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Murah', 3, 59, 4, '2018-11-15 05:44:22', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (653, 177, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 59, 4, '2018-11-15 05:44:22', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (654, 177, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 59, 4, '2018-11-15 05:44:22', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (655, 177, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 59, 4, '2018-11-15 05:44:22', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (656, 177, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Cukup', 2, 59, 4, '2018-11-15 05:44:22', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (657, 177, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Ada tetapi tidak berfungsi', 2, 59, 4, '2018-11-15 05:44:22', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (658, 178, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sangat Sesuai', 4, 16, 4, '2019-01-01 16:29:10', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (659, 178, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 16, 4, '2019-01-01 16:29:10', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (660, 178, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 16, 4, '2019-01-01 16:29:10', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (661, 178, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 16, 4, '2019-01-01 16:29:10', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (662, 178, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 16, 4, '2019-01-01 16:29:10', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (663, 178, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 16, 4, '2019-01-01 16:29:10', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (664, 178, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 16, 4, '2019-01-01 16:29:10', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (665, 178, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 16, 4, '2019-01-01 16:29:10', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (666, 178, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2019-01-01 16:29:10', '2019-01-01 23:39:15');
INSERT INTO `tr_respon_detail` VALUES (667, 179, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 16, 4, '2019-01-01 16:32:51', '2019-01-01 23:32:51');
INSERT INTO `tr_respon_detail` VALUES (668, 179, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 16, 4, '2019-01-01 16:32:51', '2019-01-01 23:32:51');
INSERT INTO `tr_respon_detail` VALUES (669, 179, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 16, 4, '2019-01-01 16:32:51', '2019-01-01 23:32:51');
INSERT INTO `tr_respon_detail` VALUES (670, 179, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Murah', 3, 16, 4, '2019-01-01 16:32:51', '2019-01-01 23:32:51');
INSERT INTO `tr_respon_detail` VALUES (671, 179, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 16, 4, '2019-01-01 16:32:51', '2019-01-01 23:32:51');
INSERT INTO `tr_respon_detail` VALUES (672, 179, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 16, 4, '2019-01-01 16:32:51', '2019-01-01 23:32:51');
INSERT INTO `tr_respon_detail` VALUES (673, 179, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 16, 4, '2019-01-01 16:32:51', '2019-01-01 23:32:51');
INSERT INTO `tr_respon_detail` VALUES (674, 179, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 16, 4, '2019-01-01 16:32:51', '2019-01-01 23:32:51');
INSERT INTO `tr_respon_detail` VALUES (675, 179, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2019-01-01 16:32:52', '2019-01-01 23:32:52');
INSERT INTO `tr_respon_detail` VALUES (676, 180, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sangat Sesuai', 4, 59, 4, '2019-01-05 02:57:38', '2019-01-05 09:57:39');
INSERT INTO `tr_respon_detail` VALUES (677, 180, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 59, 4, '2019-01-05 02:57:38', '2019-01-05 09:57:39');
INSERT INTO `tr_respon_detail` VALUES (678, 180, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 59, 4, '2019-01-05 02:57:38', '2019-01-05 09:57:39');
INSERT INTO `tr_respon_detail` VALUES (679, 180, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2019-01-05 02:57:38', '2019-01-05 09:57:39');
INSERT INTO `tr_respon_detail` VALUES (680, 180, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 59, 4, '2019-01-05 02:57:38', '2019-01-05 09:57:39');
INSERT INTO `tr_respon_detail` VALUES (681, 180, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 59, 4, '2019-01-05 02:57:38', '2019-01-05 09:57:39');
INSERT INTO `tr_respon_detail` VALUES (682, 180, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 59, 4, '2019-01-05 02:57:38', '2019-01-05 09:57:39');
INSERT INTO `tr_respon_detail` VALUES (683, 180, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 59, 4, '2019-01-05 02:57:38', '2019-01-05 09:57:39');
INSERT INTO `tr_respon_detail` VALUES (684, 180, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2019-01-05 02:57:38', '2019-01-05 09:57:39');
INSERT INTO `tr_respon_detail` VALUES (685, 181, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 59, 4, '2019-01-05 03:03:48', '2019-01-05 10:03:48');
INSERT INTO `tr_respon_detail` VALUES (686, 181, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 59, 4, '2019-01-05 03:03:48', '2019-01-05 10:03:48');
INSERT INTO `tr_respon_detail` VALUES (687, 181, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 59, 4, '2019-01-05 03:03:48', '2019-01-05 10:03:48');
INSERT INTO `tr_respon_detail` VALUES (688, 181, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2019-01-05 03:03:48', '2019-01-05 10:03:48');
INSERT INTO `tr_respon_detail` VALUES (689, 181, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 59, 4, '2019-01-05 03:03:48', '2019-01-05 10:03:48');
INSERT INTO `tr_respon_detail` VALUES (690, 181, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 59, 4, '2019-01-05 03:03:48', '2019-01-05 10:03:48');
INSERT INTO `tr_respon_detail` VALUES (691, 181, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 59, 4, '2019-01-05 03:03:48', '2019-01-05 10:03:48');
INSERT INTO `tr_respon_detail` VALUES (692, 181, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 59, 4, '2019-01-05 03:03:48', '2019-01-05 10:03:48');
INSERT INTO `tr_respon_detail` VALUES (693, 181, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2019-01-05 03:03:48', '2019-01-05 10:03:48');
INSERT INTO `tr_respon_detail` VALUES (694, 182, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 59, 4, '2019-01-09 01:57:55', '2019-01-09 08:57:58');
INSERT INTO `tr_respon_detail` VALUES (695, 182, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 59, 4, '2019-01-09 01:57:55', '2019-01-09 08:57:58');
INSERT INTO `tr_respon_detail` VALUES (696, 182, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 59, 4, '2019-01-09 01:57:55', '2019-01-09 08:57:58');
INSERT INTO `tr_respon_detail` VALUES (697, 182, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 59, 4, '2019-01-09 01:57:55', '2019-01-09 08:57:58');
INSERT INTO `tr_respon_detail` VALUES (698, 182, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 59, 4, '2019-01-09 01:57:55', '2019-01-09 08:57:58');
INSERT INTO `tr_respon_detail` VALUES (699, 182, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 59, 4, '2019-01-09 01:57:55', '2019-01-09 08:57:58');
INSERT INTO `tr_respon_detail` VALUES (700, 182, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 59, 4, '2019-01-09 01:57:55', '2019-01-09 08:57:58');
INSERT INTO `tr_respon_detail` VALUES (701, 182, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 59, 4, '2019-01-09 01:57:55', '2019-01-09 08:57:58');
INSERT INTO `tr_respon_detail` VALUES (702, 182, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 59, 4, '2019-01-09 01:57:55', '2019-01-09 08:57:58');

-- ----------------------------
-- Table structure for tr_respon_detail_copy1
-- ----------------------------
DROP TABLE IF EXISTS `tr_respon_detail_copy1`;
CREATE TABLE `tr_respon_detail_copy1`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_respon` int(11) NULL DEFAULT NULL,
  `pertanyaan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `id_unsur_skm` int(11) NULL DEFAULT NULL,
  `jawaban` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `poin` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 676 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tr_respon_detail_copy1
-- ----------------------------
INSERT INTO `tr_respon_detail_copy1` VALUES (1, 1, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Kurang sesuai', 2, 42, 4, '2018-11-01 22:10:07', '2019-01-01 23:33:10');
INSERT INTO `tr_respon_detail_copy1` VALUES (2, 1, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 42, 4, '2018-11-01 22:10:07', '2019-01-01 23:33:11');
INSERT INTO `tr_respon_detail_copy1` VALUES (3, 1, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Kurang Cepat', 2, 42, 4, '2018-11-01 22:10:07', '2019-01-01 23:33:11');
INSERT INTO `tr_respon_detail_copy1` VALUES (4, 1, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Murah', 3, 42, 4, '2018-11-01 22:10:07', '2019-01-01 23:33:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (5, 1, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Kurang Sesuai', 2, 42, 4, '2018-11-01 22:10:07', '2019-01-01 23:33:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (6, 1, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 42, 4, '2018-11-01 22:10:07', '2019-01-01 23:33:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (7, 1, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Kurang Sopan dan ramah', 2, 42, 4, '2018-11-01 22:10:07', '2019-01-01 23:33:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (8, 1, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 42, 4, '2018-11-01 22:10:07', '2019-01-01 23:33:17');
INSERT INTO `tr_respon_detail_copy1` VALUES (9, 1, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Ada tetapi tidak berfungsi', 2, 42, 4, '2018-11-01 22:10:07', '2019-01-01 23:33:16');
INSERT INTO `tr_respon_detail_copy1` VALUES (10, 2, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Kurang sesuai', 2, 42, 4, '2018-11-01 22:14:34', '2019-01-01 23:33:37');
INSERT INTO `tr_respon_detail_copy1` VALUES (11, 2, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Tidak mudah', 1, 42, 4, '2018-11-01 22:14:34', '2019-01-01 23:33:37');
INSERT INTO `tr_respon_detail_copy1` VALUES (12, 2, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 42, 4, '2018-11-01 22:14:34', '2019-01-01 23:33:38');
INSERT INTO `tr_respon_detail_copy1` VALUES (13, 2, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Gratis', 4, 42, 4, '2018-11-01 22:14:34', '2019-01-01 23:33:38');
INSERT INTO `tr_respon_detail_copy1` VALUES (14, 2, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Kurang Sesuai', 2, 42, 4, '2018-11-01 22:14:34', '2019-01-01 23:33:39');
INSERT INTO `tr_respon_detail_copy1` VALUES (15, 2, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kurang Kompeten', 2, 42, 4, '2018-11-01 22:14:34', '2019-01-01 23:33:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (16, 2, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 42, 4, '2018-11-01 22:14:34', '2019-01-01 23:33:40');
INSERT INTO `tr_respon_detail_copy1` VALUES (17, 2, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 42, 4, '2018-11-01 22:14:34', '2019-01-01 23:33:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (18, 2, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Ada tetapi tidak berfungsi', 2, 42, 4, '2018-11-01 22:14:34', '2019-01-01 23:33:44');
INSERT INTO `tr_respon_detail_copy1` VALUES (19, 3, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 42, 4, '2018-11-01 22:44:08', '2019-01-01 23:33:45');
INSERT INTO `tr_respon_detail_copy1` VALUES (20, 3, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 42, 4, '2018-11-01 22:44:08', '2019-01-01 23:33:45');
INSERT INTO `tr_respon_detail_copy1` VALUES (21, 3, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 42, 4, '2018-11-01 22:44:08', '2019-01-01 23:33:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (22, 3, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Murah', 3, 42, 4, '2018-11-01 22:44:08', '2019-01-01 23:33:47');
INSERT INTO `tr_respon_detail_copy1` VALUES (23, 3, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 42, 4, '2018-11-01 22:44:08', '2019-01-01 23:33:48');
INSERT INTO `tr_respon_detail_copy1` VALUES (24, 3, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Sangat Kompeten', 4, 42, 4, '2018-11-01 22:44:08', '2019-01-01 23:33:48');
INSERT INTO `tr_respon_detail_copy1` VALUES (25, 3, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sangat sopan dan ramah', 4, 42, 4, '2018-11-01 22:44:08', '2019-01-01 23:33:49');
INSERT INTO `tr_respon_detail_copy1` VALUES (26, 3, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 42, 4, '2018-11-01 22:44:08', '2019-01-01 23:33:51');
INSERT INTO `tr_respon_detail_copy1` VALUES (27, 3, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 42, 4, '2018-11-01 22:44:08', '2019-01-01 23:33:51');
INSERT INTO `tr_respon_detail_copy1` VALUES (28, 4, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Kurang sesuai', 2, 42, 4, '2018-11-02 00:52:35', '2019-01-01 23:33:52');
INSERT INTO `tr_respon_detail_copy1` VALUES (29, 4, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 42, 4, '2018-11-02 00:52:35', '2019-01-01 23:33:53');
INSERT INTO `tr_respon_detail_copy1` VALUES (30, 4, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Cepat', 3, 42, 4, '2018-11-02 00:52:35', '2019-01-01 23:33:54');
INSERT INTO `tr_respon_detail_copy1` VALUES (31, 4, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Murah', 3, 42, 4, '2018-11-02 00:52:35', '2019-01-01 23:33:56');
INSERT INTO `tr_respon_detail_copy1` VALUES (32, 4, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Kurang Sesuai', 2, 42, 4, '2018-11-02 00:52:35', '2019-01-01 23:33:56');
INSERT INTO `tr_respon_detail_copy1` VALUES (33, 4, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 42, 4, '2018-11-02 00:52:35', '2019-01-01 23:33:57');
INSERT INTO `tr_respon_detail_copy1` VALUES (34, 4, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 42, 4, '2018-11-02 00:52:35', '2019-01-01 23:33:58');
INSERT INTO `tr_respon_detail_copy1` VALUES (35, 4, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 42, 4, '2018-11-02 00:52:35', '2019-01-01 23:33:59');
INSERT INTO `tr_respon_detail_copy1` VALUES (36, 4, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Berfungsi kurang maksimal', 3, 42, 4, '2018-11-02 00:52:35', '2019-01-01 23:34:00');
INSERT INTO `tr_respon_detail_copy1` VALUES (37, 5, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sangat Sesuai', 4, 42, 4, '2018-11-02 02:07:26', '2019-01-01 23:34:07');
INSERT INTO `tr_respon_detail_copy1` VALUES (38, 5, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Sangat Mudah', 4, 42, 4, '2018-11-02 02:07:26', '2019-01-01 23:34:11');
INSERT INTO `tr_respon_detail_copy1` VALUES (39, 5, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 42, 4, '2018-11-02 02:07:26', '2019-01-01 23:34:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (40, 5, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Murah', 3, 42, 4, '2018-11-02 02:07:26', '2019-01-01 23:34:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (41, 5, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sesuai', 3, 42, 4, '2018-11-02 02:07:26', '2019-01-01 23:34:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (42, 5, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 42, 4, '2018-11-02 02:07:26', '2019-01-01 23:34:14');
INSERT INTO `tr_respon_detail_copy1` VALUES (43, 5, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 42, 4, '2018-11-02 02:07:26', '2019-01-01 23:34:14');
INSERT INTO `tr_respon_detail_copy1` VALUES (44, 5, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Baik', 3, 42, 4, '2018-11-02 02:07:26', '2019-01-01 23:34:15');
INSERT INTO `tr_respon_detail_copy1` VALUES (45, 5, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 42, 4, '2018-11-02 02:07:26', '2019-01-01 23:34:16');
INSERT INTO `tr_respon_detail_copy1` VALUES (46, 6, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 42, 4, '2018-11-02 02:15:37', '2018-11-02 09:15:37');
INSERT INTO `tr_respon_detail_copy1` VALUES (47, 6, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 42, 4, '2018-11-02 02:15:37', '2018-11-02 09:15:37');
INSERT INTO `tr_respon_detail_copy1` VALUES (48, 6, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 42, 4, '2018-11-02 02:15:37', '2018-11-02 09:15:37');
INSERT INTO `tr_respon_detail_copy1` VALUES (49, 6, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Murah', 3, 42, 4, '2018-11-02 02:15:37', '2018-11-02 09:15:37');
INSERT INTO `tr_respon_detail_copy1` VALUES (50, 6, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 42, 4, '2018-11-02 02:15:37', '2018-11-02 09:15:37');
INSERT INTO `tr_respon_detail_copy1` VALUES (51, 6, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 42, 4, '2018-11-02 02:15:37', '2018-11-02 09:15:37');
INSERT INTO `tr_respon_detail_copy1` VALUES (52, 6, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 42, 4, '2018-11-02 02:15:37', '2018-11-02 09:15:37');
INSERT INTO `tr_respon_detail_copy1` VALUES (53, 6, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 42, 4, '2018-11-02 02:15:37', '2018-11-02 09:15:37');
INSERT INTO `tr_respon_detail_copy1` VALUES (54, 6, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Ada tetapi tidak berfungsi', 2, 42, 4, '2018-11-02 02:15:37', '2018-11-02 09:15:37');
INSERT INTO `tr_respon_detail_copy1` VALUES (55, 7, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 42, 4, '2018-11-05 06:10:18', '2018-11-05 13:10:18');
INSERT INTO `tr_respon_detail_copy1` VALUES (56, 7, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 42, 4, '2018-11-05 06:10:18', '2018-11-05 13:10:18');
INSERT INTO `tr_respon_detail_copy1` VALUES (57, 7, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Sangat Cepat', 4, 42, 4, '2018-11-05 06:10:18', '2018-11-05 13:10:18');
INSERT INTO `tr_respon_detail_copy1` VALUES (58, 7, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 42, 4, '2018-11-05 06:10:18', '2018-11-05 13:10:18');
INSERT INTO `tr_respon_detail_copy1` VALUES (59, 7, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 42, 4, '2018-11-05 06:10:18', '2018-11-05 13:10:18');
INSERT INTO `tr_respon_detail_copy1` VALUES (60, 7, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 42, 4, '2018-11-05 06:10:18', '2018-11-05 13:10:18');
INSERT INTO `tr_respon_detail_copy1` VALUES (61, 7, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 42, 4, '2018-11-05 06:10:18', '2018-11-05 13:10:18');
INSERT INTO `tr_respon_detail_copy1` VALUES (62, 7, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 42, 4, '2018-11-05 06:10:18', '2018-11-05 13:10:18');
INSERT INTO `tr_respon_detail_copy1` VALUES (63, 7, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 42, 4, '2018-11-05 06:10:18', '2018-11-05 13:10:18');
INSERT INTO `tr_respon_detail_copy1` VALUES (64, 8, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sangat Sesuai', 4, 42, 4, '2018-11-06 16:55:30', '2018-11-06 23:55:30');
INSERT INTO `tr_respon_detail_copy1` VALUES (65, 8, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 42, 4, '2018-11-06 16:55:30', '2018-11-06 23:55:30');
INSERT INTO `tr_respon_detail_copy1` VALUES (66, 8, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Sangat Cepat', 4, 42, 4, '2018-11-06 16:55:30', '2018-11-06 23:55:30');
INSERT INTO `tr_respon_detail_copy1` VALUES (67, 8, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Murah', 3, 42, 4, '2018-11-06 16:55:30', '2018-11-06 23:55:30');
INSERT INTO `tr_respon_detail_copy1` VALUES (68, 8, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 42, 4, '2018-11-06 16:55:30', '2018-11-06 23:55:30');
INSERT INTO `tr_respon_detail_copy1` VALUES (69, 8, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 42, 4, '2018-11-06 16:55:30', '2018-11-06 23:55:30');
INSERT INTO `tr_respon_detail_copy1` VALUES (70, 8, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 42, 4, '2018-11-06 16:55:30', '2018-11-06 23:55:30');
INSERT INTO `tr_respon_detail_copy1` VALUES (71, 8, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 42, 4, '2018-11-06 16:55:30', '2018-11-06 23:55:30');
INSERT INTO `tr_respon_detail_copy1` VALUES (72, 8, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Berfungsi kurang maksimal', 3, 42, 4, '2018-11-06 16:55:30', '2018-11-06 23:55:30');
INSERT INTO `tr_respon_detail_copy1` VALUES (73, 9, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 18, 4, '2018-11-07 03:13:07', '2018-11-07 10:13:07');
INSERT INTO `tr_respon_detail_copy1` VALUES (74, 9, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 18, 4, '2018-11-07 03:13:07', '2018-11-07 10:13:07');
INSERT INTO `tr_respon_detail_copy1` VALUES (75, 9, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 18, 4, '2018-11-07 03:13:07', '2018-11-07 10:13:07');
INSERT INTO `tr_respon_detail_copy1` VALUES (76, 9, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 18, 4, '2018-11-07 03:13:07', '2018-11-07 10:13:07');
INSERT INTO `tr_respon_detail_copy1` VALUES (77, 9, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 18, 4, '2018-11-07 03:13:07', '2018-11-07 10:13:07');
INSERT INTO `tr_respon_detail_copy1` VALUES (78, 9, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 18, 4, '2018-11-07 03:13:07', '2018-11-07 10:13:07');
INSERT INTO `tr_respon_detail_copy1` VALUES (79, 9, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 18, 4, '2018-11-07 03:13:07', '2018-11-07 10:13:07');
INSERT INTO `tr_respon_detail_copy1` VALUES (80, 9, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 18, 4, '2018-11-07 03:13:07', '2018-11-07 10:13:07');
INSERT INTO `tr_respon_detail_copy1` VALUES (81, 9, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 18, 4, '2018-11-07 03:13:07', '2018-11-07 10:13:07');
INSERT INTO `tr_respon_detail_copy1` VALUES (82, 10, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 59, 4, '2018-11-07 03:18:13', '2018-11-07 10:18:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (83, 10, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 59, 4, '2018-11-07 03:18:13', '2018-11-07 10:18:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (84, 10, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 59, 4, '2018-11-07 03:18:13', '2018-11-07 10:18:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (85, 10, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-07 03:18:13', '2018-11-07 10:18:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (86, 10, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 59, 4, '2018-11-07 03:18:13', '2018-11-07 10:18:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (87, 10, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 59, 4, '2018-11-07 03:18:13', '2018-11-07 10:18:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (88, 10, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 59, 4, '2018-11-07 03:18:13', '2018-11-07 10:18:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (89, 10, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 59, 4, '2018-11-07 03:18:13', '2018-11-07 10:18:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (90, 10, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-07 03:18:13', '2018-11-07 10:18:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (91, 11, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 17, 4, '2018-11-07 03:19:04', '2018-11-07 10:19:04');
INSERT INTO `tr_respon_detail_copy1` VALUES (92, 11, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 17, 4, '2018-11-07 03:19:04', '2018-11-07 10:19:04');
INSERT INTO `tr_respon_detail_copy1` VALUES (93, 11, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Kurang Cepat', 2, 17, 4, '2018-11-07 03:19:04', '2018-11-07 10:19:04');
INSERT INTO `tr_respon_detail_copy1` VALUES (94, 11, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 17, 4, '2018-11-07 03:19:04', '2018-11-07 10:19:04');
INSERT INTO `tr_respon_detail_copy1` VALUES (95, 11, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 17, 4, '2018-11-07 03:19:04', '2018-11-07 10:19:04');
INSERT INTO `tr_respon_detail_copy1` VALUES (96, 11, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 17, 4, '2018-11-07 03:19:04', '2018-11-07 10:19:04');
INSERT INTO `tr_respon_detail_copy1` VALUES (97, 11, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 17, 4, '2018-11-07 03:19:04', '2018-11-07 10:19:04');
INSERT INTO `tr_respon_detail_copy1` VALUES (98, 11, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 17, 4, '2018-11-07 03:19:04', '2018-11-07 10:19:04');
INSERT INTO `tr_respon_detail_copy1` VALUES (99, 11, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Berfungsi kurang maksimal', 3, 17, 4, '2018-11-07 03:19:04', '2018-11-07 10:19:04');
INSERT INTO `tr_respon_detail_copy1` VALUES (100, 12, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 16, 4, '2018-11-07 03:20:21', '2018-11-07 10:20:21');
INSERT INTO `tr_respon_detail_copy1` VALUES (101, 12, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 16, 4, '2018-11-07 03:20:21', '2018-11-07 10:20:21');
INSERT INTO `tr_respon_detail_copy1` VALUES (102, 12, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 16, 4, '2018-11-07 03:20:21', '2018-11-07 10:20:21');
INSERT INTO `tr_respon_detail_copy1` VALUES (103, 12, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-07 03:20:21', '2018-11-07 10:20:21');
INSERT INTO `tr_respon_detail_copy1` VALUES (104, 12, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 16, 4, '2018-11-07 03:20:21', '2018-11-07 10:20:21');
INSERT INTO `tr_respon_detail_copy1` VALUES (105, 12, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 16, 4, '2018-11-07 03:20:21', '2018-11-07 10:20:21');
INSERT INTO `tr_respon_detail_copy1` VALUES (106, 12, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 16, 4, '2018-11-07 03:20:21', '2018-11-07 10:20:21');
INSERT INTO `tr_respon_detail_copy1` VALUES (107, 12, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 16, 4, '2018-11-07 03:20:21', '2018-11-07 10:20:21');
INSERT INTO `tr_respon_detail_copy1` VALUES (108, 12, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2018-11-07 03:20:21', '2018-11-07 10:20:21');
INSERT INTO `tr_respon_detail_copy1` VALUES (109, 13, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 16, 4, '2018-11-07 03:20:23', '2018-11-07 10:20:23');
INSERT INTO `tr_respon_detail_copy1` VALUES (110, 13, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 16, 4, '2018-11-07 03:20:23', '2018-11-07 10:20:23');
INSERT INTO `tr_respon_detail_copy1` VALUES (111, 13, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 16, 4, '2018-11-07 03:20:23', '2018-11-07 10:20:23');
INSERT INTO `tr_respon_detail_copy1` VALUES (112, 13, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-07 03:20:23', '2018-11-07 10:20:23');
INSERT INTO `tr_respon_detail_copy1` VALUES (113, 13, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 16, 4, '2018-11-07 03:20:23', '2018-11-07 10:20:23');
INSERT INTO `tr_respon_detail_copy1` VALUES (114, 13, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 16, 4, '2018-11-07 03:20:23', '2018-11-07 10:20:23');
INSERT INTO `tr_respon_detail_copy1` VALUES (115, 13, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 16, 4, '2018-11-07 03:20:23', '2018-11-07 10:20:23');
INSERT INTO `tr_respon_detail_copy1` VALUES (116, 13, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 16, 4, '2018-11-07 03:20:23', '2018-11-07 10:20:23');
INSERT INTO `tr_respon_detail_copy1` VALUES (117, 13, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2018-11-07 03:20:23', '2018-11-07 10:20:23');
INSERT INTO `tr_respon_detail_copy1` VALUES (118, 14, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 61, 4, '2018-11-07 03:22:48', '2018-11-07 10:22:48');
INSERT INTO `tr_respon_detail_copy1` VALUES (119, 14, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 61, 4, '2018-11-07 03:22:48', '2018-11-07 10:22:48');
INSERT INTO `tr_respon_detail_copy1` VALUES (120, 14, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 61, 4, '2018-11-07 03:22:48', '2018-11-07 10:22:48');
INSERT INTO `tr_respon_detail_copy1` VALUES (121, 14, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 61, 4, '2018-11-07 03:22:48', '2018-11-07 10:22:48');
INSERT INTO `tr_respon_detail_copy1` VALUES (122, 14, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 61, 4, '2018-11-07 03:22:48', '2018-11-07 10:22:48');
INSERT INTO `tr_respon_detail_copy1` VALUES (123, 14, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 61, 4, '2018-11-07 03:22:48', '2018-11-07 10:22:48');
INSERT INTO `tr_respon_detail_copy1` VALUES (124, 14, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 61, 4, '2018-11-07 03:22:48', '2018-11-07 10:22:48');
INSERT INTO `tr_respon_detail_copy1` VALUES (125, 14, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 61, 4, '2018-11-07 03:22:48', '2018-11-07 10:22:48');
INSERT INTO `tr_respon_detail_copy1` VALUES (126, 14, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 61, 4, '2018-11-07 03:22:48', '2018-11-07 10:22:48');
INSERT INTO `tr_respon_detail_copy1` VALUES (127, 15, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 14, 4, '2018-11-07 03:22:58', '2018-11-07 10:22:58');
INSERT INTO `tr_respon_detail_copy1` VALUES (128, 15, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 14, 4, '2018-11-07 03:22:58', '2018-11-07 10:22:58');
INSERT INTO `tr_respon_detail_copy1` VALUES (129, 15, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 14, 4, '2018-11-07 03:22:58', '2018-11-07 10:22:58');
INSERT INTO `tr_respon_detail_copy1` VALUES (130, 15, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 14, 4, '2018-11-07 03:22:58', '2018-11-07 10:22:58');
INSERT INTO `tr_respon_detail_copy1` VALUES (131, 15, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 14, 4, '2018-11-07 03:22:58', '2018-11-07 10:22:58');
INSERT INTO `tr_respon_detail_copy1` VALUES (132, 15, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 14, 4, '2018-11-07 03:22:58', '2018-11-07 10:22:58');
INSERT INTO `tr_respon_detail_copy1` VALUES (133, 15, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 14, 4, '2018-11-07 03:22:58', '2018-11-07 10:22:58');
INSERT INTO `tr_respon_detail_copy1` VALUES (134, 15, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Cukup', 2, 14, 4, '2018-11-07 03:22:58', '2018-11-07 10:22:58');
INSERT INTO `tr_respon_detail_copy1` VALUES (135, 15, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Berfungsi kurang maksimal', 3, 14, 4, '2018-11-07 03:22:58', '2018-11-07 10:22:58');
INSERT INTO `tr_respon_detail_copy1` VALUES (136, 16, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sangat Sesuai', 4, 38, 4, '2018-11-07 03:23:35', '2018-11-07 10:23:35');
INSERT INTO `tr_respon_detail_copy1` VALUES (137, 16, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 38, 4, '2018-11-07 03:23:35', '2018-11-07 10:23:35');
INSERT INTO `tr_respon_detail_copy1` VALUES (138, 16, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 38, 4, '2018-11-07 03:23:35', '2018-11-07 10:23:35');
INSERT INTO `tr_respon_detail_copy1` VALUES (139, 16, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 38, 4, '2018-11-07 03:23:35', '2018-11-07 10:23:35');
INSERT INTO `tr_respon_detail_copy1` VALUES (140, 16, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 38, 4, '2018-11-07 03:23:35', '2018-11-07 10:23:35');
INSERT INTO `tr_respon_detail_copy1` VALUES (141, 16, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 38, 4, '2018-11-07 03:23:35', '2018-11-07 10:23:35');
INSERT INTO `tr_respon_detail_copy1` VALUES (142, 16, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 38, 4, '2018-11-07 03:23:35', '2018-11-07 10:23:35');
INSERT INTO `tr_respon_detail_copy1` VALUES (143, 16, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 38, 4, '2018-11-07 03:23:35', '2018-11-07 10:23:35');
INSERT INTO `tr_respon_detail_copy1` VALUES (144, 16, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 38, 4, '2018-11-07 03:23:35', '2018-11-07 10:23:35');
INSERT INTO `tr_respon_detail_copy1` VALUES (145, 17, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 16, 4, '2018-11-07 03:24:31', '2018-11-07 10:24:31');
INSERT INTO `tr_respon_detail_copy1` VALUES (146, 17, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 16, 4, '2018-11-07 03:24:31', '2018-11-07 10:24:31');
INSERT INTO `tr_respon_detail_copy1` VALUES (147, 17, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Sangat Cepat', 4, 16, 4, '2018-11-07 03:24:31', '2018-11-07 10:24:31');
INSERT INTO `tr_respon_detail_copy1` VALUES (148, 17, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-07 03:24:31', '2018-11-07 10:24:31');
INSERT INTO `tr_respon_detail_copy1` VALUES (149, 17, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 16, 4, '2018-11-07 03:24:31', '2018-11-07 10:24:31');
INSERT INTO `tr_respon_detail_copy1` VALUES (150, 17, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 16, 4, '2018-11-07 03:24:31', '2018-11-07 10:24:31');
INSERT INTO `tr_respon_detail_copy1` VALUES (151, 17, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 16, 4, '2018-11-07 03:24:31', '2018-11-07 10:24:31');
INSERT INTO `tr_respon_detail_copy1` VALUES (152, 17, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 16, 4, '2018-11-07 03:24:31', '2018-11-07 10:24:31');
INSERT INTO `tr_respon_detail_copy1` VALUES (153, 17, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2018-11-07 03:24:31', '2018-11-07 10:24:31');
INSERT INTO `tr_respon_detail_copy1` VALUES (154, 18, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 61, 4, '2018-11-07 03:29:38', '2018-11-07 10:29:38');
INSERT INTO `tr_respon_detail_copy1` VALUES (155, 18, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 61, 4, '2018-11-07 03:29:38', '2018-11-07 10:29:38');
INSERT INTO `tr_respon_detail_copy1` VALUES (156, 18, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 61, 4, '2018-11-07 03:29:38', '2018-11-07 10:29:38');
INSERT INTO `tr_respon_detail_copy1` VALUES (157, 18, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 61, 4, '2018-11-07 03:29:38', '2018-11-07 10:29:38');
INSERT INTO `tr_respon_detail_copy1` VALUES (158, 18, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 61, 4, '2018-11-07 03:29:38', '2018-11-07 10:29:38');
INSERT INTO `tr_respon_detail_copy1` VALUES (159, 18, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 61, 4, '2018-11-07 03:29:38', '2018-11-07 10:29:38');
INSERT INTO `tr_respon_detail_copy1` VALUES (160, 18, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 61, 4, '2018-11-07 03:29:38', '2018-11-07 10:29:38');
INSERT INTO `tr_respon_detail_copy1` VALUES (161, 18, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 61, 4, '2018-11-07 03:29:38', '2018-11-07 10:29:38');
INSERT INTO `tr_respon_detail_copy1` VALUES (162, 18, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 61, 4, '2018-11-07 03:29:38', '2018-11-07 10:29:38');
INSERT INTO `tr_respon_detail_copy1` VALUES (163, 19, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-07 03:31:00', '2018-11-07 10:31:00');
INSERT INTO `tr_respon_detail_copy1` VALUES (164, 19, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 59, 4, '2018-11-07 03:31:00', '2018-11-07 10:31:00');
INSERT INTO `tr_respon_detail_copy1` VALUES (165, 19, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 59, 4, '2018-11-07 03:31:00', '2018-11-07 10:31:00');
INSERT INTO `tr_respon_detail_copy1` VALUES (166, 19, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Murah', 3, 59, 4, '2018-11-07 03:31:00', '2018-11-07 10:31:00');
INSERT INTO `tr_respon_detail_copy1` VALUES (167, 19, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-07 03:31:00', '2018-11-07 10:31:00');
INSERT INTO `tr_respon_detail_copy1` VALUES (168, 19, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 59, 4, '2018-11-07 03:31:00', '2018-11-07 10:31:00');
INSERT INTO `tr_respon_detail_copy1` VALUES (169, 19, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 59, 4, '2018-11-07 03:31:00', '2018-11-07 10:31:00');
INSERT INTO `tr_respon_detail_copy1` VALUES (170, 19, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 59, 4, '2018-11-07 03:31:00', '2018-11-07 10:31:00');
INSERT INTO `tr_respon_detail_copy1` VALUES (171, 19, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-07 03:31:00', '2018-11-07 10:31:00');
INSERT INTO `tr_respon_detail_copy1` VALUES (172, 20, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 59, 4, '2018-11-07 03:34:43', '2018-11-07 10:34:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (173, 20, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 59, 4, '2018-11-07 03:34:43', '2018-11-07 10:34:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (174, 20, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Sangat Cepat', 4, 59, 4, '2018-11-07 03:34:43', '2018-11-07 10:34:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (175, 20, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-07 03:34:43', '2018-11-07 10:34:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (176, 20, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-07 03:34:43', '2018-11-07 10:34:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (177, 20, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 59, 4, '2018-11-07 03:34:43', '2018-11-07 10:34:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (178, 20, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 59, 4, '2018-11-07 03:34:43', '2018-11-07 10:34:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (179, 20, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 59, 4, '2018-11-07 03:34:43', '2018-11-07 10:34:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (180, 20, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-07 03:34:43', '2018-11-07 10:34:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (181, 21, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sangat Sesuai', 4, 18, 4, '2018-11-07 03:42:35', '2018-11-07 10:42:35');
INSERT INTO `tr_respon_detail_copy1` VALUES (182, 21, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 18, 4, '2018-11-07 03:42:35', '2018-11-07 10:42:35');
INSERT INTO `tr_respon_detail_copy1` VALUES (183, 21, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Sangat Cepat', 4, 18, 4, '2018-11-07 03:42:35', '2018-11-07 10:42:35');
INSERT INTO `tr_respon_detail_copy1` VALUES (184, 21, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 18, 4, '2018-11-07 03:42:35', '2018-11-07 10:42:35');
INSERT INTO `tr_respon_detail_copy1` VALUES (185, 21, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 18, 4, '2018-11-07 03:42:35', '2018-11-07 10:42:35');
INSERT INTO `tr_respon_detail_copy1` VALUES (186, 21, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 18, 4, '2018-11-07 03:42:35', '2018-11-07 10:42:35');
INSERT INTO `tr_respon_detail_copy1` VALUES (187, 21, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 18, 4, '2018-11-07 03:42:35', '2018-11-07 10:42:35');
INSERT INTO `tr_respon_detail_copy1` VALUES (188, 21, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 18, 4, '2018-11-07 03:42:35', '2018-11-07 10:42:35');
INSERT INTO `tr_respon_detail_copy1` VALUES (189, 21, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 18, 4, '2018-11-07 03:42:35', '2018-11-07 10:42:35');
INSERT INTO `tr_respon_detail_copy1` VALUES (190, 22, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 16, 4, '2018-11-07 04:52:16', '2018-11-07 11:52:16');
INSERT INTO `tr_respon_detail_copy1` VALUES (191, 22, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 16, 4, '2018-11-07 04:52:16', '2018-11-07 11:52:16');
INSERT INTO `tr_respon_detail_copy1` VALUES (192, 22, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 16, 4, '2018-11-07 04:52:16', '2018-11-07 11:52:16');
INSERT INTO `tr_respon_detail_copy1` VALUES (193, 22, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-07 04:52:16', '2018-11-07 11:52:16');
INSERT INTO `tr_respon_detail_copy1` VALUES (194, 22, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 16, 4, '2018-11-07 04:52:16', '2018-11-07 11:52:16');
INSERT INTO `tr_respon_detail_copy1` VALUES (195, 22, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 16, 4, '2018-11-07 04:52:16', '2018-11-07 11:52:16');
INSERT INTO `tr_respon_detail_copy1` VALUES (196, 22, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 16, 4, '2018-11-07 04:52:16', '2018-11-07 11:52:16');
INSERT INTO `tr_respon_detail_copy1` VALUES (197, 22, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 16, 4, '2018-11-07 04:52:16', '2018-11-07 11:52:16');
INSERT INTO `tr_respon_detail_copy1` VALUES (198, 22, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2018-11-07 04:52:16', '2018-11-07 11:52:16');
INSERT INTO `tr_respon_detail_copy1` VALUES (199, 23, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 59, 4, '2018-11-07 05:01:09', '2018-11-07 12:01:09');
INSERT INTO `tr_respon_detail_copy1` VALUES (200, 23, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 59, 4, '2018-11-07 05:01:09', '2018-11-07 12:01:09');
INSERT INTO `tr_respon_detail_copy1` VALUES (201, 23, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 59, 4, '2018-11-07 05:01:09', '2018-11-07 12:01:09');
INSERT INTO `tr_respon_detail_copy1` VALUES (202, 23, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-07 05:01:09', '2018-11-07 12:01:09');
INSERT INTO `tr_respon_detail_copy1` VALUES (203, 23, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-07 05:01:09', '2018-11-07 12:01:09');
INSERT INTO `tr_respon_detail_copy1` VALUES (204, 23, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 59, 4, '2018-11-07 05:01:09', '2018-11-07 12:01:09');
INSERT INTO `tr_respon_detail_copy1` VALUES (205, 23, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 59, 4, '2018-11-07 05:01:09', '2018-11-07 12:01:09');
INSERT INTO `tr_respon_detail_copy1` VALUES (206, 23, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 59, 4, '2018-11-07 05:01:09', '2018-11-07 12:01:09');
INSERT INTO `tr_respon_detail_copy1` VALUES (207, 23, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-07 05:01:09', '2018-11-07 12:01:09');
INSERT INTO `tr_respon_detail_copy1` VALUES (208, 25, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 59, 4, '2018-11-07 05:18:12', '2018-11-07 12:18:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (209, 25, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 59, 4, '2018-11-07 05:18:12', '2018-11-07 12:18:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (210, 25, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Sangat Cepat', 4, 59, 4, '2018-11-07 05:18:12', '2018-11-07 12:18:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (211, 25, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-07 05:18:12', '2018-11-07 12:18:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (212, 25, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-07 05:18:12', '2018-11-07 12:18:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (213, 25, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 59, 4, '2018-11-07 05:18:12', '2018-11-07 12:18:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (214, 25, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 59, 4, '2018-11-07 05:18:12', '2018-11-07 12:18:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (215, 25, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 59, 4, '2018-11-07 05:18:12', '2018-11-07 12:18:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (216, 25, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-07 05:18:12', '2018-11-07 12:18:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (217, 26, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 38, 4, '2018-11-07 05:30:53', '2018-11-07 12:30:53');
INSERT INTO `tr_respon_detail_copy1` VALUES (218, 26, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 38, 4, '2018-11-07 05:30:53', '2018-11-07 12:30:53');
INSERT INTO `tr_respon_detail_copy1` VALUES (219, 26, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 38, 4, '2018-11-07 05:30:53', '2018-11-07 12:30:53');
INSERT INTO `tr_respon_detail_copy1` VALUES (220, 26, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 38, 4, '2018-11-07 05:30:53', '2018-11-07 12:30:53');
INSERT INTO `tr_respon_detail_copy1` VALUES (221, 26, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 38, 4, '2018-11-07 05:30:53', '2018-11-07 12:30:53');
INSERT INTO `tr_respon_detail_copy1` VALUES (222, 26, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 38, 4, '2018-11-07 05:30:53', '2018-11-07 12:30:53');
INSERT INTO `tr_respon_detail_copy1` VALUES (223, 26, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 38, 4, '2018-11-07 05:30:53', '2018-11-07 12:30:53');
INSERT INTO `tr_respon_detail_copy1` VALUES (224, 26, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 38, 4, '2018-11-07 05:30:53', '2018-11-07 12:30:53');
INSERT INTO `tr_respon_detail_copy1` VALUES (225, 26, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 38, 4, '2018-11-07 05:30:53', '2018-11-07 12:30:53');
INSERT INTO `tr_respon_detail_copy1` VALUES (226, 27, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sangat Sesuai', 4, 16, 4, '2018-11-07 06:12:11', '2018-11-07 13:12:11');
INSERT INTO `tr_respon_detail_copy1` VALUES (227, 27, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 16, 4, '2018-11-07 06:12:11', '2018-11-07 13:12:11');
INSERT INTO `tr_respon_detail_copy1` VALUES (228, 27, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 16, 4, '2018-11-07 06:12:11', '2018-11-07 13:12:11');
INSERT INTO `tr_respon_detail_copy1` VALUES (229, 27, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-07 06:12:11', '2018-11-07 13:12:11');
INSERT INTO `tr_respon_detail_copy1` VALUES (230, 27, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 16, 4, '2018-11-07 06:12:11', '2018-11-07 13:12:11');
INSERT INTO `tr_respon_detail_copy1` VALUES (231, 27, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 16, 4, '2018-11-07 06:12:11', '2018-11-07 13:12:11');
INSERT INTO `tr_respon_detail_copy1` VALUES (232, 27, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 16, 4, '2018-11-07 06:12:11', '2018-11-07 13:12:11');
INSERT INTO `tr_respon_detail_copy1` VALUES (233, 27, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 16, 4, '2018-11-07 06:12:11', '2018-11-07 13:12:11');
INSERT INTO `tr_respon_detail_copy1` VALUES (234, 27, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2018-11-07 06:12:11', '2018-11-07 13:12:11');
INSERT INTO `tr_respon_detail_copy1` VALUES (235, 28, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-07 06:30:23', '2018-11-07 13:30:23');
INSERT INTO `tr_respon_detail_copy1` VALUES (236, 28, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 59, 4, '2018-11-07 06:30:23', '2018-11-07 13:30:23');
INSERT INTO `tr_respon_detail_copy1` VALUES (237, 28, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 59, 4, '2018-11-07 06:30:23', '2018-11-07 13:30:23');
INSERT INTO `tr_respon_detail_copy1` VALUES (238, 28, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-07 06:30:23', '2018-11-07 13:30:23');
INSERT INTO `tr_respon_detail_copy1` VALUES (239, 28, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-07 06:30:23', '2018-11-07 13:30:23');
INSERT INTO `tr_respon_detail_copy1` VALUES (240, 28, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 59, 4, '2018-11-07 06:30:23', '2018-11-07 13:30:23');
INSERT INTO `tr_respon_detail_copy1` VALUES (241, 28, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 59, 4, '2018-11-07 06:30:23', '2018-11-07 13:30:23');
INSERT INTO `tr_respon_detail_copy1` VALUES (242, 28, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 59, 4, '2018-11-07 06:30:23', '2018-11-07 13:30:23');
INSERT INTO `tr_respon_detail_copy1` VALUES (243, 28, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-07 06:30:23', '2018-11-07 13:30:23');
INSERT INTO `tr_respon_detail_copy1` VALUES (244, 29, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-07 06:35:33', '2018-11-07 13:35:33');
INSERT INTO `tr_respon_detail_copy1` VALUES (245, 29, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 59, 4, '2018-11-07 06:35:33', '2018-11-07 13:35:33');
INSERT INTO `tr_respon_detail_copy1` VALUES (246, 29, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Sangat Cepat', 4, 59, 4, '2018-11-07 06:35:33', '2018-11-07 13:35:33');
INSERT INTO `tr_respon_detail_copy1` VALUES (247, 29, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-07 06:35:33', '2018-11-07 13:35:33');
INSERT INTO `tr_respon_detail_copy1` VALUES (248, 29, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-07 06:35:33', '2018-11-07 13:35:33');
INSERT INTO `tr_respon_detail_copy1` VALUES (249, 29, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 59, 4, '2018-11-07 06:35:33', '2018-11-07 13:35:33');
INSERT INTO `tr_respon_detail_copy1` VALUES (250, 29, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 59, 4, '2018-11-07 06:35:33', '2018-11-07 13:35:33');
INSERT INTO `tr_respon_detail_copy1` VALUES (251, 29, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 59, 4, '2018-11-07 06:35:33', '2018-11-07 13:35:33');
INSERT INTO `tr_respon_detail_copy1` VALUES (252, 29, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-07 06:35:33', '2018-11-07 13:35:33');
INSERT INTO `tr_respon_detail_copy1` VALUES (253, 30, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 00:41:22', '2018-11-08 07:41:22');
INSERT INTO `tr_respon_detail_copy1` VALUES (254, 30, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 59, 4, '2018-11-08 00:41:22', '2018-11-08 07:41:22');
INSERT INTO `tr_respon_detail_copy1` VALUES (255, 30, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Sangat Cepat', 4, 59, 4, '2018-11-08 00:41:22', '2018-11-08 07:41:22');
INSERT INTO `tr_respon_detail_copy1` VALUES (256, 30, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-08 00:41:22', '2018-11-08 07:41:22');
INSERT INTO `tr_respon_detail_copy1` VALUES (257, 30, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 00:41:22', '2018-11-08 07:41:22');
INSERT INTO `tr_respon_detail_copy1` VALUES (258, 30, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 59, 4, '2018-11-08 00:41:22', '2018-11-08 07:41:22');
INSERT INTO `tr_respon_detail_copy1` VALUES (259, 30, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 59, 4, '2018-11-08 00:41:22', '2018-11-08 07:41:22');
INSERT INTO `tr_respon_detail_copy1` VALUES (260, 30, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 59, 4, '2018-11-08 00:41:22', '2018-11-08 07:41:22');
INSERT INTO `tr_respon_detail_copy1` VALUES (261, 30, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 00:41:22', '2018-11-08 07:41:22');
INSERT INTO `tr_respon_detail_copy1` VALUES (262, 31, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 00:43:46', '2018-11-08 07:43:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (263, 31, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 59, 4, '2018-11-08 00:43:46', '2018-11-08 07:43:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (264, 31, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Sangat Cepat', 4, 59, 4, '2018-11-08 00:43:46', '2018-11-08 07:43:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (265, 31, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-08 00:43:46', '2018-11-08 07:43:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (266, 31, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 00:43:46', '2018-11-08 07:43:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (267, 31, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 59, 4, '2018-11-08 00:43:46', '2018-11-08 07:43:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (268, 31, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 59, 4, '2018-11-08 00:43:46', '2018-11-08 07:43:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (269, 31, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 59, 4, '2018-11-08 00:43:46', '2018-11-08 07:43:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (270, 31, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 00:43:46', '2018-11-08 07:43:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (271, 32, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 59, 4, '2018-11-08 00:46:15', '2018-11-08 07:46:15');
INSERT INTO `tr_respon_detail_copy1` VALUES (272, 32, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 59, 4, '2018-11-08 00:46:15', '2018-11-08 07:46:15');
INSERT INTO `tr_respon_detail_copy1` VALUES (273, 32, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 59, 4, '2018-11-08 00:46:15', '2018-11-08 07:46:15');
INSERT INTO `tr_respon_detail_copy1` VALUES (274, 32, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-08 00:46:15', '2018-11-08 07:46:15');
INSERT INTO `tr_respon_detail_copy1` VALUES (275, 32, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 59, 4, '2018-11-08 00:46:15', '2018-11-08 07:46:15');
INSERT INTO `tr_respon_detail_copy1` VALUES (276, 32, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 59, 4, '2018-11-08 00:46:15', '2018-11-08 07:46:15');
INSERT INTO `tr_respon_detail_copy1` VALUES (277, 32, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 00:46:15', '2018-11-08 07:46:15');
INSERT INTO `tr_respon_detail_copy1` VALUES (278, 32, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 59, 4, '2018-11-08 00:46:15', '2018-11-08 07:46:15');
INSERT INTO `tr_respon_detail_copy1` VALUES (279, 32, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Berfungsi kurang maksimal', 3, 59, 4, '2018-11-08 00:46:15', '2018-11-08 07:46:15');
INSERT INTO `tr_respon_detail_copy1` VALUES (280, 33, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 59, 4, '2018-11-08 00:48:46', '2018-11-08 07:48:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (281, 33, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 59, 4, '2018-11-08 00:48:46', '2018-11-08 07:48:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (282, 33, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 59, 4, '2018-11-08 00:48:46', '2018-11-08 07:48:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (283, 33, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-08 00:48:46', '2018-11-08 07:48:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (284, 33, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 59, 4, '2018-11-08 00:48:46', '2018-11-08 07:48:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (285, 33, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 59, 4, '2018-11-08 00:48:46', '2018-11-08 07:48:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (286, 33, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 00:48:46', '2018-11-08 07:48:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (287, 33, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 59, 4, '2018-11-08 00:48:46', '2018-11-08 07:48:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (288, 33, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 00:48:46', '2018-11-08 07:48:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (289, 34, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 59, 4, '2018-11-08 00:51:48', '2018-11-08 07:51:48');
INSERT INTO `tr_respon_detail_copy1` VALUES (290, 34, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 59, 4, '2018-11-08 00:51:48', '2018-11-08 07:51:48');
INSERT INTO `tr_respon_detail_copy1` VALUES (291, 34, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 59, 4, '2018-11-08 00:51:48', '2018-11-08 07:51:48');
INSERT INTO `tr_respon_detail_copy1` VALUES (292, 34, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-08 00:51:48', '2018-11-08 07:51:48');
INSERT INTO `tr_respon_detail_copy1` VALUES (293, 34, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 59, 4, '2018-11-08 00:51:48', '2018-11-08 07:51:48');
INSERT INTO `tr_respon_detail_copy1` VALUES (294, 34, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 59, 4, '2018-11-08 00:51:48', '2018-11-08 07:51:48');
INSERT INTO `tr_respon_detail_copy1` VALUES (295, 34, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 00:51:48', '2018-11-08 07:51:48');
INSERT INTO `tr_respon_detail_copy1` VALUES (296, 34, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 59, 4, '2018-11-08 00:51:48', '2018-11-08 07:51:48');
INSERT INTO `tr_respon_detail_copy1` VALUES (297, 34, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 00:51:48', '2018-11-08 07:51:48');
INSERT INTO `tr_respon_detail_copy1` VALUES (298, 35, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 59, 4, '2018-11-08 00:54:54', '2018-11-08 07:54:54');
INSERT INTO `tr_respon_detail_copy1` VALUES (299, 35, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 59, 4, '2018-11-08 00:54:54', '2018-11-08 07:54:54');
INSERT INTO `tr_respon_detail_copy1` VALUES (300, 35, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 59, 4, '2018-11-08 00:54:54', '2018-11-08 07:54:54');
INSERT INTO `tr_respon_detail_copy1` VALUES (301, 35, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-08 00:54:54', '2018-11-08 07:54:54');
INSERT INTO `tr_respon_detail_copy1` VALUES (302, 35, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 59, 4, '2018-11-08 00:54:54', '2018-11-08 07:54:54');
INSERT INTO `tr_respon_detail_copy1` VALUES (303, 35, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 59, 4, '2018-11-08 00:54:54', '2018-11-08 07:54:54');
INSERT INTO `tr_respon_detail_copy1` VALUES (304, 35, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 00:54:54', '2018-11-08 07:54:54');
INSERT INTO `tr_respon_detail_copy1` VALUES (305, 35, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 59, 4, '2018-11-08 00:54:54', '2018-11-08 07:54:54');
INSERT INTO `tr_respon_detail_copy1` VALUES (306, 35, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 00:54:54', '2018-11-08 07:54:54');
INSERT INTO `tr_respon_detail_copy1` VALUES (307, 36, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 01:31:05', '2018-11-08 08:31:05');
INSERT INTO `tr_respon_detail_copy1` VALUES (308, 36, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 16, 4, '2018-11-08 01:31:05', '2018-11-08 08:31:05');
INSERT INTO `tr_respon_detail_copy1` VALUES (309, 36, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 16, 4, '2018-11-08 01:31:05', '2018-11-08 08:31:05');
INSERT INTO `tr_respon_detail_copy1` VALUES (310, 36, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-08 01:31:05', '2018-11-08 08:31:05');
INSERT INTO `tr_respon_detail_copy1` VALUES (311, 36, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 01:31:05', '2018-11-08 08:31:05');
INSERT INTO `tr_respon_detail_copy1` VALUES (312, 36, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 16, 4, '2018-11-08 01:31:05', '2018-11-08 08:31:05');
INSERT INTO `tr_respon_detail_copy1` VALUES (313, 36, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 16, 4, '2018-11-08 01:31:05', '2018-11-08 08:31:05');
INSERT INTO `tr_respon_detail_copy1` VALUES (314, 36, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 16, 4, '2018-11-08 01:31:05', '2018-11-08 08:31:05');
INSERT INTO `tr_respon_detail_copy1` VALUES (315, 36, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 01:31:05', '2018-11-08 08:31:05');
INSERT INTO `tr_respon_detail_copy1` VALUES (316, 37, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 01:54:34', '2018-11-08 08:54:34');
INSERT INTO `tr_respon_detail_copy1` VALUES (317, 37, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 16, 4, '2018-11-08 01:54:34', '2018-11-08 08:54:34');
INSERT INTO `tr_respon_detail_copy1` VALUES (318, 37, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 16, 4, '2018-11-08 01:54:34', '2018-11-08 08:54:34');
INSERT INTO `tr_respon_detail_copy1` VALUES (319, 37, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-08 01:54:34', '2018-11-08 08:54:34');
INSERT INTO `tr_respon_detail_copy1` VALUES (320, 37, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 01:54:34', '2018-11-08 08:54:34');
INSERT INTO `tr_respon_detail_copy1` VALUES (321, 37, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 16, 4, '2018-11-08 01:54:34', '2018-11-08 08:54:34');
INSERT INTO `tr_respon_detail_copy1` VALUES (322, 37, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 16, 4, '2018-11-08 01:54:34', '2018-11-08 08:54:34');
INSERT INTO `tr_respon_detail_copy1` VALUES (323, 37, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 16, 4, '2018-11-08 01:54:34', '2018-11-08 08:54:34');
INSERT INTO `tr_respon_detail_copy1` VALUES (324, 37, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 01:54:34', '2018-11-08 08:54:34');
INSERT INTO `tr_respon_detail_copy1` VALUES (325, 38, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 61, 4, '2018-11-08 01:57:27', '2018-11-08 08:57:27');
INSERT INTO `tr_respon_detail_copy1` VALUES (326, 38, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 61, 4, '2018-11-08 01:57:27', '2018-11-08 08:57:27');
INSERT INTO `tr_respon_detail_copy1` VALUES (327, 38, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 61, 4, '2018-11-08 01:57:27', '2018-11-08 08:57:27');
INSERT INTO `tr_respon_detail_copy1` VALUES (328, 38, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 61, 4, '2018-11-08 01:57:27', '2018-11-08 08:57:27');
INSERT INTO `tr_respon_detail_copy1` VALUES (329, 38, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 61, 4, '2018-11-08 01:57:27', '2018-11-08 08:57:27');
INSERT INTO `tr_respon_detail_copy1` VALUES (330, 38, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 61, 4, '2018-11-08 01:57:27', '2018-11-08 08:57:27');
INSERT INTO `tr_respon_detail_copy1` VALUES (331, 38, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 61, 4, '2018-11-08 01:57:27', '2018-11-08 08:57:27');
INSERT INTO `tr_respon_detail_copy1` VALUES (332, 38, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 61, 4, '2018-11-08 01:57:27', '2018-11-08 08:57:27');
INSERT INTO `tr_respon_detail_copy1` VALUES (333, 38, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 61, 4, '2018-11-08 01:57:27', '2018-11-08 08:57:27');
INSERT INTO `tr_respon_detail_copy1` VALUES (334, 39, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sangat Sesuai', 4, 16, 4, '2018-11-08 02:10:30', '2018-11-08 09:10:30');
INSERT INTO `tr_respon_detail_copy1` VALUES (335, 39, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 16, 4, '2018-11-08 02:10:30', '2018-11-08 09:10:30');
INSERT INTO `tr_respon_detail_copy1` VALUES (336, 39, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 16, 4, '2018-11-08 02:10:30', '2018-11-08 09:10:30');
INSERT INTO `tr_respon_detail_copy1` VALUES (337, 39, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-08 02:10:30', '2018-11-08 09:10:30');
INSERT INTO `tr_respon_detail_copy1` VALUES (338, 39, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 16, 4, '2018-11-08 02:10:30', '2018-11-08 09:10:30');
INSERT INTO `tr_respon_detail_copy1` VALUES (339, 39, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 16, 4, '2018-11-08 02:10:30', '2018-11-08 09:10:30');
INSERT INTO `tr_respon_detail_copy1` VALUES (340, 39, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 16, 4, '2018-11-08 02:10:30', '2018-11-08 09:10:30');
INSERT INTO `tr_respon_detail_copy1` VALUES (341, 39, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 16, 4, '2018-11-08 02:10:30', '2018-11-08 09:10:30');
INSERT INTO `tr_respon_detail_copy1` VALUES (342, 39, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 02:10:30', '2018-11-08 09:10:30');
INSERT INTO `tr_respon_detail_copy1` VALUES (343, 40, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sangat Sesuai', 4, 17, 4, '2018-11-08 02:11:49', '2018-11-08 09:11:49');
INSERT INTO `tr_respon_detail_copy1` VALUES (344, 40, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 17, 4, '2018-11-08 02:11:49', '2018-11-08 09:11:49');
INSERT INTO `tr_respon_detail_copy1` VALUES (345, 40, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 17, 4, '2018-11-08 02:11:49', '2018-11-08 09:11:49');
INSERT INTO `tr_respon_detail_copy1` VALUES (346, 40, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Murah', 3, 17, 4, '2018-11-08 02:11:49', '2018-11-08 09:11:49');
INSERT INTO `tr_respon_detail_copy1` VALUES (347, 40, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 17, 4, '2018-11-08 02:11:49', '2018-11-08 09:11:49');
INSERT INTO `tr_respon_detail_copy1` VALUES (348, 40, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 17, 4, '2018-11-08 02:11:49', '2018-11-08 09:11:49');
INSERT INTO `tr_respon_detail_copy1` VALUES (349, 40, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 17, 4, '2018-11-08 02:11:49', '2018-11-08 09:11:49');
INSERT INTO `tr_respon_detail_copy1` VALUES (350, 40, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Cukup', 2, 17, 4, '2018-11-08 02:11:49', '2018-11-08 09:11:49');
INSERT INTO `tr_respon_detail_copy1` VALUES (351, 40, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Berfungsi kurang maksimal', 3, 17, 4, '2018-11-08 02:11:49', '2018-11-08 09:11:49');
INSERT INTO `tr_respon_detail_copy1` VALUES (352, 41, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sangat Sesuai', 4, 16, 4, '2018-11-08 02:23:41', '2018-11-08 09:23:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (353, 41, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 16, 4, '2018-11-08 02:23:41', '2018-11-08 09:23:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (354, 41, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Sangat Cepat', 4, 16, 4, '2018-11-08 02:23:41', '2018-11-08 09:23:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (355, 41, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-08 02:23:41', '2018-11-08 09:23:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (356, 41, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 02:23:41', '2018-11-08 09:23:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (357, 41, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 16, 4, '2018-11-08 02:23:41', '2018-11-08 09:23:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (358, 41, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 02:23:41', '2018-11-08 09:23:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (359, 41, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 16, 4, '2018-11-08 02:23:41', '2018-11-08 09:23:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (360, 41, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 02:23:41', '2018-11-08 09:23:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (361, 42, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Kurang sesuai', 2, 16, 4, '2018-11-08 02:30:01', '2018-11-08 09:30:01');
INSERT INTO `tr_respon_detail_copy1` VALUES (362, 42, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 16, 4, '2018-11-08 02:30:01', '2018-11-08 09:30:01');
INSERT INTO `tr_respon_detail_copy1` VALUES (363, 42, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 16, 4, '2018-11-08 02:30:01', '2018-11-08 09:30:01');
INSERT INTO `tr_respon_detail_copy1` VALUES (364, 42, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-08 02:30:01', '2018-11-08 09:30:01');
INSERT INTO `tr_respon_detail_copy1` VALUES (365, 42, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 02:30:01', '2018-11-08 09:30:01');
INSERT INTO `tr_respon_detail_copy1` VALUES (366, 42, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 16, 4, '2018-11-08 02:30:01', '2018-11-08 09:30:01');
INSERT INTO `tr_respon_detail_copy1` VALUES (367, 42, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 02:30:01', '2018-11-08 09:30:01');
INSERT INTO `tr_respon_detail_copy1` VALUES (368, 42, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 16, 4, '2018-11-08 02:30:01', '2018-11-08 09:30:01');
INSERT INTO `tr_respon_detail_copy1` VALUES (369, 42, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Berfungsi kurang maksimal', 3, 16, 4, '2018-11-08 02:30:01', '2018-11-08 09:30:01');
INSERT INTO `tr_respon_detail_copy1` VALUES (370, 43, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 02:34:04', '2018-11-08 09:34:04');
INSERT INTO `tr_respon_detail_copy1` VALUES (371, 43, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 16, 4, '2018-11-08 02:34:04', '2018-11-08 09:34:04');
INSERT INTO `tr_respon_detail_copy1` VALUES (372, 43, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 16, 4, '2018-11-08 02:34:04', '2018-11-08 09:34:04');
INSERT INTO `tr_respon_detail_copy1` VALUES (373, 43, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-08 02:34:04', '2018-11-08 09:34:04');
INSERT INTO `tr_respon_detail_copy1` VALUES (374, 43, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 02:34:04', '2018-11-08 09:34:04');
INSERT INTO `tr_respon_detail_copy1` VALUES (375, 43, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 16, 4, '2018-11-08 02:34:04', '2018-11-08 09:34:04');
INSERT INTO `tr_respon_detail_copy1` VALUES (376, 43, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 02:34:04', '2018-11-08 09:34:04');
INSERT INTO `tr_respon_detail_copy1` VALUES (377, 43, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 16, 4, '2018-11-08 02:34:04', '2018-11-08 09:34:04');
INSERT INTO `tr_respon_detail_copy1` VALUES (378, 43, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 02:34:04', '2018-11-08 09:34:04');
INSERT INTO `tr_respon_detail_copy1` VALUES (379, 44, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 61, 4, '2018-11-08 02:41:33', '2018-11-08 09:41:33');
INSERT INTO `tr_respon_detail_copy1` VALUES (380, 44, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 61, 4, '2018-11-08 02:41:33', '2018-11-08 09:41:33');
INSERT INTO `tr_respon_detail_copy1` VALUES (381, 44, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Kurang Cepat', 2, 61, 4, '2018-11-08 02:41:33', '2018-11-08 09:41:33');
INSERT INTO `tr_respon_detail_copy1` VALUES (382, 44, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 61, 4, '2018-11-08 02:41:33', '2018-11-08 09:41:33');
INSERT INTO `tr_respon_detail_copy1` VALUES (383, 44, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 61, 4, '2018-11-08 02:41:33', '2018-11-08 09:41:33');
INSERT INTO `tr_respon_detail_copy1` VALUES (384, 44, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 61, 4, '2018-11-08 02:41:33', '2018-11-08 09:41:33');
INSERT INTO `tr_respon_detail_copy1` VALUES (385, 44, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 61, 4, '2018-11-08 02:41:33', '2018-11-08 09:41:33');
INSERT INTO `tr_respon_detail_copy1` VALUES (386, 44, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 61, 4, '2018-11-08 02:41:33', '2018-11-08 09:41:33');
INSERT INTO `tr_respon_detail_copy1` VALUES (387, 44, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 61, 4, '2018-11-08 02:41:33', '2018-11-08 09:41:33');
INSERT INTO `tr_respon_detail_copy1` VALUES (388, 45, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 17, 4, '2018-11-08 02:43:50', '2018-11-08 09:43:50');
INSERT INTO `tr_respon_detail_copy1` VALUES (389, 45, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 17, 4, '2018-11-08 02:43:50', '2018-11-08 09:43:50');
INSERT INTO `tr_respon_detail_copy1` VALUES (390, 45, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 17, 4, '2018-11-08 02:43:50', '2018-11-08 09:43:50');
INSERT INTO `tr_respon_detail_copy1` VALUES (391, 45, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 17, 4, '2018-11-08 02:43:50', '2018-11-08 09:43:50');
INSERT INTO `tr_respon_detail_copy1` VALUES (392, 45, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 17, 4, '2018-11-08 02:43:50', '2018-11-08 09:43:50');
INSERT INTO `tr_respon_detail_copy1` VALUES (393, 45, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 17, 4, '2018-11-08 02:43:50', '2018-11-08 09:43:50');
INSERT INTO `tr_respon_detail_copy1` VALUES (394, 45, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 17, 4, '2018-11-08 02:43:50', '2018-11-08 09:43:50');
INSERT INTO `tr_respon_detail_copy1` VALUES (395, 45, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 17, 4, '2018-11-08 02:43:50', '2018-11-08 09:43:50');
INSERT INTO `tr_respon_detail_copy1` VALUES (396, 45, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 17, 4, '2018-11-08 02:43:50', '2018-11-08 09:43:50');
INSERT INTO `tr_respon_detail_copy1` VALUES (397, 46, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 02:50:40', '2018-11-08 09:50:40');
INSERT INTO `tr_respon_detail_copy1` VALUES (398, 46, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 16, 4, '2018-11-08 02:50:40', '2018-11-08 09:50:40');
INSERT INTO `tr_respon_detail_copy1` VALUES (399, 46, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 16, 4, '2018-11-08 02:50:40', '2018-11-08 09:50:40');
INSERT INTO `tr_respon_detail_copy1` VALUES (400, 46, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-08 02:50:40', '2018-11-08 09:50:40');
INSERT INTO `tr_respon_detail_copy1` VALUES (401, 46, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 02:50:40', '2018-11-08 09:50:40');
INSERT INTO `tr_respon_detail_copy1` VALUES (402, 46, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 16, 4, '2018-11-08 02:50:40', '2018-11-08 09:50:40');
INSERT INTO `tr_respon_detail_copy1` VALUES (403, 46, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 16, 4, '2018-11-08 02:50:40', '2018-11-08 09:50:40');
INSERT INTO `tr_respon_detail_copy1` VALUES (404, 46, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 16, 4, '2018-11-08 02:50:40', '2018-11-08 09:50:40');
INSERT INTO `tr_respon_detail_copy1` VALUES (405, 46, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 02:50:40', '2018-11-08 09:50:40');
INSERT INTO `tr_respon_detail_copy1` VALUES (406, 47, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 61, 4, '2018-11-08 02:59:19', '2018-11-08 09:59:19');
INSERT INTO `tr_respon_detail_copy1` VALUES (407, 47, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 61, 4, '2018-11-08 02:59:19', '2018-11-08 09:59:19');
INSERT INTO `tr_respon_detail_copy1` VALUES (408, 47, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 61, 4, '2018-11-08 02:59:19', '2018-11-08 09:59:19');
INSERT INTO `tr_respon_detail_copy1` VALUES (409, 47, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 61, 4, '2018-11-08 02:59:19', '2018-11-08 09:59:19');
INSERT INTO `tr_respon_detail_copy1` VALUES (410, 47, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 61, 4, '2018-11-08 02:59:19', '2018-11-08 09:59:19');
INSERT INTO `tr_respon_detail_copy1` VALUES (411, 47, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 61, 4, '2018-11-08 02:59:19', '2018-11-08 09:59:19');
INSERT INTO `tr_respon_detail_copy1` VALUES (412, 47, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 61, 4, '2018-11-08 02:59:19', '2018-11-08 09:59:19');
INSERT INTO `tr_respon_detail_copy1` VALUES (413, 47, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Cukup', 2, 61, 4, '2018-11-08 02:59:19', '2018-11-08 09:59:19');
INSERT INTO `tr_respon_detail_copy1` VALUES (414, 47, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 61, 4, '2018-11-08 02:59:19', '2018-11-08 09:59:19');
INSERT INTO `tr_respon_detail_copy1` VALUES (415, 48, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 03:05:44', '2018-11-08 10:05:44');
INSERT INTO `tr_respon_detail_copy1` VALUES (416, 48, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 16, 4, '2018-11-08 03:05:44', '2018-11-08 10:05:44');
INSERT INTO `tr_respon_detail_copy1` VALUES (417, 48, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 16, 4, '2018-11-08 03:05:44', '2018-11-08 10:05:44');
INSERT INTO `tr_respon_detail_copy1` VALUES (418, 48, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-08 03:05:44', '2018-11-08 10:05:44');
INSERT INTO `tr_respon_detail_copy1` VALUES (419, 48, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 03:05:44', '2018-11-08 10:05:44');
INSERT INTO `tr_respon_detail_copy1` VALUES (420, 48, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 16, 4, '2018-11-08 03:05:44', '2018-11-08 10:05:44');
INSERT INTO `tr_respon_detail_copy1` VALUES (421, 48, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 03:05:44', '2018-11-08 10:05:44');
INSERT INTO `tr_respon_detail_copy1` VALUES (422, 48, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 16, 4, '2018-11-08 03:05:44', '2018-11-08 10:05:44');
INSERT INTO `tr_respon_detail_copy1` VALUES (423, 48, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 03:05:44', '2018-11-08 10:05:44');
INSERT INTO `tr_respon_detail_copy1` VALUES (424, 49, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 61, 4, '2018-11-08 03:26:12', '2018-11-08 10:26:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (425, 49, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 61, 4, '2018-11-08 03:26:12', '2018-11-08 10:26:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (426, 49, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 61, 4, '2018-11-08 03:26:12', '2018-11-08 10:26:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (427, 49, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 61, 4, '2018-11-08 03:26:12', '2018-11-08 10:26:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (428, 49, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 61, 4, '2018-11-08 03:26:12', '2018-11-08 10:26:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (429, 49, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 61, 4, '2018-11-08 03:26:12', '2018-11-08 10:26:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (430, 49, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 61, 4, '2018-11-08 03:26:12', '2018-11-08 10:26:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (431, 49, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 61, 4, '2018-11-08 03:26:12', '2018-11-08 10:26:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (432, 49, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 61, 4, '2018-11-08 03:26:12', '2018-11-08 10:26:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (433, 50, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 17, 4, '2018-11-08 03:27:07', '2018-11-08 10:27:07');
INSERT INTO `tr_respon_detail_copy1` VALUES (434, 50, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 17, 4, '2018-11-08 03:27:07', '2018-11-08 10:27:07');
INSERT INTO `tr_respon_detail_copy1` VALUES (435, 50, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 17, 4, '2018-11-08 03:27:07', '2018-11-08 10:27:07');
INSERT INTO `tr_respon_detail_copy1` VALUES (436, 50, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 17, 4, '2018-11-08 03:27:07', '2018-11-08 10:27:07');
INSERT INTO `tr_respon_detail_copy1` VALUES (437, 50, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 17, 4, '2018-11-08 03:27:07', '2018-11-08 10:27:07');
INSERT INTO `tr_respon_detail_copy1` VALUES (438, 50, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 17, 4, '2018-11-08 03:27:07', '2018-11-08 10:27:07');
INSERT INTO `tr_respon_detail_copy1` VALUES (439, 50, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 17, 4, '2018-11-08 03:27:07', '2018-11-08 10:27:07');
INSERT INTO `tr_respon_detail_copy1` VALUES (440, 50, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 17, 4, '2018-11-08 03:27:07', '2018-11-08 10:27:07');
INSERT INTO `tr_respon_detail_copy1` VALUES (441, 50, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 17, 4, '2018-11-08 03:27:07', '2018-11-08 10:27:07');
INSERT INTO `tr_respon_detail_copy1` VALUES (442, 51, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 03:32:41', '2018-11-08 10:32:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (443, 51, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 16, 4, '2018-11-08 03:32:41', '2018-11-08 10:32:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (444, 51, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Kurang Cepat', 2, 16, 4, '2018-11-08 03:32:41', '2018-11-08 10:32:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (445, 51, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-08 03:32:41', '2018-11-08 10:32:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (446, 51, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 03:32:41', '2018-11-08 10:32:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (447, 51, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 16, 4, '2018-11-08 03:32:41', '2018-11-08 10:32:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (448, 51, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 03:32:41', '2018-11-08 10:32:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (449, 51, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 16, 4, '2018-11-08 03:32:41', '2018-11-08 10:32:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (450, 51, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 03:32:41', '2018-11-08 10:32:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (451, 52, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 61, 4, '2018-11-08 03:36:43', '2018-11-08 10:36:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (452, 52, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 61, 4, '2018-11-08 03:36:43', '2018-11-08 10:36:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (453, 52, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Kurang Cepat', 2, 61, 4, '2018-11-08 03:36:43', '2018-11-08 10:36:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (454, 52, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 61, 4, '2018-11-08 03:36:43', '2018-11-08 10:36:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (455, 52, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 61, 4, '2018-11-08 03:36:43', '2018-11-08 10:36:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (456, 52, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 61, 4, '2018-11-08 03:36:43', '2018-11-08 10:36:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (457, 52, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 61, 4, '2018-11-08 03:36:43', '2018-11-08 10:36:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (458, 52, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 61, 4, '2018-11-08 03:36:43', '2018-11-08 10:36:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (459, 52, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 61, 4, '2018-11-08 03:36:43', '2018-11-08 10:36:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (460, 53, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 03:40:15', '2018-11-08 10:40:15');
INSERT INTO `tr_respon_detail_copy1` VALUES (461, 53, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 16, 4, '2018-11-08 03:40:15', '2018-11-08 10:40:15');
INSERT INTO `tr_respon_detail_copy1` VALUES (462, 53, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Kurang Cepat', 2, 16, 4, '2018-11-08 03:40:15', '2018-11-08 10:40:15');
INSERT INTO `tr_respon_detail_copy1` VALUES (463, 53, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-08 03:40:15', '2018-11-08 10:40:15');
INSERT INTO `tr_respon_detail_copy1` VALUES (464, 53, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 03:40:15', '2018-11-08 10:40:15');
INSERT INTO `tr_respon_detail_copy1` VALUES (465, 53, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 16, 4, '2018-11-08 03:40:15', '2018-11-08 10:40:15');
INSERT INTO `tr_respon_detail_copy1` VALUES (466, 53, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 16, 4, '2018-11-08 03:40:15', '2018-11-08 10:40:15');
INSERT INTO `tr_respon_detail_copy1` VALUES (467, 53, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Cukup', 2, 16, 4, '2018-11-08 03:40:15', '2018-11-08 10:40:15');
INSERT INTO `tr_respon_detail_copy1` VALUES (468, 53, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Berfungsi kurang maksimal', 3, 16, 4, '2018-11-08 03:40:15', '2018-11-08 10:40:15');
INSERT INTO `tr_respon_detail_copy1` VALUES (469, 54, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 61, 4, '2018-11-08 03:42:31', '2018-11-08 10:42:31');
INSERT INTO `tr_respon_detail_copy1` VALUES (470, 54, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 61, 4, '2018-11-08 03:42:31', '2018-11-08 10:42:31');
INSERT INTO `tr_respon_detail_copy1` VALUES (471, 54, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Kurang Cepat', 2, 61, 4, '2018-11-08 03:42:31', '2018-11-08 10:42:31');
INSERT INTO `tr_respon_detail_copy1` VALUES (472, 54, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 61, 4, '2018-11-08 03:42:31', '2018-11-08 10:42:31');
INSERT INTO `tr_respon_detail_copy1` VALUES (473, 54, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 61, 4, '2018-11-08 03:42:31', '2018-11-08 10:42:31');
INSERT INTO `tr_respon_detail_copy1` VALUES (474, 54, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 61, 4, '2018-11-08 03:42:31', '2018-11-08 10:42:31');
INSERT INTO `tr_respon_detail_copy1` VALUES (475, 54, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 61, 4, '2018-11-08 03:42:31', '2018-11-08 10:42:31');
INSERT INTO `tr_respon_detail_copy1` VALUES (476, 54, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 61, 4, '2018-11-08 03:42:31', '2018-11-08 10:42:31');
INSERT INTO `tr_respon_detail_copy1` VALUES (477, 54, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 61, 4, '2018-11-08 03:42:31', '2018-11-08 10:42:31');
INSERT INTO `tr_respon_detail_copy1` VALUES (478, 55, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 17, 4, '2018-11-08 03:43:13', '2018-11-08 10:43:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (479, 55, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 17, 4, '2018-11-08 03:43:13', '2018-11-08 10:43:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (480, 55, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 17, 4, '2018-11-08 03:43:13', '2018-11-08 10:43:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (481, 55, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Murah', 3, 17, 4, '2018-11-08 03:43:13', '2018-11-08 10:43:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (482, 55, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 17, 4, '2018-11-08 03:43:13', '2018-11-08 10:43:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (483, 55, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 17, 4, '2018-11-08 03:43:13', '2018-11-08 10:43:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (484, 55, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 17, 4, '2018-11-08 03:43:13', '2018-11-08 10:43:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (485, 55, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Cukup', 2, 17, 4, '2018-11-08 03:43:13', '2018-11-08 10:43:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (486, 55, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 17, 4, '2018-11-08 03:43:13', '2018-11-08 10:43:13');
INSERT INTO `tr_respon_detail_copy1` VALUES (487, 56, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 17, 4, '2018-11-08 03:47:41', '2018-11-08 10:47:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (488, 56, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 17, 4, '2018-11-08 03:47:41', '2018-11-08 10:47:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (489, 56, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 17, 4, '2018-11-08 03:47:41', '2018-11-08 10:47:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (490, 56, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Murah', 3, 17, 4, '2018-11-08 03:47:41', '2018-11-08 10:47:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (491, 56, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 17, 4, '2018-11-08 03:47:41', '2018-11-08 10:47:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (492, 56, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 17, 4, '2018-11-08 03:47:41', '2018-11-08 10:47:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (493, 56, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 17, 4, '2018-11-08 03:47:41', '2018-11-08 10:47:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (494, 56, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 17, 4, '2018-11-08 03:47:41', '2018-11-08 10:47:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (495, 56, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 17, 4, '2018-11-08 03:47:41', '2018-11-08 10:47:41');
INSERT INTO `tr_respon_detail_copy1` VALUES (496, 59, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 04:04:43', '2018-11-08 11:04:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (497, 59, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 59, 4, '2018-11-08 04:04:43', '2018-11-08 11:04:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (498, 59, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 59, 4, '2018-11-08 04:04:43', '2018-11-08 11:04:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (499, 59, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-08 04:04:43', '2018-11-08 11:04:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (500, 59, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 04:04:43', '2018-11-08 11:04:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (501, 59, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 59, 4, '2018-11-08 04:04:43', '2018-11-08 11:04:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (502, 59, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 04:04:43', '2018-11-08 11:04:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (503, 59, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 59, 4, '2018-11-08 04:04:43', '2018-11-08 11:04:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (504, 59, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 04:04:43', '2018-11-08 11:04:43');
INSERT INTO `tr_respon_detail_copy1` VALUES (505, 122, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 04:09:56', '2018-11-08 11:09:56');
INSERT INTO `tr_respon_detail_copy1` VALUES (506, 122, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 16, 4, '2018-11-08 04:09:56', '2018-11-08 11:09:56');
INSERT INTO `tr_respon_detail_copy1` VALUES (507, 122, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 16, 4, '2018-11-08 04:09:56', '2018-11-08 11:09:56');
INSERT INTO `tr_respon_detail_copy1` VALUES (508, 122, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-08 04:09:56', '2018-11-08 11:09:56');
INSERT INTO `tr_respon_detail_copy1` VALUES (509, 122, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 04:09:56', '2018-11-08 11:09:56');
INSERT INTO `tr_respon_detail_copy1` VALUES (510, 122, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 16, 4, '2018-11-08 04:09:56', '2018-11-08 11:09:56');
INSERT INTO `tr_respon_detail_copy1` VALUES (511, 122, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 04:09:56', '2018-11-08 11:09:56');
INSERT INTO `tr_respon_detail_copy1` VALUES (512, 122, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 16, 4, '2018-11-08 04:09:56', '2018-11-08 11:09:56');
INSERT INTO `tr_respon_detail_copy1` VALUES (513, 122, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 04:09:56', '2018-11-08 11:09:56');
INSERT INTO `tr_respon_detail_copy1` VALUES (514, 162, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (515, 162, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (516, 162, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Sangat Cepat', 4, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (517, 162, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (518, 162, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (519, 162, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (520, 162, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (521, 162, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (522, 162, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (523, 163, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (524, 163, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (525, 163, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Sangat Cepat', 4, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (526, 163, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (527, 163, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (528, 163, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (529, 163, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (530, 163, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (531, 163, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (532, 163, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (533, 163, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (534, 163, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Sangat Cepat', 4, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (535, 163, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (536, 163, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (537, 163, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (538, 163, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (539, 163, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (540, 163, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 04:12:46', '2018-11-08 11:12:46');
INSERT INTO `tr_respon_detail_copy1` VALUES (541, 165, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 59, 4, '2018-11-08 04:12:47', '2018-11-08 11:12:47');
INSERT INTO `tr_respon_detail_copy1` VALUES (542, 165, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 59, 4, '2018-11-08 04:12:47', '2018-11-08 11:12:47');
INSERT INTO `tr_respon_detail_copy1` VALUES (543, 165, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Sangat Cepat', 4, 59, 4, '2018-11-08 04:12:47', '2018-11-08 11:12:47');
INSERT INTO `tr_respon_detail_copy1` VALUES (544, 165, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-08 04:12:47', '2018-11-08 11:12:47');
INSERT INTO `tr_respon_detail_copy1` VALUES (545, 165, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 04:12:47', '2018-11-08 11:12:47');
INSERT INTO `tr_respon_detail_copy1` VALUES (546, 165, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 59, 4, '2018-11-08 04:12:47', '2018-11-08 11:12:47');
INSERT INTO `tr_respon_detail_copy1` VALUES (547, 165, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 04:12:47', '2018-11-08 11:12:47');
INSERT INTO `tr_respon_detail_copy1` VALUES (548, 165, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 59, 4, '2018-11-08 04:12:47', '2018-11-08 11:12:47');
INSERT INTO `tr_respon_detail_copy1` VALUES (549, 165, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 04:12:47', '2018-11-08 11:12:47');
INSERT INTO `tr_respon_detail_copy1` VALUES (550, 166, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 59, 4, '2018-11-08 04:12:53', '2018-11-08 11:12:53');
INSERT INTO `tr_respon_detail_copy1` VALUES (551, 166, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 59, 4, '2018-11-08 04:12:53', '2018-11-08 11:12:53');
INSERT INTO `tr_respon_detail_copy1` VALUES (552, 166, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Sangat Cepat', 4, 59, 4, '2018-11-08 04:12:53', '2018-11-08 11:12:53');
INSERT INTO `tr_respon_detail_copy1` VALUES (553, 166, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-08 04:12:53', '2018-11-08 11:12:53');
INSERT INTO `tr_respon_detail_copy1` VALUES (554, 166, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 04:12:53', '2018-11-08 11:12:53');
INSERT INTO `tr_respon_detail_copy1` VALUES (555, 166, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 59, 4, '2018-11-08 04:12:53', '2018-11-08 11:12:53');
INSERT INTO `tr_respon_detail_copy1` VALUES (556, 166, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 04:12:53', '2018-11-08 11:12:53');
INSERT INTO `tr_respon_detail_copy1` VALUES (557, 166, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 59, 4, '2018-11-08 04:12:53', '2018-11-08 11:12:53');
INSERT INTO `tr_respon_detail_copy1` VALUES (558, 166, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 04:12:53', '2018-11-08 11:12:53');
INSERT INTO `tr_respon_detail_copy1` VALUES (559, 167, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sangat Sesuai', 4, 16, 4, '2018-11-08 04:19:58', '2018-11-08 11:19:58');
INSERT INTO `tr_respon_detail_copy1` VALUES (560, 167, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 16, 4, '2018-11-08 04:19:58', '2018-11-08 11:19:58');
INSERT INTO `tr_respon_detail_copy1` VALUES (561, 167, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 16, 4, '2018-11-08 04:19:58', '2018-11-08 11:19:58');
INSERT INTO `tr_respon_detail_copy1` VALUES (562, 167, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-08 04:19:58', '2018-11-08 11:19:58');
INSERT INTO `tr_respon_detail_copy1` VALUES (563, 167, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 04:19:58', '2018-11-08 11:19:58');
INSERT INTO `tr_respon_detail_copy1` VALUES (564, 167, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 16, 4, '2018-11-08 04:19:58', '2018-11-08 11:19:58');
INSERT INTO `tr_respon_detail_copy1` VALUES (565, 167, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 04:19:58', '2018-11-08 11:19:58');
INSERT INTO `tr_respon_detail_copy1` VALUES (566, 167, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 16, 4, '2018-11-08 04:19:58', '2018-11-08 11:19:58');
INSERT INTO `tr_respon_detail_copy1` VALUES (567, 167, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 04:19:58', '2018-11-08 11:19:58');
INSERT INTO `tr_respon_detail_copy1` VALUES (568, 168, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 59, 4, '2018-11-08 04:24:12', '2018-11-08 11:24:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (569, 168, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 59, 4, '2018-11-08 04:24:12', '2018-11-08 11:24:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (570, 168, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 59, 4, '2018-11-08 04:24:12', '2018-11-08 11:24:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (571, 168, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-08 04:24:12', '2018-11-08 11:24:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (572, 168, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 04:24:12', '2018-11-08 11:24:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (573, 168, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 59, 4, '2018-11-08 04:24:12', '2018-11-08 11:24:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (574, 168, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 59, 4, '2018-11-08 04:24:12', '2018-11-08 11:24:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (575, 168, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 59, 4, '2018-11-08 04:24:12', '2018-11-08 11:24:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (576, 168, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 04:24:12', '2018-11-08 11:24:12');
INSERT INTO `tr_respon_detail_copy1` VALUES (577, 169, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 59, 4, '2018-11-08 04:26:18', '2018-11-08 11:26:18');
INSERT INTO `tr_respon_detail_copy1` VALUES (578, 169, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 59, 4, '2018-11-08 04:26:18', '2018-11-08 11:26:18');
INSERT INTO `tr_respon_detail_copy1` VALUES (579, 169, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 59, 4, '2018-11-08 04:26:18', '2018-11-08 11:26:18');
INSERT INTO `tr_respon_detail_copy1` VALUES (580, 169, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-08 04:26:18', '2018-11-08 11:26:18');
INSERT INTO `tr_respon_detail_copy1` VALUES (581, 169, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 59, 4, '2018-11-08 04:26:18', '2018-11-08 11:26:18');
INSERT INTO `tr_respon_detail_copy1` VALUES (582, 169, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 59, 4, '2018-11-08 04:26:18', '2018-11-08 11:26:18');
INSERT INTO `tr_respon_detail_copy1` VALUES (583, 169, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 04:26:18', '2018-11-08 11:26:18');
INSERT INTO `tr_respon_detail_copy1` VALUES (584, 169, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 59, 4, '2018-11-08 04:26:18', '2018-11-08 11:26:18');
INSERT INTO `tr_respon_detail_copy1` VALUES (585, 169, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 04:26:18', '2018-11-08 11:26:18');
INSERT INTO `tr_respon_detail_copy1` VALUES (586, 170, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 04:44:54', '2018-11-08 11:44:54');
INSERT INTO `tr_respon_detail_copy1` VALUES (587, 170, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 16, 4, '2018-11-08 04:44:54', '2018-11-08 11:44:54');
INSERT INTO `tr_respon_detail_copy1` VALUES (588, 170, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 16, 4, '2018-11-08 04:44:54', '2018-11-08 11:44:54');
INSERT INTO `tr_respon_detail_copy1` VALUES (589, 170, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-08 04:44:54', '2018-11-08 11:44:54');
INSERT INTO `tr_respon_detail_copy1` VALUES (590, 170, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 04:44:54', '2018-11-08 11:44:54');
INSERT INTO `tr_respon_detail_copy1` VALUES (591, 170, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 16, 4, '2018-11-08 04:44:54', '2018-11-08 11:44:54');
INSERT INTO `tr_respon_detail_copy1` VALUES (592, 170, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 04:44:54', '2018-11-08 11:44:54');
INSERT INTO `tr_respon_detail_copy1` VALUES (593, 170, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 16, 4, '2018-11-08 04:44:54', '2018-11-08 11:44:54');
INSERT INTO `tr_respon_detail_copy1` VALUES (594, 170, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 04:44:54', '2018-11-08 11:44:54');
INSERT INTO `tr_respon_detail_copy1` VALUES (595, 171, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sangat Sesuai', 4, 59, 4, '2018-11-08 04:49:57', '2018-11-08 11:49:57');
INSERT INTO `tr_respon_detail_copy1` VALUES (596, 171, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 59, 4, '2018-11-08 04:49:57', '2018-11-08 11:49:57');
INSERT INTO `tr_respon_detail_copy1` VALUES (597, 171, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Sangat Cepat', 4, 59, 4, '2018-11-08 04:49:57', '2018-11-08 11:49:57');
INSERT INTO `tr_respon_detail_copy1` VALUES (598, 171, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 59, 4, '2018-11-08 04:49:57', '2018-11-08 11:49:57');
INSERT INTO `tr_respon_detail_copy1` VALUES (599, 171, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 59, 4, '2018-11-08 04:49:57', '2018-11-08 11:49:57');
INSERT INTO `tr_respon_detail_copy1` VALUES (600, 171, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 59, 4, '2018-11-08 04:49:57', '2018-11-08 11:49:57');
INSERT INTO `tr_respon_detail_copy1` VALUES (601, 171, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 59, 4, '2018-11-08 04:49:57', '2018-11-08 11:49:57');
INSERT INTO `tr_respon_detail_copy1` VALUES (602, 171, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 59, 4, '2018-11-08 04:49:57', '2018-11-08 11:49:57');
INSERT INTO `tr_respon_detail_copy1` VALUES (603, 171, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 59, 4, '2018-11-08 04:49:57', '2018-11-08 11:49:57');
INSERT INTO `tr_respon_detail_copy1` VALUES (604, 172, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 04:55:44', '2018-11-08 11:55:44');
INSERT INTO `tr_respon_detail_copy1` VALUES (605, 172, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 16, 4, '2018-11-08 04:55:44', '2018-11-08 11:55:44');
INSERT INTO `tr_respon_detail_copy1` VALUES (606, 172, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 16, 4, '2018-11-08 04:55:44', '2018-11-08 11:55:44');
INSERT INTO `tr_respon_detail_copy1` VALUES (607, 172, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-08 04:55:44', '2018-11-08 11:55:44');
INSERT INTO `tr_respon_detail_copy1` VALUES (608, 172, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 04:55:44', '2018-11-08 11:55:44');
INSERT INTO `tr_respon_detail_copy1` VALUES (609, 172, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 16, 4, '2018-11-08 04:55:44', '2018-11-08 11:55:44');
INSERT INTO `tr_respon_detail_copy1` VALUES (610, 172, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 04:55:44', '2018-11-08 11:55:44');
INSERT INTO `tr_respon_detail_copy1` VALUES (611, 172, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 16, 4, '2018-11-08 04:55:44', '2018-11-08 11:55:44');
INSERT INTO `tr_respon_detail_copy1` VALUES (612, 172, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 04:55:44', '2018-11-08 11:55:44');
INSERT INTO `tr_respon_detail_copy1` VALUES (613, 173, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 05:02:02', '2018-11-08 12:02:02');
INSERT INTO `tr_respon_detail_copy1` VALUES (614, 173, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 16, 4, '2018-11-08 05:02:02', '2018-11-08 12:02:02');
INSERT INTO `tr_respon_detail_copy1` VALUES (615, 173, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 16, 4, '2018-11-08 05:02:02', '2018-11-08 12:02:02');
INSERT INTO `tr_respon_detail_copy1` VALUES (616, 173, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-08 05:02:02', '2018-11-08 12:02:02');
INSERT INTO `tr_respon_detail_copy1` VALUES (617, 173, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 05:02:02', '2018-11-08 12:02:02');
INSERT INTO `tr_respon_detail_copy1` VALUES (618, 173, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 16, 4, '2018-11-08 05:02:02', '2018-11-08 12:02:02');
INSERT INTO `tr_respon_detail_copy1` VALUES (619, 173, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 05:02:02', '2018-11-08 12:02:02');
INSERT INTO `tr_respon_detail_copy1` VALUES (620, 173, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 16, 4, '2018-11-08 05:02:02', '2018-11-08 12:02:02');
INSERT INTO `tr_respon_detail_copy1` VALUES (621, 173, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 05:02:02', '2018-11-08 12:02:02');
INSERT INTO `tr_respon_detail_copy1` VALUES (622, 174, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 05:02:03', '2018-11-08 12:02:03');
INSERT INTO `tr_respon_detail_copy1` VALUES (623, 174, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 16, 4, '2018-11-08 05:02:03', '2018-11-08 12:02:03');
INSERT INTO `tr_respon_detail_copy1` VALUES (624, 174, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 16, 4, '2018-11-08 05:02:03', '2018-11-08 12:02:03');
INSERT INTO `tr_respon_detail_copy1` VALUES (625, 174, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-08 05:02:03', '2018-11-08 12:02:03');
INSERT INTO `tr_respon_detail_copy1` VALUES (626, 174, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 05:02:03', '2018-11-08 12:02:03');
INSERT INTO `tr_respon_detail_copy1` VALUES (627, 174, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 16, 4, '2018-11-08 05:02:03', '2018-11-08 12:02:03');
INSERT INTO `tr_respon_detail_copy1` VALUES (628, 174, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 05:02:03', '2018-11-08 12:02:03');
INSERT INTO `tr_respon_detail_copy1` VALUES (629, 174, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Baik', 3, 16, 4, '2018-11-08 05:02:03', '2018-11-08 12:02:03');
INSERT INTO `tr_respon_detail_copy1` VALUES (630, 174, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 05:02:03', '2018-11-08 12:02:03');
INSERT INTO `tr_respon_detail_copy1` VALUES (631, 175, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sangat Sesuai', 4, 16, 4, '2018-11-08 05:09:45', '2018-11-08 12:09:45');
INSERT INTO `tr_respon_detail_copy1` VALUES (632, 175, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 16, 4, '2018-11-08 05:09:45', '2018-11-08 12:09:45');
INSERT INTO `tr_respon_detail_copy1` VALUES (633, 175, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Sangat Cepat', 4, 16, 4, '2018-11-08 05:09:45', '2018-11-08 12:09:45');
INSERT INTO `tr_respon_detail_copy1` VALUES (634, 175, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2018-11-08 05:09:45', '2018-11-08 12:09:45');
INSERT INTO `tr_respon_detail_copy1` VALUES (635, 175, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 16, 4, '2018-11-08 05:09:45', '2018-11-08 12:09:45');
INSERT INTO `tr_respon_detail_copy1` VALUES (636, 175, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 16, 4, '2018-11-08 05:09:45', '2018-11-08 12:09:45');
INSERT INTO `tr_respon_detail_copy1` VALUES (637, 175, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 16, 4, '2018-11-08 05:09:45', '2018-11-08 12:09:45');
INSERT INTO `tr_respon_detail_copy1` VALUES (638, 175, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 16, 4, '2018-11-08 05:09:45', '2018-11-08 12:09:45');
INSERT INTO `tr_respon_detail_copy1` VALUES (639, 175, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2018-11-08 05:09:45', '2018-11-08 12:09:45');
INSERT INTO `tr_respon_detail_copy1` VALUES (640, 176, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Kurang sesuai', 2, 59, 4, '2018-11-14 07:32:42', '2018-11-14 14:32:42');
INSERT INTO `tr_respon_detail_copy1` VALUES (641, 176, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Kurang mudah', 2, 59, 4, '2018-11-14 07:32:42', '2018-11-14 14:32:42');
INSERT INTO `tr_respon_detail_copy1` VALUES (642, 176, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Cepat', 3, 59, 4, '2018-11-14 07:32:42', '2018-11-14 14:32:42');
INSERT INTO `tr_respon_detail_copy1` VALUES (643, 176, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Cukup Mahal', 2, 59, 4, '2018-11-14 07:32:42', '2018-11-14 14:32:42');
INSERT INTO `tr_respon_detail_copy1` VALUES (644, 176, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 59, 4, '2018-11-14 07:32:42', '2018-11-14 14:32:42');
INSERT INTO `tr_respon_detail_copy1` VALUES (645, 176, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 59, 4, '2018-11-14 07:32:42', '2018-11-14 14:32:42');
INSERT INTO `tr_respon_detail_copy1` VALUES (646, 176, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 59, 4, '2018-11-14 07:32:42', '2018-11-14 14:32:42');
INSERT INTO `tr_respon_detail_copy1` VALUES (647, 176, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Cukup', 2, 59, 4, '2018-11-14 07:32:42', '2018-11-14 14:32:42');
INSERT INTO `tr_respon_detail_copy1` VALUES (648, 176, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Berfungsi kurang maksimal', 3, 59, 4, '2018-11-14 07:32:42', '2018-11-14 14:32:42');
INSERT INTO `tr_respon_detail_copy1` VALUES (649, 177, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sesuai', 3, 59, 4, '2018-11-15 05:44:22', '2018-11-15 12:44:22');
INSERT INTO `tr_respon_detail_copy1` VALUES (650, 177, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Mudah', 3, 59, 4, '2018-11-15 05:44:22', '2018-11-15 12:44:22');
INSERT INTO `tr_respon_detail_copy1` VALUES (651, 177, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Kurang Cepat', 2, 59, 4, '2018-11-15 05:44:22', '2018-11-15 12:44:22');
INSERT INTO `tr_respon_detail_copy1` VALUES (652, 177, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Murah', 3, 59, 4, '2018-11-15 05:44:22', '2018-11-15 12:44:22');
INSERT INTO `tr_respon_detail_copy1` VALUES (653, 177, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sesuai', 3, 59, 4, '2018-11-15 05:44:22', '2018-11-15 12:44:22');
INSERT INTO `tr_respon_detail_copy1` VALUES (654, 177, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Kompeten', 3, 59, 4, '2018-11-15 05:44:22', '2018-11-15 12:44:22');
INSERT INTO `tr_respon_detail_copy1` VALUES (655, 177, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sopan dan ramah', 3, 59, 4, '2018-11-15 05:44:22', '2018-11-15 12:44:22');
INSERT INTO `tr_respon_detail_copy1` VALUES (656, 177, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Cukup', 2, 59, 4, '2018-11-15 05:44:22', '2018-11-15 12:44:22');
INSERT INTO `tr_respon_detail_copy1` VALUES (657, 177, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Ada tetapi tidak berfungsi', 2, 59, 4, '2018-11-15 05:44:22', '2018-11-15 12:44:22');
INSERT INTO `tr_respon_detail_copy1` VALUES (658, 178, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', NULL, 'Sangat Sesuai', 4, 16, 4, '2019-01-01 16:29:10', '2019-01-01 23:29:10');
INSERT INTO `tr_respon_detail_copy1` VALUES (659, 178, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', NULL, 'Sangat Mudah', 4, 16, 4, '2019-01-01 16:29:10', '2019-01-01 23:29:10');
INSERT INTO `tr_respon_detail_copy1` VALUES (660, 178, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', NULL, 'Sangat Cepat', 4, 16, 4, '2019-01-01 16:29:10', '2019-01-01 23:29:10');
INSERT INTO `tr_respon_detail_copy1` VALUES (661, 178, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', NULL, 'Gratis', 4, 16, 4, '2019-01-01 16:29:10', '2019-01-01 23:29:10');
INSERT INTO `tr_respon_detail_copy1` VALUES (662, 178, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', NULL, 'Sangat Sesuai', 4, 16, 4, '2019-01-01 16:29:10', '2019-01-01 23:29:10');
INSERT INTO `tr_respon_detail_copy1` VALUES (663, 178, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', NULL, 'Sangat Kompeten', 4, 16, 4, '2019-01-01 16:29:10', '2019-01-01 23:29:10');
INSERT INTO `tr_respon_detail_copy1` VALUES (664, 178, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', NULL, 'Sangat sopan dan ramah', 4, 16, 4, '2019-01-01 16:29:10', '2019-01-01 23:29:10');
INSERT INTO `tr_respon_detail_copy1` VALUES (665, 178, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', NULL, 'Sangat Baik', 4, 16, 4, '2019-01-01 16:29:10', '2019-01-01 23:29:10');
INSERT INTO `tr_respon_detail_copy1` VALUES (666, 178, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', NULL, 'Dikelola dengan baik', 4, 16, 4, '2019-01-01 16:29:10', '2019-01-01 23:29:10');
INSERT INTO `tr_respon_detail_copy1` VALUES (667, 179, 'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?', 1, 'Sesuai', 3, 16, 4, '2019-01-01 16:32:51', '2019-01-01 23:32:51');
INSERT INTO `tr_respon_detail_copy1` VALUES (668, 179, 'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?', 2, 'Mudah', 3, 16, 4, '2019-01-01 16:32:51', '2019-01-01 23:32:51');
INSERT INTO `tr_respon_detail_copy1` VALUES (669, 179, 'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?', 3, 'Sangat Cepat', 4, 16, 4, '2019-01-01 16:32:51', '2019-01-01 23:32:51');
INSERT INTO `tr_respon_detail_copy1` VALUES (670, 179, 'Bagaimana pendapat saudara tentang kewajaran biaya/tarif dalam pelayanan?', 4, 'Murah', 3, 16, 4, '2019-01-01 16:32:51', '2019-01-01 23:32:51');
INSERT INTO `tr_respon_detail_copy1` VALUES (671, 179, 'Bagaimana pendapat saudara tentang kesesuaian produk layanan antara yang tercantum dalam standar pelayanan dengan hasil yang diberikan?', 5, 'Sangat Sesuai', 4, 16, 4, '2019-01-01 16:32:51', '2019-01-01 23:32:51');
INSERT INTO `tr_respon_detail_copy1` VALUES (672, 179, 'Bagaimana pendapat saudara tentang kompetensi/kemampuan petugas dalam memberikan pelayanan', 6, 'Kompeten', 3, 16, 4, '2019-01-01 16:32:51', '2019-01-01 23:32:51');
INSERT INTO `tr_respon_detail_copy1` VALUES (673, 179, 'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?', 7, 'Sopan dan ramah', 3, 16, 4, '2019-01-01 16:32:51', '2019-01-01 23:32:51');
INSERT INTO `tr_respon_detail_copy1` VALUES (674, 179, 'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?', 8, 'Sangat Baik', 4, 16, 4, '2019-01-01 16:32:51', '2019-01-01 23:32:51');
INSERT INTO `tr_respon_detail_copy1` VALUES (675, 179, 'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?', 9, 'Dikelola dengan baik', 4, 16, 4, '2019-01-01 16:32:52', '2019-01-01 23:32:52');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `jenis_kelamin` int(11) NULL DEFAULT NULL,
  `id_opd` int(11) NULL DEFAULT NULL,
  `alamat` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `email` varchar(320) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `no_telp` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `tgl_lahir` date NULL DEFAULT NULL,
  `foto` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 158 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (65, 'Galih Setyo Wibowo, S.Kom', 'galihsetyo', 1, NULL, 'Dukuh Krajan, RT 05 / 02, Ds. Tegalrejo, Kec. Tegalrejo, Kab. Magelang', 'galihsetyo777@gmail.com', '087830515407', '1992-07-20', '65_galihsetyo_1538628439.jpg', '$2y$10$n4aCVhVcupTSM0oVzVn/9eeCI7wKNtqm1SMYlOy6lHp953oeMIKem', 1, 'yvJSzGit3p5yJHobronUoVR50mf7qydvLp9w4Xh2yEaXg4i2SXbD16rnHiUQ', '2018-12-30 20:06:29', '2019-01-09 04:42:49', NULL);
INSERT INTO `users` VALUES (82, 'Super Admin', 'super', NULL, NULL, NULL, '', NULL, NULL, NULL, '$2y$10$VGct8cE.quNPWBLumbhfnuso.X..Xg43Ij.Cwbzlun7uiZaVUN8bO', 2, '57RgbDo0PuYJzDCACA9a21VCAaNWhP0qL2lslebTDXEZGsmL60Kn9cPRrhTq', '2018-12-21 06:32:43', '2018-12-21 06:32:43', NULL);
INSERT INTO `users` VALUES (83, 'Admin SKM', 'OPD01', NULL, 1, NULL, '', NULL, NULL, NULL, '$2y$10$YRFaNgxXYGokeqTz1usteuuZFqHOTgRGD.vbO6QncDpm04j6pPbB.', 4, '', '2018-12-07 10:28:00', '2018-12-07 10:28:00', NULL);
INSERT INTO `users` VALUES (84, '', 'OPD02', NULL, 2, NULL, '', NULL, NULL, NULL, '$2y$10$cPIUVcZ9ohU6P4K4laNyxOZ85bmiGo4aREkYNi00I26qEchk4yUje', 4, '', '2018-11-09 02:06:23', '2018-11-09 02:06:23', NULL);
INSERT INTO `users` VALUES (85, '', 'OPD03', NULL, 3, NULL, '', NULL, NULL, NULL, '$2y$10$UwX4gD1wwLKCu34aLVatQO6E3Jn/psRimuXjxWw656ims8DVpJ6Ii', 4, '', '2018-10-28 18:27:12', '2018-10-29 01:29:23', NULL);
INSERT INTO `users` VALUES (86, '', 'OPD04', NULL, 4, NULL, '', NULL, NULL, NULL, '$2y$10$kDV4rDRVyfstM2brrH.TM.cDA8jbxbQ8xnw4n44fMd3lIZoEx365C', 4, NULL, '2018-10-28 18:27:12', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (87, '', 'OPD05', NULL, 5, NULL, '', NULL, NULL, NULL, '$2y$10$HnWctmEUS8R2lV1k8hvSm.DEDomyVowrhrAqq8o9N.UbxsFMV7.Ua', 4, NULL, '2018-10-28 18:27:12', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (88, '', 'OPD06', NULL, 6, NULL, '', NULL, NULL, NULL, '$2y$10$r7eUMQbUTsUaOCfi9grKqOt/tc1lZQQ6NiPfipdW1k1APIWaVxZOG', 4, NULL, '2018-10-28 18:27:12', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (89, '', 'OPD07', NULL, 7, NULL, '', NULL, NULL, NULL, '$2y$10$eVdVlul9gwqUTuxemcJvnuzpCBecWqWtJEuA.Jk9husuaHTyr4KwG', 4, NULL, '2018-10-28 18:27:12', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (90, '', 'OPD08', NULL, 8, NULL, '', NULL, NULL, NULL, '$2y$10$sqtcG8mx9juUh9yiq/SycuTQhEu/K5G0vKm70MZFO.cBMQk9fFHKC', 4, NULL, '2018-10-28 18:27:12', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (91, '', 'OPD09', NULL, 9, NULL, '', NULL, NULL, NULL, '$2y$10$tDebyx.RyWynB3qrn/RaVOaQ1a7wYNHJNI1FN33FWEYimsfMRO8HO', 4, NULL, '2018-10-28 18:27:12', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (92, '', 'OPD10', NULL, 10, NULL, '', NULL, NULL, NULL, '$2y$10$NAS./xSv5mdeEpgkbB0RC.WjEPOEncpnZ/r6E9mLPIkyuwd52La7u', 4, NULL, '2018-10-28 18:27:12', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (93, '', 'OPD11', NULL, 11, NULL, '', NULL, NULL, NULL, '$2y$10$hYdTaRPK6kKANeIe8B84WeMkgeGKUiMT20SzFDarMy4xZ0HkVf9hi', 4, NULL, '2018-10-28 18:27:12', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (94, '', 'OPD12', NULL, 12, NULL, '', NULL, NULL, NULL, '$2y$10$jdTzHbUXq.s1IHGdEMCRv.5xvHJyBZQAAQmlnMqLRqNt.9pf.WxAW', 4, NULL, '2018-10-28 18:27:13', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (95, '', 'OPD13', NULL, 13, NULL, '', NULL, NULL, NULL, '$2y$10$HTMqxtAjGIoLUTVjjBsXEOICJWOH9uIDeAia/qrvWcNf3DUXWNaiy', 4, NULL, '2018-10-28 18:27:13', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (96, '', 'OPD14', NULL, 14, NULL, '', NULL, NULL, NULL, '$2y$10$oyebxC0105eD4WX61Q4yEOAzl72eQvMs7lWD673XMZZhd3oGy1UPi', 4, NULL, '2018-10-28 18:27:13', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (97, '', 'OPD15', NULL, 15, NULL, '', NULL, NULL, NULL, '$2y$10$f8hpf9L5A04/w3iT1I2y9e5.oHKQcOvHDou7fg4rKxz/4J7aDH/92', 4, NULL, '2018-10-28 18:27:13', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (98, '', 'OPD16', NULL, 16, NULL, '', NULL, NULL, NULL, '$2y$10$CsxryYEdQrfcZAIvRGEgoO7d2uJ5g4mKcOFbO5QwqlLQDJsF.vGuO', 4, NULL, '2018-10-28 18:27:13', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (99, '', 'OPD17', NULL, 17, NULL, '', NULL, NULL, NULL, '$2y$10$akdzN9aMrU8DevJFJx36f.qI/ZAHM5jpGAt/eIEiy.MaMKM4wes0q', 4, NULL, '2018-10-28 18:27:13', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (100, '', 'OPD18', NULL, 18, NULL, '', NULL, NULL, NULL, '$2y$10$5t0GFPMIvs3a.7FGO5QsNuaSBafDD3oSgwyOul9OsIhrl.NObHqiW', 4, NULL, '2018-10-28 18:27:13', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (101, '', 'OPD19', NULL, 19, NULL, '', NULL, NULL, NULL, '$2y$10$/6ddZ6Ru.WJJQTrCoSXeE.ddf4Jyj1cX/O7/EHBL4YQBkQcj6lehW', 4, NULL, '2018-10-28 18:27:13', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (102, '', 'OPD20', NULL, 20, NULL, '', NULL, NULL, NULL, '$2y$10$z9L1s2v1TaSQ.T6cjXQy9OKbvoqQ2ACsD1E1DijlEl0UOYQRGkU4u', 4, NULL, '2018-10-28 18:27:13', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (103, '', 'OPD21', NULL, 21, NULL, '', NULL, NULL, NULL, '$2y$10$CltJZBm68ZiSu.e/ut6h1eQ3F8aP4IIL3KPaOeihzGztn/HX6ta3.', 4, NULL, '2018-10-28 18:27:13', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (104, '', 'OPD22', NULL, 22, NULL, '', NULL, NULL, NULL, '$2y$10$OukJ6JtTDfbM3WXxcyX2CeKS7l9zbAOUbr5SXOJ3xLxmL9m1aOY1i', 4, NULL, '2018-10-28 18:27:13', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (105, '', 'OPD23', NULL, 23, NULL, '', NULL, NULL, NULL, '$2y$10$S8oOxW2wesNsawzqAAavquTVqPlO51Lgzf1LebjMdSGKsRykjUCmy', 4, NULL, '2018-10-28 18:27:13', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (106, '', 'OPD24', NULL, 24, NULL, '', NULL, NULL, NULL, '$2y$10$ppT2Z73L6p55z3eIVIZSr./L6XvdM6qPIKjnevGCae0jGMSxZDtMC', 4, NULL, '2018-10-28 18:27:14', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (107, '', 'OPD25', NULL, 25, NULL, '', NULL, NULL, NULL, '$2y$10$zClxhlNfS2nAavpYHjbVpO1dh8xma9P42.VbaoV2i1haWIqsAKdly', 4, NULL, '2018-10-28 18:27:14', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (108, '', 'OPD26', NULL, 26, NULL, '', NULL, NULL, NULL, '$2y$10$HLD1zQwFNNIAbIasn7cY8elbG5.y6V0OqBtFurx.vZH/XaLWLBm1S', 4, NULL, '2018-10-28 18:27:14', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (109, '', 'OPD27', NULL, 27, NULL, '', NULL, NULL, NULL, '$2y$10$T3iuzOHIi2GHp6cHO6.dFezhl0CPCrhZQiqCVhAhc5e4FZ/FyaBQO', 4, NULL, '2018-10-28 18:27:14', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (110, '', 'OPD28', NULL, 28, NULL, '', NULL, NULL, NULL, '$2y$10$NE1CGlmhqspy.CloKleFaeRHLep8e5zYEjHbalRfHIymSod81EHfG', 4, NULL, '2018-10-28 18:27:14', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (111, '', 'OPD29', NULL, 29, NULL, '', NULL, NULL, NULL, '$2y$10$uhWg/62eyr3v69b32LoLbudxNx7iGTTZq/onZONX7sfM7Iehh63yK', 4, NULL, '2018-10-28 18:27:14', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (112, '', 'OPD30', NULL, 30, NULL, '', NULL, NULL, NULL, '$2y$10$AeNC./fzI01YOdrfLabQbeXISEQ29kh3TT/eCUyzAgjCcG0C4EACe', 4, NULL, '2018-10-28 18:27:14', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (113, '', 'OPD31', NULL, 31, NULL, '', NULL, NULL, NULL, '$2y$10$5kq.bpYQegORyTaQk6TTgO./XlrDOkL2pV.AwTSfDlk8bnRZ2cze2', 4, NULL, '2018-10-28 18:27:14', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (114, '', 'OPD32', NULL, 32, NULL, '', NULL, NULL, NULL, '$2y$10$QJKwXQ9QcFGqx8OWqYfsz.hZd2J2Glum0r0pMqb5H4FqVw/hW/EaC', 4, NULL, '2018-10-28 18:27:14', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (115, '', 'OPD33', NULL, 33, NULL, '', NULL, NULL, NULL, '$2y$10$ABp65RcH8RGaHPxhfSJSheBFqSOU7LxB9ZXeWzG5cYXEW8/aiaJGu', 4, NULL, '2018-10-28 18:27:14', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (116, '', 'OPD34', NULL, 34, NULL, '', NULL, NULL, NULL, '$2y$10$zKzskKAXyMaSzGo8Chdzs.vWrauIFCu0YqVrmbi6.Y3nf3D1ZhZji', 4, NULL, '2018-10-28 18:27:14', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (117, '', 'OPD35', NULL, 35, NULL, '', NULL, NULL, NULL, '$2y$10$/AOVsdadBQYa2AImzeqsHOquKNcS9CeDWtvRPK9XOs74VabYHDh1.', 4, NULL, '2018-10-28 18:27:14', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (118, '', 'OPD36', NULL, 36, NULL, '', NULL, NULL, NULL, '$2y$10$jm.R7vHh7tMTg8kZJH5Zm.ODwPs4.Txb1myoFY5trhc0X3UdjvV1a', 4, NULL, '2018-10-28 18:27:14', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (119, '', 'OPD37', NULL, 37, NULL, '', NULL, NULL, NULL, '$2y$10$BG3IjlgXSjLcepl7N8g8xeqhZ/cu50Bkt0vbfiBm2LTgEWn6N1KM.', 4, NULL, '2018-10-28 18:27:15', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (120, '', 'OPD38', NULL, 38, NULL, '', NULL, NULL, NULL, '$2y$10$ABFgYX3HCzgNh.96L3LgAeo9fzGVRFjC6n65brYzCOrMeotPlJ97m', 4, NULL, '2018-10-28 18:27:15', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (121, '', 'OPD39', NULL, 39, NULL, '', NULL, NULL, NULL, '$2y$10$zPl7ZyTMlXSG28SYlZJJ.OmYJxXO.mid47CJNXx48MIND7AzYCO5O', 4, NULL, '2018-10-28 18:27:15', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (122, '', 'OPD40', NULL, 40, NULL, '', NULL, NULL, NULL, '$2y$10$I5tpIkcwuG1pz6aKjaYgqeDjLzwL5dwysF7plPn1DAKtUY9lDUijK', 4, NULL, '2018-10-28 18:27:15', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (123, '', 'OPD41', NULL, 41, NULL, '', NULL, NULL, NULL, '$2y$10$js8hBOyhMgY2Jo0jGesOpelNQuacpgjJIqC3AUbnzDiXnMHriXusC', 4, NULL, '2018-10-28 18:27:15', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (124, '', 'OPD42', NULL, 42, NULL, '', NULL, NULL, NULL, '$2y$10$Jyb2LNlaKEpHhYDgrWYz4OKLb5XU1Ipy4NXl6Phm5hXJ6xQQDveL2', 4, 'S3MCJz9lv1qohLcSy8B91wVnIMfRjD8IM4GN5eASIlmkRfjIHu8WUYgaJjZN', '2018-10-28 18:27:15', '2018-11-06 07:16:58', NULL);
INSERT INTO `users` VALUES (125, '', 'OPD43', NULL, 43, NULL, '', NULL, NULL, NULL, '$2y$10$mG4B2JOfpODbQ3tsi./oq.Y3TyTZ4Q7uPJQOY.gTlrbwz7q3kgldW', 4, '7GPkROHoh6pfJBvAGs154W5urgCe6ooch4mZSxaXicP6deyk8NC4ygInFHiQ', '2018-10-28 18:27:15', '2018-11-01 06:21:31', NULL);
INSERT INTO `users` VALUES (126, '', 'OPD44', NULL, 44, NULL, '', NULL, NULL, NULL, '$2y$10$qpNtcc8O5j1HGM3VdyN1NeXDvX6CL9sfOMJKlJYNSkR5SJkYqdb06', 4, NULL, '2018-10-28 18:27:15', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (127, '', 'OPD45', NULL, 45, NULL, '', NULL, NULL, NULL, '$2y$10$fB9Q/6xy1Cy.l8u4xoQCV.J65EhRZxCLZbHSjTqsT5YpOqd7R6fG.', 4, NULL, '2018-10-28 18:27:15', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (128, '', 'OPD46', NULL, 46, NULL, '', NULL, NULL, NULL, '$2y$10$jK3a3VcF1bMIPvhmtqntDu1R5lkr7eo0vUXXXUuv/4Le0AXJ56WiS', 4, NULL, '2018-10-28 18:27:15', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (129, '', 'OPD47', NULL, 47, NULL, '', NULL, NULL, NULL, '$2y$10$U4k5rinn6/XzIBanMtvQYu/Q5TbD3dFipxj7v8NX0suD88/6FNRJu', 4, NULL, '2018-10-28 18:27:15', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (130, '', 'OPD48', NULL, 48, NULL, '', NULL, NULL, NULL, '$2y$10$mwZ0w3hWt0/MnbEB4SIEfeZ8m9d.f0Ill.dL6R8tj5nojSjZUg7Cm', 4, NULL, '2018-10-28 18:27:15', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (131, '', 'OPD49', NULL, 49, NULL, '', NULL, NULL, NULL, '$2y$10$js2E9KTMK6EKlI8beLa7.OKIb4gEiycLGcJG4/nNuxItiN1kWrioy', 4, NULL, '2018-10-28 18:27:16', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (132, '', 'OPD50', NULL, 50, NULL, '', NULL, NULL, NULL, '$2y$10$t3jNZkHkpeg6pMXgLefi2.qQry.8BAifazo4YcH.1HU4CXvK/Y9EK', 4, NULL, '2018-10-28 18:27:16', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (133, '', 'OPD51', NULL, 51, NULL, '', NULL, NULL, NULL, '$2y$10$x/DJkbttBvCx1D6aGx9/X.48T8qKBdPwLBJvyKputtWb5H0i7B7wy', 4, NULL, '2018-10-28 18:27:16', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (134, '', 'OPD52', NULL, 52, NULL, '', NULL, NULL, NULL, '$2y$10$pphDTTWD/NCnY/V4bRfhHeZy.tKDrxciPIX8BIe5WDSg6grkqVoR6', 4, NULL, '2018-10-28 18:27:16', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (135, '', 'OPD53', NULL, 53, NULL, '', NULL, NULL, NULL, '$2y$10$NgpY.4GNlOG0VCtjrltTsOkzYo6NWZdXZgAaUdcxNEYFD.HXza4MS', 4, NULL, '2018-10-28 18:27:16', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (136, '', 'OPD54', NULL, 54, NULL, '', NULL, NULL, NULL, '$2y$10$JYovqAccAilpjbG0vl2l6u7/IE8eVSDe3PMT4LI1JCZUacFQ0oqvy', 4, NULL, '2018-10-28 18:27:16', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (137, '', 'OPD55', NULL, 55, NULL, '', NULL, NULL, NULL, '$2y$10$kGGD52euGJB9Lj9f5C8eCesE3OYqPBbVf7RqlrfDtwkbP1kOChMZK', 4, NULL, '2018-10-28 18:27:16', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (138, '', 'OPD56', NULL, 56, NULL, '', NULL, NULL, NULL, '$2y$10$SEKob4Hs3.bfginTub2h9OrplSoKcf0OMae80aPBNJt8xPN/2jy8.', 4, NULL, '2018-10-28 18:27:16', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (139, '', 'OPD57', NULL, 57, NULL, '', NULL, NULL, NULL, '$2y$10$88kNJezIbwRdpW0m5ApaNuxg5Ug3krwTZdd5bo6fOYNvJHmwRO53W', 4, NULL, '2018-10-28 18:27:16', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (140, '', 'OPD58', NULL, 58, NULL, '', NULL, NULL, NULL, '$2y$10$Rj4q.hfpOStvG3iKofj9IOyLO0/SuARi5RqWaPC16ERxiADfFV/RG', 4, NULL, '2018-10-28 18:27:16', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (141, '', 'OPD59', NULL, 59, NULL, '', NULL, NULL, NULL, '$2y$10$jl7CrfUwmBF/seGkgcoCj.n91ryKxND.WrmaHrpLxY3knPPyemDdG', 4, NULL, '2018-10-28 18:27:16', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (142, '', 'OPD60', NULL, 60, NULL, '', NULL, NULL, NULL, '$2y$10$3comlMInmJnFjW3u1PED2uma3Li713cGmUFW9h6uGuw8p8BcjSvIa', 4, NULL, '2018-10-28 18:27:16', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (143, '', 'OPD61', NULL, 61, NULL, '', NULL, NULL, NULL, '$2y$10$Yjqi3Yl0NBqSslIq68cMOuzxh.DgXqBkvhhouQv0qG6PfdzEMcwW6', 4, NULL, '2018-10-28 18:27:17', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (144, '', 'OPD62', NULL, 62, NULL, '', NULL, NULL, NULL, '$2y$10$q1KcKY2lpou4hMBZ47U6LeFkrpzNqWeAPkFZdn23i5YHzM4GiXny.', 4, NULL, '2018-10-28 18:27:17', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (145, '', 'OPD63', NULL, 63, NULL, '', NULL, NULL, NULL, '$2y$10$PuE7S0ZJFkhKnAEeD.Io0OCJm7Y.DUij.V9CquFI03qlRo4j4waCu', 4, NULL, '2018-10-28 18:27:17', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (146, '', 'OPD64', NULL, 64, NULL, '', NULL, NULL, NULL, '$2y$10$L1myCm5mnNF9S2Mcoud0uOdpQjAvY95QWGVUCFLdIa09Q9WewUa5m', 4, NULL, '2018-10-28 18:27:17', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (147, '', 'OPD65', NULL, 65, NULL, '', NULL, NULL, NULL, '$2y$10$8/TVC4.f/2YX1wtXzu.QEu5nFAGsG8KmAEQKA/fgI1F254jnpVXni', 4, NULL, '2018-10-28 18:27:17', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (148, '', 'OPD66', NULL, 66, NULL, '', NULL, NULL, NULL, '$2y$10$Gsb/ue7XYbmO7rNpB2S.9OhQH0N5M8p35qSNPWQH9J7XKalWCU7LK', 4, NULL, '2018-10-28 18:27:17', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (149, '', 'OPD67', NULL, 67, NULL, '', NULL, NULL, NULL, '$2y$10$SuWN25mm/tsWxtF2229yhO3urx6q7bVhBaE1KRRdlNCsnFny8qa1K', 4, NULL, '2018-10-28 18:27:17', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (150, '', 'OPD68', NULL, 68, NULL, '', NULL, NULL, NULL, '$2y$10$Vp7Qzca4IdWWUDzTUNutxOiOhdz/SU78./3R857IpOoL18pf4jn4m', 4, NULL, '2018-10-28 18:27:17', '0000-00-00 00:00:00', NULL);
INSERT INTO `users` VALUES (151, '', 'OPD69', NULL, 69, NULL, '', NULL, NULL, NULL, '$2y$10$rOiYYTzspQUY37h.UEWOu.mDyOXUQNSdyckzDv4iyKQRQ0VeUwboC', 4, NULL, '2018-12-06 10:55:37', '2018-12-06 10:55:37', NULL);
INSERT INTO `users` VALUES (154, '', 'OPD71', NULL, 71, NULL, '', NULL, NULL, NULL, ' ', 4, NULL, '2018-12-04 14:51:22', '2018-12-04 14:51:22', NULL);
INSERT INTO `users` VALUES (155, 'test', 'test', NULL, NULL, NULL, '', NULL, NULL, NULL, '$2y$10$3IYTmO/P03VfT10s0bw5dORng984Y1qXvGtIV7jhLxmGku8kb4Q9G', 5, NULL, '2018-12-21 13:09:48', '2018-12-21 06:09:48', '2018-12-21 06:09:48');
INSERT INTO `users` VALUES (156, 'Admin KKI', 'kkiadmin', NULL, NULL, NULL, '', NULL, NULL, NULL, '$2y$10$TTGFrcgstFpDdC1mvoxwGeP9aHQ8YdIKQ70ma8Y6/ivlJgq0eDqcu', 2, 'ZbidFdMgB55jC4pAnpsKw7bwDO9edaw86CEY5C0eIDdBcFua0ovZaL6B9lne', NULL, '2019-01-09 04:44:14', NULL);
INSERT INTO `users` VALUES (157, 'KKI Dev', 'kkidev', NULL, NULL, NULL, '', NULL, NULL, NULL, '$2y$10$GzfmNodQn3JFr/r9W5B22.O9YcV3abAVqhiw2HTP6mZGYrqfLkR6S', 1, NULL, NULL, '2019-01-09 11:43:57', NULL);

SET FOREIGN_KEY_CHECKS = 1;
