<?php

use App\Modules\settings\permissionsmatrix\Models\PermissionsmatrixModel;

// use \Session, \Request;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::get('/', function () {
    //mainkan Portal Disini
    return view('home::login.index');
});

Route::get('/faicons', function () {
    $icons = \DB::table('icons')
        ->select('id', 'icon')
        ->get();

    return $icons;
});

Route::get('/databeranda', array('as' => 'databeranda', 'uses' => 'PortalController@getDataberanda'));

Route::get('/dataopd', array('as' => 'getdataopd', 'uses' => 'PortalController@getDataopd'));

Route::get('/nilaiikm', array('as' => 'getnilaiikm', 'uses' => 'PortalController@getNilaiikm'));

Route::get('/refdata', array('as' => 'getrefdata', 'uses' => 'PortalController@getRefdata'));

Route::get('/datakuesioner', array('as' => 'getdatakuesioner', 'uses' => 'PortalController@getDatakuesioner'));

Route::post('/login-survey', array('as' => 'dologin-survey', 'uses' => 'AuthController@postLoginsurvey'));

Route::post('/simpansurvey', array('as' => 'simpansurvey', 'uses' => 'PortalController@postSimpansurvey'));

Route::get('/berita', array('as' => 'berita', 'uses' => 'PortalController@getBerita'));

Route::get('/isiberita', array('as' => 'isiberita', 'uses' => 'PortalController@getIsiberita'));

Route::group(['middleware' => 'guest'], function () {
    Route::get('/auth/login', function () {
        return view('home::login.index');
    });
    Route::get('/login', function () {
        return view('home::login.index');
    });
    Route::post('/login', array('as' => 'dologin', 'uses' => 'AuthController@postLogin'));
});

Route::group(['middleware' => 'auth'], function () {

    Route::get('/dashboard', function () {
        $modules = \DB::table('modules')->where('flag', '1')->get();
        return view('home::dashboard.index', compact('modules', 'devmodules'));
    });

    Route::get('beranda', function () {
        return view('home::dashboard.beranda');
    });

    Route::get('/login', function () {
        return redirect('/dashboard');
    });

    Route::get('/datadashboard', array('as' => 'datadashboard', 'uses' => 'BaseController@getDatadashboard'));

    Route::get('/menu', array('as' => 'menu', 'uses' => 'BaseController@getMenu'));

    Route::get('/appdata', array('as' => 'appdata', 'uses' => 'BaseController@getAppdata'));

    Route::get('/userprofile/{id}', array('as' => 'userprofile', 'uses' => 'ProfileController@getUserprofile'));

    Route::get('/profil', array('as' => 'profil', 'uses' => 'ProfileController@getProfil'));

    Route::get('/profil/profiledata', array('as' => 'profiledata', 'uses' => 'ProfileController@getProfiledata'));

    Route::post('/profil/updateakun', array('as' => 'updateakun', 'uses' => 'ProfileController@postUpdateakun'));

    Route::post('/profil/updatebasicinfo', array('as' => 'updatebasicinfo', 'uses' => 'ProfileController@postUpdatebasicinfo'));

    Route::post('/profil/updatecontactinfo', array('as' => 'updatecontactinfo', 'uses' => 'ProfileController@postUpdatecontactinfo'));

    Route::post('/profil/uploadfoto', array('as' => 'upload', 'uses' => 'ProfileController@postUpload'));

    Route::post('/profil/cekoldpwd', array('as' => 'cekoldpwd', 'uses' => 'ProfileController@postCekoldpwd'));
});

Route::get('/logout', array('as' => 'logout', 'uses' => 'AuthController@getLogout'));
Route::get('/logout-survey', array('as' => 'logout-survey', 'uses' => 'AuthController@getLogoutsurvey'));
