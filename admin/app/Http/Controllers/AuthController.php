<?php

namespace App\Http\Controllers;

use View,
    Validator,
    Input,
    Session,
    Redirect,
    Auth,
    File,
    Request;

class AuthController extends Controller {

    public function getIndex() {
        return View::make("claravel::portal.index");
    }

    public function postLogin() {
        // validate the info, create rules for the inputs
        $rules = array(
            'username' => 'required', // make sure the email is an actual email
            'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);
        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::to('/login')
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        } else {
            // create our user data for the authentication
            $userdata = array(
                'username' => Input::get('username'),
                'password' => Input::get('password')
            );

            // attempt to do the login
            if (Auth::attempt($userdata)) {

                $role = \DB::table('roles')
                                ->where('id', '=', Auth::user()->role_id)
                                ->first()->name;

                $rolesModel = \RolesModel::find(Auth::user()->role_id);
                \Session::put('role_id', Auth::user()->role_id);
                \Session::put('role', $role);
                \Session::put('user_id', Auth::user()->id);
                \Session::put('user_name', Auth::user()->username);
                \Session::put('name', Auth::user()->name);
                \Session::put('foto', Auth::user()->foto);

                $data = array(
                    'id_user' => Auth::user()->id,
                    'login_time' => date('Y-m-d H:i:s'),
                    'ip' => \Request::ip(),
                    'browser' => $_SERVER['HTTP_USER_AGENT']
                );

                $log = \DB::table('login_log')
                        ->insert($data);

                $pms = \PermissionsmatrixModel::with(array('permissions' => function($q) {
                                $q->where('name', '=', 'site-login');
                            }))->where('role_id', \Session::get('role_id'))->get();

                if ($pms->count() > 0) {
                    return Redirect::to('/' . $rolesModel->login_destination);
                } else {
                    Auth::logout();
                    \Session::flash('message', 'You don\'t have permission to sign in into this applications.');
                    return Redirect::to('/login');
                }
            } else {
                return Redirect::to('/login');
                //return Redirect::to('/login');
            }
        }
    }

    public function postLoginsurvey() {
        $rules = array(
            'username' => 'required',
            'password' => 'required|alphaNum|min:3'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            $result = array('result' => 'invalid');
        } else {
            $cek = \DB::table('users')
            ->select('users.id_opd','users.username','users.password','mst_opd.nama_opd')
            ->join('mst_opd','mst_opd.id','=','users.id_opd')
            ->where('username','=',\Input::get('username'))
            ->first();

            if($cek){
                if (\Hash::check(\Input::get('password'), $cek->password)){
                    $layanan = \DB::table('layanan_opd')
                    ->select('mst_layanan.nama_layanan','mst_layanan.id')
                    ->join('mst_layanan','mst_layanan.id','=','layanan_opd.id_layanan')
                    ->where('layanan_opd.id_opd','=',$cek->id_opd)
                    ->get();

                    $logged_user = array(
                        'id_opd' => $cek->id_opd,
                        'username' => $cek->username,
                        'nama_opd' => $cek->nama_opd
                    );

                    $result = array('result' => 'success', 'logged_user' => $logged_user, 'layanan' => $layanan);
                }else{
                    $result = array('result' => 'failed');
                }
            }else{
                $result = array('result' => 'failed');
            }
        }
        return json_encode($result);
    }

    public function getLogout() {
        \Session::forget('role_id');
        \Session::forget('role');
        \Session::forget('user_id');
        \Session::forget('user_name');
        \Session::forget('name');
        \Session::forget('foto');
        Auth::logout();
        return Redirect::to('/login');
    }

    public function getLogoutsurvey(){
        \Session::flush();
    }

}
