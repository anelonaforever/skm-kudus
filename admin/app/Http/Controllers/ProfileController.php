<?php

namespace App\Http\Controllers;

use View, Validator, Input, Session, Redirect, Auth, File, Request;

class ProfileController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout(){
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

    public function getProfil(){
        return View::make("home::dashboard.profil");                
    }

    public function getUserprofile(){
        return View::make("home::dashboard.userprofile");                
    }

    public function postCekoldpwd(){
        $a = \UsersModel::find(\Session::get('user_id'));
        $cek = \Hash::check(\Input::get('pwd'), $a->password);
        if(!$cek){
            return json_encode(array('result' => 'false'));
        }else{
            return json_encode(array('result' => 'true'));
        }
    }

    //upload foto profil
    public function postUpload(){
        if(\Input::has('file')){
            $data = \Input::get('file');
            $base64_str = substr($data, strpos($data, ",")+1);
            $image = base64_decode($base64_str);
            $mimetype = getImageMimeType($image);
            $image = \Image::make($image);
            $filename = \Input::get('id').'_'.\Input::get('username').'_'.time().'.'.$mimetype;
            $destinationPath = base_path().'/packages/upload/fmanager/'.\Session::get('user_name').'/';

            $cek = \DB::table('users')
            ->where('id','=',\Input::get('id'))
            ->first();

            @unlink($destinationPath.$cek->foto);

            $upd = \DB::table('users')
            ->where('id','=',\Input::get('id'))
            ->update(array('foto' => $filename));

            if($upd){
                $uploadSuccess = \Image::make($image)->resize(280, 280)->save($destinationPath.$filename);
                $user = \DB::table('users')
                ->where('id','=',\Input::get('id'))
                ->first();
                $foto = url().'/packages/upload/fmanager/'.$user->username.'/'.$user->foto;
                return json_encode(array('result' => 'success', 'foto' => $foto));
            }else{
                return json_encode(array('result' => 'failed'));
            }
        }else{
            return json_encode(array('result' => 'failed'));
        }
    }

    public function getProfiledata(){
        $id = \Input::get('id');

        $profil = \DB::table('users AS a')
        ->select('a.id','a.name','a.username','a.jenis_kelamin','a.alamat','a.email','a.no_telp','a.tgl_lahir','a.foto','b.name AS role','a.created_at','a.updated_at')
        ->join('roles AS b','b.id','=','a.role_id')
        ->where('a.id','=',$id)
        ->first();

        if($profil){
            $profil = \DB::table('users AS a')
            ->select('a.id','a.name','a.username','a.jenis_kelamin','a.alamat','a.email','a.no_telp','a.tgl_lahir','a.foto','b.name AS role','a.created_at','a.updated_at')
            ->join('roles AS b','b.id','=','a.role_id')
            ->where('a.id','=',\Session::get('user_id'))
            ->first();
        }

        if($profil->foto == ''){
            $profil->fotoprofil = url().'/packages/tugumuda/images/unknown-256.png';
        }else{
            $profil->fotoprofil = url().'/packages/upload/fmanager/'.$profil->username.'/'.$profil->foto;
        }

        return json_encode($profil);
    }

    public function postUpdatebasicinfo(){
        $array = array(
            'alamat' => \Input::get('alamat'),
            'jenis_kelamin' => \Input::get('jenis_kelamin'),
            'name' => \Input::get('name'),
            'tgl_lahir' => \Input::get('tgl_lahir')!=''?date('Y-m-d', strtotime(\Input::get('tgl_lahir'))):''
        );

        $simpan = \DB::table('users')
        ->where('id','=',\Session::get('user_id'))
        ->update($array);

        if($simpan){
            $profil = \DB::table('users AS a')
            ->select('a.id','a.name','a.username','a.jenis_kelamin','a.alamat','a.email','a.no_telp','a.tgl_lahir','a.foto','b.name AS role','a.created_at','a.updated_at')
            ->join('roles AS b','b.id','=','a.role_id')
            ->where('a.id','=',session('user_id'))
            ->first();

            if($profil->foto == ''){
                $profil->fotoprofil = url().'/packages/tugumuda/images/unknown-128.png';
            }else{
                $profil->fotoprofil = url().'/packages/upload/fmanager/'.$profil->username.'/'.$profil->foto;
            }

            return json_encode($profil);
        }
    }

    public function postUpdatecontactinfo(){
        $array = array(
            'no_telp' => \Input::get('no_telp'),
            'email' => \Input::get('email'),
        );

        $simpan = \DB::table('users')
        ->where('id','=',\Session::get('user_id'))
        ->update($array);

        if($simpan){
            $profil = \DB::table('users AS a')
            ->select('a.id','a.name','a.username','a.jenis_kelamin','a.alamat','a.email','a.no_telp','a.tgl_lahir','a.foto','b.name AS role','a.created_at','a.updated_at')
            ->join('roles AS b','b.id','=','a.role_id')
            ->where('a.id','=',session('user_id'))
            ->first();

            if($profil->foto == ''){
                $profil->fotoprofil = url().'/packages/tugumuda/images/unknown-128.png';
            }else{
                $profil->fotoprofil = url().'/packages/upload/fmanager/'.$profil->username.'/'.$profil->foto;
            }

            return json_encode($profil);
        }
    }

	public function postUpdateakun(){
        $input = array(
            'username' => \Input::get('username')
        );

        if(\Input::has('ubahpwd') && \Input::get('ubahpwd') == true && \Input::get('newpassword') === \Input::get('cpassword')){
            $input['password'] = \Hash::make(\Input::get('newpassword'));
        }

        $before = \DB::table('users')
        ->where('id','=',\Session::get('user_id'))
        ->first();

        $simpan = \DB::table('users')
        ->where('id','=',\Session::get('user_id'))
        ->update($input);

        if($simpan){
            $last_pwd_change = '-';
            if($before->username != Input::get('username')){
                \Session::put('user_name', Input::get('username'));
                rename(base_path().'/packages/upload/fmanager/'.$before->username.'/', base_path().'/packages/upload/fmanager/'.Input::get('username').'/');
            }
            if(\Input::has('ubahpwd') && \Input::get('ubahpwd') == true && \Input::get('newpassword') === \Input::get('cpassword')){
                $log = \DB::table('password_change_log')
                ->insert(array('user_id' => \Session::get('user_id'), 'ip' => \Request::ip(), 'browser' => $_SERVER['HTTP_USER_AGENT']));
            }
            $last_pwd_change = \DB::table('password_change_log')
            ->where('user_id','=',\Session::get('user_id'))
            ->orderby('time','desc')
            ->first();

            if($last_pwd_change){
                $last_pwd_change = date('d-m-Y H:i', strtotime($last_pwd_change->time));
            }else{
                $last_pwd_change = '-';
            }
            return json_encode(array('result' => 'success', 'username'  => \Input::get('username'), 'last_pwd_change' => $last_pwd_change));
        }else{
            return json_encode(array('result' => 'failed'));
        }
	}

}