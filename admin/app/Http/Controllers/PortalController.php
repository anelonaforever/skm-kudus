<?php

namespace App\Http\Controllers;

use Input;
use View;

class PortalController extends Controller
{

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

    public function getDataberanda()
    {
        $daftaropd = \DB::table('mst_opd AS opd')
            ->select('opd.id', 'lay.id AS id_layanan', 'opd.nama_opd', 'lay.nama_layanan', 'nil.nilai_interval', 'nil.nilai_interval_konversi')
            ->join('tr_respon AS res', 'res.id_opd', '=', 'opd.id')
            ->join('mst_layanan AS lay', 'lay.id', '=', 'res.id_layanan')
            ->join(\DB::raw("(SELECT res.id, ROUND( AVG( det.poin ), 1 ) AS nilai_interval, ROUND( AVG( det.poin ) * 25, 1 ) AS nilai_interval_konversi FROM tr_respon AS res JOIN tr_respon_detail AS det ON det.id_respon = res.id GROUP BY res.id_layanan) as nil"), function ($join) {
                $join->on("nil.id", "=", "res.id");
            })
            ->groupby('res.id_opd', 'res.id_layanan')
            ->get();

        $pengantar = \DB::table('mst_konten')
            ->where('id', '=', 1)
            ->first();

        $berita = \DB::table('mst_berita')
            ->select('mst_berita.*', 'users.name AS penulis')
            ->join('users', 'users.id', '=', 'mst_berita.user_id')
            ->whereNull('mst_berita.deleted_at')
            ->orderby(\DB::raw("RAND()"))
            ->first();

        $berita->thumbnail = asset('packages/upload/berita') . '/' . $berita->thumbnail;

        $berita->tgl_terbit = date('d-m-Y', strtotime($berita->created_at));
        $berita->jam_terbit = date('H:i', strtotime($berita->created_at));

        return json_encode(array(
            'daftaropd' => $daftaropd,
            'pengantar' => $pengantar->isi,
            'berita' => $berita,
        ));
    }

    public function getBerita()
    {
        $berita = \DB::table('mst_berita')
            ->select('mst_berita.*', 'users.name AS penulis')
            ->join('users', 'users.id', '=', 'mst_berita.user_id')
            ->whereNull('mst_berita.deleted_at')
            ->orderby('created_at', 'DESC')
            ->paginate(3);

        foreach ($berita as $row) {
            $row->thumbnail = asset('packages/upload/berita') . '/' . $row->thumbnail;
            $row->tgl_terbit = date('d-m-Y', strtotime($row->created_at));
            $row->jam_terbit = date('H:i', strtotime($row->created_at));
        }

        return $berita;
    }

    public function getIsiberita()
    {
        $data = \DB::table('mst_berita')
            ->select('mst_berita.*', 'users.name AS penulis')
            ->join('users', 'users.id', '=', 'mst_berita.user_id')
            ->where('url_berita', '=', \Input::get('url'))
            ->whereNull('mst_berita.deleted_at')
            ->first();

        $data->thumbnail = asset('packages/upload/berita') . '/' . $data->thumbnail;

        $data->tgl_terbit = date('d-m-Y', strtotime($data->created_at));
        $data->jam_terbit = date('H:i', strtotime($data->created_at));

        return json_encode($data);
    }

    public function getDataopd()
    {
        $opd = \DB::table('mst_opd')
            ->select('nama_opd')
            ->where('id', '=', \Input::get('id_opd'))
            ->first();

        $layanan = \DB::table('layanan_opd AS a')
            ->select('b.id AS id_layanan', 'b.nama_layanan')
            ->join('mst_layanan AS b', 'b.id', '=', 'a.id_layanan')
            ->where('a.id_opd', '=', \Input::get('id_opd'))
            ->get();

        $tanggal = \DB::table('tr_respon')
        ->select(\DB::raw("DATE_FORMAT(MIN(created_at),'%d-%m-%Y') AS tgl_awal"), \DB::raw("DATE_FORMAT(MAX(created_at), '%d-%m-%Y') AS tgl_akhir"))
        ->where('id_opd','=',\Input::get('id_opd'))
        ->get();

        $data = array(
            'opd' => $opd,
            'layanan' => $layanan,
            'tgl_awal' => $tanggal[0]->tgl_awal,
            'tgl_akhir' => $tanggal[0]->tgl_akhir
        );

        return json_encode($data);
    }

    public function getNilaiikm()
    {
        $range = array(date('Y-m-d', strtotime(\Input::get('tgl_awal'))), date('Y-m-d', strtotime(\Input::get('tgl_akhir'))));

        $nilai_ikm = \DB::table('tr_respon AS res')
            ->select(
                \DB::raw("ROUND(AVG(det.nilai),1) AS nilai"), \DB::raw("COUNT(res.id) AS jml_responden"),
                \DB::raw("SUM(CASE WHEN res.jenis_kelamin = 1 THEN 1 ELSE 0 END) AS laki_laki"),
                \DB::raw("SUM(CASE WHEN res.jenis_kelamin = 2 THEN 1 ELSE 0 END) AS perempuan"),
                \DB::raw("SUM(CASE WHEN res.id_jenjang_pendidikan = 1 THEN 1 ELSE 0 END) AS sd"),
                \DB::raw("SUM(CASE WHEN res.id_jenjang_pendidikan = 2 THEN 1 ELSE 0 END) AS smp"),
                \DB::raw("SUM(CASE WHEN res.id_jenjang_pendidikan = 3 THEN 1 ELSE 0 END) AS sma"),
                \DB::raw("SUM(CASE WHEN res.id_jenjang_pendidikan = 4 THEN 1 ELSE 0 END) AS S1"),
                \DB::raw("SUM(CASE WHEN res.id_jenjang_pendidikan = 5 THEN 1 ELSE 0 END) AS S2"),
                \DB::raw("SUM(CASE WHEN res.id_jenjang_pendidikan = 6 THEN 1 ELSE 0 END) AS S3"))
            ->join(\DB::raw('(SELECT id_respon, ROUND(AVG(poin) * 25,1) AS nilai FROM tr_respon_detail GROUP BY id_respon) AS det'), function ($join) {
                $join->on('det.id_respon', '=', 'res.id');
            })
            ->where('res.id_opd', '=', \Input::get('id_opd'));

        if (\Input::has('id_layanan') && \Input::get('id_layanan') > 0) {
            $nilai_ikm = $nilai_ikm->where('res.id_layanan', '=', \Input::get('id_layanan'));
        }

        $nilai_ikm = $nilai_ikm
        ->whereBetween('res.created_at', $range)
        ->groupby('res.id_opd')
            ->get();

        if ($nilai_ikm) {
            return json_encode(array('res' => true, 'nilai' => $nilai_ikm[0]));
        } else {
            return json_encode(array('res' => false));
        }
    }

    public function getRefdata()
    {
        $jenjang_pendidikan = \DB::table('mst_jenjang_pendidikan')
            ->select('id', 'jenjang')
            ->get();

        $pekerjaan = \DB::table('mst_pekerjaan')
            ->select('id', 'pekerjaan')
            ->get();

        $layanan = \DB::table('layanan_opd')
            ->select('mst_layanan.nama_layanan', 'mst_layanan.id')
            ->join('mst_layanan', 'mst_layanan.id', '=', 'layanan_opd.id_layanan')
            ->where('layanan_opd.id_opd', '=', \Input::get('id_opd'))
            ->get();

        $data = array(
            'jenjang_pendidikan' => $jenjang_pendidikan,
            'pekerjaan' => $pekerjaan,
            'jenis_layanan' => $layanan,
        );

        return json_encode($data);
    }

    public function getDatakuesioner()
    {
        $data = \DB::table('layanan_opd AS a')
            ->select('p.id', 'p.pertanyaan', 'p.id_kuesioner', 'p.id_unsur_skm')
            ->join('mst_pertanyaan AS p', 'p.id_kuesioner', '=', 'a.id_kuesioner')
            ->where('a.id_opd', '=', \Input::get('id_opd'))
            ->where('a.id_layanan', '=', \Input::get('id_layanan'))
            ->get();

        $n = 0;
        foreach ($data as $row) {
            $pilihan = \DB::table('mst_pilihan')
                ->select('id', 'pilihan', 'poin')
                ->where('id_pertanyaan', '=', $row->id)
                ->get();

            $data[$n]->pilihan = $pilihan;

            $n++;
        }

        return json_encode($data);
    }

    public function postSimpansurvey()
    {
        $respon = array(
            'id_layanan' => \Input::get('jenis_layanan'),
            'id_opd' => \Input::get('logged_user')['id_opd'],
            'jenis_kelamin' => \Input::get('profil')['jenis_kelamin'],
            'id_jenjang_pendidikan' => \Input::get('profil')['jenjang_pendidikan'],
            'id_pekerjaan' => \Input::get('profil')['pekerjaan'],
            'usia' => \Input::get('profil')['usia'],
            'saran' => \Input::get('saran'),
            'created_at' => date('Y-m-d H:i:s'),
            'user_id' => \Input::get('logged_user')['id_opd'],
            'role_id' => 4,
        );

        $simpan = \DB::table('tr_respon')
            ->insert($respon);

        $get = \DB::table('tr_respon')
            ->select('id')
            ->where('id_opd', '=', \Input::get('logged_user')['id_opd'])
            ->where('id_layanan', '=', \Input::get('jenis_layanan'))
            ->orderby('created_at', 'DESC')
            ->first();

        for ($n = 0; $n < sizeof(\Input::get('kuesioner')); $n++) {
            $jawaban = \DB::table('mst_pilihan')
                ->where('id', '=', \Input::get('kuesioner')[$n]['jawaban'])
                ->first();

            $detail = array(
                'id_respon' => $get->id,
                'user_id' => \Input::get('logged_user')['id_opd'],
                'role_id' => 4,
                'created_at' => date('Y-m-d H:i:s'),
                'pertanyaan' => \Input::get('kuesioner')[$n]['pertanyaan'],
                'jawaban' => $jawaban->pilihan,
                'poin' => $jawaban->poin,
                'id_unsur_skm' => \Input::get('kuesioner')[$n]['id_unsur_skm'],
            );

            $ins = \DB::table('tr_respon_detail')
                ->insert($detail);
        }
    }

}
