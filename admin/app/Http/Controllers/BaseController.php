<?php

namespace App\Http\Controllers;

use Session;
use View;

class BaseController extends Controller
{

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

    public function getMenu()
    {
        $contexts = \ContextModel::where('is_nav_bar', 1)
            ->where('flag', 1)
            ->orderBy('order')->get();

        $array = array();

        $n = 0;
        foreach ($contexts as $context) {
            $contextName = strtolower(str_replace(" ", "", $context->name));
            if (\PermissionsLibrary::hasPermission('context-' . $contextName)) {
                $array[$n] = $context;
                $submenu = array();

                $modules = \ContextModel::find($context->id)->modules()
                    ->where('id_parent', '0')
                    ->where('flag', 1)
                    ->orderBy('order')->get();

                $nn = 0;
                foreach ($modules as $module) {
                    $module_name = str_replace(" ", "", $module->name);
                    if (\PermissionsLibrary::hasPermission('mod-' . strtolower($module_name) . '-index')) {
                        $submenu[$nn] = $module;
                    }
                    $nn++;
                }

                $array[$n]->submenu = $submenu;
            }
            $n++;
        }

        return $array;
    }

    public function getAppdata()
    {
        $contexts = \ContextModel::where('is_nav_bar', 1)
            ->where('flag', 1)
            ->orderBy('order')->get();

        $array = array();

        $n = 0;
        foreach ($contexts as $context) {
            $contextName = strtolower(str_replace(" ", "", $context->name));
            if (\PermissionsLibrary::hasPermission('context-' . $contextName)) {
                $array[$n] = $context;
                $submenu = array();

                $modules = \ContextModel::find($context->id)->modules()
                    ->where('id_parent', '0')
                    ->where('flag', 1)
                    ->orderBy('order')->get();

                $nn = 0;
                foreach ($modules as $module) {
                    $module_name = str_replace(" ", "", $module->name);
                    if (\PermissionsLibrary::hasPermission('mod-' . strtolower($module_name) . '-index')) {
                        $submenu[$nn] = $module;
                    }
                    $nn++;
                }

                $array[$n]->submenu = $submenu;
            }
            $n++;
        }

        $profil = \DB::table('users AS a');

        if (\Session::get('role_id') == 4) {
            $profil = $profil->select('a.id', 'c.nama_opd AS name', 'a.username', 'a.jenis_kelamin', 'a.alamat', 'a.email', 'a.no_telp', 'a.tgl_lahir', 'a.foto', 'b.name AS role', 'a.created_at', 'a.updated_at');
        } else {
            $profil = $profil->select('a.id', 'a.name', 'a.username', 'a.jenis_kelamin', 'a.alamat', 'a.email', 'a.no_telp', 'a.tgl_lahir', 'a.foto', 'b.name AS role', 'a.created_at', 'a.updated_at');
        }

        $profil = $profil->join('roles AS b', 'b.id', '=', 'a.role_id');

        if (\Session::get('role_id') == 4) {
            $profil = $profil->join('mst_opd AS c', 'c.id', '=', 'a.id_opd');
        }

        $profil = $profil->where('a.id', '=', session('user_id'))
            ->first();

        if ($profil->foto == '') {
            $profil->fotoprofil = url() . '/packages/tugumuda/images/unknown-256.png';
        } else {
            $profil->fotoprofil = url() . '/packages/upload/fmanager/' . $profil->username . '/' . $profil->foto;
        }

        $last_pwd_change = \DB::table('password_change_log')
            ->where('user_id', '=', \Session::get('user_id'))
            ->orderby('time', 'desc')
            ->first();

        if ($last_pwd_change) {
            $profil->last_pwd_change = date('d-m-Y H:i', strtotime($last_pwd_change->time));
        } else {
            $profil->last_pwd_change = '-';
        }

        $lastlogin = \DB::table('login_log')
            ->where('id_user', '=', \Session::get('user_id'))
            ->orderby('login_time', 'desc')
            ->skip(1)->first();

        if (!$lastlogin) {
            $last = '-';
        } else {
            $last = date('d-m-Y H:i', strtotime($lastlogin->login_time));
        }

        $data = array('menu' => $array, 'profil' => $profil, 'lastlogin' => $last);

        return json_encode($data);
    }

    public function getDatadashboard()
    {
        $data = \DB::table('tr_respon')
            ->select(\DB::raw("SUM(CASE WHEN DATE(created_at) = CURDATE() THEN 1 ELSE 0 END) AS today"),
                \DB::raw("SUM(CASE WHEN DATE(created_at) = DATE(NOW() - INTERVAL 1 DAY) THEN 1 ELSE 0 END) AS yesterday"),
                \DB::raw("SUM(CASE WHEN YEARWEEK(created_at, 1) = YEARWEEK(CURDATE(), 1) THEN 1 ELSE 0 END) AS thisweek"),
                \DB::raw("SUM(CASE WHEN created_at >= curdate() - INTERVAL DAYOFWEEK(curdate())+6 DAY AND created_at < curdate() - INTERVAL DAYOFWEEK(curdate())-1 DAY THEN 1 ELSE 0 END) AS lastweek"),
                \DB::raw("COUNT(*) AS total"))
            ->get();

        return json_encode($data[0]);
    }

}
