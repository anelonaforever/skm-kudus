<section class="content-header">
    <h1>
        Edit Pekerjaan<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li><a href="{!!ClaravelHelpers::backEdit()!!}"> Users</a></li>
        <li class="active">Edit Pekerjaan</li>
    </ol>
</section>
<section class="content" ng-controller="PekerjaanControllerEdit">
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <div class="col-md-8">
                    <form name="moduleForm" class="form-horizontal" ng-submit="submitForm()" id="simpan">
                        <div class="form-group">
							<label for='pekerjaan' class='col-sm-2 control-label'>Pekerjaan:</label>
							<div class="col-sm-6">
								<input class='form-control' ng-model='formData.pekerjaan' name='pekerjaan' id='pekerjaan' type='text'>
							</div>
						</div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                {!! ClaravelHelpers::btnSave('moduleForm')!!}
                                &nbsp;
                                &nbsp;
                                <button class="btn btn-default" type="button" ng-click="reset()"><i class="ion-ios-undo"></i> Reset</button>
                                &nbsp;
                                &nbsp;
                                {!! ClaravelHelpers::btnCancelEdit(); !!}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>