<section class="content-header">
    <h1>
        Buat Pekerjaan<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li><a href="{!!ClaravelHelpers::backCreate()!!}"> Users</a></li>
        <li class="active">Buat Pekerjaan</li>
    </ol>
</section>
<section class="content" ng-controller="PekerjaanControllerCreate">
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <div class="col-md-8">
                    <form name="moduleForm" class="form-horizontal" ng-submit="submitForm()" novalidate id="simpan" autocomplete="off">
                        <div class="form-group">
							<label for='pekerjaan' class='col-sm-2 control-label'>Pekerjaan:</label>
							<div class="col-sm-6">
								<input class='form-control' ng-model='formData.pekerjaan' name='pekerjaan' id='pekerjaan' type='text'>
							</div>
						</div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-6">
                                {!! ClaravelHelpers::btnSave('moduleForm')!!}
                                &nbsp;
                                &nbsp;
                                {!! ClaravelHelpers::btnCancel()!!}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>