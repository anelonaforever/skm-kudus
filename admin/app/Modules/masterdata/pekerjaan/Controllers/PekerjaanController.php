<?php namespace App\Modules\masterdata\pekerjaan\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\masterdata\pekerjaan\Models\PekerjaanModel;
use Input,View, Request, Form, File;

/**
* Pekerjaan Controller
* @var Pekerjaan
* Generate from Custom Laravel 5.1 by Aa Gun. 
*
* Developed by Divisi Software Development - Dinustek. 
* Please write log when you do some modification, don't change anything unless you know what you do
* Semarang, 2016
*/

class PekerjaanController extends Controller {
    protected $pekerjaan;

    public function __construct(PekerjaanModel $pekerjaan){
        $this->pekerjaan = $pekerjaan;
    }

        public function getIndex(){
        return View::make('pekerjaan::index');
    }

    public function getData(){
		if(Input::has('cari') AND strlen(Input::get('cari')) > 0){
            $data = $this->pekerjaan
            ->where(function($q){
                $q->where('pekerjaan', 'LIKE', '%'.Input::get('cari').'%');
            })
            ->wherenull('deleted_at')
            ->paginate($_ENV['configurations']['list-limit']);
        }else{
            $data = $this->pekerjaan
            ->wherenull('deleted_at')
            ->paginate($_ENV['configurations']['list-limit']);
        }
		return $data;
	}

    public function getCreate(){
        return View::make('pekerjaan::create');
    }

    public function postCreate(){
        $input = Input::all();
        $validation = \Validator::make($input, PekerjaanModel::$rules);
        if($validation->passes()){
            $input['user_id'] = \Session::get('user_id');
            $input['role_id'] = \Session::get('role_id');
            $simpan = $this->pekerjaan->create($input);

            if($simpan){
                $result = array('result' => 'success');
            }else{
                $result = array('result' => 'failed');
            }
        }
        else{
            $result = array('result' => 'invalid');
        }

        return json_encode($result);
    }



    //{controller-show}

    public function getEdit(){
        return View::make('pekerjaan::edit');
    }

    public function getDataedit(){
        $id = Request::segment(4);
        $data = $this->pekerjaan->find($id);
        $data->_token = csrf_token();
        return json_encode($data);
    }
    
    public function postEdit(){
        $id = Input::get('id');
        $input = Input::all();
        $validation = \Validator::make($input, PekerjaanModel::$rules);
        
        if($validation->passes()){
            $pekerjaan = $this->pekerjaan->find($id);
            $pekerjaan->update($input);

            if($pekerjaan){
                $result = array('result' => 'success');
            }else{
                $result = array('result' => 'failed');
            }
        }
        else{
            $result = array('result' => 'invalid');
        }

        return json_encode($result);
    }


	
    public function postDelete(){
        $ids = Input::get('id');
        if (is_array($ids)){
            foreach($ids as $id){
                $hapus = $this->pekerjaan->find($id)->update(array('deleted_at' => date('Y-m-d H:i:s')));
            }
        }
        else{
            $hapus = $this->pekerjaan->find($ids)->update(array('deleted_at' => date('Y-m-d H:i:s')));
        }

        if($hapus){
            $result = array('result' => 'success');
        }else{
            $result = array('result' => 'failed');
        }

        return json_encode($result);
    }

}
