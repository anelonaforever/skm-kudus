<?php namespace App\Modules\masterdata\opd\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\masterdata\opd\Models\OpdModel;
use Input,View, Request, Form, File;

/**
* Opd Controller
* @var Opd
* Generate from Custom Laravel 5.1 by Aa Gun. 
*
* Developed by Divisi Software Development - Dinustek. 
* Please write log when you do some modification, don't change anything unless you know what you do
* Semarang, 2016
*/

class OpdController extends Controller {
    protected $opd;

    public function __construct(OpdModel $opd){
        $this->opd = $opd;
    }

        public function getIndex(){
        return View::make('opd::index');
    }

    public function getData(){
        if(Input::has('cari') AND strlen(Input::get('cari')) > 0){
            $data = $this->opd
            ->where('role_id','=',4)
            ->where(function($q){
                $q->where('name', 'LIKE', '%'.Input::get('cari').'%');
                $q->orwhere('username', 'LIKE', '%'.Input::get('cari').'%');
            })
            ->wherenull('deleted_at')
            ->paginate($_ENV['configurations']['list-limit']);
        }else{
            $data = $this->opd
            ->select('b.nama_opd','users.username','users.id','b.pwd_default')
            ->join('mst_opd AS b','b.id','=','users.id_opd')
            ->where('users.role_id','=',4)
            ->wherenull('deleted_at')
            ->paginate($_ENV['configurations']['list-limit']);
        }
        return $data;
    }

    public function getCreate(){
        return View::make('opd::create');
    }
    
    public function getUsername(){
        $lastid = \DB::select("SELECT AUTO_INCREMENT AS lastid
        FROM  INFORMATION_SCHEMA.TABLES
        WHERE TABLE_SCHEMA = 'skm-kudus'
        AND   TABLE_NAME   = 'mst_opd'");

        return "OPD".$lastid[0]->lastid;
    }

    public function postCreate(){
        $input = array(
            'nama_opd' => \Input::get('nama_opd'),
            'pwd_default' => \Input::get('password'),
            'created_at' => date('Y-m-d H:i:s')
        );

        $lastid = \DB::select("SELECT AUTO_INCREMENT AS lastid
        FROM  INFORMATION_SCHEMA.TABLES
        WHERE TABLE_SCHEMA = 'skm-kudus'
        AND   TABLE_NAME   = 'mst_opd'");

        if((Int)substr(\Input::get('username'), 3, 2) != (Int)$lastid[0]->lastid){
            $result = array('result' => 'idused');
        }else{
            $validation = \Validator::make($input, OpdModel::$rules);
            if ($validation->passes()) {
                $input['user_id'] = \Session::get('user_id');
                $input['role_id'] = \Session::get('role_id');
                $simpan = \DB::table('mst_opd')
                    ->insert($input);

                if ($simpan) {
                    $user = array(
                        'username' => \Input::get('username'),
                        'id_opd' => substr(\Input::get('username'), 3, 2),
                        'password' => \Hash::make(\Input::get('password')),
                        'role_id' => 4,
                        'created_at' => date('Y-m-d H:i:s'),
                    );

                    $cu = $this->opd
                        ->insert($user);

                    $result = array('result' => 'success');
                } else {
                    $result = array('result' => 'failed');
                }
            } else {
                $result = array('result' => 'invalid');
            }
        }

        return json_encode($result);
    }

    public function getEdit(){
        return View::make('opd::edit');
    }

    public function getDataedit(){
        $id = Request::segment(4);
        $data = $this->opd
        ->select('mst_opd.nama_opd','users.username','users.id')
        ->join('mst_opd','mst_opd.id','=','users.id_opd')
        ->where('users.id','=',$id)
        ->wherenull('deleted_at')
        ->first();
        $data->_token = csrf_token();
        return json_encode($data);
    }
    
    public function postEdit(){
        $id = Input::get('id');
        $input = Input::all();
        $validation = \Validator::make($input, OpdModel::$editrules);
        
        if($validation->passes()){
            $upd = \DB::table('mst_opd')
            ->where('id','=',substr(\Input::get('username'), 3, 2))
            ->update(array('nama_opd' => \Input::get('nama_opd')));

            if(\Input::get('ubahpwd') == true){
                $opd = $this->opd->find($id);
                $opd->update(array('password' => \Hash::make(\Input::get('password'))));
            }

            $result = array('result' => 'success');
        }
        else{
            $result = array('result' => 'invalid');
        }

        return json_encode($result);
    }


	
    public function postDelete(){
        $ids = Input::get('id');
        if (is_array($ids)){
            foreach($ids as $id){
                $hapus = $this->opd->find($id)->update(array('deleted_at' => date('Y-m-d H:i:s')));
            }
        }
        else{
            $hapus = $this->opd->find($ids)->update(array('deleted_at' => date('Y-m-d H:i:s')));
        }

        if($hapus){
            $result = array('result' => 'success');
        }else{
            $result = array('result' => 'failed');
        }

        return json_encode($result);
    }

    public function postResetpwd(){
        $user = \DB::table('users')
        ->join('mst_opd','mst_opd.id','=','users.id_opd')
        ->where('users.id','=',\Input::get('id'))
        ->first();

        $reset = \DB::table('users')
        ->where('id','=',\Input::get('id'))
        ->update(array('password' => \Hash::make($user->pwd_default)));

        if($reset){
            $result = array('result' => 'success');
        }else{
            $result = array('result' => 'failed');
        }

        return json_encode($result);
    }

}
