<section class="content-header">
    <h1>
        Edit OPD<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li><a href="{!!ClaravelHelpers::backEdit()!!}"> Users</a></li>
        <li class="active">Edit OPD</li>
    </ol>
</section>
<section class="content" ng-controller="OpdControllerEdit">
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <div class="col-md-8">
                    <form name="moduleForm" class="form-horizontal" ng-submit="submitForm()" id="simpan">
                        <div class="form-group">
							<label for='nama_opd' class='col-sm-2 control-label'>Nama OPD:</label>
							<div class="col-sm-6">
								<input class='form-control' ng-model='formData.nama_opd' name='nama_opd' id='nama_opd' type='text' required>
							</div>
                        </div>
                        <div class="form-group">
							<label for='username' class='col-sm-2 control-label'>Username:</label>
							<div class="col-sm-6">
								<input class='form-control' ng-model='formData.username' name='username' id='username' type='text' required readonly>
							</div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" ng-model="formData.ubahpwd"> Ubah Password?
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" ng-show="formData.ubahpwd">
							<label for='password' class='col-sm-2 control-label'>Password:</label>
							<div class="col-sm-6">
								<input class='form-control' ng-model='formData.password' name='password' id='password' type='text'>
							</div>
						</div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                {!! ClaravelHelpers::btnSave('moduleForm')!!}
                                &nbsp;
                                &nbsp;
                                <button class="btn btn-default" type="button" ng-click="reset()"><i class="ion-ios-undo"></i> Reset</button>
                                &nbsp;
                                &nbsp;
                                {!! ClaravelHelpers::btnCancelEdit(); !!}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>