<?php namespace App\Modules\masterdata\layanan\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\masterdata\layanan\Models\LayananModel;
use Input,View, Request, Form, File;

/**
* Layanan Controller
* @var Layanan
* Generate from Custom Laravel 5.1 by Aa Gun. 
*
* Developed by Divisi Software Development - Dinustek. 
* Please write log when you do some modification, don't change anything unless you know what you do
* Semarang, 2016
*/

class LayananController extends Controller {
    protected $layanan;

    public function __construct(LayananModel $layanan){
        $this->layanan = $layanan;
    }

        public function getIndex(){
        return View::make('layanan::index');
    }

    public function getData(){
		if(Input::has('cari') AND strlen(Input::get('cari')) > 0){
            $data = $this->layanan
            ->where(function($q){
                $q->where('nama_layanan', 'LIKE', '%'.Input::get('cari').'%');
            })
            ->wherenull('deleted_at')
            ->paginate($_ENV['configurations']['list-limit']);
        }else{
            $data = $this->layanan
            ->wherenull('deleted_at')
            ->paginate($_ENV['configurations']['list-limit']);
        }
		return $data;
	}

    public function getCreate(){
        return View::make('layanan::create');
    }

    public function postCreate(){
        $input = Input::all();
        $validation = \Validator::make($input, LayananModel::$rules);
        if($validation->passes()){
            $input['user_id'] = \Session::get('user_id');
            $input['role_id'] = \Session::get('role_id');
            $simpan = $this->layanan->create($input);

            if($simpan){
                $result = array('result' => 'success');
            }else{
                $result = array('result' => 'failed');
            }
        }
        else{
            $result = array('result' => 'invalid');
        }

        return json_encode($result);
    }



    //{controller-show}

    public function getEdit(){
        return View::make('layanan::edit');
    }

    public function getDataedit(){
        $id = Request::segment(4);
        $data = $this->layanan->find($id);
        $data->_token = csrf_token();
        return json_encode($data);
    }
    
    public function postEdit(){
        $id = Input::get('id');
        $input = Input::all();
        $validation = \Validator::make($input, LayananModel::$rules);
        
        if($validation->passes()){
            $layanan = $this->layanan->find($id);
            $layanan->update($input);

            if($layanan){
                $result = array('result' => 'success');
            }else{
                $result = array('result' => 'failed');
            }
        }
        else{
            $result = array('result' => 'invalid');
        }

        return json_encode($result);
    }


	
    public function postDelete(){
        $ids = Input::get('id');
        if (is_array($ids)){
            foreach($ids as $id){
                $hapus = $this->layanan->find($id)->update(array('deleted_at' => date('Y-m-d H:i:s')));
            }
        }
        else{
            $hapus = $this->layanan->find($ids)->update(array('deleted_at' => date('Y-m-d H:i:s')));
        }

        if($hapus){
            $result = array('result' => 'success');
        }else{
            $result = array('result' => 'failed');
        }

        return json_encode($result);
    }

}
