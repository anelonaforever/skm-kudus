<?php namespace App\Modules\masterdata\rentangikm\Models;
use Illuminate\Database\Eloquent\Model;


/**
* Rentangikm Model
* @var Rentangikm
* Generate from Custom Laravel 5.1 by Aa Gun. 
*
* Developed by Dinustek. 
* Please write log when you do some modification, don't change anything unless you know what you do
* Semarang, 2016
*/

class RentangikmModel extends Model {
	protected $guarded = array();
	
	protected $table = "mst_rentang_ikm";

	public static $rules = array(
    		'kode' => 'required',
		'keterangan' => 'required',
		'range1' => 'required',
		'range2' => 'required',

    );

	public static function all($columns = array('*')){
		$instance = new static;
		if (\PermissionsLibrary::hasPermission('mod-rentangikm-listall')){
			return $instance->newQuery()->paginate($_ENV['configurations']['list-limit']);
		}else{
			return $instance->newQuery()
			->where('role_id', \Session::get('role_id'))
			->paginate($_ENV['configurations']['list-limit']);	
			
		}
	}

}
