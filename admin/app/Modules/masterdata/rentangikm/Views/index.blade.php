<section class="content-header">
    <h1>
        Rentang IKM<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li class="active">Rentang IKM</li>
    </ol>
</section>
<section class="content" table-tools ng-controller="RentangikmController">
    <div class="box box-primary box-table">
        <div class="box-header box-header-default">
            <h3 class="box-title hidden-xs">Daftar Rentang IKM</h3>
        </div>
        <div class="table-responsive">
            <div class="box-body no-padding">
                <table class="table table-condensed table-striped table-hover" id="tabel">
                    <thead class='bg-default'>
                        <tr>
                            @if(\PermissionsLibrary::canDel())
                            <th class="hapusbeberapa" ng-show="hapusbeberapa">
                                <center>
                                    <div class="checkbox checkbox-danger">
                                        <input type="checkbox" id="checkbox-checkall" ng-model="selectedAll" ng-click="checkAll()">
                                        <label for="checkbox-checkall">
                                            Pilih Semua
                                        </label>
                                    </div>
                                </center>
                            </th>
                            @endif
                            					<th>Kode</th>
					<th>Keterangan</th>
					<th>Range 1</th>
					<th>Range 2</th>

                            <th>Act.</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="record in Records">
                            @if(\PermissionsLibrary::canDel())
                            <td class="hapusbeberapa" ng-show="hapusbeberapa">
                                <center>
                                    <div class="checkbox checkbox-warning">
                                        <input type="checkbox" id="checkbox[[record.id]]" ng-model="record.Selected">
                                        <label for="checkbox[[record.id]]"></label>
                                    </div>
                                </center>
                            </td>
                            @endif
                            					<td class="nowrap">[[record.kode]]</td>
					<td class="nowrap">[[record.keterangan]]</td>
					<td class="nowrap">[[record.range1]]</td>
					<td class="nowrap">[[record.range2]]</td>

                            <td class="nowrap">
                                {!! ClaravelHelpers::btnEdit('[[location.path()]]', '[[record.id]]') !!}
                                &nbsp;&nbsp;&nbsp;
                                {!! ClaravelHelpers::btnDelete('Rentang IKM', '[[record.id]]') !!}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <center ng-show="paging.totalItems>{!! $_ENV['configurations']['list-limit'] !!}">
                    <ul uib-pagination total-items="paging.totalItems" ng-model="paging.currentPage" max-size="5" class="pagination-sm" boundary-links="true" num-pages="paging.numPages" items-per-page="{!! $_ENV['configurations']['list-limit'] !!}" force-ellipses="true"></ul>
                </center>
            </div>
        </div>
        <div class="tombol-container">
            {!! ClaravelHelpers::btnCreate() !!}
            @if(\PermissionsLibrary::canDel())
            {!! ClaravelHelpers::btnDeleteAll('Rentang IKM', csrf_token()) !!}
            @endif
        </div>
    </div>
</section>