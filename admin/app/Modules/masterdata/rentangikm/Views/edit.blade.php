<section class="content-header">
    <h1>
        Edit Rentang IKM<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li><a href="{!!ClaravelHelpers::backEdit()!!}"> Users</a></li>
        <li class="active">Edit Rentang IKM</li>
    </ol>
</section>
<section class="content" ng-controller="RentangikmControllerEdit">
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <div class="col-md-8">
                    <form name="moduleForm" class="form-horizontal" ng-submit="submitForm()" id="simpan">
                        <div class="form-group">
							<label for='kode' class='col-sm-2 control-label'>Kode:</label>
							<div class="col-sm-6">
								<input class='form-control' ng-model='formData.kode' name='kode' id='kode' type='text' readonly>
							</div>
						</div>
						<div class="form-group">
							<label for='keterangan' class='col-sm-2 control-label'>Keterangan:</label>
							<div class="col-sm-6">
								<input class='form-control' ng-model='formData.keterangan' name='keterangan' id='keterangan' type='text' readonly>
							</div>
						</div>
						<div class="form-group">
							<label for='range1' class='col-sm-2 control-label'>Range 1:</label>
							<div class="col-sm-6">
								<input class='form-control' ng-model='formData.range1' name='range1' id='range1' type='text'>
							</div>
						</div>
						<div class="form-group">
							<label for='range2' class='col-sm-2 control-label'>Range 2:</label>
							<div class="col-sm-6">
								<input class='form-control' ng-model='formData.range2' name='range2' id='range2' type='text'>
							</div>
						</div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                {!! ClaravelHelpers::btnSave('moduleForm')!!}
                                &nbsp;
                                &nbsp;
                                <button class="btn btn-default" type="button" ng-click="reset()"><i class="ion-ios-undo"></i> Reset</button>
                                &nbsp;
                                &nbsp;
                                {!! ClaravelHelpers::btnCancelEdit(); !!}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>