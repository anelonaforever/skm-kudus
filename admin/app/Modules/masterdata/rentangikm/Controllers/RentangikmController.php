<?php namespace App\Modules\masterdata\rentangikm\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\masterdata\rentangikm\Models\RentangikmModel;
use Input,View, Request, Form, File;

/**
* Rentangikm Controller
* @var Rentangikm
* Generate from Custom Laravel 5.1 by Aa Gun. 
*
* Developed by Divisi Software Development - Dinustek. 
* Please write log when you do some modification, don't change anything unless you know what you do
* Semarang, 2016
*/

class RentangikmController extends Controller {
    protected $rentangikm;

    public function __construct(RentangikmModel $rentangikm){
        $this->rentangikm = $rentangikm;
    }

        public function getIndex(){
        return View::make('rentangikm::index');
    }

    public function getData(){
		if(Input::has('cari') AND strlen(Input::get('cari')) > 0){
            $data = $this->rentangikm
            			->orWhere('kode', 'LIKE', '%'.Input::get('cari').'%')
			->orWhere('keterangan', 'LIKE', '%'.Input::get('cari').'%')
			->orWhere('range1', 'LIKE', '%'.Input::get('cari').'%')
			->orWhere('range2', 'LIKE', '%'.Input::get('cari').'%')

            ->paginate($_ENV['configurations']['list-limit']);
        }else{
            $data = $this->rentangikm->paginate($_ENV['configurations']['list-limit']);
        }
		return $data;
	}

    

    //{controller-show}

    public function getEdit(){
        return View::make('rentangikm::edit');
    }

    public function getDataedit(){
        $id = Request::segment(4);
        $data = $this->rentangikm->find($id);
        $data->_token = csrf_token();
        return json_encode($data);
    }
    
    public function postEdit(){
        $id = Input::get('id');
        $input = Input::all();
        $validation = \Validator::make($input, RentangikmModel::$rules);
        
        if($validation->passes()){
            $rentangikm = $this->rentangikm->find($id);
            $rentangikm->update($input);

            if($rentangikm){
                $result = array('result' => 'success');
            }else{
                $result = array('result' => 'failed');
            }
        }
        else{
            $result = array('result' => 'invalid');
        }

        return json_encode($result);
    }


	
    
}
