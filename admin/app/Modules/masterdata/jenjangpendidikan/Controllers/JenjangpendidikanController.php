<?php namespace App\Modules\masterdata\jenjangpendidikan\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\masterdata\jenjangpendidikan\Models\JenjangpendidikanModel;
use Input,View, Request, Form, File;

/**
* Jenjangpendidikan Controller
* @var Jenjangpendidikan
* Generate from Custom Laravel 5.1 by Aa Gun. 
*
* Developed by Divisi Software Development - Dinustek. 
* Please write log when you do some modification, don't change anything unless you know what you do
* Semarang, 2016
*/

class JenjangpendidikanController extends Controller {
    protected $jenjangpendidikan;

    public function __construct(JenjangpendidikanModel $jenjangpendidikan){
        $this->jenjangpendidikan = $jenjangpendidikan;
    }

        public function getIndex(){
        return View::make('jenjangpendidikan::index');
    }

    public function getData(){
		if(Input::has('cari') AND strlen(Input::get('cari')) > 0){
            $data = $this->jenjangpendidikan
            ->where(function($q){
                $q->where('jenjang', 'LIKE', '%'.Input::get('cari').'%');
            })
            ->wherenull('deleted_at')
            ->paginate($_ENV['configurations']['list-limit']);
        }else{
            $data = $this->jenjangpendidikan
            ->wherenull('deleted_at')
            ->paginate($_ENV['configurations']['list-limit']);
        }
		return $data;
	}

    public function getCreate(){
        return View::make('jenjangpendidikan::create');
    }

    public function postCreate(){
        $input = Input::all();
        $validation = \Validator::make($input, JenjangpendidikanModel::$rules);
        if($validation->passes()){
            $input['user_id'] = \Session::get('user_id');
            $input['role_id'] = \Session::get('role_id');
            $simpan = $this->jenjangpendidikan->create($input);

            if($simpan){
                $result = array('result' => 'success');
            }else{
                $result = array('result' => 'failed');
            }
        }
        else{
            $result = array('result' => 'invalid');
        }

        return json_encode($result);
    }



    //{controller-show}

    public function getEdit(){
        return View::make('jenjangpendidikan::edit');
    }

    public function getDataedit(){
        $id = Request::segment(4);
        $data = $this->jenjangpendidikan->find($id);
        $data->_token = csrf_token();
        return json_encode($data);
    }
    
    public function postEdit(){
        $id = Input::get('id');
        $input = Input::all();
        $validation = \Validator::make($input, JenjangpendidikanModel::$rules);
        
        if($validation->passes()){
            $jenjangpendidikan = $this->jenjangpendidikan->find($id);
            $jenjangpendidikan->update($input);

            if($jenjangpendidikan){
                $result = array('result' => 'success');
            }else{
                $result = array('result' => 'failed');
            }
        }
        else{
            $result = array('result' => 'invalid');
        }

        return json_encode($result);
    }


	
    public function postDelete(){
        $ids = Input::get('id');
        if (is_array($ids)){
            foreach($ids as $id){
                $hapus = $this->jenjangpendidikan->find($id)->update(array('deleted_at' => date('Y-m-d H:i:s')));
            }
        }
        else{
            $hapus = $this->jenjangpendidikan->find($ids)->update(array('deleted_at' => date('Y-m-d H:i:s')));
        }

        if($hapus){
            $result = array('result' => 'success');
        }else{
            $result = array('result' => 'failed');
        }

        return json_encode($result);
    }

}
