<?php namespace App\Modules\masterdata\jenjangpendidikan\Models;
use Illuminate\Database\Eloquent\Model;


/**
* Jenjangpendidikan Model
* @var Jenjangpendidikan
* Generate from Custom Laravel 5.1 by Aa Gun. 
*
* Developed by Dinustek. 
* Please write log when you do some modification, don't change anything unless you know what you do
* Semarang, 2016
*/

class JenjangpendidikanModel extends Model {
	protected $guarded = array();
	
	protected $table = "mst_jenjang_pendidikan";

	public static $rules = array(
    	'jenjang' => 'required',
    );

	public static function all($columns = array('*')){
		$instance = new static;
		if (\PermissionsLibrary::hasPermission('mod-jenjangpendidikan-listall')){
			return $instance->newQuery()->paginate($_ENV['configurations']['list-limit']);
		}else{
			return $instance->newQuery()
			->where('role_id', \Session::get('role_id'))
			->paginate($_ENV['configurations']['list-limit']);	
			
		}
	}

}
