<section class="content-header">
    <h1>
        Edit Jenjang Pendidikan<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li><a href="{!!ClaravelHelpers::backEdit()!!}"> Users</a></li>
        <li class="active">Edit Jenjang Pendidikan</li>
    </ol>
</section>
<section class="content" ng-controller="JenjangpendidikanControllerEdit">
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <div class="col-md-8">
                    <form name="moduleForm" class="form-horizontal" ng-submit="submitForm()" id="simpan">
                        <div class="form-group">
							<label for='jenjang' class='col-sm-2 control-label'>Jenjang:</label>
							<div class="col-sm-6">
								<input class='form-control' ng-model='formData.jenjang' name='jenjang' id='jenjang' type='text'>
							</div>
						</div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                {!! ClaravelHelpers::btnSave('moduleForm')!!}
                                &nbsp;
                                &nbsp;
                                <button class="btn btn-default" type="button" ng-click="reset()"><i class="ion-ios-undo"></i> Reset</button>
                                &nbsp;
                                &nbsp;
                                {!! ClaravelHelpers::btnCancelEdit(); !!}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>