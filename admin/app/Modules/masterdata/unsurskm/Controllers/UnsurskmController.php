<?php namespace App\Modules\masterdata\unsurskm\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\masterdata\unsurskm\Models\UnsurskmModel;
use Input,View, Request, Form, File;

/**
* Unsurskm Controller
* @var Unsurskm
* Generate from Custom Laravel 5.1 by Aa Gun. 
*
* Developed by Divisi Software Development - Dinustek. 
* Please write log when you do some modification, don't change anything unless you know what you do
* Semarang, 2016
*/

class UnsurskmController extends Controller {
    protected $unsurskm;

    public function __construct(UnsurskmModel $unsurskm){
        $this->unsurskm = $unsurskm;
    }

        public function getIndex(){
        return View::make('unsurskm::index');
    }

    public function getData(){
		$data = $this->unsurskm->get();
		return $data;
	}
}
