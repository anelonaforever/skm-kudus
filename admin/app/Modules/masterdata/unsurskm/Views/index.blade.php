<section class="content-header">
    <h1>
        Unsur SKM<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li class="active">Unsur SKM</li>
    </ol>
</section>
<section class="content" table-tools ng-controller="UnsurskmController">
    <div class="box box-primary box-table">
        <div class="table-responsive">
            <div class="box-body no-padding">
                <table class="table table-condensed table-striped table-hover" id="tabel">
                    <thead class='bg-default'>
                        <tr>
                            <th>Nama Unsur</th>
                            <th>Kode</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="record in Records">
                            <td class="nowrap">[[record.nama_unsur]]</td>
                            <td class="nowrap">[[record.kode]]</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>