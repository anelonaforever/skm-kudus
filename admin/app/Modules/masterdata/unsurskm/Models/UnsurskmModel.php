<?php namespace App\Modules\masterdata\unsurskm\Models;
use Illuminate\Database\Eloquent\Model;


/**
* Unsurskm Model
* @var Unsurskm
* Generate from Custom Laravel 5.1 by Aa Gun. 
*
* Developed by Dinustek. 
* Please write log when you do some modification, don't change anything unless you know what you do
* Semarang, 2016
*/

class UnsurskmModel extends Model {
	protected $guarded = array();
	
	protected $table = "mst_unsur_skm";

	public static $rules = array(
    		'nama_unsur' => 'required',
		'kode' => 'required',

    );

	public static function all($columns = array('*')){
		$instance = new static;
		if (\PermissionsLibrary::hasPermission('mod-unsurskm-listall')){
			return $instance->newQuery()->paginate($_ENV['configurations']['list-limit']);
		}else{
			return $instance->newQuery()
			->where('role_id', \Session::get('role_id'))
			->paginate($_ENV['configurations']['list-limit']);	
			
		}
	}

}
