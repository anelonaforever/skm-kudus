<?php namespace App\Modules\masterdata\kuesioner\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\masterdata\kuesioner\Models\KuesionerModel;
use Input,View, Request, Form, File;

/**
* Kuesioner Controller
* @var Kuesioner
* Generate from Custom Laravel 5.1 by Aa Gun. 
*
* Developed by Divisi Software Development - Dinustek. 
* Please write log when you do some modification, don't change anything unless you know what you do
* Semarang, 2016
*/

class KuesionerController extends Controller {
    protected $kuesioner;

    public function __construct(KuesionerModel $kuesioner){
        $this->kuesioner = $kuesioner;
    }

        public function getIndex(){
        return View::make('kuesioner::index');
    }

    public function getData(){
		if(Input::has('cari') AND strlen(Input::get('cari')) > 0){
            $data = $this->kuesioner
            ->select('mst_kuesioner.*',\DB::raw("COUNT(b.id) AS jml_pertanyaan"))
            ->join('mst_pertanyaan AS b','b.id_kuesioner','=','mst_kuesioner.id')
            ->groupby('b.id_kuesioner')
            ->whereNull('deleted_at')
            ->where(function($q){
                $q->where('nama_kuesioner', 'LIKE', '%'.Input::get('cari').'%')
			    ->orWhere('deskripsi', 'LIKE', '%'.Input::get('cari').'%');
            })
            ->paginate($_ENV['configurations']['list-limit']);
        }else{
            $data = $this->kuesioner
            ->select('mst_kuesioner.*',\DB::raw("COUNT(b.id) AS jml_pertanyaan"))
            ->join('mst_pertanyaan AS b','b.id_kuesioner','=','mst_kuesioner.id')
            ->whereNull('deleted_at')
            ->groupby('b.id_kuesioner')
            ->paginate($_ENV['configurations']['list-limit']);
        }
		return $data;
	}

    public function getCreate(){
        return View::make('kuesioner::create');
    }

    public function postCreate(){
        $input = array(
            'nama_kuesioner' => \Input::get('nama_kuesioner'),
            'deskripsi' => \Input::get('deskripsi')
        );
        $validation = \Validator::make($input, KuesionerModel::$rules);
        if($validation->passes()){
            $input['user_id'] = \Session::get('user_id');
            $input['role_id'] = \Session::get('role_id');
            $input['created_at'] = date('Y-m-d H:i:s');
            $simpan = $this->kuesioner->create($input);

            if($simpan){
                $last = $this->kuesioner
                ->where('nama_kuesioner','=',\Input::get('nama_kuesioner'))
                ->orderby('id','DESC')
                ->first();

                for($n=0 ; $n<sizeof(\Input::get('kuesioner')['pertanyaan']) ; $n++){
                    $ins = \DB::table('mst_pertanyaan')
                    ->insert(array(
                        'pertanyaan' => \Input::get('kuesioner')['pertanyaan'][$n]['tanya'],
                        'id_unsur_skm' => \Input::get('kuesioner')['pertanyaan'][$n]['id'],
                        'id_kuesioner' => $last->id,
                        'user_id' => \Session::get('user_id'),
                        'role_id' => \Session::get('role_id'),
                        'created_at' => date('Y-m-d H:i:s')
                    ));

                    $last_tanya = \DB::table('mst_pertanyaan')
                    ->where('id_kuesioner','=',$last->id)
                    ->where('id_unsur_skm','=',\Input::get('kuesioner')['pertanyaan'][$n]['id'])
                    ->where('pertanyaan','=',\Input::get('kuesioner')['pertanyaan'][$n]['tanya'])
                    ->orderby('id','DESC')
                    ->first();

                    for($i=0 ; $i<sizeof(\Input::get('kuesioner')['pertanyaan'][$n]['pilihan']) ; $i++){
                        $ins = \DB::table('mst_pilihan')
                        ->insert(array(
                            'id_pertanyaan' => $last_tanya->id,
                            'pilihan' => \Input::get('kuesioner')['pertanyaan'][$n]['pilihan'][$i],
                            'poin' => $i+1,
                            'user_id' => \Session::get('user_id'),
                            'role_id' => \Session::get('role_id'),
                            'created_at' => date('Y-m-d H:i:s')
                        ));
                    }
                }

                $result = array('result' => 'success');
            }else{
                $result = array('result' => 'failed');
            }
        }
        else{
            $result = array('result' => 'invalid');
        }

        return json_encode($result);
    }



    //{controller-show}

    public function getEdit(){
        return View::make('kuesioner::edit');
    }

    public function getDataedit(){
        $id = Request::segment(4);
        $data = $this->kuesioner->whereNull('deleted_at')->find($id);
        $data->_token = csrf_token();

        $pertanyaan = \DB::table('mst_pertanyaan AS a')
        ->select('b.kode','b.nama_unsur','a.id','a.pertanyaan AS tanya')
        ->join('mst_unsur_skm AS b','b.id','=','a.id_unsur_skm')
        ->where('a.id_kuesioner','=',$id)
        ->get();

        $n=0;
        foreach($pertanyaan AS $row){
            $pilihan = \DB::table('mst_pilihan')
            ->select('id','pilihan','poin')
            ->where('id_pertanyaan','=',$row->id)
            ->get();
            $pertanyaan[$n]->pilihan = $pilihan;
            $n++;
        }

        $data->kuesioner = $pertanyaan;

        return json_encode($data);
    }
    
    public function postEdit(){
        $id = Input::get('id');
        $input = array(
            'nama_kuesioner' => \Input::get('nama_kuesioner'),
            'deskripsi' => \Input::get('deskripsi')
        );
        $validation = \Validator::make($input, KuesionerModel::$rules);
        
        if($validation->passes()){
            $kuesioner = $this->kuesioner->find($id);
            $kuesioner->update($input);

            for($n=0 ; $n<sizeof(\Input::get('kuesioner')) ; $n++){
                $upd = \DB::table('mst_pertanyaan')
                ->where('id','=',\Input::get('kuesioner')[$n]['id'])
                ->update(array('pertanyaan' => \Input::get('kuesioner')[$n]['tanya']));

                for($i=0 ; $i<sizeof(\Input::get('kuesioner')[$n]['pilihan']) ; $i++){
                    $upd = \DB::table('mst_pilihan')
                    ->where('id','=',\Input::get('kuesioner')[$n]['pilihan'][$i]['id'])
                    ->update(array('pilihan' => \Input::get('kuesioner')[$n]['pilihan'][$i]['pilihan']));
                }
            }

            if($kuesioner){
                $result = array('result' => 'success');
            }else{
                $result = array('result' => 'failed');
            }
        }
        else{
            $result = array('result' => 'invalid');
        }

        return json_encode($result);
    }


	
    public function postDelete(){
        $ids = Input::get('id');
        if (is_array($ids)){
            foreach($ids as $id){
                $hapus = $this->kuesioner->find($id)->update(array('deleted_at' => date('Y-m-d H:i:s')));
            }
        }
        else{
            $hapus = $this->kuesioner->find($ids)->update(array('deleted_at' => date('Y-m-d H:i:s')));
        }

        if($hapus){
            $result = array('result' => 'success');
        }else{
            $result = array('result' => 'failed');
        }

        return json_encode($result);
    }

    public function getUnsur(){
        $data = \DB::table('mst_unsur_skm')
        ->select('id','nama_unsur','kode')
        ->get();

        return $data;
    }

}
