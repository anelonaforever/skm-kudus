<style>
    .kuesioner-table tbody tr td{
        vertical-align: middle;
        text-align: center;
    }
</style>
<section class="content-header">
    <h1>
        Buat Kuesioner<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li><a href="{!!ClaravelHelpers::backCreate()!!}"> Users</a></li>
        <li class="active">Buat Kuesioner</li>
    </ol>
</section>
<section class="content" ng-controller="KuesionerControllerCreate">
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <form name="moduleForm" class="form-horizontal" ng-submit="submitForm()" novalidate id="simpan" autocomplete="off">
                        <div class="form-group">
							<label for='nama_kuesioner' class='col-sm-2 control-label'>Nama Kuesioner:</label>
							<div class="col-sm-6">
								<input class='form-control' ng-model='formData.nama_kuesioner' name='nama_kuesioner' id='nama_kuesioner' type='text'>
							</div>
						</div>
						<div class="form-group">
							<label for='deskripsi' class='col-sm-2 control-label'>Deskripsi:</label>
							<div class="col-sm-6">
								<textarea ng-model='formData.deskripsi' class="form-control"></textarea>
							</div>
						</div>
                        <div class="form-group" ng-repeat="item in formData.kuesioner.pertanyaan">
							<label for='deskripsi' class='col-sm-2 control-label'>[[$index+1]]:</label>
							<div class="col-sm-6">
                                Unsur: [[item.nama_unsur]]
                                <textarea ng-model='formData.kuesioner.pertanyaan[$index].tanya' class="form-control" placeholder="Pertanyaan..." required></textarea>
								<table class="table table-condensed kuesioner-table">
                                    <thead>
                                        <tr>
                                            <td width="50px">Poin</td>
                                            <td>Jawaban</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td><input type="text" class="form-control" ng-model="formData.kuesioner.pertanyaan[$index].pilihan[0]" required></td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td><input type="text" class="form-control" ng-model="formData.kuesioner.pertanyaan[$index].pilihan[1]" required></td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td><input type="text" class="form-control" ng-model="formData.kuesioner.pertanyaan[$index].pilihan[2]" required></td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td><input type="text" class="form-control" ng-model="formData.kuesioner.pertanyaan[$index].pilihan[3]" required></td>
                                        </tr>
                                    </tbody>
                                </table>
							</div>
						</div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-6">
                                {!! ClaravelHelpers::btnSave('moduleForm')!!}
                                &nbsp;
                                &nbsp;
                                {!! ClaravelHelpers::btnCancel()!!}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>