<section class="content-header">
    <h1>
        Edit Kuesioner<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li><a href="{!!ClaravelHelpers::backEdit()!!}"> Users</a></li>
        <li class="active">Edit Kuesioner</li>
    </ol>
</section>
<section class="content" ng-controller="KuesionerControllerEdit">
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <form name="moduleForm" class="form-horizontal" ng-submit="submitForm()" id="simpan">
                        <div class="form-group">
							<label for='nama_kuesioner' class='col-sm-2 control-label'>Nama Kuesioner:</label>
							<div class="col-sm-6">
								<input class='form-control' ng-model='formData.nama_kuesioner' name='nama_kuesioner' id='nama_kuesioner' type='text'>
							</div>
						</div>
						<div class="form-group">
							<label for='deskripsi' class='col-sm-2 control-label'>Deskripsi:</label>
							<div class="col-sm-6">
								<textarea ng-model='formData.deskripsi' class="form-control"></textarea>
							</div>
						</div>
                        <div class="form-group" ng-repeat="item in formData.kuesioner">
							<label for='deskripsi' class='col-sm-2 control-label'>[[$index+1]]:</label>
							<div class="col-sm-6">
                                Unsur: [[item.nama_unsur]]
                                <textarea ng-model='formData.kuesioner[$index].tanya' class="form-control" placeholder="Pertanyaan..."></textarea>
								<table class="table table-condensed kuesioner-table">
                                    <thead>
                                        <tr>
                                            <td width="50px">Poin</td>
                                            <td>Jawaban</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="pil in formData.kuesioner[$index].pilihan">
                                            <td>[[pil.poin]]</td>
                                            <td><input type="text" class="form-control" ng-model="pil.pilihan"></td>
                                        </tr>
                                    </tbody>
                                </table>
							</div>
						</div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                {!! ClaravelHelpers::btnSave('moduleForm')!!}
                                &nbsp;
                                &nbsp;
                                <button class="btn btn-default" type="button" ng-click="reset()"><i class="ion-ios-undo"></i> Reset</button>
                                &nbsp;
                                &nbsp;
                                {!! ClaravelHelpers::btnCancelEdit(); !!}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>