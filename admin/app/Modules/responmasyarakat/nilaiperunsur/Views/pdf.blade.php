<?php

use rizalafani\fpdflaravel\Fpdf as FPDFS;

require app_path() . "/easyTable.php";

class exFPDF extends FPDFS
{

    public function Header()
    {
        $opd = \DB::table('mst_opd')
            ->where('id', '=', \Input::get('id_opd'))
            ->first();

        $layanan = \DB::table('mst_layanan')
            ->where('id', '=', \Input::get('id_layanan'))
            ->first();

        // Arial bold 15
        $this->SetFont('Arial', 'B', 12);
        // Title
        $this->Cell(180, 7, 'DATA SURVEY KEPUASAN MASYARAKAT PER UNSUR PELAYANAN', 0, 1, 'C');
        $this->SetFont('Arial', 'B', 7);
        $this->Cell(180, 7, 'OPD ' . strtoupper($opd->nama_opd), 0, 1, 'C');
        if (\Input::has('id_layanan')) {
            $this->Cell(180, 7, strtoupper($layanan->nama_layanan), 0, 1, 'C');
        }
        $this->Ln(5);
    }

    public function Footer()
    {
        $this->SetY(-11);
        $this->SetFont('Arial', 'I', 7);
        $this->Cell(78, 0, 'Dicetak pada tanggal : ' . date('d-m-Y') . ' | Aku Puas! Kabupaten Kudus', 0, 1, 'R');
    }

    public function PageBreak()
    {
        return $this->PageBreakTrigger;
    }

    public function current_font($c)
    {
        if ($c == 'family') {
            return $this->FontFamily;
        } elseif ($c == 'style') {
            return $this->FontStyle;
        } elseif ($c == 'size') {
            return $this->FontSizePt;
        }
    }

    public function get_color($c)
    {
        if ($c == 'fill') {
            return $this->FillColor;
        } elseif ($c == 'text') {
            return $this->TextColor;
        }
    }

    public function get_page_width()
    {
        return $this->w;
    }

    public function get_margin($c)
    {
        if ($c == 'l') {
            return $this->lMargin;
        } elseif ($c == 'r') {
            return $this->rMargin;
        } elseif ($c == 't') {
            return $this->tMargin;
        }
    }

    public function get_linewidth()
    {
        return $this->LineWidth;
    }

    public function get_orientation()
    {
        return $this->CurOrientation;
    }
    /***********************************************************************
     *
     * Based on FPDF method SetFont
     *
     ************************************************************************/

    private function &FontData($family, $style, $size)
    {
        if ($family == '') {
            $family = $this->FontFamily;
        } else {
            $family = strtolower($family);
        }

        $style = strtoupper($style);
        if (strpos($style, 'U') !== false) {
            $style = str_replace('U', '', $style);
        }
        if ($style == 'IB') {
            $style = 'BI';
        }

        $fontkey = $family . $style;
        if (!isset($this->fonts[$fontkey])) {
            if ($family == 'arial') {
                $family = 'helvetica';
            }

            if (in_array($family, $this->CoreFonts)) {
                if ($family == 'symbol' || $family == 'zapfdingbats') {
                    $style = '';
                }

                $fontkey = $family . $style;
                if (!isset($this->fonts[$fontkey])) {
                    $this->AddFont($family, $style);
                }

            } else {
                $this->Error('Undefined font: ' . $family . ' ' . $style);
            }

        }
        $result['FontSize'] = $size / $this->k;
        $result['CurrentFont'] = &$this->fonts[$fontkey];
        return $result;
    }

    /***********************************************************************
     *
     * Based on FPDF method MultiCell
     *
     ************************************************************************/

    public function extMultiCell($font_family, $font_style, $font_size, $w, $txt)
    {
        $result = array();
        $font = $this->FontData($font_family, $font_style, $font_size);
        $cw = $font['CurrentFont']['cw'];
        if ($w == 0) {
            return $result;
        }
        $wmax = $w * 1000 / $font['FontSize'];
        $s = trim(str_replace("\r", '', $txt));
        $chs = strlen($s);
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        while ($i < $chs) {
            $c = $s[$i];
            if ($c == "\n") {
                $result[] = substr($s, $j, $i - $j);
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                continue;
            }
            if ($c == ' ') {
                if ($l == 0) {
                    while ($s[$i] == ' ') {
                        $i++;
                    }
                    $j = $i;
                }
                $sep = $i;
            }
            $l += $cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j) {
                        $i++;
                    }

                    $result[] = substr($s, $j, $i - $j);
                } else {
                    $result[] = substr($s, $j, $sep - $j);
                    $i = $sep + 1;
                }
                $sep = -1;
                $j = $i;
                $l = 0;
            } else {
                $i++;
            }
        }
        $s = trim(substr($s, $j, $i - $j));
        if ($s != '') {
            $result[] = $s;
        }
        return $result;
    }

    /***********************************************************************
     *
     * Based on FPDF method Cell
     *
     ************************************************************************/

    public function CellBlock($w, $h, &$array_txt, $align = 'J', $link = '')
    {
        if (!isset($this->CurrentFont)) {
            $this->Error('No font has been set');
        }

        foreach ($array_txt as $ti => $txt) {
            $k = $this->k;
            if ($this->y + $h > $this->PageBreakTrigger) {
                break;
            }
            if ($w == 0) {
                return;
            }
            $s = '';
            if ($txt !== '') {
                $stringWidth = $this->GetStringWidth($txt);
                if ($align == 'R') {
                    $dx = $w - $stringWidth;
                } elseif ($align == 'C') {
                    $dx = ($w - $stringWidth) / 2;
                } else {
                    $dx = 0;
                }
                if ($align == 'J') {
                    $ns = count(explode(' ', $txt));
                    $wx = $w;
                    $this->ws = ($ns > 1) ? (($wx - $stringWidth) * (1 / ($ns - 1))) : 0;
                    $this->_out(sprintf('%.3F Tw', $this->ws * $this->k));
                }
                if ($this->ColorFlag) {
                    $s .= 'q ' . $this->TextColor . ' ';
                }

                $s .= sprintf('BT %.2F %.2F Td (%s) Tj ET', ($this->x + $dx) * $k, ($this->h - ($this->y + .5 * $h + .3 * $this->FontSize)) * $k, $this->_escape($txt));
                if ($this->underline) {
                    $s .= ' ' . $this->_dounderline($this->x + $dx, $this->y + .5 * $h + .3 * $this->FontSize, $txt);
                }

                if ($this->ColorFlag) {
                    $s .= ' Q';
                }

                if ($link) {
                    $this->Link($this->x + $dx, $this->y + .5 * $h - .5 * $this->FontSize, $stringWidth, $this->FontSize, $link);
                }

                unset($array_txt[$ti]);
            }
            if ($s) {
                $this->_out($s);
            }

            $this->lasth = $h;
            $this->y += $h;
        }
        if ($this->ws > 0) {
            $this->ws = 0;
            $this->_out('0 Tw');
        }
    }
}

$data = \DB::table('tr_respon AS a')
    ->select('a.id', 'a.created_at', \DB::raw("SUM(IF(b.id_unsur_skm = 1, b.poin, NULL)) AS u1"),
        \DB::raw("SUM(IF(b.id_unsur_skm = 2, b.poin, NULL)) AS u2"),
        \DB::raw("SUM(IF(b.id_unsur_skm = 3, b.poin, NULL)) AS u3"),
        \DB::raw("SUM(IF(b.id_unsur_skm = 4, b.poin, NULL)) AS u4"),
        \DB::raw("SUM(IF(b.id_unsur_skm = 5, b.poin, NULL)) AS u5"),
        \DB::raw("SUM(IF(b.id_unsur_skm = 6, b.poin, NULL)) AS u6"),
        \DB::raw("SUM(IF(b.id_unsur_skm = 7, b.poin, NULL)) AS u7"),
        \DB::raw("SUM(IF(b.id_unsur_skm = 8, b.poin, NULL)) AS u8"),
        \DB::raw("SUM(IF(b.id_unsur_skm = 9, b.poin, NULL)) AS u9"))
    ->join('tr_respon_detail AS b', 'b.id_respon', '=', 'a.id')
    ->where('a.id_opd', '=', \Input::get('id_opd'));

if (\Input::has('id_layanan')) {
    $data = $data->where('a.id_layanan', '=', \Input::get('id_layanan'));
}

$data = $data->groupby('b.id_respon')->get();

foreach ($data as $row) {
    $row->created_at = date('d-m-Y H:i', strtotime($row->created_at));
}

$pdf = new exFPDF('P', 'mm', 'A4');

$pdf->SetAutoPageBreak(true, 5);
$pdf->SetMargins(15, 10, 15);
$pdf->AddPage();

$pdf->SetFont('Arial', '', '8');

$table = new easyTable($pdf, '%{5, 23, 8, 8, 8, 8, 8, 8, 8, 8, 8}', 'align:C; border:1');
$table->easyCell('No', 'rowspan:2 ; align:C');
$table->easyCell('Tgl Survey', 'rowspan:2 ; align:C');
$table->easyCell('NILAI UNSUR PELAYANAN', 'colspan:9 ; align:C');
$table->printRow();

$table->easyCell('U1', 'align:C');
$table->easyCell('U2', 'align:C');
$table->easyCell('U3', 'align:C');
$table->easyCell('U4', 'align:C');
$table->easyCell('U5', 'align:C');
$table->easyCell('U6', 'align:C');
$table->easyCell('U7', 'align:C');
$table->easyCell('U8', 'align:C');
$table->easyCell('U9', 'align:C');
$table->printRow();

$no = 1;
$ju1 = 0;
$ju2 = 0;
$ju3 = 0;
$ju4 = 0;
$ju5 = 0;
$ju6 = 0;
$ju7 = 0;
$ju8 = 0;
$ju9 = 0;
foreach ($data as $row) {
    $table->easyCell($no, 'align:C');
    $table->easyCell($row->created_at, 'align:C');
    $table->easyCell($row->u1, 'align:C');
    $table->easyCell($row->u2, 'align:C');
    $table->easyCell($row->u3, 'align:C');
    $table->easyCell($row->u4, 'align:C');
    $table->easyCell($row->u5, 'align:C');
    $table->easyCell($row->u6, 'align:C');
    $table->easyCell($row->u7, 'align:C');
    $table->easyCell($row->u8, 'align:C');
    $table->easyCell($row->u9, 'align:C');
    $table->printRow();

    $ju1 += $row->u1;
    $ju2 += $row->u2;
    $ju3 += $row->u3;
    $ju4 += $row->u4;
    $ju5 += $row->u5;
    $ju6 += $row->u6;
    $ju7 += $row->u7;
    $ju8 += $row->u8;
    $ju9 += $row->u9;

    $no++;
}

$table->easyCell('Jumlah', 'colspan:2', 'align:C');
$table->easyCell($ju1, 'align:C');
$table->easyCell($ju2, 'align:C');
$table->easyCell($ju3, 'align:C');
$table->easyCell($ju4, 'align:C');
$table->easyCell($ju5, 'align:C');
$table->easyCell($ju6, 'align:C');
$table->easyCell($ju7, 'align:C');
$table->easyCell($ju8, 'align:C');
$table->easyCell($ju9, 'align:C');
$table->printRow();

$table->easyCell('Nilai Rata-rata', 'colspan:2', 'align:C');
$table->easyCell(ROUND($ju1 / ($no - 1), 1), 'align:C');
$table->easyCell(ROUND($ju2 / ($no - 1), 1), 'align:C');
$table->easyCell(ROUND($ju3 / ($no - 1), 1), 'align:C');
$table->easyCell(ROUND($ju4 / ($no - 1), 1), 'align:C');
$table->easyCell(ROUND($ju5 / ($no - 1), 1), 'align:C');
$table->easyCell(ROUND($ju6 / ($no - 1), 1), 'align:C');
$table->easyCell(ROUND($ju7 / ($no - 1), 1), 'align:C');
$table->easyCell(ROUND($ju8 / ($no - 1), 1), 'align:C');
$table->easyCell(ROUND($ju9 / ($no - 1), 1), 'align:C');
$table->printRow();

$table->easyCell('Nilai Rata-rata Konversi (*25)', 'colspan:2', 'align:C');
$table->easyCell(ROUND(($ju1 / ($no - 1)) * 25, 1), 'align:C');
$table->easyCell(ROUND(($ju2 / ($no - 1)) * 25, 1), 'align:C');
$table->easyCell(ROUND(($ju3 / ($no - 1)) * 25, 1), 'align:C');
$table->easyCell(ROUND(($ju4 / ($no - 1)) * 25, 1), 'align:C');
$table->easyCell(ROUND(($ju5 / ($no - 1)) * 25, 1), 'align:C');
$table->easyCell(ROUND(($ju6 / ($no - 1)) * 25, 1), 'align:C');
$table->easyCell(ROUND(($ju7 / ($no - 1)) * 25, 1), 'align:C');
$table->easyCell(ROUND(($ju8 / ($no - 1)) * 25, 1), 'align:C');
$table->easyCell(ROUND(($ju9 / ($no - 1)) * 25, 1), 'align:C');
$table->printRow();

$ikm = ((($ju1 / ($no - 1)) * 25) 
        + (($ju2 / ($no - 1)) * 25) 
        + (($ju3 / ($no - 1)) * 25)
        + (($ju4 / ($no - 1)) * 25)
        + (($ju5 / ($no - 1)) * 25)
        + (($ju6 / ($no - 1)) * 25)
        + (($ju7 / ($no - 1)) * 25)
        + (($ju8 / ($no - 1)) * 25)
        + (($ju9 / ($no - 1)) * 25))
        / 9 ;


$table->easyCell('IKM', 'colspan:2', 'align:C');
$table->easyCell(ROUND($ikm, 1), 'align:C');
$table->easyCell('', 'colspan:8');
$table->printRow();

$table->endTable(5);

$pdf->Ln(5);

$unsur = \DB::table('mst_unsur_skm')
    ->get();

$pdf->Cell(80, 7, 'Keterangan: ', 0, 1, 'L');
$table = new easyTable($pdf, '{10, 70}', 'align:L; border:1');
$table->easyCell('Kode', 'align:C');
$table->easyCell('Unsur Pelayanan', 'align:C');
$table->printRow();

$no = 1;
foreach ($unsur as $row) {
    $table->easyCell('U' . $no, 'align:C');
    $table->easyCell($row->nama_unsur, 'align:L');
    $table->printRow();
    $no++;
}

$opd = \DB::table('mst_opd')
    ->where('id', '=', \Input::get('id_opd'))
    ->first();

$layanan = \DB::table('mst_layanan')
    ->where('id', '=', \Input::get('id_layanan'))
    ->first();

$judul = "Data Nilai IKM per Unsur Pelayanan - OPD " . $opd->nama_opd;

if (\Input::has('id_layanan')) {
    $judul .= ", " . $layanan->nama_layanan;
}

$pdf->Output($judul, 'I');
