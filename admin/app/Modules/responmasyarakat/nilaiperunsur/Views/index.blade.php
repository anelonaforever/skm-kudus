<section class="content-header">
    <h1>
        Nilai per Unsur<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li class="active">Nilai per Unsur</li>
    </ol>
</section>
<section class="content" table-tools ng-controller="NilaiperunsurController">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Pilih OPD!, dan Layanan (Optional)</h3>
                </div>
                <div class="box-body">
                    <form class="form-horizontal " ng-submit="submitForm()">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label">OPD:</label>
                                    <div class="col-sm-10">
                                        <select required class="form-control" ng-model="formData.id_opd" ng-options="obj.id_opd as obj.nama_opd for obj in opds" ng-change="getlayanan()">
                                            <option value="">.:Pilih OPD:.</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label">Layanan:</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" ng-model="formData.id_layanan" ng-options="obj.id_layanan as obj.nama_layanan for obj in layanans">
                                            <option value="">.:Pilih Layanan:.</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary"><i class="ion-checkmark"></i> Tampilkan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row" ng-show="nilai">
        <div class="col-md-12">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <center>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-4">
                                        <h3>[[nilai.u1]]</h3>
                                        <p>Unsur 1</p>
                                    </div>
                                    <div class="col-md-4">
                                        <h3>[[nilai.u2]]</h3>
                                        <p>Unsur 2</p>
                                    </div>
                                    <div class="col-md-4">
                                        <h3>[[nilai.u3]]</h3>
                                        <p>Unsur 3</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-4">
                                        <h3>[[nilai.u4]]</h3>
                                        <p>Unsur 4</p>
                                    </div>
                                    <div class="col-md-4">
                                        <h3>[[nilai.u5]]</h3>
                                        <p>Unsur 5</p>
                                    </div>
                                    <div class="col-md-4">
                                        <h3>[[nilai.u6]]</h3>
                                        <p>Unsur 6</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-4">
                                        <h3>[[nilai.u7]]</h3>
                                        <p>Unsur 7</p>
                                    </div>
                                    <div class="col-md-4">
                                        <h3>[[nilai.u8]]</h3>
                                        <p>Unsur 8</p>
                                    </div>
                                    <div class="col-md-4">
                                        <h3>[[nilai.u9]]</h3>
                                        <p>Unsur 9</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>
                </div>
                <!-- <div class="icon">
                    <i class="fa fa-shopping-cart"></i>
                </div> -->
                <a role="button" ng-click="downloadPdf()" class="small-box-footer">
                    Cetak Detail Nilai per Unsur <i class="fa fa-print"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="row" ng-show="nilai">
        <div class="col-md-5">
            <div class="box box-warning">
                <div class="box-header with-border">
                <h3 class="box-title">Keterangan</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-condensed table-striped">
                        <tr ng-repeat="row in keterangan">
                            <td>Unsur [[$index+1]]</td>
                            <td>:</td>
                            <td>[[row.nama_unsur]]</td>
                        </tr>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>
