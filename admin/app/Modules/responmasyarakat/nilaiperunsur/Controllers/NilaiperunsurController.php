<?php namespace App\Modules\responmasyarakat\nilaiperunsur\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\responmasyarakat\nilaiperunsur\Models\NilaiperunsurModel;
use Input;
use View;

/**
 * Nilaiperunsur Controller
 * @var Nilaiperunsur
 * Generate from Custom Laravel 5.1 by Aa Gun.
 *
 * Developed by Divisi Software Development - Dinustek.
 * Please write log when you do some modification, don't change anything unless you know what you do
 * Semarang, 2016
 */

class NilaiperunsurController extends Controller
{
    protected $nilaiperunsur;

    public function __construct(NilaiperunsurModel $nilaiperunsur)
    {
        $this->nilaiperunsur = $nilaiperunsur;
    }

    public function getIndex()
    {
        return View::make('nilaiperunsur::index');
    }

    public function getData()
    {
        $data = \DB::table('tr_respon AS a')
            ->select(\DB::raw("ROUND(AVG(CASE WHEN b.id_unsur_skm = 1 THEN b.poin ELSE null END)*25,1) AS u1"),
                \DB::raw("ROUND(AVG(CASE WHEN b.id_unsur_skm = 2 THEN b.poin ELSE null END)*25,1) AS u2"),
                \DB::raw("ROUND(AVG(CASE WHEN b.id_unsur_skm = 3 THEN b.poin ELSE null END)*25,1) AS u3"),
                \DB::raw("ROUND(AVG(CASE WHEN b.id_unsur_skm = 4 THEN b.poin ELSE null END)*25,1) AS u4"),
                \DB::raw("ROUND(AVG(CASE WHEN b.id_unsur_skm = 5 THEN b.poin ELSE null END)*25,1) AS u5"),
                \DB::raw("ROUND(AVG(CASE WHEN b.id_unsur_skm = 6 THEN b.poin ELSE null END)*25,1) AS u6"),
                \DB::raw("ROUND(AVG(CASE WHEN b.id_unsur_skm = 7 THEN b.poin ELSE null END)*25,1) AS u7"),
                \DB::raw("ROUND(AVG(CASE WHEN b.id_unsur_skm = 8 THEN b.poin ELSE null END)*25,1) AS u8"),
                \DB::raw("ROUND(AVG(CASE WHEN b.id_unsur_skm = 9 THEN b.poin ELSE null END)*25,1) AS u9"))
            ->join('tr_respon_detail AS b', 'b.id_respon', '=', 'a.id')
            ->where('a.id_opd', '=', \Input::get('id_opd'));

        if (\Input::has('id_layanan')) {
            $data = $data->where('a.id_layanan', '=', \Input::get('id_layanan'))
                ->groupby('a.id_opd', 'a.id_layanan');
        } else {
            $data = $data->groupby('a.id_opd');
        }

        $data = $data->get();

        return json_encode($data[0]);
    }

    public function getMaindata()
    {
        $data = \DB::table('mst_unsur_skm')
            ->get();

        $opds = \DB::table('mst_opd')
            ->select('id AS id_opd', 'nama_opd');

        if (\Session::get('role_id') == 4) {
            $user = \DB::table('users')
                ->where('id', '=', \Session::get('user_id'))
                ->first();

            $opds = $opds->where('id', '=', $user->id_opd);
        }

        $opds = $opds->get();

        return json_encode(array(
            'keterangan' => $data,
            'opds' => $opds,
        ));
    }

    public function getLayanan()
    {
        $data = \DB::table('layanan_opd AS a')
            ->select('a.id_layanan', 'b.nama_layanan')
            ->join('mst_layanan AS b', 'b.id', '=', 'a.id_layanan')
            ->where('a.id_opd', '=', \Input::get('id_opd'))
            ->get();

        return $data;
    }

    public function getDownloadpdf()
    {
        $contents = view('nilaiperunsur::pdf');
        $response = \Response::make($contents);
        $response->header('Content-Type', 'application/pdf');
        return $response;
    }

    //{controller-show}

}
