<?php namespace App\Modules\responmasyarakat\nilaiikm\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\responmasyarakat\nilaiikm\Models\NilaiikmModel;
use Input,View, Request, Form, File;

/**
* Nilaiikm Controller
* @var Nilaiikm
* Generate from Custom Laravel 5.1 by Aa Gun. 
*
* Developed by Divisi Software Development - Dinustek. 
* Please write log when you do some modification, don't change anything unless you know what you do
* Semarang, 2016
*/

class NilaiikmController extends Controller {
    protected $nilaiikm;

    public function __construct(NilaiikmModel $nilaiikm){
        $this->nilaiikm = $nilaiikm;
    }

        public function getIndex(){
        return View::make('nilaiikm::index');
    }

    public function getData(){
		if(Input::has('cari') AND strlen(Input::get('cari')) > 0){
            $data = $this->nilaiikm
            			->orWhere('nama_opd', 'LIKE', '%'.Input::get('cari').'%')

            ->paginate($_ENV['configurations']['list-limit']);
        }else{
            $data = $this->nilaiikm->paginate($_ENV['configurations']['list-limit']);
        }
		return $data;
	}

    

    //{controller-show}

    
	
    
}
