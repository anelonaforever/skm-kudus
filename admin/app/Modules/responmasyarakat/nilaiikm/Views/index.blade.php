<section class="content-header">
    <h1>
        Nilai IKM<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li class="active">Nilai IKM</li>
    </ol>
</section>
<section class="content" table-tools ng-controller="NilaiikmController">
    <div class="box box-primary box-table">
        <div class="box-header box-header-default">
            <div id="tablesearchbar" class="animated">
                <form ng-submit="caridata()">
                    <input ng-model="pencarian" id="kotakpencarian" type="text" class="form-control" placeholder="Cari di sini..." autocomplete="off">
                    <a role="button" class="btn-submit" type="submit"><i class="fa fa-angle-left"></i></a>
                    <a role="button" class="btn-close"><i class="fa fa-times"></i></a>
                </form>
            </div>
            <h3 class="box-title hidden-xs">Daftar Nilai IKM</h3>
            <div class="box-tools pull-right">
                <a id="searchbutton" type="button" class="btn btn-flat btn-box-tool"><i class="ion-android-search fa-lg"></i> Cari</a>
            </div>
        </div>
        <div class="table-responsive">
            <div class="box-body no-padding">
                <table class="table table-condensed table-striped table-hover" id="tabel">
                    <thead class='bg-default'>
                        <tr>
                            <th>Nama OPD</th>
                            <th>Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="record in Records">
                            <td class="nowrap"><a href="[[mainUrl]]/dashboard#/responmasyarakat/nilaiikm/[[record.id]]">[[record.nama_opd]]</a></td>
                            <td class="nowrap">
                                [[record.nilai]]
                            </td>
                        </tr>
                    </tbody>
                </table>
                <center ng-show="paging.totalItems>{!! $_ENV['configurations']['list-limit'] !!}">
                    <ul uib-pagination total-items="paging.totalItems" ng-model="paging.currentPage" max-size="5" class="pagination-sm" boundary-links="true" num-pages="paging.numPages" items-per-page="{!! $_ENV['configurations']['list-limit'] !!}" force-ellipses="true"></ul>
                </center>
            </div>
        </div>
    </div>
</section>
