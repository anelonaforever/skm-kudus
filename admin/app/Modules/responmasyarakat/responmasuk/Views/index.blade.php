<section class="content-header">
    <h1>
        Respon Masuk<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li class="active">Respon Masuk</li>
    </ol>
</section>
<section class="content" table-tools ng-controller="ResponmasukController">
    <div class="box box-primary box-table">
        <div class="box-header box-header-default">
            <div id="tablesearchbar" class="animated">
                <form ng-submit="caridata()">
                    <input ng-model="pencarian" id="kotakpencarian" type="text" class="form-control" placeholder="Cari di sini..." autocomplete="off">
                    <a role="button" class="btn-submit" type="submit"><i class="fa fa-angle-left"></i></a>
                    <a role="button" class="btn-close"><i class="fa fa-times"></i></a>
                </form>
            </div>
            <h3 class="box-title hidden-xs">Daftar Respon Masuk</h3>
            <div class="box-tools pull-right">
                <a id="searchbutton" type="button" class="btn btn-flat btn-box-tool"><i class="ion-android-search fa-lg"></i> Cari</a>
            </div>
        </div>
        <div class="table-responsive">
            <div class="box-body no-padding">
                <table class="table table-condensed table-striped table-hover" id="tabel">
                    <thead class='bg-default'>
                        <tr>
                            <th>Nama OPD</th>
                            <th>Layanan</th>
                            <th>Tgl / Waktu Survey</th>
                            <th>Nilai IKM</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat-start="record in Records">
                            <td class="nowrap">[[record.nama_opd]]</td>
                            <td class="nowrap">[[record.nama_layanan]]</td>
                            <td class="nowrap">[[record.tgl_survey]] WIB</td>
                            <td class="nowrap">[[record.nilai]]</td>
                            <th><a ng-click="expand($index)" role="button">Detail <i class="fa" ng-class="{'fa-chevron-down' : !record.expand, 'fa-chevron-up' : record.expand}" aria-hidden="true"></i></a></th>
                        </tr>
                        <tr ng-repeat-end ng-show="record.expand">
                            <td colspan="5" style="padding: 0 !important">
                                <table class="table table-condensed" style="margin-bottom: 0 !important">
                                    <tr class="bg-blue">
                                        <td colspan="2">Unsur SKM</td>
                                        <td>Penilaian</td>
                                    </tr>
                                    <tr ng-repeat="unsur in record.nilaiunsur">
                                        <td>[[unsur.nama_unsur]]</td>
                                        <td>:</td>
                                        <td>[[unsur.jawaban]]</td>
                                    </tr>
                                    <tr class="bg-yellow">
                                        <td>Saran</td>
                                        <td>:</td>
                                        <td>[[record.saran]]</td>
                                    </tr>
                                    <tr class="bg-blue">
                                        <td colspan="3">
                                            Responden:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="300px">Jenis Kelamin</td>
                                        <td width="50px">:</td>
                                        <td>[[record.jenis_kelamin]]</td>
                                    </tr>
                                    <tr>
                                        <td>Jenjang Pendidikan</td>
                                        <td>:</td>
                                        <td>[[record.jenjang]]</td>
                                    </tr>
                                    <tr>
                                        <td>Pekerjaan</td>
                                        <td>:</td>
                                        <td>[[record.pekerjaan]]</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <center ng-show="paging.totalItems>{!! $_ENV['configurations']['list-limit'] !!}">
                    <ul uib-pagination total-items="paging.totalItems" ng-model="paging.currentPage" max-size="5" class="pagination-sm" boundary-links="true" num-pages="paging.numPages" items-per-page="{!! $_ENV['configurations']['list-limit'] !!}" force-ellipses="true"></ul>
                </center>
            </div>
        </div>
        <div class="tombol-container">
            {!! ClaravelHelpers::btnCreate() !!}
            @if(\PermissionsLibrary::canDel())
            {!! ClaravelHelpers::btnDeleteAll('Respon Masuk', csrf_token()) !!}
            @endif
        </div>
    </div>
</section>
