<?php namespace App\Modules\responmasyarakat\responmasuk\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\responmasyarakat\responmasuk\Models\ResponmasukModel;
use Input;
use View;

/**
 * Responmasuk Controller
 * @var Responmasuk
 * Generate from Custom Laravel 5.1 by Aa Gun.
 *
 * Developed by Divisi Software Development - Dinustek.
 * Please write log when you do some modification, don't change anything unless you know what you do
 * Semarang, 2016
 */

class ResponmasukController extends Controller
{
    protected $responmasuk;

    public function __construct(ResponmasukModel $responmasuk)
    {
        $this->responmasuk = $responmasuk;
    }

    public function getIndex()
    {
        return View::make('responmasuk::index');
    }

    public function getData()
    {
        if (Input::has('cari') and strlen(Input::get('cari')) > 0) {
            $data = $this->responmasuk
                ->select('tr_respon.id', 'tr_respon.saran', 'mst_opd.nama_opd', 'mst_layanan.nama_layanan', 'tr_respon.created_at', 'pend.jenjang', 'pek.pekerjaan', \DB::raw("CASE WHEN tr_respon.jenis_kelamin = 1 THEN 'Laki-laki' ELSE 'Perempuan' END AS jenis_kelamin"), 'det.nilai')
                ->join('mst_opd', 'mst_opd.id', '=', 'tr_respon.id_opd')
                ->join('mst_layanan', 'mst_layanan.id', '=', 'tr_respon.id_layanan')
                ->join('mst_jenjang_pendidikan AS pend', 'pend.id', '=', 'tr_respon.id_jenjang_pendidikan')
                ->join('mst_pekerjaan AS pek', 'pek.id', '=', 'tr_respon.id_pekerjaan')
                ->join(\DB::raw('(SELECT id_respon, ROUND(AVG(poin) * 25,1) AS nilai FROM tr_respon_detail GROUP BY id_respon) AS det'), function ($join) {
                    $join->on('det.id_respon', '=', 'tr_respon.id');
                })
                ->where(function ($q) {
                    $q->where('mst_opd.nama_opd', 'LIKE', '%' . Input::get('cari') . '%')
                        ->orWhere('mst_layanan.nama_layanan', 'LIKE', '%' . Input::get('cari') . '%');
                });

            if (\Session::get('role_id') == 4) {
                $user = \DB::table('users')
                    ->where('id', '=', \Session::get('user_id'))
                    ->first();

                $data = $data->where('tr_respon.id_opd', '=', $user->id_opd);
            }

            $data = $data->orderby('tr_respon.created_at', 'DESC')
                ->paginate($_ENV['configurations']['list-limit']);
        } else {
            $data = $this->responmasuk
                ->select('tr_respon.id', 'tr_respon.saran', 'mst_opd.nama_opd', 'mst_layanan.nama_layanan', 'tr_respon.created_at', 'pend.jenjang', 'pek.pekerjaan', \DB::raw("CASE WHEN tr_respon.jenis_kelamin = 1 THEN 'Laki-laki' ELSE 'Perempuan' END AS jenis_kelamin"), 'det.nilai')
                ->join('mst_opd', 'mst_opd.id', '=', 'tr_respon.id_opd')
                ->join('mst_layanan', 'mst_layanan.id', '=', 'tr_respon.id_layanan')
                ->join('mst_jenjang_pendidikan AS pend', 'pend.id', '=', 'tr_respon.id_jenjang_pendidikan')
                ->join('mst_pekerjaan AS pek', 'pek.id', '=', 'tr_respon.id_pekerjaan')
                ->join(\DB::raw('(SELECT id_respon, ROUND(AVG(poin) * 25,1) AS nilai FROM tr_respon_detail GROUP BY id_respon) AS det'), function ($join) {
                    $join->on('det.id_respon', '=', 'tr_respon.id');
                });

            if (\Session::get('role_id') == 4) {
                $user = \DB::table('users')
                    ->where('id', '=', \Session::get('user_id'))
                    ->first();

                $data = $data->where('tr_respon.id_opd', '=', $user->id_opd);
            }

            $data = $data->orderby('tr_respon.created_at', 'DESC')
                ->paginate($_ENV['configurations']['list-limit']);
        }

        foreach ($data as $row) {
            $row->expand = false;
            $row->tgl_survey = date('d-m-Y / H:i', strtotime($row->created_at));
            $row->nilaiunsur = \DB::table('tr_respon_detail AS res')
                ->select('uns.nama_unsur', 'jawaban')
                ->join('mst_unsur_skm AS uns', 'uns.id', '=', 'res.id_unsur_skm')
                ->where('res.id_respon', '=', $row->id)
                ->get();
        }

        return $data;

    }

    //{controller-show}

}
