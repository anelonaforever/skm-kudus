<?php namespace App\Modules\pengaturan\pengguna\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\pengaturan\pengguna\Models\PenggunaModel;
use Input,View, Request, Form, File;

/**
* Pengguna Controller
* @var Pengguna
* Generate from Custom Laravel 5.1 by Aa Gun. 
*
* Developed by Divisi Software Development - Dinustek. 
* Please write log when you do some modification, don't change anything unless you know what you do
* Semarang, 2016
*/

class PenggunaController extends Controller {
    protected $pengguna;

    public function __construct(PenggunaModel $pengguna){
        $this->pengguna = $pengguna;
    }

        public function getIndex(){
        return View::make('pengguna::index');
    }

    public function getData(){
		if(Input::has('cari') AND strlen(Input::get('cari')) > 0){
            $data = $this->pengguna
            ->select('users.*','roles.name as role')
            ->join('roles','roles.id','=','users.role_id')
            ->where(function($q){
                $q->Where('users.name', 'LIKE', '%'.Input::get('cari').'%')
			    ->orWhere('users.username', 'LIKE', '%'.Input::get('cari').'%');
            })
            ->where('role_id','!=',1)
            ->where('role_id','!=',4)
            ->whereNull('deleted_at')
            ->paginate($_ENV['configurations']['list-limit']);
        }else{
            $data = $this->pengguna
            ->select('users.*','roles.name as role')
            ->join('roles','roles.id','=','users.role_id')
            ->where('role_id','!=',1)
            ->where('role_id','!=',4)
            ->whereNull('deleted_at')
            ->paginate($_ENV['configurations']['list-limit']);
        }
		return $data;
	}

    public function getCreate(){
        return View::make('pengguna::create');
    }

    public function postCreate(){
        $input = array(
            'name' => \Input::get('name'),
            'username' => \Input::get('username'),
            'password' => \Hash::make(\Input::get('password')),
            'role_id' => \Input::get('role_id'),
            'created_at' => date('Y-m-d H:i:s')
        );
        $validation = \Validator::make($input, PenggunaModel::$rules);
        if($validation->passes()){
            $simpan = $this->pengguna->create($input);

            if($simpan){
                $result = array('result' => 'success');
            }else{
                $result = array('result' => 'failed');
            }
        }
        else{
            $result = array('result' => 'invalid');
        }

        return json_encode($result);
    }



    //{controller-show}

    public function getEdit(){
        return View::make('pengguna::edit');
    }

    public function getDataedit(){
        $id = Request::segment(4);
        $data = $this->pengguna->select('id','name','username','role_id')->find($id);
        $data->_token = csrf_token();
        return json_encode($data);
    }
    
    public function postEdit(){
        $id = Input::get('id');
        $input = array(
            'name' => \Input::get('name'),
            'username' => \Input::get('username'),
            'role_id' => \Input::get('role_id'),
            'created_at' => date('Y-m-d H:i:s'),
        );
        if(\Input::get('ubahpwd') == true){
            $input['password'] = \Hash::make(\Input::get('password'));
        }
        $validation = \Validator::make($input, PenggunaModel::$rules);
        
        if($validation->passes()){
            $pengguna = $this->pengguna->find($id);
            $pengguna->update($input);

            if($pengguna){
                $result = array('result' => 'success');
            }else{
                $result = array('result' => 'failed');
            }
        }
        else{
            $result = array('result' => 'invalid');
        }

        return json_encode($result);
    }
	
    public function postDelete(){
        $ids = Input::get('id');
        if (is_array($ids)){
            foreach($ids as $id){
                $hapus = $this->pengguna->find($id)->update(array('deleted_at' => date('Y-m-d H:i:s')));
            }
        }
        else{
            $hapus = $this->pengguna->find($ids)->update(array('deleted_at' => date('Y-m-d H:i:s')));
        }

        if($hapus){
            $result = array('result' => 'success');
        }else{
            $result = array('result' => 'failed');
        }

        return json_encode($result);
    }

    public function getRole(){
        $data = \DB::table('roles')
        ->select('id AS role_id','name')
        ->where('id','!=',1)
        ->where('id','!=',4)
        ->get();

        return $data;
    }

}
