<section class="content-header">
    <h1>
        Pengguna<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li class="active">Pengguna</li>
    </ol>
</section>
<section class="content" table-tools ng-controller="PenggunaController">
    <div class="box box-primary box-table">
        <div class="box-header box-header-default">
            <div id="tablesearchbar" class="animated">
                <form ng-submit="caridata()">
                    <input ng-model="pencarian" id="kotakpencarian" type="text" class="form-control" placeholder="Cari di sini..." autocomplete="off">
                    <a role="button" class="btn-submit" type="submit"><i class="fa fa-angle-left"></i></a>
                    <a role="button" class="btn-close"><i class="fa fa-times"></i></a>
                </form>
            </div>
            <h3 class="box-title hidden-xs">Daftar Pengguna</h3>
            <div class="box-tools pull-right">
                @if(\PermissionsLibrary::canDel())
                <a class="btn-box-tool">Hapus Beberapa</a>
                <toggle ng-model="hapusbeberapa" size="btn-sm" onstyle="btn-warning"></toggle>
                @endif
                <a id="searchbutton" type="button" class="btn btn-flat btn-box-tool"><i class="ion-android-search fa-lg"></i> Cari</a>
            </div>
        </div>
        <div class="table-responsive">
            <div class="box-body no-padding">
                <table class="table table-condensed table-striped table-hover" id="tabel">
                    <thead class='bg-default'>
                        <tr>
                            @if(\PermissionsLibrary::canDel())
                            <th class="hapusbeberapa" ng-show="hapusbeberapa">
                                <center>
                                    <div class="checkbox checkbox-danger">
                                        <input type="checkbox" id="checkbox-checkall" ng-model="selectedAll" ng-click="checkAll()">
                                        <label for="checkbox-checkall">
                                            Pilih Semua
                                        </label>
                                    </div>
                                </center>
                            </th>
                            @endif
                            <th>Nama</th>
					        <th>Username</th>
					        <th>Role</th>
                            <th>Act.</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="record in Records">
                            @if(\PermissionsLibrary::canDel())
                            <td class="hapusbeberapa" ng-show="hapusbeberapa">
                                <center>
                                    <div class="checkbox checkbox-warning">
                                        <input type="checkbox" id="checkbox[[record.id]]" ng-model="record.Selected">
                                        <label for="checkbox[[record.id]]"></label>
                                    </div>
                                </center>
                            </td>
                            @endif
                            <td class="nowrap">[[record.name]]</td>
                            <td class="nowrap">[[record.username]]</td>
                            <td class="nowrap">[[record.role]]</td>
                            <td class="nowrap">
                                {!! ClaravelHelpers::btnEdit('[[location.path()]]', '[[record.id]]') !!}
                                &nbsp;&nbsp;&nbsp;
                                {!! ClaravelHelpers::btnDelete('Pengguna', '[[record.id]]') !!}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <center ng-show="paging.totalItems>{!! $_ENV['configurations']['list-limit'] !!}">
                    <ul uib-pagination total-items="paging.totalItems" ng-model="paging.currentPage" max-size="5" class="pagination-sm" boundary-links="true" num-pages="paging.numPages" items-per-page="{!! $_ENV['configurations']['list-limit'] !!}" force-ellipses="true"></ul>
                </center>
            </div>
        </div>
        <div class="tombol-container">
            {!! ClaravelHelpers::btnCreate() !!}
            @if(\PermissionsLibrary::canDel())
            {!! ClaravelHelpers::btnDeleteAll('Pengguna', csrf_token()) !!}
            @endif
        </div>
    </div>
</section>