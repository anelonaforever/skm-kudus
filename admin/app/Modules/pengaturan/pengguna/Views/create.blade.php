<section class="content-header">
    <h1>
        Buat Pengguna<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li><a href="{!!ClaravelHelpers::backCreate()!!}"> Users</a></li>
        <li class="active">Buat Pengguna</li>
    </ol>
</section>
<section class="content" ng-controller="PenggunaControllerCreate">
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <div class="col-md-8">
                    <form name="moduleForm" class="form-horizontal" ng-submit="submitForm()" novalidate id="simpan" autocomplete="off">
                        <div class="form-group">
							<label for='name' class='col-sm-2 control-label'>Nama:</label>
							<div class="col-sm-6">
								<input class='form-control' ng-model='formData.name' name='name' id='name' type='text'>
							</div>
						</div>
						<div class="form-group">
							<label for='username' class='col-sm-2 control-label'>Username:</label>
							<div class="col-sm-6">
								<input class='form-control' ng-model='formData.username' name='username' id='username' type='text'>
							</div>
						</div>
						<div class="form-group">
							<label for='role_id' class='col-sm-2 control-label'>Role:</label>
							<div class="col-sm-6">
								<select class="form-control" ng-model="formData.role_id" ng-options="obj.role_id as obj.name for obj in roles">
                                    <option value="">-Pilih Role-</option>
                                </select>
							</div>
						</div>
                        <div class="form-group">
							<label for='username' class='col-sm-2 control-label'>Password:</label>
							<div class="col-sm-6">
								<input class='form-control' ng-model='formData.password' name='password' id='password' type='text'>
							</div>
						</div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-6">
                                {!! ClaravelHelpers::btnSave('moduleForm')!!}
                                &nbsp;
                                &nbsp;
                                {!! ClaravelHelpers::btnCancel()!!}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>