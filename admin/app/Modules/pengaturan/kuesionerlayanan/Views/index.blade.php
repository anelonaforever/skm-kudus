<section class="content-header">
    <h1>
        Kuesioner Layanan<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li class="active">Kuesioner Layanan</li>
    </ol>
</section>
<section class="content" table-tools ng-controller="KuesionerlayananController">
    <div class="box box-primary box-table">
        <div class="table-responsive">
            <div class="box-body no-padding">
                <table class="table table-condensed table-striped table-hover" id="tabel">
                    <thead class='bg-default'>
                        <tr>
                            @if(\PermissionsLibrary::canDel())
                            <th class="hapusbeberapa" ng-show="hapusbeberapa">
                                <center>
                                    <div class="checkbox checkbox-danger">
                                        <input type="checkbox" id="checkbox-checkall" ng-model="selectedAll" ng-click="checkAll()">
                                        <label for="checkbox-checkall">
                                            Pilih Semua
                                        </label>
                                    </div>
                                </center>
                            </th>
                            @endif
                            <th>Nama Layanan</th>
					        <th>Kuesioner</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="record in Records">
                            @if(\PermissionsLibrary::canDel())
                            <td class="hapusbeberapa" ng-show="hapusbeberapa">
                                <center>
                                    <div class="checkbox checkbox-warning">
                                        <input type="checkbox" id="checkbox[[record.id]]" ng-model="record.Selected">
                                        <label for="checkbox[[record.id]]"></label>
                                    </div>
                                </center>
                            </td>
                            @endif
                            <td class="nowrap">[[record.nama_layanan]]</td>
					        <td class="nowrap">
                                <select class="form-control" ng-model="record.id_kuesioner" ng-options="obj.id_kuesioner as obj.nama_kuesioner for obj in kuesioner" ng-change="ubahkuesioner($index)">
                                    <option value="">--Pilih Kuesioner--</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <center ng-show="paging.totalItems>{!! $_ENV['configurations']['list-limit'] !!}">
                    <ul uib-pagination total-items="paging.totalItems" ng-model="paging.currentPage" max-size="5" class="pagination-sm" boundary-links="true" num-pages="paging.numPages" items-per-page="{!! $_ENV['configurations']['list-limit'] !!}" force-ellipses="true"></ul>
                </center>
            </div>
        </div>
        <div class="tombol-container">
            {!! ClaravelHelpers::btnCreate() !!}
            @if(\PermissionsLibrary::canDel())
            {!! ClaravelHelpers::btnDeleteAll('Kuesioner Layanan', csrf_token()) !!}
            @endif
        </div>
    </div>
</section>