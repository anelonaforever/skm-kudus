<?php namespace App\Modules\pengaturan\kuesionerlayanan\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\pengaturan\kuesionerlayanan\Models\KuesionerlayananModel;
use Input,View, Request, Form, File;

/**
* Kuesionerlayanan Controller
* @var Kuesionerlayanan
* Generate from Custom Laravel 5.1 by Aa Gun. 
*
* Developed by Divisi Software Development - Dinustek. 
* Please write log when you do some modification, don't change anything unless you know what you do
* Semarang, 2016
*/

class KuesionerlayananController extends Controller {
    protected $kuesionerlayanan;

    public function __construct(KuesionerlayananModel $kuesionerlayanan){
        $this->kuesionerlayanan = $kuesionerlayanan;
    }

    public function getIndex(){
        return View::make('kuesionerlayanan::index');
    }

    public function getData(){
        $data = $this->kuesionerlayanan
        ->select('id','nama_layanan','id_kuesioner')
        ->paginate($_ENV['configurations']['list-limit']);
		return $data;
	}

    public function getDatakuesioner(){
        $data = \DB::table('mst_kuesioner')
         ->select('id AS id_kuesioner','nama_kuesioner','deskripsi')
         ->whereNull('deleted_at')
         ->get();
        return $data;
    }

    public function postUbahkuesioner(){
        $upd = \DB::table('mst_layanan')
        ->where('id','=',\Input::get('id_layanan'))
        ->update(array('id_kuesioner' => \Input::get('id_kuesioner')));

        if($upd){
            $result = array('result' => 'success');
        }else{
            $result = array('result' => 'failed');
        }

        return json_encode($result);

    }
}
