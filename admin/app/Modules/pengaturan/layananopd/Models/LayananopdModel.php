<?php namespace App\Modules\pengaturan\layananopd\Models;
use Illuminate\Database\Eloquent\Model;


/**
* Layananopd Model
* @var Layananopd
* Generate from Custom Laravel 5.1 by Aa Gun. 
*
* Developed by Dinustek. 
* Please write log when you do some modification, don't change anything unless you know what you do
* Semarang, 2016
*/

class LayananopdModel extends Model {
	protected $guarded = array();
	
	protected $table = "mst_opd";

	public static $rules = array(
    		'id_opd' => 'required',
		'id_layanan' => 'required',

    );

	public static function all($columns = array('*')){
		$instance = new static;
		if (\PermissionsLibrary::hasPermission('mod-layananopd-listall')){
			return $instance->newQuery()->paginate($_ENV['configurations']['list-limit']);
		}else{
			return $instance->newQuery()
			->where('role_id', \Session::get('role_id'))
			->paginate($_ENV['configurations']['list-limit']);	
			
		}
	}

}
