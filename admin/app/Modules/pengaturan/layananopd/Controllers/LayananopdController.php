<?php namespace App\Modules\pengaturan\layananopd\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\pengaturan\layananopd\Models\LayananopdModel;
use Input,View, Request, Form, File;

/**
* Layananopd Controller
* @var Layananopd
* Generate from Custom Laravel 5.1 by Aa Gun. 
*
* Developed by Divisi Software Development - Dinustek. 
* Please write log when you do some modification, don't change anything unless you know what you do
* Semarang, 2016
*/

class LayananopdController extends Controller {
    protected $layananopd;

    public function __construct(LayananopdModel $layananopd){
        $this->layananopd = $layananopd;
    }

    public function getIndex(){
        return View::make('layananopd::index');
    }

    public function getData(){
        $id_opd = \DB::table('users')
        ->where('id','=',\Session::get('user_id'))
        ->first()->id_opd;

		if(Input::has('cari') AND strlen(Input::get('cari')) > 0){
            $data = $this->layananopd
            ->select('mst_opd.id','mst_opd.nama_opd',\DB::raw("COUNT(lay.id) AS jml_layanan"))
            ->leftjoin('layanan_opd','layanan_opd.id_opd','=','mst_opd.id')
            ->leftjoin('mst_layanan AS lay','lay.id','=','layanan_opd.id_layanan');

            if(\Session::get('role_id') == 4){
                $data=$data->where('mst_opd.id','=',$id_opd);
            }

            $data=$data
            ->Where('mst_opd.nama_opd', 'LIKE', '%'.Input::get('cari').'%')
            ->groupby('mst_opd.id')
            ->paginate($_ENV['configurations']['list-limit']);
        }else{
            $data = $this->layananopd
            ->select('mst_opd.id','mst_opd.nama_opd',\DB::raw("COUNT(lay.id) AS jml_layanan"))
            ->leftjoin('layanan_opd','layanan_opd.id_opd','=','mst_opd.id')
            ->leftjoin('mst_layanan AS lay','lay.id','=','layanan_opd.id_layanan');

            if(\Session::get('role_id') == 4){
                $data=$data->where('mst_opd.id','=',$id_opd);
            }

            $data=$data
            ->groupby('mst_opd.id')
            ->paginate($_ENV['configurations']['list-limit']);
        }

        $n=0;
        foreach($data AS $row){
            $layanans = \DB::table('layanan_opd')
            ->select('lay.id','lay.nama_layanan')
            ->join('mst_layanan AS lay','lay.id','=','layanan_opd.id_layanan')
            ->where('layanan_opd.id_opd','=',$row->id)
            ->get();

            $data[$n]->layanan = $layanans;
            $data[$n]->collapse = false;
            $n++;
        }

		return $data;
	}

    public function getCreate(){
        return View::make('layananopd::create');
    }

    public function getCarilayanan(){
        $data = \DB::table('mst_layanan')
        ->select('id','nama_layanan')
        ->where('nama_layanan','LIKE','%'.\Input::get('cari').'%');
        // ->whereNotIn('id', function($q){
        //     $q->select('id_layanan AS id')
        //     ->from('layanan_opd')
        //     ->where('id_opd','=',\Input::get('id_opd'));
        // });

        if(\Input::has('selected')){
            $selected = json_decode(\Input::get('selected'));

            if(is_array($selected)){
                $data=$data->whereNotIn('id',$selected);
            }else{
                $data=$data->where('id','!=',$selected);
            }
        }
        
        $data=$data->get();

        return json_encode($data);
    }

    public function postCreate(){
        $input = Input::all();
        $validation = \Validator::make($input, LayananopdModel::$rules);
        if($validation->passes()){
            $input['user_id'] = \Session::get('user_id');
            $input['role_id'] = \Session::get('role_id');
            $simpan = $this->layananopd->create($input);

            if($simpan){
                $result = array('result' => 'success');
            }else{
                $result = array('result' => 'failed');
            }
        }
        else{
            $result = array('result' => 'invalid');
        }

        return json_encode($result);
    }


    //{controller-show}

    public function getEdit(){
        return View::make('layananopd::edit');
    }

    public function getDataedit(){
        $id = Request::segment(4);
        $data = $this->layananopd->find($id);

        $layanans = \DB::table('layanan_opd')
        ->select('lay.id','lay.nama_layanan')
        ->join('mst_layanan AS lay','lay.id','=','layanan_opd.id_layanan')
        ->where('layanan_opd.id_opd','=',$id)
        ->get();


        $data->_token = csrf_token();
        $data->layanans = $layanans;
        return json_encode($data);
    }
    
    public function postEdit(){
        $del = \DB::table('layanan_opd')
        ->where('id_opd','=',\Input::get('id_opd'))
        ->delete();

        $layanans = json_decode(\Input::get('layanans'));

        for($n=0 ; $n<sizeof($layanans) ; $n++){
            $ins = \DB::table('layanan_opd')
            ->insert(array('id_opd' => \Input::get('id_opd'), 'id_layanan' => $layanans[$n], 'id_kuesioner' => 1, 'role_id' => \Session::get('role_id'), 'user_id' => \Session::get('user_id')));
        }

        if($ins){
            $result = array('result' => 'success');
        }else{
            $result = array('result' => 'failed');
        }

        return json_encode($result);
    }


	
    public function postDelete(){
        $ids = Input::get('id');
        if (is_array($ids)){
            foreach($ids as $id){
                $hapus = $this->layananopd->find($id)->delete();
            }
        }
        else{
            $hapus = $this->layananopd->find($ids)->delete();
        }

        if($hapus){
            $result = array('result' => 'success');
        }else{
            $result = array('result' => 'failed');
        }

        return json_encode($result);
    }

    public function postHapuslayanan(){
        $del = \DB::table('layanan_opd')
        ->where('id_opd','=',\Input::get('id_opd'))
        ->where('id_layanan','=',\Input::get('id_layanan'))
        ->delete();
    }

}
