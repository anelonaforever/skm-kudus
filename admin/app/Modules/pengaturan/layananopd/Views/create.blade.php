<section class="content-header">
    <h1>
        Buat Layanan OPD<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li><a href="{!!ClaravelHelpers::backCreate()!!}"> Users</a></li>
        <li class="active">Buat Layanan OPD</li>
    </ol>
</section>
<section class="content" ng-controller="LayananopdControllerCreate as ctrl">
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <div class="col-md-8">
                    <form name="moduleForm" class="form-horizontal" ng-submit="submitForm()" novalidate id="simpan" autocomplete="off">
                        <div class="form-group">
							<label for='id_opd' class='col-sm-2 control-label'>OPD:</label>
							<div class="col-sm-6">
								<ui-select ng-model="ctrl.formData.id_opd" theme="bootstrap" title="Cari OPD">
                                    <ui-select-match placeholder="Cari OPD...">[[$select.selected.nama_opd || $select.selected]]</ui-select-match>
                                    <ui-select-choices repeat="item.id_opd as item in ctrl.opds track by $index" refresh="ctrl.refreshDataOpd($select.search)" refresh-delay="0" position="down">
                                      <div ng-bind-html="item.nama_opd | highlight: $select.search"></div>
                                    </ui-select-choices>
                                </ui-select>
							</div>
						</div>
						<div class="form-group">
							<label for='id_layanan' class='col-sm-2 control-label'>Layanan:</label>
							<div class="col-sm-6">
								
							</div>
						</div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-6">
                                {!! ClaravelHelpers::btnSave('moduleForm')!!}
                                &nbsp;
                                &nbsp;
                                {!! ClaravelHelpers::btnCancel()!!}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>