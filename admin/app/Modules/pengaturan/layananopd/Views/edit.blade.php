<section class="content-header">
    <h1>
        Edit Layanan OPD<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li><a href="{!!ClaravelHelpers::backEdit()!!}"> Users</a></li>
        <li class="active">Edit Layanan OPD</li>
    </ol>
</section>
<section class="content" ng-controller="LayananopdControllerEdit as ctrl">
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <div class="col-md-8">
                    <form name="moduleForm" class="form-horizontal" ng-submit="submitForm()" id="simpan">
                        <div class="form-group">
							<label for='id_opd' class='col-sm-2 control-label'>OPD:</label>
							<div class="col-sm-10">
								<input class='form-control' ng-model='ctrl.formData.nama_opd' readonly type='text'>
							</div>
						</div>
						<div class="form-group">
							<label for='id_layanan' class='col-sm-2 control-label'>Layanan:</label>
							<div class="col-sm-10">
								<table class="table table-condensed table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Layanan</th>
                                            <th>Act.</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="lay in ctrl.datalayanans">
                                            <td>[[$index+1]]</td>
                                            <td>[[lay.nama_layanan]]</td>
                                            <td><a role="button" ng-click="ctrl.hapuslayanan($index)">Hapus</a></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <ui-select ng-model="ctrl.formData.id_layanan" theme="bootstrap" title="Tambahkan Layanan" on-select="ctrl.tambahkanLayanan($select.selected)" allow-clear="true">
                                                    <ui-select-match placeholder="Cari dan Tambahkan Layanan...">[[$select.selected.nama_layanan || $select.selected]]</ui-select-match>
                                                    <ui-select-choices repeat="item.id_layanan as item in ctrl.layanans track by $index" refresh="ctrl.refreshDataLayanan($select.search)" refresh-delay="0" position="down">
                                                      <div ng-bind-html="item.nama_layanan | highlight: $select.search"></div>
                                                    </ui-select-choices>
                                                </ui-select>
                                            </td>
                                        </tr>
                                    </tbody>             
                                </table>
							</div>
						</div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                {!! ClaravelHelpers::btnSave('moduleForm')!!}
                                &nbsp;
                                &nbsp;
                                {!! ClaravelHelpers::btnCancelEdit(); !!}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>