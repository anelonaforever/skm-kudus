<section class="content-header">
    <h1>
        Buat Role<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li><a href="{!!ClaravelHelpers::backCreate()!!}"> Roles</a></li>
        <li class="active">Buat Role</li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
      <div class="row">
        <div class="col-md-8">
          <form name="roleForm" name="roleform" class="form-horizontal" ng-controller="RolesControllerCreate" ng-submit="submitForm()" id="simpan">
          	<div class="box-body">
      				<div class="form-group" ng-class="{ 'has-error' : roleForm.name.$error.required && !roleForm.name.$pristine }">
      					<label for="name" class="col-sm-2 control-label">Nama:</label>
      					<div class="col-sm-6">
                  <input type="text" class="form-control" ng-model="formData.name" required="true" name="name">
                  <span class="help-block" ng-show="roleForm.name.$error.required && !roleForm.name.$pristine">Nama diperlukan.</span>
      					</div>
      				</div>
      				<div class="form-group">
                <label for="description" class="col-sm-2 control-label">Deskripsi:</label>
      					<div class="col-sm-6">
                  <input type="text" class="form-control" ng-model="formData.description" name="description">
      					</div>
      				</div>
      				<div class="form-group">
                <label for="login_destination" class="col-sm-2 control-label">Destinasi Login:</label>
      					<div class="col-sm-6">
                  <input type="text" class="form-control" ng-model="formData.login_destination" name="login_destination">
      					</div>
      				</div>
      				<div class="form-group">
      					<label for="status" class="col-sm-2 control-label">Status:</label>
      					<div class="col-sm-6">
      						<select id="status" name="status" class="form-control" ng-model="formData.status" ng-options="val for val in state" ng-init="formData.status=state[0]"></select>
      					</div>
      				</div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-6">
                  {!! ClaravelHelpers::btnSave('roleForm')!!}
                  &nbsp;
                  &nbsp;
                  {!! ClaravelHelpers::btnCancel()!!}
                </div>
              </div>
          	</div>
          </form>
        </div>
      </div>
    </div>
</section>
<script type="text/javascript">
  $('#name').focus();
</script>