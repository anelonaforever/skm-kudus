<?php
  error_reporting(null);
?>
<section class="content-header">
    <h1>
        Edit Role<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li><a href="{!!ClaravelHelpers::backEdit()!!}"> Role</a></li>
        <li class="active">Edit Role</li>
    </ol>
</section>
<section class="content">
  <form name="roleForm" name="roleform" class="form-horizontal" ng-controller="RolesControllerEdit" ng-submit="submitForm()" id="simpan">
    <div class="box box-primary">
      <div class="row">
        <div class="col-md-8">
          <div class="box-body">
            <div class="form-group" ng-class="{ 'has-error' : roleForm.name.$error.required && !roleForm.name.$pristine }">
              <label for="name" class="col-sm-2 control-label">Nama:</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" ng-model="formData.name" required="true" name="name">
                <span class="help-block" ng-show="roleForm.name.$error.required && !roleForm.name.$pristine">Nama diperlukan.</span>
              </div>
            </div>
            <div class="form-group">
              <label for="description" class="col-sm-2 control-label">Deskripsi:</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" ng-model="formData.description" name="description">
              </div>
            </div>
            <div class="form-group">
              <label for="login_destination" class="col-sm-2 control-label">Destinasi Login:</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" ng-model="formData.login_destination" name="login_destination">
              </div>
            </div>
            <div class="form-group">
              <label for="status" class="col-sm-2 control-label">Status:</label>
              <div class="col-sm-6">
                <select id="status" name="status" class="form-control" ng-model="formData.status" ng-options="val for val in state"></select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button class="btn btn-default" type="button" ng-click="resetRole()"><i class="ion-ios-undo"></i> Reset</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">Context Permissions Matrix</h3>
        <div class="box-tools pull-right">
          <a type="button" class="btn btn-flat btn-box-tool" ng-click="resetContext()"><i class="ion-ios-undo fa-lg"></i> Reset</a>         
        </div>
      </div>
      <div class="box-body no-padding">
        <table class="table table-striped table-hover">
          <thead class="bg-default">
            <tr>
              <th>Permissions</th>
              <th>
                <div class="checkbox checkbox-warning">
                  <input type="checkbox" id="checkall" ng-model="selectedAllContext" ng-click="checkAllContext()">
                  <label for="checkall">Check All</label>
                </div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="permissionsContext in permissions_context">
              <td>[[permissionsContext.name]]</td>
              <td>
                <div class="checkbox checkbox-info">
                  <input type="checkbox" id="checkbox-[[permissionsContext.name]]" ng-model="permissionsContext.flag">
                  <label for="checkbox-[[permissionsContext.name]]"></label>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">Modules Permissions Matrix</h3>
        <div class="box-tools pull-right">
          <a type="button" class="btn btn-flat btn-box-tool" ng-click="resetModules()"><i class="ion-ios-undo fa-lg"></i> Reset</a>         
        </div>
      </div>
      <div class="box-body no-padding">
        <div class="table-responsive">
          <table class="table table-striped table-hover">
            <thead class="bg-default">
              <tr>
                <th>Permissions</th>
                <th ng-repeat="moduleActions in module_actions">
                  <div class="checkbox checkbox-warning">
                    <input type="checkbox" id="checkall-[[moduleActions]]" ng-model="selectedAllModules[moduleActions]" ng-click="checkAllModules(moduleActions)">
                    <label for="checkall-[[moduleActions]]">[[moduleActions]]</label>
                  </div>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="(permissionsModulesKey, permissionsModulesValue) in permissions_modules">
                <td>[[ permissionsModulesValue.name ]]</td>
                <td ng-repeat="(moduleActionsKey, moduleActionsValue) in module_actions">
                  <div ng-if="action[permissionsModulesValue.name][moduleActionsValue]" class="checkbox checkbox-info">
                    <input type="checkbox" id="checkbox-[[permissionsModulesValue.name]]-[[moduleActionsValue]]" ng-model="permissionModules[permissionsModulesValue.name][moduleActionsValue]['flag']">
                    <label for="checkbox-[[permissionsModulesValue.name]]-[[moduleActionsValue]]">[[moduleActions]]</label>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    {!! ClaravelHelpers::btnSave('roleForm')!!}
    &nbsp;
    &nbsp;
    {!! ClaravelHelpers::btnCancelEdit()!!}
  </form>

</section>