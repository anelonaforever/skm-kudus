<section class="content-header">
    <h1>
        Permissions Matrix<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li class="active">Permissions Matrix</li>
    </ol>
</section>
<section class="content" table-tools ng-controller="PermissionMatrixController">
  <div class="box box-primary box-table">
    <div class="box-header box-header-default">
      <div id="tablesearchbar" class="animated">
        <form>
          <input ng-model="pencarian" id="kotakpencarian" type="text" class="form-control" placeholder="Cari di sini..." autocomplete="off">
          <a role="button" class="btn-submit" type="submit"><i class="fa fa-angle-left"></i></a>
          <a role="button" class="btn-close"><i class="fa fa-times"></i></a>
        </form>
      </div>
      <h3 class="box-title hidden-xs">Daftar Permissions Matrix</h3>
      <div class="box-tools pull-right">
        <a id="searchbutton" type="button" class="btn btn-flat btn-box-tool"><i class="ion-android-search fa-lg"></i> Cari</a>         
      </div>
    </div>
    <div class="table-responsive">
      <div class="box-body no-padding">
        <form>
          <table class="table table-striped table-hover" id="tabel">
            <thead class="bg-default">
              <tr>
                <th>Permissions</th>
                <th>Descriptions</th>
                <th ng-repeat="role in Records.roles">[[role.name]]</th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="permission in Records.permissions | filter:pencarian">
                <td>[[permission.name]]</td>
                <td>[[permission.description]]</td>
                <td ng-repeat="role1 in permission.roles" >
                  <div class="checkbox checkbox-info">
                    <input type="checkbox" id="checkbox-[[permission.name]]-[[role1.id]]" ng-model="role1.status" ng-change="stateChanged([[role1.status]], [[permission.id]], [[role1.id]])">
                    <label for="checkbox-[[permission.name]]-[[role1.id]]"></label>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </form>
      </div>
    </div>
  </div>
</section>