<section class="content-header">
    <h1>
        Buat User<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li><a href="{!!ClaravelHelpers::backCreate()!!}"> Users</a></li>
        <li class="active">Buat User</li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
      <div class="box-body">
        <div class="row">
          <div class="col-md-8">
            <form name="userForm" class="form-horizontal" ng-controller="UsersControllerCreate" ng-submit="submitForm()" novalidate id="simpan" autocomplete="off">
              <div class="form-group" ng-class="{ 'has-error' : userForm.name.$error.required && !userForm.name.$pristine }">
                <label for="name" class="col-sm-2 control-label">Nama:</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" ng-model="formData.name" name="name" id="name" required="true">
                  <span class="help-block" ng-show="userForm.name.$error.required && !userForm.name.$pristine">Nama diperlukan.</span>
                </div>
              </div>
              <div class="form-group" ng-class="{ 'has-error' : userForm.username.$error.ngRemoteValidate || (userForm.username.$error.required && userForm.username.$touched)}">
                <label for="username" class="col-sm-2 control-label">Username:</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control ng-pristine" ng-model="formData.username" ng-remote-validate="[[currentUrl]]/validusername" name="username" id="username" required="true">
                  <span class="message" ng-show="userForm.username.$pending">Memeriksa...</span>
                  <span class="help-block" ng-show="userForm.username.$error.ngRemoteValidate">Sudah digunakan.</span>
                  <span class="help-block" ng-show="userForm.username.$error.required && userForm.username.$touched">Username diperlukan.</span>
                </div>
              </div>
              <div class="form-group" ng-class="{ 'has-error' : userForm.password.$error.required && !userForm.password.$pristine }">
                <label for="password" class="col-sm-2 control-label">Password:</label>
                <div class="col-sm-6">
                  <input type="password" class="form-control" ng-model="formData.password" name="password" id="password" required="true">
                  <span class="help-block" ng-show="userForm.password.$error.required && !userForm.password.$pristine">Password diperlukan.</span>
                  <div class="checkbox checkbox-info">
                    <input type="checkbox" id="showpassword">
                    <label for="showpassword">Show Password</label>
                  </div>
                </div>
              </div>
              <div class="form-group" ng-class="{ 'has-error' : userForm.role_id.$error.required && !userForm.role_id.$pristine }">
                <label for="role_id" class="col-sm-2 control-label">Role:</label>
                <div class="col-sm-6">
                  <select class="form-control" ng-model="formData.role_id" ng-options="obj.role_id as obj.name for obj in roles">
                    <option value="">--Pilih Role--</option>
                  </select>
                  <span class="help-block" ng-show="userForm.role_id.$error.required && !userForm.role_id.$pristine">Role diperlukan.</span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-6">
                  {!! ClaravelHelpers::btnSave('userForm')!!}
                  &nbsp;
                  &nbsp;
                  {!! ClaravelHelpers::btnCancel()!!}
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</section>
<script type="text/javascript">
  $('#name').focus();
  $('#showpassword').change(function () {
    if ($(this).prop('checked')==true){ 
      $('input[name=password]').prop('type', 'text');
    }else{
      $('input[name=password]').prop('type', 'password');
    }
  });
</script>
