<section class="content-header">
    <h1>
        Edit User<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li><a href="{!!ClaravelHelpers::backEdit()!!}"> Users</a></li>
        <li class="active">Edit User</li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
      <div class="box-body">
        <div class="row">
          <div class="col-md-8">
            <form name="userForm" class="form-horizontal" ng-controller="UsersControllerEdit" ng-submit="submitForm()" id="simpan">
              <div class="form-group" ng-class="{ 'has-error' : userForm.name.$error.required && !userForm.name.$pristine }">
                <label for="name" class="col-sm-2 control-label">Name:</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" ng-model="formData.name" name="name" id="name" required="true">
                  <span class="help-block" ng-show="userForm.name.$error.required && !userForm.name.$pristine">Nama diperlukan.</span>
                </div>
              </div>
              <div class="form-group" ng-class="{ 'has-error' : userForm.username.$error.ngRemoteValidate || (userForm.username.$error.required && userForm.username.$touched)}">
                <label for="username" class="col-sm-2 control-label">Username:</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" ng-model="formData.username" ng-remote-validate="[[currentUrl]]/validusername?id={{\Request::segment(4)}}" name="username"  id="username" required="true">
                  <span class="message" ng-show="userForm.username.$pending">Memeriksa...</span>
                  <span class="help-block" ng-show="userForm.username.$error.ngRemoteValidate">Sudah digunakan.</span>
                  <span class="help-block" ng-show="userForm.username.$error.required && userForm.username.$touched">Username diperlukan.</span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-7">
                  <div class="checkbox checkbox-warning">
                    <input type="checkbox" ng-model="formData.ubahpassword" name="ubahpassword" id="ubahpassword">
                    <label for="ubahpassword">Ubah Password?</label>
                  </div>
                </div>
              </div>
              <div class="form-group" ng-show="formData.ubahpassword">
                <label for="password" class="col-sm-2 control-label">Password:</label>
                <div class="col-sm-6">
                  <input type="password" class="form-control" ng-model="formData.password" name="password" id="password">
                  <div class="checkbox checkbox-info">
                    <input type="checkbox" id="showpassword">
                    <label for="showpassword">Show Password</label>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="role_id" class="col-sm-2 control-label">Role:</label>
                <div class="col-sm-6">
                  <select name="role_id" id="role_id" class="form-control" ng-model="formData.role_id" ng-options="obj.role_id as obj.name for obj in roles"></select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  {!! ClaravelHelpers::btnSave('userForm')!!}
                  &nbsp;
                  &nbsp;
                  <button class="btn btn-default" type="button" ng-click="reset()"><i class="ion-ios-undo"></i> Reset</button>
                  &nbsp;
                  &nbsp;
                  {!! ClaravelHelpers::btnCancelEdit(); !!}
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</section>
<script type="text/javascript">
  $('#showpassword').change(function () {
    if ($(this).prop('checked')==true){ 
      $('input[name=password]').prop('type', 'text');
    }else{
      $('input[name=password]').prop('type', 'password');
    }
  });
</script>
