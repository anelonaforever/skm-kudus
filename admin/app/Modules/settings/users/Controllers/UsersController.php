<?php namespace App\Modules\settings\users\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\settings\users\Models\UsersModel;
use Input,View, Request, Form, File, Storage;
/**
 * IndexController
 *
 * Controller to house all the functionality directly
 * related to the ModuleOne.
 */
class UsersController extends Controller {

	protected $users;

	public function __construct(UsersModel $users)
	{
		$this->users = $users;
	}

		/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return View::make('users::index');
	}

	public function getData(){
		$userss = $this->users
		->select('users.*','roles.name as role')
		->join('roles','roles.id','=','users.role_id');

		if(\Session::get('role_id') == 2){
			$userss = $userss->where(function($query){
                $query->where('role_id','=',2)
                      ->orwhere('role_id','=',3);
            });
		}else
		if(\Session::get('role_id') == 3){
			$userss = $userss->where('role_id','=',3);
		}

		if(\Input::has('cari')){
			$userss = $userss->where(function($query){
	            $query->where('users.name','LIKE','%'.\Input::get('cari').'%')
	                  ->orwhere('username','LIKE','%'.\Input::get('cari').'%')
	                  ->orwhere('roles.name','LIKE','%'.\Input::get('cari').'%');
	        });
		}

		$userss = $userss->paginate($_ENV['configurations']['list-limit']);
		return $userss;
	}

	public function postValidusername(){
		$username = Input::get('value');

		$data = \DB::table('users')
        ->where('username','=',$username);

        if(\Input::has('id')){
        	$data=$data->where('id','!=',\Input::get('id'));
        }

        $data=$data->count();

        if($data > 0){
            $isAvailable = false; 
        }else{
            $isAvailable = true;
        }

		$response = array(
			'isValid' => $isAvailable
		);

		return json_encode($response);
	}

		/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		$rolesModel = new \RolesModel;
		$data['roles'] = $rolesModel->getTreeArray();
		return View::make('users::create')->with($data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postCreate()
	{
		$input = Input::all();
		$validation = \Validator::make($input, UsersModel::$rules);

		if($validation->passes())
		{
			$array = array(
				'name' => Input::get('name'),
				'username' => Input::get('username'),
				'password' => \Hash::make(Input::get('password')),
				'role_id' => Input::get('role_id')
			);

			$simpan = \DB::table('users')
			->insert($array);

			if($simpan){
				\File::makeDirectory(base_path().'/packages/upload/fmanager/'.Input::get('username'), 0777, true, true);
				$result = array('result' => 'success');
			}else{
				$result = array('result' => 'failed');
			}
		}else{
			$result = array('result' => 'invalid');
		}

		return json_encode($result);
	}



	//{controller-show}

		/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit()
	{
		return View::make('users::edit');
	}

	public function getDataedit(){
		$id = Request::segment(4);
		$data = \DB::table('users')
		->select('name','username','role_id','id')
		->where('id','=',$id)
		->first();

		$roles = \DB::table('roles')
		->select('id AS role_id','name');

		if(\Session::get('role_id') != 1){
			$roles=$roles->where('id','!=',1);
		}

		$roles=$roles->get();

		$array = array(
			'data' => $data,
			'roles' => $roles
		);

		return json_encode($array);
	}

	public function getRolesdata(){
		$data = \DB::table('roles')
		->select('id AS role_id','name');

		if(\Session::get('role_id') != 1){
			$data=$data->where('id','!=',1);
		}

		$data=$data->get();

		return json_encode($data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postEdit()
	{
		$id = Input::get('id');
		$input = array_except(Input::all(), '_method');
		$validation = \Validator::make($input, UsersModel::$rules);

		if ($validation->passes())
		{
			$before = \DB::table('users')
			->where('id','=',$id)
			->first();

			$users = $this->users->find($id);
			$users->name = Input::get('name');
			$users->username = Input::get('username');

			if(Input::has('ubahpassword') && Input::get('ubahpassword') == true && Input::get('password') != ''){
				$users->password = \Hash::make(Input::get('password'));
			}

			$users->role_id = Input::get('role_id');
			$users->save();

			if($users){
				if($before->username != Input::get('username')){
					if($id == \Session::get('user_id')){
						\Session::put('user_name', Input::get('username'));
					}
					rename(base_path().'/packages/upload/fmanager/'.$before->username.'/', base_path().'/packages/upload/fmanager/'.Input::get('username').'/');
				}
				$result = array('result' => 'success');
			}else{
				$result = array('result' => 'failed');
			}
		}else{
			$result = array('result' => 'invalid');
		}

		return json_encode($result);
	}


	
		/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function postDelete(){
        $ids = Input::get('id');
        if (is_array($ids)){
            foreach($ids as $id){
            	$before = \DB::table('users')
				->where('id','=',$id)
				->first();
				$hapus = $this->users->find($id)->delete();
				if($hapus){
					\File::deleteDirectory(base_path().'/packages/upload/fmanager/'.$before->username);
				}
			}
        }
        else{
        	$before = \DB::table('users')
			->where('id','=',$ids)
			->first();
            $hapus = $this->users->find($ids)->delete();
            if($hapus){
				\File::deleteDirectory(base_path().'/packages/upload/fmanager/'.$before->username);
			}
        }

        if($hapus){
        	$result = array('result' => 'success');
        }else{
        	$result = array('result' => 'failed');
        }

        return json_encode($result);
    }
}