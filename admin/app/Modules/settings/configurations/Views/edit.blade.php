<section class="content-header">
    <h1>
        Edit Konfigurasi<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li><a href="{!!ClaravelHelpers::backEdit()!!}"> Configurations</a></li>
        <li class="active">Edit Konfigurasi</li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
      <div class="box-body">
        <div class="row">
          <div class="col-md-8">
            <form name="configurationForm" class="form-horizontal" ng-controller="ConfigurationsControllerEdit" ng-submit="submitForm()" id="simpan">
              <div class="form-group" ng-class="{ 'has-error' : configurationForm.name.$error.required && !configurationForm.name.$pristine }">
                <label for="name" class="col-sm-2 control-label">Nama:</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" ng-model="formData.name" name="name" id="name" required="true">
                  <span class="help-block" ng-show="configurationForm.name.$error.required && !configurationForm.name.$pristine">Nama diperlukan.</span>
                </div>
              </div>
              <div class="form-group" ng-class="{ 'has-error' : configurationForm.value.$error.required && !configurationForm.value.$pristine }">
                <label for="value" class="col-sm-2 control-label">Isi:</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" ng-model="formData.value" name="value" id="value" required="true">
                  <span class="help-block" ng-show="configurationForm.value.$error.required && !configurationForm.value.$pristine">Isi diperlukan.</span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-6">
                  {!! ClaravelHelpers::btnSave('configurationForm')!!}
                  &nbsp;
                  &nbsp;
                  {!! ClaravelHelpers::btnCancelEdit(); !!}
                </div>
              </div>        
            </form>
          </div>
        </div>
      </div>
    </div>
</section>