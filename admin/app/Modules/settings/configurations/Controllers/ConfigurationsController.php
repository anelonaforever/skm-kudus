<?php namespace App\Modules\settings\configurations\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\settings\configurations\Models\ConfigurationsModel;
use Input,View, Request, Form;
/**
 * IndexController
 *
 * Controller to house all the functionality directly
 * related to the ModuleOne.
 */
class ConfigurationsController extends Controller{
    private $configurations;
	function __construct( ConfigurationsModel $Model)
	{
		$this->configurations = $Model;
	}
    
	public function getIndex()
	{
		return View::make('configurations::index');
	}

	public function getData(){
		$data = [];
		$configurationss = $this->configurations->get();
		$data['configurations'] = $configurationss;
		return json_encode($data);
	}

		/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		return View::make('configurations::create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postCreate()
	{
		$input = Input::all();
		$validation = \Validator::make($input, ConfigurationsModel::$rules);

		if ($validation->passes()){
			$simpan = $this->configurations->create($input);
			if($simpan){
				$result = array('result' => 'success');
			}else{
				$result = array('result' => 'failed');
			}
        }else{
            $result = array('result' => 'invalid');
        }

        return json_encode($result);
	}

	public function getEdit($id=false){
		return View::make('configurations::edit');
	}

	public function getDataedit(){
		$id = Request::segment(4);
		$configurations = $this->configurations->find($id);
		return json_encode($configurations);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postEdit($id=false)
	{
		$id = ($id==false)?Input::get('id'):$id;
		$input = Input::all();
		$validation = \Validator::make($input, ConfigurationsModel::$rules);

		if ($validation->passes())
		{
			$configurations = $this->configurations->find($id);
			$configurations->update($input);
			if($configurations){
				$result = array('result' => 'success');
			}else{
				$result = array('result' => 'failed');
			}
		}else{
			$result = array('result' => 'invalid');
		}

		return json_encode($result);
	}


	
		/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postDelete()
	{
		$ids = Input::get('id');

		if (is_array($ids)){
			foreach($ids as $id){
				$hapus = $this->configurations->find($id)->delete();
			}
		}else{
			$hapus = $this->configurations->find($ids)->delete();
		}

		if($hapus){
        	$result = array('result' => 'success');
        }else{
        	$result = array('result' => 'failed');
        }

        return json_encode($result);
	}    
    
}