<section class="content-header">
    <h1>
        Buat Permissions<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li><a href="{!!ClaravelHelpers::backCreate()!!}"> Permissions</a></li>
        <li class="active">Create New Permissions</li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
      <div class="box-body">
        <div class="row">
          <div class="col-md-8">
            <form name="permissionForm" class="form-horizontal" ng-controller="PermissionsControllerCreate" ng-submit="submitForm()" id="simpan">
              <div class="form-group" ng-class="{ 'has-error' : permissionForm.name.$error.required && !permissionForm.name.$pristine }">
                <label for="name" class="col-sm-2 control-label">Nama:</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" ng-model="formData.name" name="name" id="name" required="true">
                  <span class="help-block" ng-show="permissionForm.name.$error.required && !permissionForm.name.$pristine">Nama diperlukan.</span>
                </div>
              </div>
              <div class="form-group" ng-class="{ 'has-error' : permissionForm.description.$error.required && !permissionForm.description.$pristine }">
                <label for="description" class="col-sm-2 control-label">Deskripsi:</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" ng-model="formData.description" name="description" id="description" required="true">
                  <span class="help-block" ng-show="permissionForm.description.$error.required && !permissionForm.description.$pristine">Nama diperlukan.</span>
                </div>
              </div>
              <div class="form-group">
                <label for="status" class="col-sm-2 control-label">Status:</label>
                <div class="col-sm-6">
                  <select id="status" name="status" class="form-control" ng-model="formData.status" ng-options="val for val in state" ng-init="formData.status=state[0]"></select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-6">
                  {!! ClaravelHelpers::btnSave('permissionForm')!!}
                  &nbsp;
                  &nbsp;
                  {!! ClaravelHelpers::btnCancel()!!}
                </div>
              </div>       
            </form>
          </div>
        </div>
      </div>
    </div>
</section>
<script type="text/javascript">
  $('#name').focus();
</script>