<?php namespace App\Modules\settings\permissions\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\settings\permissions\Models\PermissionsModel;
use Input,View, Request, Form;
/**
 * IndexController
 *
 * Controller to house all the functionality directly
 * related to the ModuleOne.
 */
class PermissionsController extends Controller {

	/**
	 * Permissions Repository
	 *
	 * @var Permissions
	 */
	protected $permissions;

	public function __construct(PermissionsModel $permissions)
	{
		$this->permissions = $permissions;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return View::make('permissions::index');
	}

	public function getData(){
		$data = [];
		$permissionss = $this->permissions->get();
		$data['permissions'] = $permissionss;
		return json_encode($data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		return View::make('permissions::create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postCreate()
	{
		$input = Input::all();
		$validation = \Validator::make($input, PermissionsModel::$rules);

		if ($validation->passes())
		{
			$simpan = $this->permissions->create($input);
			if($simpan){
				$result = array('result' => 'success');
			}else{
				$result = array('result' => 'failed');
			}
		}else{
			$result = array('result' => 'invalid');
		}

		return json_encode($result);
	}



	//{controller-show}

		/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit()
	{
		return View::make('permissions::edit');
	}

	public function getDataedit(){
		$id = Request::segment(4);
		$permissions = $this->permissions->find($id);
		return json_encode($permissions);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postEdit()
	{
		$id = Input::get('id');
		$input = array_except(Input::all(), '_method');
		$validation = \Validator::make($input, PermissionsModel::$rules);

		if ($validation->passes())
		{
			$permissions = $this->permissions->find($id);
			$permissions->update($input);

			if($permissions){
				$result = array('result' => 'success');
			}else{
				$result = array('result' => 'failed');
			}
		}else{
			$result = array('result' => 'invalid');
		}

		return json_encode($result);
	}


	
		/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postDelete()
	{
		$ids = Input::get('id');

		if (is_array($ids)){
			foreach($ids as $id){
				$hapus = $this->permissions->find($id)->delete();
			}
		}else{
			$hapus = $this->permissions->find($ids)->delete();
		}

		if($hapus){
        	$result = array('result' => 'success');
        }else{
        	$result = array('result' => 'failed');
        }

        return json_encode($result);
	}


}
