<?php namespace App\Modules\portal\beranda\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\portal\beranda\Models\BerandaModel;
use Input,View, Request, Form, File;

class BerandaController extends Controller {

	/**
	 * Beranda Repository
	 *
	 * @var Beranda
	 */
	protected $beranda;

	public function __construct()
	{
	
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return View::make('beranda::index');
	}

	public function getKonten(){
		$data = \DB::table('mst_konten')
		->where('id','=',1)
		->first();

		return json_encode($data);
	}

	public function postSimpankonten(){
		$simpan = \DB::table('mst_konten')
		->where('id','=',1)
		->update(array('isi' => \Input::get('konten')));

		if($simpan){
            $result = array('result' => 'success');
        }else{
            $result = array('result' => 'failed');
		}
		
		return json_encode($result);
	}

    

}
