<style>
    .thumb {
        width: 100%;
        height: auto;
        float: none;
        position: relative;
        display: inline;
    }
</style>
<section class="content-header">
    <h1>
        Buat Berita<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li><a href="{!!ClaravelHelpers::backCreate()!!}"> Users</a></li>
        <li class="active">Buat Berita</li>
    </ol>
</section>
<section class="content" ng-controller="BeritaControllerCreate">
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <div class="col-md-11">
                    <form name="moduleForm" class="form-horizontal" ng-submit="submitForm()" novalidate id="simpan" autocomplete="off">
                        <div class="form-group">
							<label for='judul_berita' class='col-sm-2 control-label'>Judul Berita:</label>
							<div class="col-sm-10">
								<input class='form-control' ng-model='formData.judul_berita' name='judul_berita' id='judul_berita' type='text' required>
							</div>
						</div>
						<div class="form-group">
							<label for='isi_berita' class='col-sm-2 control-label'>Isi Berita:</label>
							<div class="col-sm-10">
                                <textarea ui-tinymce="tinymceOptions" ng-model="formData.isi_berita" required></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for='gambar' class='col-sm-2 control-label'>Thumbnail:</label>
							<div class="col-sm-6">
                                <img class="thumb" ngf-thumbnail="gambar" ngf-size="{width:200, height:200, quality:1.0}"/>
								<input type="file" ngf-select ng-model="gambar" ngf-accept="'image/*'" accept="image/*" style="border-radius: 5px; overflow: hidden; text-overflow:hidden; white-space: nowrap; width: 100%;">
							</div>
						</div>
                        <div class="form-group" ng-show="gambar.progress >= 0">
                            <div class="col-sm-6 col-sm-offset-2">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:[[gambar.progress]]%" ng-bind="gambar.progress + '%'"></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-6">
                                {!! ClaravelHelpers::btnSave('moduleForm')!!}
                                &nbsp;
                                &nbsp;
                                {!! ClaravelHelpers::btnCancel()!!}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>