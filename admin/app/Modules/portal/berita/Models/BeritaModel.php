<?php namespace App\Modules\portal\berita\Models;
use Illuminate\Database\Eloquent\Model;


/**
* Berita Model
* @var Berita
* Generate from Custom Laravel 5.1 by Aa Gun. 
*
* Developed by Dinustek. 
* Please write log when you do some modification, don't change anything unless you know what you do
* Semarang, 2016
*/

class BeritaModel extends Model {
	protected $guarded = array();
	
	protected $table = "mst_berita";

	public static $rules = array(
    	'judul_berita' => 'required',
		'isi_berita' => 'required'
    );

	public static function all($columns = array('*')){
		$instance = new static;
		if (\PermissionsLibrary::hasPermission('mod-berita-listall')){
			return $instance->newQuery()->paginate($_ENV['configurations']['list-limit']);
		}else{
			return $instance->newQuery()
			->where('role_id', \Session::get('role_id'))
			->paginate($_ENV['configurations']['list-limit']);	
			
		}
	}

}
