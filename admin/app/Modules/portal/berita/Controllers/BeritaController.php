<?php namespace App\Modules\portal\berita\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\portal\berita\Models\BeritaModel;
use Input,View, Request, Form, File;

/**
* Berita Controller
* @var Berita
* Generate from Custom Laravel 5.1 by Aa Gun. 
*
* Developed by Divisi Software Development - Dinustek. 
* Please write log when you do some modification, don't change anything unless you know what you do
* Semarang, 2016
*/

class BeritaController extends Controller {
    protected $berita;

    public function __construct(BeritaModel $berita){
        $this->berita = $berita;
    }

        public function getIndex(){
        return View::make('berita::index');
    }

    public function getData(){
		if(Input::has('cari') AND strlen(Input::get('cari')) > 0){
            $data = $this->berita
            ->where(function($q){
                $q->where('judul_berita', 'LIKE', '%'.Input::get('cari').'%')
			    ->orWhere('isi_berita', 'LIKE', '%'.Input::get('cari').'%');
            })
            ->whereNull('deleted_at')
            ->paginate($_ENV['configurations']['list-limit']);
        }else{
            $data = $this->berita
            ->whereNull('deleted_at')
            ->paginate($_ENV['configurations']['list-limit']);
        }

        $idx = 0;
        foreach ($data as $row) {
            $data[$idx]->jml_kata = str_word_count(strip_tags($row->isi_berita));
            $data[$idx]->thumbnail = asset('packages/image.php?width=200&image=/skm-kudus/admin/packages/upload/berita').'/'.$row->thumbnail;
            $idx++;
        }


		return $data;
	}

    public function getCreate(){
        return View::make('berita::create');
    }

    public function postCreate(){
        $input = array(
            'judul_berita' => \Input::get('judul_berita'),
            'isi_berita' => \Input::get('isi_berita'),
            'created_at' => date('Y-m-d H:i:s'),
            'url_berita' => clean(\Input::get('judul_berita'))
        );
        $validation = \Validator::make($input, BeritaModel::$rules);
        if($validation->passes()){
            $input['user_id'] = \Session::get('user_id');
            $input['role_id'] = \Session::get('role_id');

            if(\Input::hasfile('gambar')){
                $file = Input::file('gambar');
                $tipefile = $file->getClientOriginalExtension();
                $filename = clean(\Input::get('judul_berita')) . '-' . str_random(5) . '.' . $tipefile;
                $destinationPath = base_path() . '/packages/upload/berita';
                $size = $file->getSize();
                $type = $file->getMimeType();
                $uploadSuccess = $file->move($destinationPath, $filename);

                if($uploadSuccess){
                    $input['thumbnail'] = $filename;
                }
            }

            $simpan = $this->berita->create($input);

            if($simpan){
                $result = array('result' => 'success');
            }else{
                $result = array('result' => 'failed');
            }
        }
        else{
            $result = array('result' => 'invalid');
        }

        return json_encode($result);
    }



    //{controller-show}

    public function getEdit(){
        return View::make('berita::edit');
    }

    public function getDataedit(){
        $id = Request::segment(4);
        $data = $this->berita->find($id);
        $data->_token = csrf_token();
        $data->thumbnail = asset('packages/upload/berita').'/'.$data->thumbnail;
        return json_encode($data);
    }
    
    public function postEdit(){
        $id = Input::get('id');
        $input = array(
            'judul_berita' => \Input::get('judul_berita'),
            'isi_berita' => \Input::get('isi_berita'),
            'url_berita' => clean(\Input::get('judul_berita'))
        );
        $validation = \Validator::make($input, BeritaModel::$rules);
        
        if($validation->passes()){
            $berita = $this->berita->find($id);
            if(\Input::hasfile('gambar')){
                $file = Input::file('gambar');
                $tipefile = $file->getClientOriginalExtension();
                $filename = clean(\Input::get('judul_berita')) . '-' . str_random(5) . '.' . $tipefile;
                $destinationPath = base_path() . '/packages/upload/berita';
                $size = $file->getSize();
                $type = $file->getMimeType();
                $uploadSuccess = $file->move($destinationPath, $filename);

                if ($uploadSuccess) {
                    unlink($destinationPath.'/'.$berita->thumbnail);
                    $input['thumbnail'] = $filename;
                }
            }
            $berita->update($input);

            if($berita){
                $result = array('result' => 'success');
            }else{
                $result = array('result' => 'failed');
            }
        }
        else{
            $result = array('result' => 'invalid');
        }

        return json_encode($result);
    }


	
    public function postDelete(){
        $ids = Input::get('id');
        if (is_array($ids)){
            foreach($ids as $id){
                $hapus = $this->berita->find($id)->update(array('deleted_at' => date('Y-m-d H:i:s')));
            }
        }
        else{
            $hapus = $this->berita->find($ids)->update(array('deleted_at' => date('Y-m-d H:i:s')));
        }

        if($hapus){
            $result = array('result' => 'success');
        }else{
            $result = array('result' => 'failed');
        }

        return json_encode($result);
    }

}
