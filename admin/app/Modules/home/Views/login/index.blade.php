<!doctype html>
<html dir="ltr" >
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Administrator AKUPUAS Kab. Kudus</title>
        <link href="{{asset('packages/tugumuda/images/favicon.png')}}" rel='icon' type='image/x-icon'/>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('packages/tugumuda/css/login.css')}}">
        <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/font-awesome/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/Ionicons/css/ionicons.min.css')}}">
        <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/admin-lte/dist/css/AdminLTE.css')}}">
        <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/awesome-bootstrap-checkbox/demo/build.css')}}">
        <style type="text/css">
            html {
                background-image: url({{asset('packages/tugumuda/images/Masjid_Menara_Kudus.jpg')}});
                background-position: top center;
                background-size: cover;
                background-repeat: no-repeat;
            }
            body.login-page:before{
                content: "";
                z-index: -1;
                position: absolute;
                top: 0;
                bottom: auto;
                left: 0;
                right: 0;
                width: auto;
                height: 50%;
                background-image: -webkit-linear-gradient(rgba(255,255,255,0.94) 0%, rgba(248,249,250,0.66) 52%, rgba(233,236,239,0.92) 100%);
                background-image: -o-linear-gradient(rgba(255,255,255,0.94) 0%, rgba(248,249,250,0.66) 52%, rgba(233,236,239,0.92) 100%);
                background-image: linear-gradient(rgba(255,255,255,0.94) 0%, rgba(248,249,250,0.66) 52%, rgba(233,236,239,0.92) 100%);
                /*position: fixed;*/
            }
            body.login-page:after{
                content: "";
                z-index: -1;
                height: 50%;
                width: auto;
                position: absolute;
                top: auto;
                bottom: 0;
                right: 0;
                left: 0;
                background: rgba(0,123,255,0.63);
                /*position: fixed;*/
            }
            body.login-page{
                background-color: transparent;
            }
            html, body{
                height: inherit !important;
            }
            .col-md-6{
                width: 100%;
            }
            .row {
                display: -ms-flexbox;
                display: flex;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                min-height: 600px;
            }
            .align-items-center {
                -ms-flex-align: center!important;
                align-items: center!important;
            }
            .login-logo{
                padding-top: 1em;
            }
            .login-logo h1{
                color: #495057;
                font-size: 24px;
                font-weight: 700;
            }
            .login-logo p{
                color: #495057;
                font-size: 18px;
                font-weight: 400;
            }
            .login-box h2{
                color: white;
            }
            @media (min-width: 768px) {
                body.login-page:before{
                    top: 0;
                    left: 0;
                    bottom: 0;
                    right: auto;
                    width: 50%;
                    height: auto;
                }
                body.login-page:after{
                    top: 0;
                    bottom: 0;
                    right: 0;
                    left: auto;
                    width: 50%;
                    height: auto;
                }
                .col-md-6{
                    -ms-flex: 0 0 50%;
                    flex: 0 0 50%;
                    max-width: 50%;
                }
                .container {
                    width: 100%;
                }
            }
            @media (min-width: 992px){
                .col-md-6 {
                    width: 50%;
                }
                .container {
                    width: 100%;
                }
            }
            @media (min-width: 1200px){
                .container {
                    width: 100%;
                }
            }
        </style>
    </head>
    <body class="hold-transition login-page">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="login-logo">
                        <img width="70px" src="{{ asset('packages/tugumuda/images/logo_kudus.png') }}"><br>
                        <a href="{{url()}}">
                            <h1>Aplikasi Pengukuran Kepuasan Masyarakat<br>( AKUPUAS )</h1>
                            <p>
                                Pemerintah Kabupaten Kudus
                            </p>
                        </a>
                    </div>
                    
                </div>
                <div class="col-md-6">
                    <div class="login-box">
                        <h2 class="text-center">Administrator</h2>
                        <div class="login-box-body">
                            <!-- <p class="login-box-msg">Masukkan nama pengguna dan kata sandi Anda</p> -->
                            {!!Form::open(array('url' => url().'/login','id'=>'tampil', 'method' => 'POST'))!!}
                            <div class="form-group has-feedback">
                                <input type="text" name="username" id="username" class="form-control" placeholder="Nama Pengguna">
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input type="password" name="password" id="password" class="form-control" placeholder="Kata Sandi">
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            </div>
                            <div class="form-group">
                                <div class="checkbox checkbox-info">
                                    <label for="showpassword"><input type="checkbox" id="showpassword"> Tampilkan Kata Sandi</label>
                                </div>
                            </div>
                            <p>
                                <button type="submit" class="btn btn-danger btn-flat"><i class="fa fa-arrow-right" aria-hidden="true"></i> Masuk</button>
                                &nbsp;
                                <button id="reset" type="reset" class="btn btn-warning btn-flat"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button>
                            </p>
                            {!!Form::close()!!}
                        </div><br>
                        <center>
                            <p class="copyright">©2018 AKUPUAS Kabupaten Kudus. Dikembangkan oleh <a href="http://www.dinustek.com">dinustek.</a></p>
                        </center>
                    </div>
                    
                </div>
            </div>
        </div>
        <script src="{{asset('packages/tugumuda/bower_components/jquery/dist/jquery.min.js')}}"></script>
        <script src="{{asset('packages/tugumuda/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script>
            $(document).ready(function() {
                $('#reset').on('click', function() {
                    $('#username').focus();
                });
            });

            $('#showpassword').change(function() {
                if ($(this).is(":checked")) {
                    $('#password').prop('type', 'text');
                } else {
                    $('#password').prop('type', 'password');
                }
            });
        </script>
    </body>
</html>
