<!DOCTYPE html>
<html ng-app="dntApp">
<head>
  <meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="theme-color" content="#1E262B">
	<meta name="msapplication-navbutton-color" content="#1E262B">
	<meta name="apple-mobile-web-app-capable" content="#1E262B">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{$_ENV['configurations']['site-name']}}</title>
  <link href="{{ asset('packages/tugumuda/images/favicon.ico') }}" rel='icon' type='image/x-icon'>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- angular Loading Bar-->
  <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/angular-loading-bar/src/loading-bar.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/admin-lte/dist/css/AdminLTE.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/admin-lte/dist/css/skins/_all-skins.css')}}">
  <!-- angular bootstrap toggle -->
  <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/angular-bootstrap-toggle/dist/angular-bootstrap-toggle.min.css')}}">
  <!-- custom style -->
  <link rel="stylesheet" href="{{asset('packages/tugumuda/css/custom.css')}}">
  <!-- angular-bootstrap -->
  <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/angular-bootstrap/ui-bootstrap-csp.css')}}">
  <!-- ngCropper -->
  <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/ng-cropper/dist/ngCropper.all.min.css')}}">
  <!-- sweetalert2 -->
  <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/sweetalert2/dist/sweetalert2.css')}}">
  <!-- angular ui select -->
  <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/angular-ui-select/dist/select.min.css')}}">
  <!-- slect2 (required by angular ui select)-->
  <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/select2/3.4.5/select2.css')}}">
  <!-- selectize (required by angular ui select)-->
  <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/selectize/dist/css/selectize.default.css')}}">
  <!-- angucomplete-alt -->
  <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/angucomplete-alt/angucomplete-alt.css')}}">
  <!-- bootstrap material datettimepicker -->
  <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
  <!-- animate css -->
  <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/animate.css/animate.min.css')}}">
  <!-- awesome bootstrap checkbox -->
  <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/awesome-bootstrap-checkbox/demo/build.css')}}">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!-- amchart -->
  <link rel="stylesheet" href="{{asset('packages/tugumuda/bower_components/amcharts/plugins/export/export.css')}}">
  <div id="window_preloader"></div>
</head>
<body class="hold-transition skin-yellow sidebar-mini fixed">
<div class="wrapper" ng-controller="mainController">
	<div class="wrapper">      
		@include('home::dashboard.header')@include('home::dashboard.sidebar-left')<div class="content-wrapper">
			<div id="utama" ng-view>@yield('content')</div>
		</div>
		<aside class="control-sidebar control-sidebar-light">
      
		</aside>
	</div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{asset('packages/tugumuda/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('packages/tugumuda/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('packages/tugumuda/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('packages/tugumuda/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('packages/tugumuda/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- tinymce -->
<script src="{{asset('packages/tugumuda/bower_components/tinymce/tinymce.js')}}"></script>
<!-- Angular -->
<script src="{{asset('packages/tugumuda/bower_components/angular/angular.min.js')}}"></script>
<!-- Angular animate -->
<script src="{{asset('packages/tugumuda/bower_components/angular-animate/angular-animate.min.js')}}"></script>
<!-- Angular touch-->
<script src="{{asset('packages/tugumuda/bower_components/angular-touch/angular-touch.min.js')}}"></script>
<!-- Angular sanitize-->
<script src="{{asset('packages/tugumuda/bower_components/angular-sanitize/angular-sanitize.min.js')}}"></script>
<!-- Angular route-->
<script src="{{asset('packages/tugumuda/bower_components/angular-route/angular-route.js')}}"></script>
<!-- angular loading bar -->
<script src="{{asset('packages/tugumuda/bower_components/angular-loading-bar/src/loading-bar.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('packages/tugumuda/bower_components/admin-lte-2.3.11/app.js')}}"></script>
<!-- ngCropper -->
<script src="{{asset('packages/tugumuda/bower_components/ng-cropper/dist/ngCropper.all.min.js')}}"></script>
<!-- angular bootstrap toggle -->
<script src="{{asset('packages/tugumuda/bower_components/angular-bootstrap-toggle/dist/angular-bootstrap-toggle.min.js')}}"></script>
<!-- angular-bootstrap -->
<script src="{{asset('packages/tugumuda/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js')}}"></script>
<!-- sweetalert2 -->
<script src="{{asset('packages/tugumuda/bower_components/sweetalert2/dist/sweetalert2.js')}}"></script>
<!-- angular ui select -->
<script src="{{asset('packages/tugumuda/bower_components/angular-ui-select/dist/select.min.js')}}"></script>
<!-- angucomplete-alt -->
<script src="{{asset('packages/tugumuda/bower_components/angucomplete-alt/dist/angucomplete-alt.min.js')}}"></script>
<!-- ui-tinymce -->
<script src="{{asset('packages/tugumuda/bower_components/angular-ui-tinymce/src/tinymce.js')}}"></script>
<!-- momentjs -->
<script src="{{asset('packages/tugumuda/bower_components/momentjs/min/moment-with-locales.min.js')}}"></script>
<!-- bootstrap material datettimepicker -->
<script src="{{asset('packages/tugumuda/bower_components/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
<!-- angular ui sortable -->
<script src="{{asset('packages/tugumuda/bower_components/angular-ui-sortable/sortable.min.js')}}"></script>
<!-- bootstrap notify -->
<script src="{{asset('packages/tugumuda/bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js')}}"></script>
<!-- ng file upload -->
<script src="{{asset('packages/tugumuda/bower_components/ng-file-upload-master/dist/ng-file-upload-all.min.js')}}"></script>
<!-- ng-remote-validate -->
<script src="{{asset('packages/tugumuda/bower_components/ng-remote-validate/release/ngRemoteValidate.js')}}"></script>
<!-- amchart -->
<script src="{{asset('packages/tugumuda/bower_components/amcharts/amcharts.js')}}"></script>
<script src="{{asset('packages/tugumuda/bower_components/amcharts/themes/light.js')}}"></script>
<script src="{{asset('packages/tugumuda/bower_components/amcharts/themes/dark.js')}}"></script>
<script src="{{asset('packages/tugumuda/bower_components/amcharts/pie.js')}}"></script>
<script src="{{asset('packages/tugumuda/bower_components/amcharts/serial.js')}}"></script>
<script src="{{asset('packages/tugumuda/bower_components/amcharts/plugins/export/export.min.js')}}"></script>
<!-- amchart -->
<script>      
	var dntApp = angular.module('dntApp', ['ngRoute', 'ngAnimate', 'ui.select','ngSanitize', 'ui.tinymce', 'ngFileUpload', 'angular-loading-bar', 'ngCropper', 'ui.bootstrap', 'ui.toggle', 'angucomplete-alt', 'ui.sortable','remoteValidation']);
	dntApp.config(function($interpolateProvider, $routeProvider, $locationProvider, cfpLoadingBarProvider, $httpProvider) {
		$locationProvider.hashPrefix('');
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $routeProvider
		.when('/', {
			templateUrl: 'beranda',
			controller: 'navCtrl'
		})
		.when('/profil', {
			templateUrl: 'profil',
			controller: 'navCtrl'
		})
		.when('/userprofile/:id', {
			templateUrl: function(params) {
				return 'userprofile/'+params.id ;
			},
			controller: 'navCtrl'
		})
    @foreach($modules as $row)
		@if(\PermissionsLibrary::hasPermission('mod-'.strtolower(str_replace(' ','',$row->name)).'-index'))
		.when('{{$row->path}}', {
			templateUrl: '{{substr($row->path,1)}}',
			controller: 'navCtrl'
		})
		@endif
		@if(\PermissionsLibrary::hasPermission('mod-'.strtolower(str_replace(' ','',$row->name)).'-create'))
		.when('{{$row->path}}/create', {
			templateUrl: '{{substr($row->path,1)}}/create',
			controller: 'navCtrl'
		})
		@endif
		@if(\PermissionsLibrary::hasPermission('mod-'.strtolower(str_replace(' ','',$row->name)).'-edit'))
		.when('{{$row->path}}/edit/:id', {
			templateUrl: function(params) {
				return '{{substr($row->path,1)}}/edit/'+params.id ;
			},
			controller: 'navCtrl'
		})
		@endif
    @endforeach.otherwise({redirectTo: '/'});
    cfpLoadingBarProvider.includeSpinner = false;
    cfpLoadingBarProvider.parentSelector = '.loading-bar-container';
	});
</script>
<script src="{{asset('packages/angular/MainApp.js')}}"></script>
<script src="{{asset('packages/angular/navApp.js')}}"></script>
<script src="{{asset('packages/angular/DashboardApp.js')}}"></script>
<script src="{{asset('packages/angular/ProfileApp.js')}}"></script>
<script src="{{asset('packages/angular/UserprofileApp.js')}}"></script>
<script src="{{asset('packages/angular/DemoApp.js')}}"></script>
@foreach($modules as $row)
@if(\PermissionsLibrary::hasPermission('mod-'.str_replace(' ','',$row->name).'-index'))
<script type="text/javascript" src="{{asset('packages/angular/'.ucfirst(str_replace('Controller','App',$row->uses)).'.js')}}"></script>
@endif
@endforeach
</body>
</html>
