      <header class="main-header">
          <div class="loading-bar-container"></div>
          <!-- Logo -->
          <a href="{{url()}}/dashboard#/" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><img width="30px" src="{{ asset('packages/tugumuda/images/kudus-logo.png') }}"></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img width="30px" src="{{ asset('packages/tugumuda/images/kudus-logo.png') }}"> <b>SKM</b> Kudus</span>
          </a>
          <!-- Header Navbar: style can be found in header.less -->
          <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a role="button" class="sidebar-toggle" data-toggle="offcanvas" role="button">
              <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
              <ul class="nav navbar-nav">
                <li class="hidden-xs">
                  <a title="Fullscreen" role="button" ng-click="fullscreen()"><i class="ion-android-expand fa-lg"></i></a>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                  <a role="button" class="dropdown-toggle" data-toggle="dropdown">
                    {{-- <img src="[[userdata.fotoprofil]]" class="user-image" alt="User Image"> --}}
                    <i class="fa fa-user-circle-o"></i>
                    <span class="hidden-xs">[[userdata.name]]</span>
                  </a>
                  <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                      <img src="[[userdata.fotoprofil]]" class="img-circle" alt="User Image">

                      <p>
                        <small>Terakhir Login<br>[[lastlogin]]</small>
                      </p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                      <center><a href="{{url()}}/dashboard#/profil"><i class="fa fa-user"></i> Profil</a></center>
                    </li>
                  </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                {{-- <li>
                  <a role="button" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li> --}}
                <li>
                  <a role="button" title="Keluar" role="button" ng-click="signout()"><i class="ion-android-exit fa-lg"></i></a>
                </li>
              </ul>
            </div>
          </nav>
        </header>