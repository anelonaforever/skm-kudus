<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="[[userdata.fotoprofil]]" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>[[userdata.name]]</p>
          <a><i class="fa fa-circle text-success"></i> [[userdata.role]]</a>
        </div>
      </div>
      <!-- search form -->
      {{-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
        </div>
      </form> --}}
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">NAVIGASI UTAMA</li>
        <li ng-repeat="menu in menus" ng-class="{'treeview': menu.path == ''}">
          <a href="{{url()}}/dashboard#/" ng-if="menu.name == 'Dashboard' && menu.path != ''">
            <i class="[[menu.icons]]"></i> <span>[[menu.name]]</span>
          </a>
          <a ng-if="menu.name != 'Dashboard' && menu.path == ''" role="button">
            <i class="[[menu.icons]]"></i> <span>[[menu.name]]</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul ng-if="menu.name != 'Dashboard' && menu.path == ''" class="treeview-menu">
            <li ng-repeat="submenu in menu.submenu"><a href="{{url()}}/dashboard#[[submenu.path]]"><i class="[[submenu.icons]]"></i> [[submenu.name]]</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>