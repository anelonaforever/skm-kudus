<section class="content-header">
    <h1>
        Beranda<small>Indeks Kepuasan Masyarakat</small>
    </h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-home"></i> Beranda</li>
    </ol>
</section>
<section class="content" ng-controller="DashboardController">
    <div class="row">
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>[[data.today]]</h3>
              <p>Survey Hari Ini</p>
            </div>
            <div class="icon">
              <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
            </div>
            <a href="[[mainUrl]]/dashboard#/responmasyarakat/responmasuk?q=today" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-green">
            <div class="inner">
              <h3>[[data.yesterday]]</h3>
              <p>Survey Kemarin</p>
            </div>
            <div class="icon">
              <i class="fa fa-history" aria-hidden="true"></i>
            </div>
            <a href="[[mainUrl]]/dashboard#/responmasyarakat/responmasuk?q=yesterday" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>[[data.thisweek]]</h3>
              <p>Survey Minggu Ini</p>
            </div>
            <div class="icon">
              <i class="fa fa-calendar-minus-o" aria-hidden="true"></i>
            </div>
            <a href="[[mainUrl]]/dashboard#/responmasyarakat/responmasuk?q=thisweek" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h3>[[data.lastweek]]</h3>
              <p>Survey Minggu Lalu</p>
            </div>
            <div class="icon">
              <i class="fa fa-calendar" aria-hidden="true"></i>
            </div>
            <a href="[[mainUrl]]/dashboard#/responmasyarakat/responmasuk?q=lastweek" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
<!-- <pre><?php print_r(Session::all());?></pre> -->
</section>