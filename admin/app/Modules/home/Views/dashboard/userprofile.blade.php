<div ng-controller="UserprofileController">
<section class="content-header">
  <h1>
	[[profiledata.name]] <small></small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
  <li><a href="{!!url()!!}/dashboard#/settings/users"> Users</a></li>
	<li class="active">User Profile</li>
  </ol>
</section>
<section class="content">
  <div class="profil-container">
    <div class="pp-container pp-inner">
      <div class="change-pp-box-container">
        <img ng-src="[[profiledata.fotoprofil]]" style="width: 100%;">
        <div class="pp-bottom"></div>
      </div>
      <br>
      <b style="font-size:16px;">Role</b><br>
      [[profiledata.role]]
    </div>
    <div class="pp-innerTwo pp-inner">
      <div class="nav-tabs-custom" style="margin-bottom: inherit;">
        <ul class="nav nav-tabs nav-justified nav-freeze">
          <li class="active"><a showtab="" href="#tentang" data-toggle="tab" aria-expanded="true">TENTANG</a></li>
          <li class=""><a showtab="" href="#log_aktifitas" data-toggle="tab" aria-expanded="false">LOG AKTIVITAS</a></li>
        </ul>
        <div class="tab-content profile-tab-content">
          <div class="tab-pane active" id="tentang">
            <h2 class="page-header"><span class="fa fa-user fa-fw"></span> Informasi Umum</h2>
            <table class="tabel-info" ng-show="!edtBasicinfo">
              <tr>
                <td width="110px">Nama Lengkap</td>
                <td>[[profiledata.name]]</td>
              </tr>
              <tr>
                <td>Jenis Kelamin</td>
                <td>[[profiledata.jenis_kelamin == 1 ? 'Laki-Laki' : profiledata.jenis_kelamin == 2 ? 'Perempuan' : '-']]</td>
              </tr>
              <tr>
                <td>Tanggal Lahir</td>
                <td>[[profiledata.tgl_lahir | notNull | date:'dd-MM-yyyy']]</td>
              </tr>
              <tr>
                <td>Alamat</td>
                <td>[[profiledata.alamat | notNull]]</td>
              </tr>
            </table>
            <br>
            <h2 class="page-header"><span class="fa fa-phone fa-fw"></span> Informasi Kontak</h2>
            <table class="tabel-info" ng-show="!edtContactinfo">
              <tr>
                <td width="110px">Nomor Telepon</td>
                <td>[[profiledata.no_telp | notNull]]</td>
              </tr>
              <tr>
                <td>Alamat Email</td>
                <td>[[profiledata.email | notNull]]</td>
              </tr>
              <tr>
                <td>Facebook</td>
                <td>
                  <a ng-if="profiledata.facebook != ''" href="[[profiledata.facebook]]" target="_blank">[[profiledata.facebook]]</a>
                  <span ng-if="profiledata.facebook == ''">-</span>
                </td>
              </tr>
              <tr>
                <td>Twitter</td>
                <td>
                  <a ng-if="profiledata.twitter != ''" href="[[profiledata.twitter]]" target="_blank">[[profiledata.twitter]]</a>
                  <span ng-if="profiledata.twitter == ''">-</span>
                </td>
              </tr>
            </table>
            <br>
          </div>
          <div class="tab-pane" id="log_aktifitas">
            <div class="table-responsive">
              <table class="table table-condensed table-striped table-hover">
                <thead class="bg-default">
                  <tr>
                    <th>IP</th>
                    <th>Tanggal</th>
                    <th>Waktu</th>
                    <th>Aksi</th>
                    <th>URL</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</div>