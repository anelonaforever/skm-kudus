<style type="text/css">
  .user-panel{
    padding: 0px;
    height: 0px;
  }
</style>
<div ng-controller="ProfileController">
<section class="content-header">
  <h1>
	[[profiledata.name]] <small></small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
	<li class="active">Profil</li>
  </ol>
</section>
<section class="content">
  <div class="profil-container">
    <div class="pp-container pp-inner">
      <div class="change-pp-box-container" img-cropper>
        <a id="pilihfoto" role="button" class="change-pp-box"><span class="fa fa-camera"></span> Ubah Foto Profil</a>
        <img ng-if="profiledata.fotoprofil" ng-src="[[profiledata.fotoprofil]]" style="width: 100%;">
        <div class="pp-bottom"></div>
        <input id="foto" type="file" onchange="angular.element(this).scope().onFile(this.files[0])" style="display:none" accept="image/jpeg">
      </div>
      <br>
      <b style="font-size:16px;">Role</b><br>
      [[profiledata.role]]
    </div>
    <div class="pp-innerTwo pp-inner">
      <div class="nav-tabs-custom" style="margin-bottom: inherit;">
        <ul class="nav nav-tabs nav-justified nav-freeze">
          <li class="active"><a showtab="" href="#tentang" data-toggle="tab" aria-expanded="true">TENTANG</a></li>
          <li class=""><a showtab="" href="#pengaturan" data-toggle="tab" aria-expanded="false">PENGATURAN</a></li>
          <!-- <li class=""><a showtab="" href="#log_aktifitas" data-toggle="tab" aria-expanded="false">LOG AKTIVITAS</a></li> -->
        </ul>
        <div class="tab-content profile-tab-content">
          <div class="tab-pane active" id="tentang">
            <h2 class="page-header"><span class="fa fa-user fa-fw"></span> Informasi Umum
              <!-- <div class="dropdown pull-right">
                <a aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" type="button" role="button" tooltip-placement='top' uib-tooltip='Opsi'>
                  <i class="ion-more" aria-hidden="true"></i>
                </a>
                <ul class="dropdown-menu">
                  <li><a role="button" ng-click="editBasicinfo()">Edit</a></li>
                </ul>
              </div> -->
            </h2>
            <table class="tabel-info" ng-show="!edtBasicinfo">
              <tr>
                <td width="110px">Nama Lengkap</td>
                <td>[[profiledata.name]]</td>
              </tr>
              <!-- <tr>
                <td>Jenis Kelamin</td>
                <td>[[profiledata.jenis_kelamin == 1 ? 'Laki-Laki' : profiledata.jenis_kelamin == 2 ? 'Perempuan' : '-']]</td>
              </tr>
              <tr>
                <td>Tanggal Lahir</td>
                <td>[[profiledata.tgl_lahir | notNull | date:'dd-MM-yyyy']]</td>
              </tr>
              <tr>
                <td>Alamat</td>
                <td>[[profiledata.alamat | notNull]]</td>
              </tr> -->
            </table>
            <!-- <form name="basicinfoForm" class="form-horizontal edit-form" ng-show="edtBasicinfo">
      			  <div class="form-group" ng-class="{ 'has-error' : basicinfoForm.name.$error.required && !basicinfoForm.name.$pristine }">
      			    <label for="name" class="col-sm-3 control-label">Nama Lengkap</label>
      			    <div class="col-sm-9">
      			      <input type="text" class="form-control" id="name" name="name" ng-model="basicinfo.name" required="true">
                  <span class="help-block" ng-show="basicinfoForm.name.$error.required && !basicinfoForm.name.$pristine">Nama diperlukan.</span>
      			    </div>
      			  </div>
      			  <div class="form-group">
      			    <label for="jenis_kelamin" class="col-sm-3 control-label">Jenis Kelamin</label>
      			    <div class="col-sm-9">
      			      <select id="jenis_kelamin" class="form-control" ng-model="basicinfo.jenis_kelamin" ng-options="obj.jenis_kelamin as obj.name for obj in jenis_kelamin_opt">
                    <option value="">-Pilih-</option>   
                  </select>
      			    </div>
      			  </div>
      			  <div class="form-group">
      			    <label for="tgl_lahir" class="col-sm-3 control-label">Tanggal Lahir</label>
      			    <div class="col-sm-9">
                  <input material-datepicker type="text" class="form-control" placeholder="Tanggal Lahir" ng-model="basicinfo.tgl_lahir"><br>
      			    </div>
      			  </div>
      			  <div class="form-group">
      			    <label for="alamat" class="col-sm-3 control-label">Alamat</label>
      			    <div class="col-sm-9">
                  <textarea id="alamat" class="form-control" ng-model="basicinfo.alamat"></textarea>
      			    </div>
      			  </div>
      			  <div style="text-align:right;">
      			  	<button type="submit" class="btn btn-primary btn-sm" ng-click="simpanBasicinfo()" ng-disabled="basicinfoForm.$invalid">Simpan</button>&nbsp;&nbsp;&nbsp;
                <button type="button" class="btn btn-warning btn-sm" ng-click="batalEditbi()">Batal</button>
      			  </div>
      			</form> -->
            <!-- <br> -->
            <!-- <h2 class="page-header"><span class="fa fa-phone fa-fw"></span> Informasi Kontak
              <div class="dropdown pull-right">
                <a aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" type="button" role="button" tooltip-placement='top' uib-tooltip='Opsi'>
                  <i class="ion-more" aria-hidden="true"></i>
                </a>
                <ul class="dropdown-menu">
                  <li><a role="button" ng-click="editContactinfo()">Edit</a></li>
                </ul>
              </div>
            </h2> -->
            <!-- table class="tabel-info" ng-show="!edtContactinfo">
              <tr>
                <td width="110px">Nomor Telepon</td>
                <td>[[profiledata.no_telp | notNull]]</td>
              </tr>
              <tr>
                <td>Alamat Email</td>
                <td>[[profiledata.email | notNull]]</td>
              </tr>
            </table> -->
            <!-- <form class="form-horizontal edit-form" ng-show="edtContactinfo">
      			  <div class="form-group">
      			    <label for="no_telp" class="col-sm-3 control-label">Nomor Telepon</label>
      			    <div class="col-sm-9">
      			      <input type="text" class="form-control" id="no_telp" ng-model="contactinfo.no_telp">
      			    </div>
      			  </div>
      			  <div class="form-group">
      			    <label for="email" class="col-sm-3 control-label">Alamat Email</label>
      			    <div class="col-sm-9">
      			      <input type="text" class="form-control" id="email" ng-model="contactinfo.email">
      			    </div>
      			  </div>
      			  <div style="text-align:right;">
      			  	<button type="submit" class="btn btn-primary btn-sm" ng-click="simpancontactinfo()">Simpan</button>&nbsp;&nbsp;&nbsp;
                <button type="button" class="btn btn-warning btn-sm" ng-click="batalEditci()">Batal</button>
      			  </div>
      			</form><br> -->
          </div>
          <!-- <div class="tab-pane" id="log_aktifitas">
            <div class="table-responsive">
              <table class="table table-condensed table-striped table-hover">
                <thead class="bg-default">
                  <tr>
                    <th>IP</th>
                    <th>Tanggal</th>
                    <th>Waktu</th>
                    <th>Aksi</th>
                    <th>URL</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div> -->
          <div class="tab-pane" id="pengaturan">
            <h2 class="page-header"><span class="fa fa-user fa-fw"></span> Pengaturan Akun
              <div class="dropdown pull-right">
                <a aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" type="button" role="button" tooltip-placement='top' uib-tooltip='Opsi'>
                  <i class="ion-more" aria-hidden="true"></i>
                </a>
                <ul class="dropdown-menu">
                  <li><a role="button" ng-click="editakun()">Edit</a></li>
                </ul>
              </div>
            </h2>
            <table class="tabel-info" ng-show="!edtAkun">
              <tr>
                <td width="110px">Username</td>
                <td>[[akun.username]]</td>
              </tr>
              <tr>
                <td>Kata Sandi</td>
                <td>Terakhir diubah pada tanggal [[userdata.last_pwd_change]]</td>
              </tr>
            </table>
            <form role="form" name="akunForm" class="form-horizontal edit-form" ng-show="edtAkun">
              <div class="form-group" ng-class="{ 'has-error' : akunForm.username.$error.ngRemoteValidate || (akunForm.username.$error.required && akunForm.username.$touched)}">
                <label for="username" class="col-sm-3 control-label">Username:</label>
                <!-- <div class="col-sm-9">
                  <input type="text" class="form-control" ng-model="akun.username" ng-remote-validate="{{url()}}/settings/users/validusername?id=[[userdata.id]]" name="username"  id="username" required="true">
                  <span class="message" ng-show="akunForm.username.$pending">Memeriksa...</span>
                  <span class="help-block" ng-show="akunForm.username.$error.ngRemoteValidate">Sudah digunakan.</span>
                  <span class="help-block" ng-show="akunForm.username.$error.required && akunForm.username.$touched">Username wajib diisi.</span>
                </div> -->
                <div class="col-sm-9">
                  <input type="text" class="form-control" ng-model="akun.username" readonly>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                  <div class="checkbox checkbox-warning">
                    <input type="checkbox" ng-model="akun.ubahpwd" id="checkbox1" class="styled">
                    <label for="checkbox1">
                      Ubah Kata Sandi?
                    </label>
                  </div>
                </div>
              </div>
              <div class="form-group" ng-show="akun.ubahpwd">
                <label for="newpassword" class="col-sm-3 control-label">Kata Sandi Baru</label>
                <div class="col-sm-9">
                  <input type="password" class="form-control" id="newpassword" name="newpassword" ng-model="akun.newpassword">
                </div>
              </div>
              <div class="form-group" ng-show="akun.ubahpwd" ng-class="{ 'has-error' : !IsMatch }">
                <label for="cpassword" class="col-sm-3 control-label">Konfirmasi Kata Sandi</label>
                <div class="col-sm-9">
                  <input type="password" class="form-control" id="cpassword" name="cpassword" ng-model="akun.cpassword" ng-keypress="identcheck()">
                  <span class="help-block" ng-show="!IsMatch">Konfirmasi kata sandi tidak cocok</span>
                </div>
              </div>
              <div class="form-group" ng-show="akun.ubahpwd">
                <div class="col-sm-offset-3 col-sm-9">
                  <div class="checkbox checkbox-info">
                    <input type="checkbox" id="showpassword"> 
                    <label for="showpassword">
                      Tampilkan Kata Sandi?
                    </label>
                  </div>
                </div>
              </div>
              <div style="text-align:right;">
                <button type="submit" class="btn btn-primary btn-sm" ng-click="cekOldpwd()" ng-disabled="akunForm.$invalid || (akun.ubahpwd && !IsMatch)">Simpan</button>&nbsp;&nbsp;&nbsp;
                <button type="button" class="btn btn-warning btn-sm" ng-click="batalEditakun()">Batal</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/ng-template" id="myModalContent.html">
    <div class="modal-header">
      <a role="button" class="text-yellow pull-right" ng-click="cancel()"><span class="ion-close-round"></span></a>
        <div class="modal-title">Crop Foto</div>
        <div class="loading-bar-container"></div>
    </div>
    <div class="modal-body">
      <div ng-show="dataUrl" class="img-container">
        <img ng-if="dataUrl" ng-src="[[dataUrl]]" width="100%" 
        ng-cropper 
        ng-cropper-proxy="cropperProxy" 
        ng-cropper-show="showEvent" 
        ng-cropper-hide="hideEvent" 
        ng-cropper-options="options">
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" ng-click="simpanfoto()">Simpan</button>
      <button type="button" class="btn btn-warning" ng-click="cancel()">Batal</button>
    </div>
  </script>
</section>
</div>
<script type="text/javascript">
  $('#showpassword').change(function () {
    if ($(this).prop('checked')==true){ 
      $('input[name=newpassword]').prop('type', 'text');
      $('input[name=cpassword]').prop('type', 'text');
    }else{
      $('input[name=newpassword]').prop('type', 'password');
      $('input[name=cpassword]').prop('type', 'password');
    }
  });
</script>
