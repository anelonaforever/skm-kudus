<?php namespace App\Modules\developer\modules\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\developer\modules\Models\ModulesModel;
use Input,View, Request, Form, File;
class ModulesController extends Controller {

	public function __construct()
	{
		
		//$this->coba = $coba;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return View::make('modules::index');
	}

	public function getData(){
		$data = [];
		$modules = \DB::table('modules')
		->select('modules.*','contexts.order as contextorder','contexts.name as contextname')
		->join('contexts','contexts.id','=','modules.id_context')
		->orderBy('contextorder','asc')
		->orderBy('modules.order','asc')
		->get();

		$data['modules'] = $modules;

		return json_encode($data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
        $data['contexts'] =  \ContextModel::where('flag', 1)->orderBy('order')->get();
		$data['modules'] = ModulesModel::where('flag', 1)->where('module_path', '!=', '0')->get();
		$data['modulePath'] = array(1=>app_path().'/Modules');
		$data['controllerActions'] = \ModulesLibrary::controllerActions();
//		arsort(array('' => 'modules'));
		$data['columnTypes'] = \ModulesLibrary::columnTypes();
        return View::make('modules::create',$data);
	}

	public function getDatacreate(){
		$data['contexts'] =  \ContextModel::where('flag', 1)->orderBy('order')->get();
		$data['modules'] = ModulesModel::where('flag', 1)->where('module_path', '!=', '0')->get();
		$data['modulePath'] = array(1=>app_path().'/Modules');
		$data['controllerActions'] = \ModulesLibrary::controllerActions();
		$data['columnTypes'] = \ModulesLibrary::columnTypes();

		return json_encode($data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postCreate()
	{
		$realModuleName  = Input::get('name');
		$moduleName  = strtolower(Input::get('name'));
		$moduleName = str_replace(" ", "", $moduleName);
		//$modulePath = \Config::get('claravel::modulesPath');
		$contextName = \App\Modules\developer\context\Models\ContextModel::find(Input::get('id_context'));
		$contextName = str_replace(" ", "", $contextName->name);
		$contextName = strtolower($contextName);
		$modulePath = app_path() . '/Modules'.'/'.$contextName.'/'.$moduleName;
		
		$module = new ModulesModel;
		$module->module_path    = Input::get('module_path');
		$module->id_context    = Input::get('id_context');
		$module->id_parent     = Input::get('id_parent');
		$module->name       	= Input::get('name');
		
		$module->path      	= "/".$contextName.'/'.$moduleName;
		
		$module->uses 		= ucfirst($moduleName)."Controller";
		$module->flag 		= Input::get('flag');
		$module->is_nav_bar 	= 1;
		$module->icons 		= str_replace("fa-lg","",Input::get('icons'));
		$module->order 		= Input::get('order');
		$module->table_name 		= Input::get('table_name');
		
		if (Input::get('controller') == '1'){
		
			$modulesLibrary = new \ModulesLibrary;
			$createModules = $modulesLibrary->run($realModuleName, $modulePath, $contextName);	
			if ($createModules){
				$module->save();	
            }
		}else{
			$module->save();	
			
			$permission['name'] = 'mod-'.$moduleName.'-index';
			$permission['description'] = 'Allow Access '.\Input::get('name').' Index';
			\PermissionsLibrary::assignPermission($permission);
		}

		if($module){
			$result = array('result' => 'success');
		}else{
			$result = array('result' => 'failed');
		}

		return json_encode($result);
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('modules::show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit()
	{
		return View::make('modules::edit');	
	}

	public function getDataedit(){
		$id = Request::segment(4);
		$data = \App\Modules\developer\modules\Models\ModulesModel::find($id);
		return json_encode($data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postEdit(){
		$id = Input::get('id'); 
		$modules = \App\Modules\developer\modules\Models\ModulesModel::find($id);

		$modules->icons = str_replace(' fa-lg','',Input::get('icons'));
		$modules->order = Input::get('order');
		$modules->flag = Input::get('flag');
		$modules->save();

		if($modules){
			$result = array('result' => 'success');
		}else{
			$result = array('result' => 'failed');
		}

		return json_encode($result);
	}

	public function postSort(){
		$data = \Input::get('data');
		$sort = explode(',', $data);

		for($a=0 ; $a<sizeof($sort) ; $a++){
			$update = \DB::table('modules')
			->where('id','=',$sort[$a])
			->update(array('order' => $a+1));
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function postDelete(){
        $ids = Input::get('id');
        if (is_array($ids)){
            foreach($ids as $id){
				$data = ModulesModel::with('context')->find($id);
				$moduleName = strtolower($data->name);
				$moduleName = str_replace(" ", "", $moduleName);
				$moduleBasePath = array(1=>app_path().'/Modules');

				$contextName = str_replace(" ", "", $data->context->name);
				$contextName = strtolower($contextName);

				if (\File::isDirectory(app_path().'/Modules'.'/'.$contextName.'/'.$moduleName)){
					\File::deleteDirectory(app_path().'/Modules'.'/'.$contextName.'/'.$moduleName);
					\File::delete(base_path().'/packages/angular/'.ucfirst($moduleName).'App.js');
					if ($data->table_name !=""){
						\Schema::drop($data->table_name);
					}
				}
				
				$permissionsModel = \PermissionsModel::where('name', 'LIKE', 'mod-'.$moduleName.'%')->get();
				foreach ($permissionsModel as $pm) {
					\PermissionsmatrixModel::where('permission_id','=',$pm->id)->delete();
					\PermissionsModel::find($pm->id)->delete();
				}
				
				$data->delete();
			}
        }
        else{
            $data = ModulesModel::with('context')->find($ids);
			$moduleName = strtolower($data->name);
			$moduleName = str_replace(" ", "", $moduleName);
			$moduleBasePath = array(1=>app_path().'/Modules');

			$contextName = str_replace(" ", "", $data->context->name);
			$contextName = strtolower($contextName);
			if (\File::isDirectory(app_path().'/Modules'.'/'.$contextName.'/'.$moduleName)){
				\File::deleteDirectory(app_path().'/Modules'.'/'.$contextName.'/'.$moduleName);
				\File::delete(base_path().'/packages/angular/'.ucfirst($moduleName).'App.js');
				if ($data->table_name !=""){
					\Schema::drop($data->table_name);
				}
			}
			
			$permissionsModel = \PermissionsModel::where('name', 'LIKE', 'mod-'.$moduleName.'%')->get();
			foreach ($permissionsModel as $pm) {
				\PermissionsmatrixModel::where('permission_id','=',$pm->id)->delete();
				\PermissionsModel::find($pm->id)->delete();
			}
			
			$data->delete();
        }

        if($data){
        	$result = array('result' => 'success');
        }else{
        	$result = array('result' => 'failed');
        }

        return json_encode($result);
    }


	/**
	* Get al field for existing table
	*	
	* @return json	
	*/
	public function postTablefields(){
		$table = \Input::get('table');
		
		$selects = array(
        'column_name as Field',
        'column_type as Type',
        'is_nullable as Null',
        'column_key as Key',
        'column_default as Default',
        'extra as Extra',
        'data_type as Data_Type'
    	);

		$connection = \DB::connection();
    	$connection->getSchemaBuilder();

		
		$schema =  \DB::table('information_schema.columns')
            ->where('table_schema', '=', $connection->getDatabaseName())
            ->where('table_name', '=', $table)
            ->get($selects);
        return \Response::json($schema);

	}
    
}