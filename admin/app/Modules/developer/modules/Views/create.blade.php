<section class="content-header">
    <h1>
        Buat Modul<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li><a href="{!!ClaravelHelpers::backCreate()!!}"> Modules</a></li>
        <li class="active">Buat Modul</li>
    </ol>
</section>
<section class="content">
  <form name="moduleForm" class="form-horizontal" ng-controller="ModuleControllerCreate" ng-submit="submitForm()" id="simpan">
    <div class="box box-primary">
      <div class="box-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="id_context" class="col-sm-4 control-label">Context</label>
              <div class="col-sm-6">
              <div class="radio" ng-repeat="context in contexts">
                <input id="radio-[[context.id]]" type="radio" ng-model="formData.id_context" value="[[context.id]]">
                <label for="radio-[[context.id]]">[[context.name]]</label>
              </div>
              </div>
            </div>
            <div class="form-group">
              <label for="id_parent" class="col-sm-4 control-label">Parent</label>
              <div class="col-sm-6">
                <select id="id_parent" name="id_parent" class="form-control" ng-model="formData.id_parent" ng-options="val.name for val in parents">
                  <option value="">--Pilih--</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('name', 'Name:', array('class' => 'col-sm-4 control-label')) !!}
              <div class="col-sm-6">
                <input class="form-control" name="name" type="text" id="name" ng-model="formData.name" placeholder="Nama Modul">
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('icons', 'Icon:', array('class' => 'col-sm-4 control-label')) !!}
              <div class="col-sm-6">
                <div class="btn-group">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="[[formData.icons]] fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                  </button>
                  <div class="dropdown-menu icon-picker">
                    <div class="icon-picker-head">
                      <input type="text" class="form-control" placeholder="Cari Ikon" ng-model="iconsearch">
                      <input type="hidden" ng-model="formData.icons">
                    </div>
                    <div class="icon-picker-body">
                      <div ng-repeat="icon in icons | filter:iconsearch" class="icon-item" ng-click="choseicon(icon.icon)"><i class="fa [[icon.icon]] fa-2x fa-fw"></i></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">   
            <div class="form-group">
              {!! Form::label('flag', 'Is Active ?:', array('class' => 'col-sm-4 control-label')) !!}
              <div class="col-sm-6">
                <div class="checkbox">
                  <input id="isactive" type="checkbox" ng-model="formData.flag" ng-true-value="1" ng-false-value="0" ng-checked="formData.flag == 1">
                  <label for="isactive">Yes</label>
                </div>
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('controller', 'Use Controller ?:', array('class' => 'col-sm-4 control-label')) !!}
              <div class="col-sm-6">
                <div class="checkbox">
                  <input id="usecontroller" type="checkbox" ng-model="formData.controller" ng-true-value="1" ng-false-value="0" ng-checked="formData.controller == 1">
                  <label for="usecontroller">Yes</label>
                </div>
              </div>
            </div>
            <div ng-show="formData.controller">
              <div class="form-group">
                <label for="controller_actions" class="col-sm-4 control-label">Controller Actions</label>
                <div class="col-sm-3">
                <div class="checkbox" ng-repeat="action in actions">
                  <input id="[[action.name]]" type="checkbox" ng-model="action.flag" value="[[action.name]]">
                  <label for="[[action.name]]" for="">[[action.name]]</label>
                </div>
                </div>
              </div>
              <div class="form-group">
                <label for="rmodule_table" class="col-sm-4 control-label">Module Table</label>
                <div class="col-sm-6">
                  <div class="radio">
                    <input id="notable" type="radio" ng-model="formData.rmodule_table" value="0">
                    <label for="notable">No</label>
                  </div>
                  <div class="radio">  
                    <input id="newtable" type="radio" ng-model="formData.rmodule_table" value="1">
                    <label for="newtable">Create New Table</label>
                  </div>
                  <div class="radio">
                    <input id="existtable" type="radio" ng-model="formData.rmodule_table" value="2">
                    <label for="existtable">Build from Existing Table</label>
                  </div>
                </div>
              </div>
            </div> 
          </div>
        </div>
      </div>
    </div>
    <div id="module_table" class="box box-primary" ng-show="(formData.rmodule_table == '1' || formData.rmodule_table == '2') && formData.controller == true">
      <div class="box-header">
        <h3 class="box-title">Table Properties</h3>
        <div class="box-tools pull-right">
          
        </div>
      </div>
      <div class="box-body no-padding">
        <div class="row">
          <div class="col-sm-12">
            <div class="col-sm-7">
              <div class="form-group">
                <label for="table_name" class="col-sm-3 control-label">Table name</label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" ng-model="formData.table_name" placeholder="Lowercase, no spaces">
                </div>
                <div class="col-sm-4">
                   <button type="button" class="btn btn-info" ng-show="formData.rmodule_table == '2'" ng-click="getField()">Get Field</button>
                </div>
              </div>
            </div>
            <div class="col-sm-5">
              <div class="form-group">
                <label for="primary_key" class="col-sm-3 control-label">Primary Key</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" ng-model="formData.primary_key">
                </div>
              </div>
            </div>
             <div class="clearfix">
            </div>
            <fieldset>
              &nbsp;&nbsp;&nbsp;<a role="button" ng-click="addRow()"><i class="fa fa-plus-circle"></i> Add Column</a>
              <table id="table_column" class="table table-striped table-hover table-primary">
                <thead class="bg-default">
                  <tr>
                    <th>Label</th>
                    <th>Name (no space)</th>
                    <th>Input Type</th>
                    <th>Database Type</th>
                    <th>Maximum Length</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody class="form-group">
                    <tr ng-repeat="data in tabledata">
                      <td>
                        <input type="text" class="col-sm-10 form-control" ng-model="data.label" placeholder="The name that will be used on webpages">
                      </td>
                      <td>
                        <input type="text" class="col-sm-10 form-control" ng-model="data.name" placeholder="The field name for the database. Lowercase is best.">
                      </td>
                      <td>
                        <select class="form-control" ng-model="data.input_type" ng-options="value for value in input_type">
                          <option value="">--Pilih--</option>
                        </select>
                      </td>
                      <td>
                        <select class="form-control" ng-model="data.database_type" ng-options="value for value in database_type">
                          <option value="">--Pilih--</option>
                        </select>
                      </td>
                      <td>
                        <input type="text" class="col-sm-10 form-control" ng-model="data.max_length" placeholder="30, 255, 1000, etc...">
                      </td>
                      <td>
                        <a role="button" data-ng-click="removeRow($index)"><span class="glyphicon glyphicon-minus-sign"></span></a>
                      </td>
                    </tr>
                </tbody>
              </table>
            </fieldset>
          </div>
        </div>
      </div>
    </div>
    {!! ClaravelHelpers::btnSave() !!}
    &nbsp;
    &nbsp;
    {!! ClaravelHelpers::btnCancel() !!}
  </form>
</section>