<section class="content-header">
    <h1>
        Buat Context<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!!url()!!}/dashboard#/"> Dashboard</a></li>
        <li><a href="{!!ClaravelHelpers::backCreate()!!}"> Context</a></li>
        <li class="active">Buat Context</li>
    </ol>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="row">
      <div class="col-md-12">
        <form name="contextform" class="form-horizontal" ng-controller="ContextControllerCreate" ng-submit="submitForm()" id="simpan">
          <div class="box-body">
            <div class="form-group" ng-class="{ 'has-error' : contextform.name.$error.required && !contextform.name.$pristine }">
              {!! Form::label('name', 'Name:', array('class' => 'col-sm-2 control-label')) !!}
              <div class="col-sm-5">
                {!! Form::text('name', null, array('class'=> 'form-control', 'ng-model' => 'formData.name', 'required' => 'true')) !!}
                <span class="help-block" ng-show="contextform.name.$error.required && !contextform.name.$pristine">Nama diperlukan.</span>
              </div>
            </div>
            <div class="form-group" ng-controller="DemoCtrl as ctrl">
              {!! Form::label('icons', 'Icons:', array('class' => 'col-sm-2 control-label')) !!}
              <div class="col-sm-3">
                <div class="btn-group">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="[[formData.icons]] fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                  </button>
                  <div class="dropdown-menu icon-picker">
                    <div class="icon-picker-head">
                      <input type="text" class="form-control" placeholder="Cari Ikon" ng-model="iconsearch">
                      <input type="hidden" ng-model="formData.icons">
                    </div>
                    <div class="icon-picker-body">
                      <div ng-repeat="icon in icons | filter:iconsearch" class="icon-item" ng-click="choseicon(icon.icon)"><i class="fa [[icon.icon]] fa-2x fa-fw"></i></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-6">
                <div class="checkbox">
                  <input name="flag" type="checkbox" id="flag" ng-model="formData.flag" ng-true-value="1" ng-false-value="0" > 
                  <label for="flag">Aktif?</label>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-6">
                <div class="checkbox">
                  <input name="is_nav_bar" type="checkbox" id="is_nav_bar" ng-model="formData.is_nav_bar" ng-true-value="1" ng-false-value="0">
                  <label for="is_nav_bar">Is nav bar?</label>
                </div>
              </div>
            </div>
          </div>
          <div class="box-footer">
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-6">
                {!! ClaravelHelpers::btnSave('contextform') !!}
                &nbsp;
                &nbsp;
                {!! ClaravelHelpers::btnCancel() !!}
              </div>
            </div> 
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $('#name').focus();
</script>