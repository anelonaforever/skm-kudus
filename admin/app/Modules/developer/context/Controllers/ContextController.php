<?php namespace App\Modules\developer\context\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\developer\context\Models\ContextModel;
use Input,View, Request, Form, File;
class ContextController extends Controller {

	protected $context;

	public function __construct(ContextModel $context)
	{
		$this->context = $context;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return View::make('context::index');
	}

	public function getData(){
		$data = [];

		$contexts = \DB::table('contexts')
		->select('contexts.*',\DB::raw('COUNT(modules.id) as jmlmodul'))
		->leftjoin('modules','modules.id_context','=','contexts.id')
		->orderBy('order','asc')
		->groupby('contexts.id')
		->get();

		$data['contexts'] = $contexts;

		return json_encode($data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		return View::make('context::create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postCreate(){
		$lastorder = \DB::Table('contexts')
		->where('id','!=',2)
		->where('id','!=',3)
		->orderby('order','desc')
		->first()->order;

		$context = new \App\Modules\developer\context\Models\ContextModel;
		$context->module_path       = Input::get('module_path');
		$context->name       = Input::get('name');
		$context->path      = Input::get('path');
		$context->uses = Input::get('uses');
		$context->flag = Input::get('flag');
		$context->is_nav_bar = Input::get('is_nav_bar');
		$context->icons = Input::get('icons');
		$context->order = $lastorder+1;
		$context->save();
		
		$contextName = Input::get('name');
		$contextName = str_replace(" ", "", $contextName);
		$contextName = strtolower($contextName);

		if (!\File::isDirectory(app_path().'/Modules/'.$contextName)){
			\File::makeDirectory(app_path().'/Modules/'.$contextName, 0755);
		}
		
		$permission['name'] = 'context-'.$contextName;
		$permission['description'] = 'Allow Access '.$contextName;
		\PermissionsLibrary::assignPermission($permission);

		if($context){
			$result = array('result' => 'success');
		}else{
			$result = array('result' => 'failed');
		}

		return json_encode($result);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit()
	{
		return View::make('context::edit');	
	}

	public function getDataedit(){
		$id = Request::segment(4);
		$data = \App\Modules\developer\context\Models\ContextModel::find($id);
		return json_encode($data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postEdit()
	{
		$id = Input::get('id'); 
		$context = \App\Modules\developer\context\Models\ContextModel::find($id);

		$oldContextName = $context->name;
		$oldContextName = str_replace(" ", "", $oldContextName);
		$oldContextName = strtolower($oldContextName);

		$contextName = Input::get('name');
		$contextName = str_replace(" ", "", $contextName);
		$contextName = strtolower($contextName);

		//$moduleBasePath = \Config::get('claravel::modulesPath');
		$oldDir = app_path() . '/Modules/'.$oldContextName;
		$newDir = app_path() . '/Modules/'.$contextName;
		if (\File::isDirectory($oldDir) && $oldDir != $newDir){
				\File::makeDirectory($newDir, 0755);
				\File::copyDirectory($oldDir, $newDir);
				\File::deleteDirectory($oldDir, false);
			$permissionsModel = \PermissionsModel::where('name', '=', 'context-'.$newDir)->first();
			$permissionsModel->name = 'context-'.$newDir;
			$permissionsModel->save();	
		}

		$context->name       = Input::get('name');
		$context->path      = Input::get('path');
		$context->uses = Input::get('uses');
		$context->flag = Input::get('flag');
		$context->is_nav_bar = Input::get('is_nav_bar');
		$context->icons = Input::get('icons');
		$context->save();

		if($context){
			$result = array('result' => 'success');
		}else{
			$result = array('result' => 'failed');
		}

		return json_encode($result);

	}

	public function postSort(){
		$data = \Input::get('data');
		$sort = explode(',', $data);

		for($a=0 ; $a<sizeof($sort) ; $a++){
			if($sort[$a] == 2){
				$ord=99;
			}else
			if($sort[$a] == 3){
				$ord=100;
			}else{
				$ord=$a+1;
			}
			$update = \DB::table('contexts')
			->where('id','=',$sort[$a])
			->update(array('order' => $ord));
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function postDelete(){
        $ids = Input::get('id');
        if (is_array($ids)){
            foreach($ids as $id){
				$data = \App\Modules\developer\context\Models\ContextModel::find($id);
				$contextName = str_replace(" ", "", $data->name);
				$contextName = strtolower($contextName);
					
				$moduleBasePath = '';//\Config::get('claravel::modulesPath');
				if (\File::isDirectory(app_path() . '/Modules/'.$contextName)){
					\File::deleteDirectory(app_path() . '/Modules/'.$contextName);
				}
				
				$permissionsModel = \PermissionsModel::where('name', '=', 'context-'.$contextName)->first();
				if ($permissionsModel){
					\PermissionsmatrixModel::where('permission_id','=',$permissionsModel->id)->delete();
					\PermissionsModel::find($permissionsModel->id)->delete();
				}
				$data->delete();
			}
        }
        else{
            $data = \App\Modules\developer\context\Models\ContextModel::find($ids);
			$contextName = str_replace(" ", "", $data->name);
			$contextName = strtolower($contextName);
				
			$moduleBasePath = '';//\Config::get('claravel::modulesPath');
			if (\File::isDirectory(app_path() . '/Modules/'.$contextName)){
				\File::deleteDirectory(app_path() . '/Modules/'.$contextName);
			}
			
			$permissionsModel = \PermissionsModel::where('name', '=', 'context-'.$contextName)->first();
			if ($permissionsModel){
				\PermissionsmatrixModel::where('permission_id','=',$permissionsModel->id)->delete();
				\PermissionsModel::find($permissionsModel->id)->delete();
			}
			$data->delete();
        }

        if($data){
        	$result = array('result' => 'success');
        }else{
        	$result = array('result' => 'failed');
        }

        return json_encode($result);
    }  
    
}