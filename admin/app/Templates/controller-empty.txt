<?php namespace {Namespace}\{context}\{module}\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\{context}\{module}\Models\{Module}Model;
use Input,View, Request, Form, File;

class {Module}Controller extends Controller {

	/**
	 * {Module} Repository
	 *
	 * @var {Module}
	 */
	protected ${module};

	public function __construct()
	{
	
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return View::make('{module}::index');
	}

	{controller-create-empty}

    {controller-edit-empty}

}
