dntApp.controller('RolesController', function($scope, $http, $location, DataService){
  var url = $location.absUrl().split('?')[0];
  var split = url.split('/dashboard#');
  var mainUrl = split[0];
  var currentUrl = mainUrl+split[1];

  $http.get(currentUrl+"/data")
  .then(function (response) {
    $scope.Records = response.data.roles;
  });

  $scope.checkAll = function () {
    angular.forEach($scope.Records, function (record) {
      record.Selected = $scope.selectedAll;
    });
  };

  $scope.recsDelete = function(item) {
    var data = {};
    var jml=0;
    angular.forEach($scope.Records, function (record) {
      if (record.Selected == true){
        data["id["+jml+"]"] = record.id;
        jml++;
      }
    });
    if(jml > 0){
      DataService.multipleDelete(data, item, currentUrl, jml).then(function(reports){
        if(reports != undefined){
          $scope.Records = reports.roles;
        }
      });
    }
  };

  $scope.recDelete = function(item, id) {
    DataService.singleDelete(id, item, currentUrl).then(function(reports){
      $scope.Records = reports.roles;
    });
  }
});

dntApp.controller('RolesControllerCreate', function($scope, $location, DataService){
  var url = $location.absUrl().split('?')[0];
  var split = url.split('/dashboard#');
  var mainUrl = split[0];
  var currentUrl = mainUrl+split[1].split('/create')[0];
  var hashUrl = mainUrl+'/dashboard#'+split[1].split('/create')[0];
  $scope.formData = {};
  $scope.state = ['Active','In Active'];
  $scope.formData.login_destination = 'dashboard';
  $scope.submitForm = function() {
    DataService.saveRecord('Role', currentUrl, hashUrl, this.formData);
  };
});

dntApp.controller('RolesControllerEdit', function($scope, $http, $routeParams, $location, DataService){
  var param = $routeParams.id
  var url = $location.absUrl().split('?')[0];
  var split = url.split('/dashboard#');
  var mainUrl = split[0];
  var currentUrl = mainUrl+split[1].split('/edit')[0];
  var hashUrl = mainUrl+'/dashboard#'+split[1].split('/edit')[0];

  $scope.formData = {};
  $scope.formDataBackup = {};
  $scope.permissionsContextBackup = {};
  $scope.permissionModulesBackup = {};

  $http.get(currentUrl+"/dataedit/"+param)
  .then(function (response) {
    $scope.formData = response.data.roles;
    $scope.formData.updated_at = moment().format('YYYY-MM-DD hh:mm:ss');
    $scope.formData._token = response.data._token;

    $scope.formDataBackup = angular.copy($scope.formData);

    $scope.permissions_context = response.data.permissions_context;

    if(response.data.permissionMatrix){
      $scope.permissionMatrix = response.data.permissionMatrix;
    }else{
      $scope.permissionMatrix = {};
    }

    var n = 0;
    angular.forEach($scope.permissions_context, function (permissionsContext) {
      if($scope.permissionMatrix[permissionsContext.id]){
        $scope.permissions_context[n]['flag'] = true;
      }else{
        $scope.permissions_context[n]['flag'] = false;
      }
      n++;
    });

    $scope.permissionsContextBackup = angular.copy($scope.permissions_context);

    $scope.action = response.data.action;

    $scope.permissionModules = {};

    $scope.permissions_modules = response.data.permissions_modules;
    
    $scope.module_actions = response.data.module_actions;

    angular.forEach($scope.permissions_modules, function (permissionsModules) {
      $scope.permissionModules[ permissionsModules.name ] = {};
      angular.forEach($scope.module_actions, function (moduleActions) {
        if($scope.action[permissionsModules.name][moduleActions]){
          $scope.permissionModules[ permissionsModules.name ][ moduleActions ] = {};
          $scope.permissionModules[ permissionsModules.name ][ moduleActions ]['id'] = $scope.action[permissionsModules.name][moduleActions];
          if($scope.permissionMatrix[ $scope.action[permissionsModules.name][moduleActions] ]){
            $scope.permissionModules[ permissionsModules.name ][ moduleActions ]['flag'] = true;
          }else{
            $scope.permissionModules[ permissionsModules.name ][ moduleActions ]['flag'] = false;
          }
        }
      });
    });

    $scope.permissionModulesBackup = angular.copy($scope.permissionModules);

    $scope.state = ['Active','In Active'];
  });

  $scope.checkAllContext = function () {
    angular.forEach($scope.permissions_context, function (permissionsContext) {
      permissionsContext.flag = $scope.selectedAllContext;
    });
  };

  $scope.selectedAllModules = {};

  $scope.checkAllModules = function (moduleActions) {
    angular.forEach($scope.permissions_modules, function (permissionsModules) {
      if($scope.action[permissionsModules.name][moduleActions]){
        $scope.permissionModules[ permissionsModules.name ][ moduleActions ]['flag'] = $scope.selectedAllModules[moduleActions];
      }
    });
  };

  $scope.resetRole = function () {
    $scope.formData = angular.copy($scope.formDataBackup);
  }

  $scope.resetContext = function () {
    $scope.permissions_context = angular.copy($scope.permissionsContextBackup);
  }

  $scope.resetModules = function () {
    $scope.permissionModules = angular.copy($scope.permissionModulesBackup);
  }

  function mergeObj(obj1,obj2){
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
  }

  $scope.submitForm = function() {
    var contextPermission = {};
    var n = 0;
    angular.forEach($scope.permissions_context, function (permissionsContext) {
      if(permissionsContext.flag == true){
        contextPermission["permissionmatrix["+n+"]"] = permissionsContext.id;
        n++;
      }
    });

    angular.forEach($scope.permissions_modules, function (permissionsModules) {
      angular.forEach($scope.module_actions, function (moduleActions) {
        if($scope.action[permissionsModules.name][moduleActions]){
          if($scope.permissionModules[ permissionsModules.name ][ moduleActions ]['flag'] == true){
            contextPermission["permissionmatrix["+n+"]"] = $scope.action[permissionsModules.name][moduleActions];
            n++;
          }
        }
      });
    });

    DataService.updateRecord('Role', currentUrl, hashUrl, mergeObj(this.formData, contextPermission));
  };
});