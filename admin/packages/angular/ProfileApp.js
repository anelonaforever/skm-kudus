dntApp.controller('ProfileController', function($scope, $rootScope, $timeout, Cropper, $http, $location, $log, $uibModal, $filter) {
  var url = $location.absUrl().split('?')[0];
  var split = url.split('/dashboard#');
  var mainUrl = split[0];
  var currentUrl = mainUrl+split[1];
  var file, data;

  $scope.profiledata = {};
  $scope.basicinfo = {};
  $scope.contactinfo = {};
  $scope.akun = {};

	$scope.jenis_kelamin_opt = {
		"0":{"jenis_kelamin":1, "name":"laki-laki"},
		"1":{"jenis_kelamin":2, "name":"Perempuan"},
	};

	if($rootScope.userdata == null){
		//menunggu jika masih kosong
		$timeout(function () {
      $scope.profiledata = angular.copy($rootScope.userdata);
      $scope.akun.username = angular.copy($rootScope.userdata.username);
      $scope.akun_bkp = angular.copy($scope.akun);
      $scope.basicinfo.name = $scope.profiledata.name;
			$scope.basicinfo.jenis_kelamin = $scope.profiledata.jenis_kelamin;
			$scope.basicinfo.tgl_lahir = $filter('date')($scope.profiledata.tgl_lahir, 'dd-MM-yyyy');
			$scope.basicinfo.alamat = $scope.profiledata.alamat;
			$scope.basicinfo_bkp = angular.copy($scope.basicinfo);
			$scope.contactinfo.no_telp = $scope.profiledata.no_telp;
	    $scope.contactinfo.email = $scope.profiledata.email;
	    $scope.contactinfo_bkp = angular.copy($scope.contactinfo);
    }, 3000);
	}else{
		$scope.profiledata = angular.copy($rootScope.userdata);
		$scope.akun.username = angular.copy($rootScope.userdata.username);
		$scope.akun_bkp = angular.copy($scope.akun);
		$scope.basicinfo.name = $scope.profiledata.name;
		$scope.basicinfo.jenis_kelamin = $scope.profiledata.jenis_kelamin;
		$scope.basicinfo.tgl_lahir = $filter('date')($scope.profiledata.tgl_lahir, 'dd-MM-yyyy');
		$scope.basicinfo.alamat = $scope.profiledata.alamat;
		$scope.basicinfo_bkp = angular.copy($scope.basicinfo);
		$scope.contactinfo.no_telp = $scope.profiledata.no_telp;
    $scope.contactinfo.email = $scope.profiledata.email;
    $scope.contactinfo_bkp = angular.copy($scope.contactinfo);
	}

  $scope.batalEditbi = function(){
  	$scope.edtBasicinfo = false;
  	$scope.basicinfo = angular.copy($scope.basicinfo_bkp);
  	$scope.basicinfo.tgl_lahir = $filter('date')($scope.profiledata.tgl_lahir, 'dd-MM-yyyy');
  }

  $scope.simpanBasicinfo = function(){
  	swal({
        title: "Simpan Informasi Umum?",
        type: "question",
        showCancelButton: true,
        confirmButtonColor: '#429745',
        cancelButtonColor: '#d8890b',
        confirmButtonText: '<i class="ion-checkmark"></i> Simpan',
        cancelButtonText: 'Batal'
    }).then(function(isConfirm) {
        if (isConfirm.value) {
          $http({
            url: currentUrl+"/updatebasicinfo", 
            method: "POST",
            data: $scope.basicinfo,
          }).then(function (response) {
            swal("Sukses!", "Informasi Umum telah berhasil disimpan", "success");
            $rootScope.userdata = angular.copy(response.data);
            $scope.profiledata = angular.copy($rootScope.userdata);
            $scope.edtBasicinfo = false;
          })
        }
    })
  }

  $scope.batalEditci = function(){
  	$scope.edtContactinfo = false;
  	$scope.contactinfo = angular.copy($scope.contactinfo_bkp);
  }

  $scope.simpancontactinfo = function(){
  	swal({
      title: "Simpan Informasi Kontak?",
      type: "question",
      showCancelButton: true,
      confirmButtonColor: '#429745',
      cancelButtonColor: '#d8890b',
      confirmButtonText: '<i class="ion-checkmark"></i> Simpan',
      cancelButtonText: 'Batal'
    }).then(function(isConfirm) {
      if (isConfirm.value) {
        $http({
          url: currentUrl+"/updatecontactinfo", 
          method: "POST",
          data: $scope.contactinfo,
        }).then(function (response) {
          swal("Sukses!", "Informasi Kontak telah berhasil disimpan", "success");
          $rootScope.userdata = angular.copy(response.data);
          $scope.profiledata = angular.copy($rootScope.userdata);
          $scope.edtContactinfo = false;
        })
      }
    })
  }

	/**
	* Method is called every time file input's value changes.
	* Because of Angular has not ng-change for file inputs a hack is needed -
	* call `angular.element(this).scope().onFile(this.files[0])`
	* when input's event is fired.
	*/

	$scope.onFile = function (blob) {
    Cropper.encode((file = blob)).then(function (dataUrl) {
      $rootScope.dataUrl = dataUrl;
      $timeout(hideCropper);
      $timeout(showCropper);  // wait for $digest to set image's src
    });
    $rootScope.status = true;

    var data = [];

    var modalInstance = $uibModal.open({
      animation: $rootScope.animationsEnabled,
      templateUrl: 'myModalContent.html',
      controller: 'ModalInstanceCtrl',
      size: 'sm',
      resolve: {
        data: function () {
          return data;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $rootScope.selected = selectedItem;
    }, function () {
    	$('#foto').val('');
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

	/**
	* Croppers container object should be created in controller's scope
	* for updates by directive via prototypal inheritance.
	* Pass a full proxy name to the `ng-cropper-proxy` directive attribute to
	* enable proxing.
	*/
	$rootScope.cropper = {};
	$rootScope.preview = {};
	$rootScope.cropperProxy = 'cropper.first';

	$rootScope.simpanfoto = function(){
    swal({
      title: "Simpan Foto?",
      type: "question",
      showCancelButton: true,
      confirmButtonColor: '#429745',
      cancelButtonColor: '#d8890b',
      confirmButtonText: '<i class="ion-checkmark"></i> Simpan',
      cancelButtonText: 'Batal'
    }).then(function(isConfirm) {
      if (isConfirm.value) {
      	if (!file || !data) return;
		    Cropper.crop(file, data).then(Cropper.encode).then(function(dataUrl) {
	      	$scope.preview.dataUrl = dataUrl;
	      	$http({
			      url: currentUrl+"/uploadfoto", 
			      method: "POST",
			      data: {'file': dataUrl, 'id': $scope.profiledata.id, 'username': $scope.profiledata.username, 'foto': $scope.profiledata.foto},
			    }).then(function (response) {
			    	$scope.$broadcast($scope.hideEvent);
			    	$scope.dataUrl = false;
			    	$scope.status = false;
			    	$scope.profiledata.fotoprofil = response.data.foto;
			    	$scope.userdata.fotoprofil = response.data.foto;
			    	$rootScope.cancel();
			    })
		    });
      }
    })
  }

	/**
	* Object is used to pass options to initalize a cropper.
	* More on options - https://github.com/fengyuanchen/cropper#options
	*/
	$rootScope.options = {
		aspectRatio: 1 / 1,
		viewMode: 2,
		preview: '.preview-container',
		crop: function(dataNew) {
			data = dataNew;
		}
	};

	/**
	* Showing (initializing) and hiding (destroying) of a cropper are started by
	* events. The scope of the `ng-cropper` directive is derived from the scope of
	* the controller. When initializing the `ng-cropper` directive adds two handlers
	* listening to events passed by `ng-cropper-show` & `ng-cropper-hide` attributes.
	* To show or hide a cropper `$broadcast` a proper event.
	*/
	$rootScope.showEvent = 'show';
	$rootScope.hideEvent = 'hide';

	function showCropper() { $rootScope.$broadcast($rootScope.showEvent); }
	function hideCropper() { $rootScope.$broadcast($rootScope.hideEvent); }

	$scope.edtBasicinfo = false;
  $scope.edtContactinfo = false;
  $scope.edtAkun = false;

  $scope.editBasicinfo = function(){
  	$scope.edtBasicinfo = true;
  }

  $scope.editContactinfo = function(){
  	$scope.edtContactinfo = true;
  }

  $scope.editakun = function(){
  	$scope.edtAkun = true;
  }

  $scope.batalEditakun = function(){
  	$scope.akun = angular.copy($scope.akun_bkp);
  	$scope.edtAkun = false;
  }

  $scope.simpanAkun = function(){
  	$http({
      url: currentUrl+"/updateakun", 
      method: "POST",
      data: $scope.akun,
    }).then(function (response) {
    	if (response.data.result == 'success') {
        swal({
			    type: 'success',
			    html: 'Akun berhasil diperbaharui'
				});
				$scope.edtAkun = false;
				$scope.akun.username = angular.copy(response.data.username);
				$rootScope.userdata.username = angular.copy(response.data.username);
				$rootScope.userdata.last_pwd_change = angular.copy(response.data.last_pwd_change);
				$scope.akun.ubahpwd = null;
				$scope.akun.newpassword = null;
				$scope.akun.cpassword = null;
      } else {
        swal({
			    type: 'error',
			    html: 'Terjadi Kesalahan'
				});
      }
    })	
  }

  $scope.cekOldpwd = function(){
  	swal({
		  title: 'Masukkan kata sandi lama!',
		  input: 'text',
		  showCancelButton: true,
		  inputValidator: function(value) {
        if(!value){
          return !value && 'Kata sandi lama diperlukan'
        }else{
          $http({
            url: currentUrl+"/cekoldpwd", 
            method: "POST",
            data: {'pwd': value},
          }).then(function (response) {
            if (response.data.result == 'true') {
              $scope.simpanAkun();
            } else {
              swal({type: 'warning', title: 'Kata sandi tidak tepat!'})
            }
          })
        }
		  }
		})
  }

  $scope.IsMatch=true;

	$scope.$watch('akun.cpassword', function (newValue) {
		if(newValue === $scope.akun.newpassword){
			$scope.IsMatch=true;
		}else{
			$scope.IsMatch=false;
		}
	});

	$scope.$watch('akun.newpassword', function (newValue) {
		if($scope.akun.cpassword != null && newValue != $scope.akun.cpassword){
			$scope.IsMatch=false;
		}else{
			$scope.IsMatch=true;
		}
	});

});

dntApp.controller('ModalInstanceCtrl', function($scope, $location, $rootScope, $uibModalInstance){
  var url = $location.absUrl().split('?')[0];
  var split = url.split('/dashboard#');
  var mainUrl = split[0];
  var currentUrl = mainUrl+split[1];

  $rootScope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});

dntApp.directive('imgCropper', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.find('#pilihfoto').on('click',function(){
        element.find('#foto').click();
      });
    }
  };
});