dntApp.controller('PermissionsController', function($scope, $http, $location, DataService){
  var url = $location.absUrl().split('?')[0];
  var split = url.split('/dashboard#');
  var mainUrl = split[0];
  var currentUrl = mainUrl+split[1];

  $http.get(currentUrl+"/data")
  .then(function (response) {
    $scope.Records = response.data.permissions;
  });

  $scope.checkAll = function () {
    angular.forEach($scope.Records, function (record) {
      record.Selected = $scope.selectedAll;
    });
  };

  $scope.recsDelete = function(item) {
    var data = {};
    var jml=0;
    angular.forEach($scope.Records, function (record) {
      if (record.Selected == true){
        data["id["+jml+"]"] = record.id;
        jml++;
      }
    });
    if(jml > 0){
      DataService.multipleDelete(data, item, currentUrl, jml).then(function(reports){
        if(reports != undefined){
          $scope.Records = reports.permissions;
        }
      });
    }
  };

  $scope.recDelete = function(item, id) {
    DataService.singleDelete(id, item, currentUrl).then(function(reports){
      $scope.Records = reports.permissions;
    });
  }
});

dntApp.controller('PermissionsControllerCreate', function($scope, $location, DataService){
  var url = $location.absUrl().split('?')[0];
  var split = url.split('/dashboard#');
  var mainUrl = split[0];
  var currentUrl = mainUrl+split[1].split('/create')[0];
  var hashUrl = mainUrl+'/dashboard#'+split[1].split('/create')[0];
  $scope.formData = {};
  $scope.state = ['Active','In Active'];
  $scope.submitForm = function() {
    DataService.saveRecord('Permissions', currentUrl, hashUrl, this.formData);
  };  
});

dntApp.controller('PermissionsControllerEdit', function($scope, $http, $routeParams, $location, DataService){
  var param = $routeParams.id
  var url = $location.absUrl().split('?')[0];
  var split = url.split('/dashboard#');
  var mainUrl = split[0];
  var currentUrl = mainUrl+split[1].split('/edit')[0];
  var hashUrl = mainUrl+'/dashboard#'+split[1].split('/edit')[0];
  $scope.backup = [];
  $http.get(currentUrl+"/dataedit/"+param)
  .then(function (response) {
    $scope.formData = response.data;
    $scope.backup = angular.copy($scope.formData);
  });
  $scope.reset = function () {
    $scope.formData = angular.copy($scope.backup);
    $scope.permissionForm.$setPristine();
  }
  $scope.submitForm = function() {
    DataService.updateRecord('Permissions', currentUrl, hashUrl, this.formData);
  };
});