dntApp.controller('ModulesController', function($scope, $rootScope, $http, $location, DataService, notificationService){
  var url = $location.absUrl().split('?')[0];
  var split = url.split('/dashboard#');
  var mainUrl = split[0];
  var currentUrl = mainUrl+split[1];

  $http.get(currentUrl+"/data")
  .then(function (response) {
    $scope.Records = response.data.modules;
    notificationService.once("<strong>Tips.</strong> Module dapat diurutkan dengan drag n drop.","info","moduletips");
  });

  $scope.checkAll = function () {
    angular.forEach($scope.Records, function (record) {
      record.Selected = $scope.selectedAll;
    });
  };
  
  $scope.recsDelete = function(item) {
    var data = {};
    var jml=0;

    angular.forEach($scope.Records, function (record) {
      if (record.Selected == true){
        data["id["+jml+"]"] = record.id;
        jml++;
      }
    });

    if(jml > 0){
      DataService.multipleDelete(data, item, currentUrl, jml).then(function(reports){
        if(reports != undefined){
          $scope.Records = reports.modules;
          $rootScope.refreshmenu();
        }
      });
    }
  };

  $scope.recDelete = function(item, id) {
    DataService.singleDelete(id, item, currentUrl).then(function(reports){
      $scope.Records = reports.modules;
      $rootScope.refreshmenu();
    });
  }

  $scope.sortableOptions = {
    stop: function() {      
      // this callback has the changed model
      var logEntry = $scope.Records.map(function(i){
        return i.id;
      }).join(',');

      var sortdata = {
        data : logEntry
      };

      $http({
        url: currentUrl+"/sort", 
        method: "POST",
        params: sortdata
      }).then(function () {
        $rootScope.refreshmenu();
      })
    }
  };
});

dntApp.controller('ModuleControllerCreate', function($scope, $http, $location, DataService){
  var url = $location.absUrl().split('?')[0];
  var split = url.split('/dashboard#');
  var mainUrl = split[0];
  var currentUrl = mainUrl+split[1].split('/create')[0];
  var hashUrl = mainUrl+'/dashboard#'+split[1].split('/create')[0];

  $scope.formData = {};
  $scope.formData.id_context = '';
  $scope.formData.primary_key = 'id';

  $scope.tabledata = [[{"label":"", "name":"", "input_type":"", "database_type":"", "max_length":""}]];

  $scope.input_type = ["text", "checkbox", "password", "radio", "select", "textarea"];

  $scope.database_type = ["string", "bigInteger", "integer", "float", "tinyInteger", "smallInteger", "double", "decimal", "text", "longtext", "mediumtext", "date", "dateTime", "timestamp", "time", "boolean", "binary"];

  $http.get(currentUrl+"/datacreate")
  .then(function (response) {
    $scope.contexts = response.data.contexts;
    $scope.parents = response.data.modules;
    $scope.actions = response.data.controllerActions;
    $scope.columnTypes = response.data.columnTypes;
    $scope.formData.flag = 1;
    $scope.formData.controller = 1;
    $scope.formData.rmodule_table = '0';
    $scope.formData.id_parent = 0;
    $scope.formData.module_path = 1;
    $scope.formData.order = 1;
    $scope.formData.table_name = '';
  });

  $scope.$watch(function(){
    return $scope.flag;
  }, function(){
    $scope.flag = Number($scope.flag);
  },true);

  $scope.$watch(function(){
    return $scope.controller;
  }, function(){
    $scope.controller = Number($scope.controller);
  },true);

  // remove the selected row
  $scope.removeRow = function(index){
    // remove the row specified in index
    $scope.tabledata.splice( index, 1);
    // if no rows left in the array create a blank array
    if ($scope.tabledata.length === 0){
      $scope.tabledata = [];
    }
  };

  //add a row in the array
  $scope.addRow = function(){
    // create a blank array
    var newrow = [];
      // if array is blank add a standard item
      if ($scope.tabledata.length === 0) {
        newrow = [{"label":"", "name":"", "input_type":"", "database_type":"", "max_length":""}];
      } else {
        // else cycle thru the first row's columns
        // and add the same number of items
        $scope.tabledata[0].forEach(function (row) {
          newrow.push({"label":"", "name":"", "input_type":"", "database_type":"", "max_length":""});
        });
      }
    // add the new row at the end of the array 
    $scope.tabledata.push(newrow);
  };

  $scope.getField = function(){
    $http({
      url: currentUrl+"/tablefields", 
      method: "POST",
      params: {table: $scope.formData.table_name},
    }).then(function (response) {
      $scope.tabledata = [];
      response.data.forEach(function (row) {
        if(row.Key != "PRI"){
          var tb_max="";
          if (row.Type.indexOf('varchar') >= 0){
            tb_max = row.Type.replace('varchar', '');
            tb_max = tb_max.replace('(','');
            tb_max = tb_max.replace(')','');
          }

          var data_type="";
          switch(row.Data_Type){
            case "varchar": data_type="string";break;
            case "int": data_type="integer";break;
            case "bigint": data_type="bigInteger";break;
            case "datetime": data_type="dateTime";break;
            default : data_type = row.Data_Type;break;
          };

          $scope.tabledata.push({"label":row.Field, "name":row.Field, "input_type":"text", "database_type":data_type, "max_length":tb_max});
        }else{
          $scope.formData.primary_key = row.Field;
        }
      });
    })
  };

  function mergeObj(obj1,obj2){
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
  }

  $scope.submitForm = function() {
    if($scope.rmodule_table == 0){
      $scope.table_name = '';
    }

    var data = {};
    data.controller_actions = {};
    var jml=0;

    angular.forEach($scope.actions, function (record) {
      if(record.flag == true){
        data.controller_actions[jml] = record.name;
        jml++;
      }
    });

    var data = mergeObj($scope.formData, data);
    var data1 = {};
    data1.tb_dbtype = {};
    data1.tb_intype = {};
    data1.tb_label = {};
    data1.tb_max = {};
    data1.tb_name = {};
    var jml=0;

    angular.forEach($scope.tabledata, function (record) {
      data1.tb_dbtype[jml] = record.database_type;
      data1.tb_intype[jml] = record.input_type;
      data1.tb_label[jml] = record.label;
      data1.tb_max[jml] = record.max_length;
      data1.tb_name[jml] = record.name;
      jml++;
    });

    var data = mergeObj(data, data1);

    DataService.saveRecord('Module', currentUrl, hashUrl, data);
  };

  $scope.formData.icons = 'fa fa-circle-o';

  $http.get(mainUrl+"/faicons")
  .then(function (response) {
    $scope.icons = response.data;
  });

  $scope.choseicon = function(icon){
    $scope.formData.icons = 'fa '+icon;
  }
});

dntApp.controller('ModulesControllerEdit', function($scope, $http, $routeParams, $location, DataService){
  var param = $routeParams.id
  var url = $location.absUrl().split('?')[0];
  var split = url.split('/dashboard#');
  var mainUrl = split[0];
  var currentUrl = mainUrl+split[1].split('/edit')[0];
  var hashUrl = mainUrl+'/dashboard#'+split[1].split('/edit')[0];

  $http.get(currentUrl+"/dataedit/"+param)
  .then(function (response) {
    $scope.formData = response.data;
    if($scope.formData.flag){
      $scope.formData.flag = 1;
    }else{
      $scope.formData.flag = 0;
    }
  });

  $http.get(mainUrl+"/faicons")
  .then(function (response) {
    $scope.icons = response.data;
  });

  $scope.choseicon = function(icon){
    $scope.formData.icons = 'fa '+icon;
  }
  $scope.submitForm = function() {
    DataService.updateRecord('Module', currentUrl, hashUrl, this.formData);
  };
});
