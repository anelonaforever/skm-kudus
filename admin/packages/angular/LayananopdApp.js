dntApp.controller('LayananopdController', function($scope, $http, $location, DataService){
    var url = $location.absUrl().split('?')[0];
    var split = url.split('/dashboard#');
    var mainUrl = split[0];
    var currentUrl = mainUrl+split[1];

    $scope.paging = [];
    $scope.paging.currentPage = 1;

    $scope.getData = function(currentpage = '', cari = ''){
        $http({
            url: currentUrl+"/data", 
            method: "GET",
            params: {page: currentpage, cari: cari}
        }).then(function (response) {
            $scope.paging.totalItems = response.data.total;
            $scope.Records = response.data.data;
        })
    }

    $scope.$watch('paging.currentPage', function(newValue) {
        if($scope.pencarian == undefined || $scope.pencarian == ''){
            $scope.getData(newValue, '');
        }
    });

    $scope.caridata = function(){
        $scope.getData('', $scope.pencarian);
    }

        $scope.checkAll = function () {
        angular.forEach($scope.Records, function (record) {
            record.Selected = $scope.selectedAll;
        });
    };

    $scope.recsDelete = function(item) {
        var data = {};
        var jml=0;
        angular.forEach($scope.Records, function (record) {
            if (record.Selected == true){
                data["id["+jml+"]"] = record.id;
                jml++;
            }
        });
        if(jml > 0){
            DataService.multipleDelete(data, item, currentUrl, jml, $scope.paging.currentPage).then(function(reports){
                if(reports != undefined){
                    $scope.paging.totalItems = reports.total;
                    $scope.Records = reports.data;
                }
            });
        }
    };

    $scope.recDelete = function(item, id) {
        DataService.singleDelete(id, item, currentUrl, $scope.paging.currentPage).then(function(reports){
            $scope.paging.totalItems = reports.total;
            $scope.Records = reports.data;
        });
    }

    $scope.bukatutup = function(index){
        if($scope.Records[index].collapse == true){
            $scope.Records[index].collapse = false;
        }else{
            $scope.Records[index].collapse = true;
        }
    }

});

dntApp.controller('LayananopdControllerEdit', function($scope, $http, $routeParams, $location, DataService){
    var param = $routeParams.id
    var url = $location.absUrl().split('?')[0];
    var split = url.split('/dashboard#');
    var mainUrl = split[0];
    var currentUrl = mainUrl+split[1].split('/edit')[0];
    var hashUrl = mainUrl+'/dashboard#'+split[1].split('/edit')[0];
    $scope.currentUrl = currentUrl;

    $scope.ctr = this;
    $scope.ctr.formData = [];
    $scope.ctr.layanans = [];
    $scope.ctr.datalayanans = [];

    $scope.selected = [];

    $http.get(currentUrl+"/dataedit/"+param)
    .then(function (response) {
        $scope.ctr.formData = response.data;
        $scope.ctr.datalayanans = response.data.layanans;

        angular.forEach($scope.ctr.datalayanans, function(value, key){
            $scope.selected.push(value.id);
        });
    });

    $scope.ctr.refreshDataLayanan = function(cari){
      if(cari.length > 2){
          var params = {cari: cari, id_opd: param, selected: JSON.stringify($scope.selected)};
          return $http.get(
              currentUrl + "/carilayanan",
              {params: params}
          ).then(function(response) {
              $scope.ctr.layanans = response.data;
          });
      }
    }

    $scope.ctr.tambahkanLayanan = function(data){
        $scope.ctr.datalayanans.push(data);
        $scope.selected.push(data.id);
        $scope.ctr.formData.id_layanan = '';
    }

    $scope.ctr.hapuslayanan = function(index){
        $http({
            url: currentUrl+"/hapuslayanan", 
            method: "POST",
            params: {id_opd: param, id_layanan: $scope.selected[index]}
        }).then(function (response) {
            $scope.ctr.datalayanans.splice(index, 1);
            $scope.selected.splice(index, 1);
        })
    }

    $scope.submitForm = function() {
        var data = {id_opd: param, layanans: JSON.stringify($scope.selected)};
        DataService.updateRecord('Layanan OPD', currentUrl, hashUrl, data);
    };
});