dntApp.controller('ResponmasukController', function($scope, $http, $location, DataService){
    var url = $location.absUrl().split('?')[0];
    var split = url.split('/dashboard#');
    var mainUrl = split[0];
    var currentUrl = mainUrl+split[1];

    $scope.paging = [];
    $scope.paging.currentPage = 1;

    $scope.getData = function(currentpage = '', cari = ''){
        $http({
            url: currentUrl+"/data", 
            method: "GET",
            params: {page: currentpage, cari: cari}
        }).then(function (response) {
            $scope.paging.totalItems = response.data.total;
            $scope.Records = response.data.data;
        })
    }

    $scope.$watch('paging.currentPage', function(newValue) {
        if($scope.pencarian == undefined || $scope.pencarian == ''){
            $scope.getData(newValue, '');
        }
    });

    $scope.caridata = function(){
        $scope.getData('', $scope.pencarian);
    }

    $scope.expand = function (index) {
        if ($scope.Records[index].expand == true) {
            $scope.Records[index].expand = false;
        } else {
            $scope.Records[index].expand = true
        }
    }

});



