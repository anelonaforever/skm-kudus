dntApp.controller('NilaiperunsurController', function ($scope, $http, $location, DataService, $window) {
    var url = $location.absUrl().split('?')[0];
    var split = url.split('/dashboard#');
    var mainUrl = split[0];
    var currentUrl = mainUrl + split[1];

    $scope.layanans = [];
    $scope.formData = {};

    $http.get(currentUrl + "/maindata")
        .then(function (response) {
            $scope.keterangan = response.data.keterangan;
            $scope.opds = response.data.opds;
        });

    $scope.getlayanan = function () {
        $http({
            url: currentUrl + "/layanan",
            method: "GET",
            params: {
                id_opd: $scope.formData.id_opd
            }
        }).then(function (response) {
            $scope.layanans = response.data;
        })
    }

    $scope.submitForm = function () {
        $http({
            url: currentUrl + "/data",
            method: "GET",
            params: $scope.formData
        }).then(function (response) {
            $scope.nilai = response.data;
        })
    }

    $scope.downloadPdf = function () {
        var url = currentUrl + "/downloadpdf?id_opd=" + $scope.formData.id_opd;

        if ($scope.formData.id_layanan) {
            url += "&id_layanan=" + $scope.formData.id_layanan;
        }

        var $popup = $window.open(url);
    }

});