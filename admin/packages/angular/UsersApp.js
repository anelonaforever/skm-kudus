dntApp.controller('UsersController', function($scope, $http, $location, DataService){
  var url = $location.absUrl().split('?')[0];
  var split = url.split('/dashboard#');
  var mainUrl = split[0];
  var currentUrl = mainUrl+split[1];

  $scope.paging = [];
  $scope.paging.currentPage = 1;

  $scope.getData = function(currentpage = '', cari = ''){
    $http({
      url: currentUrl+"/data", 
      method: "GET",
      params: {page: currentpage, cari: cari}
    }).then(function (response) {
      $scope.paging.totalItems = response.data.total;
      $scope.Records = response.data.data;
    })
  }

  $scope.$watch('paging.currentPage', function(newValue) {
    if($scope.pencarian == undefined || $scope.pencarian == ''){
      $scope.getData(newValue, '');
    }
  });

  $scope.caridata = function(){
    $scope.getData('', $scope.pencarian);
  }

  $scope.checkAll = function () {
    angular.forEach($scope.Records, function (record) {
      record.Selected = $scope.selectedAll;
    });
  };

  $scope.recsDelete = function(item) {
    var data = {};
    var jml=0;
    angular.forEach($scope.Records, function (record) {
      if (record.Selected == true){
        data["id["+jml+"]"] = record.id;
        jml++;
      }
    });
    if(jml > 0){
      DataService.multipleDelete(data, item, currentUrl, jml, $scope.paging.currentPage).then(function(reports){
        if(reports != undefined){
          $scope.paging.totalItems = reports.total;
          $scope.Records = reports.data;
        }
      });
    }
  };

  $scope.recDelete = function(item, id) {
    DataService.singleDelete(id, item, currentUrl, $scope.paging.currentPage).then(function(reports){
      $scope.paging.totalItems = reports.total;
      $scope.Records = reports.data;
    });
  }

});

dntApp.controller('UsersControllerCreate', function($scope, $http, $location, DataService){
  var url = $location.absUrl().split('?')[0];
  var split = url.split('/dashboard#');
  var mainUrl = split[0];
  var currentUrl = mainUrl+split[1].split('/create')[0];
  $scope.currentUrl = currentUrl;
  var hashUrl = mainUrl+'/dashboard#'+split[1].split('/create')[0];

  $http.get(currentUrl+"/rolesdata")
  .then(function (response) {
    $scope.roles = response.data;
  });

  $scope.submitForm = function() {
    DataService.saveRecord('User', currentUrl, hashUrl, this.formData);
  };
});

dntApp.controller('UsersControllerEdit', function($scope, $http, $routeParams, $location, DataService){
  var param = $routeParams.id
  var url = $location.absUrl().split('?')[0];
  var split = url.split('/dashboard#');
  var mainUrl = split[0];
  var currentUrl = mainUrl+split[1].split('/edit')[0];
  var hashUrl = mainUrl+'/dashboard#'+split[1].split('/edit')[0];
  $scope.currentUrl = currentUrl;
  $scope.backup = [];
  $http.get(currentUrl+"/dataedit/"+param)
  .then(function (response) {
    $scope.formData = response.data.data;
    $scope.roles = response.data.roles;
    $scope.backup = angular.copy($scope.formData);
    $scope.userForm.$setPristine();
  });
  $scope.submitForm = function() {
    DataService.updateRecord('User', currentUrl, hashUrl, this.formData);
  };
  $scope.reset = function () {
    $scope.formData = angular.copy($scope.backup);
    $scope.userForm.$setPristine();
  }
});