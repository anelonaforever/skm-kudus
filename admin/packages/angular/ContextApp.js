dntApp.controller('ContextController', function($scope, $rootScope, $http, $location, DataService, notificationService){
  var url = $location.absUrl().split('?')[0];
  var split = url.split('/dashboard#');
  var mainUrl = split[0];
  var currentUrl = mainUrl+split[1];

  $http.get(currentUrl+"/data")
  .then(function (response) {
    $scope.Records = response.data.contexts;
    notificationService.once("<strong>Tips.</strong> Context dapat diurutkan dengan drag n drop.","info","contexttips");
  });

  $scope.checkAll = function () {
    angular.forEach($scope.Records, function (record) {
      record.Selected = $scope.selectedAll;
    });
  };

  $scope.recsDelete = function(item) {
    var data = {};
    var jml=0;
    angular.forEach($scope.Records, function (record) {
      if (record.Selected == true && record.jmlmodul == 0 && record.id != 1){
        data["id["+jml+"]"] = record.id;
        jml++;
      }
    });
    if(jml > 0){
      DataService.multipleDelete(data, item, currentUrl, jml).then(function(reports){
        if(reports != undefined){
          $scope.Records = reports.contexts;
          $rootScope.refreshmenu();
        }
      });
    }
  };

  $scope.recDelete = function(item, id) {
    DataService.singleDelete(id, item, currentUrl).then(function(reports){
      $scope.Records = reports.contexts;
      $rootScope.refreshmenu();
    });
  }

  $scope.sortableOptions = {
    stop: function() {      
      // this callback has the changed model
      var logEntry = $scope.Records.map(function(i){
        return i.id;
      }).join(',');

      var sortdata = {
        data : logEntry
      };

      $http({
        url: currentUrl+"/sort", 
        method: "POST",
        params: sortdata
      }).then(function () {
        $rootScope.refreshmenu();
      })
    }
  };

});

dntApp.controller('ContextControllerCreate', function($scope, $http, $location, DataService){
  var url = $location.absUrl().split('?')[0];
  var split = url.split('/dashboard#');
  var mainUrl = split[0];
  var currentUrl = mainUrl+split[1].split('/create')[0];
  var hashUrl = mainUrl+'/dashboard#'+split[1].split('/create')[0];

  $scope.formData = {};
  $scope.formData.module_path = 1;
  $scope.formData.path = '';
  $scope.formData.uses = '';
  $scope.formData.flag = true;
  $scope.formData.is_nav_bar = true;

  if($scope.formData.flag){
    $scope.formData.flag = 1;
  }else{
    $scope.formData.flag = 0;
  }

  if($scope.formData.is_nav_bar){
    $scope.formData.is_nav_bar = 1;
  }else{
    $scope.formData.is_nav_bar = 0;
  }

  $scope.submitForm = function() {
    DataService.saveRecord('Context', currentUrl, hashUrl, this.formData);
  };

  $scope.formData.icons = 'fa fa-circle-o';

  $http.get(mainUrl+"/faicons")
  .then(function (response) {
    $scope.icons = response.data;
  });

  $scope.choseicon = function(icon){
    $scope.formData.icons = 'fa '+icon;
  }
});

dntApp.controller('ContextControllerEdit', function($scope, $http, $routeParams, $location, DataService){
  var param = $routeParams.id
  var url = $location.absUrl().split('?')[0];
  var split = url.split('/dashboard#');
  var mainUrl = split[0];
  var currentUrl = mainUrl+split[1].split('/edit')[0];
  var hashUrl = mainUrl+'/dashboard#'+split[1].split('/edit')[0];

  $scope.backup = [];
  $http.get(currentUrl+"/dataedit/"+param)
  .then(function (response) {
    $scope.formData = response.data;
    if($scope.formData.flag){
      $scope.formData.flag = 1;
    }else{
      $scope.formData.flag = 0;
    }
    if($scope.formData.is_nav_bar){
      $scope.formData.is_nav_bar = 1;
    }else{
      $scope.formData.is_nav_bar = 0;
    }
    $scope.backup = angular.copy($scope.formData);
  });

  $http.get(mainUrl+"/faicons")
  .then(function (response) {
    $scope.icons = response.data;
  });

  $scope.choseicon = function(icon){
    $scope.formData.icons = 'fa '+icon;
  }

  $scope.reset = function () {
    $scope.formData = angular.copy($scope.backup);
    $scope.contextform.$setPristine();
  }

  $scope.submitForm = function() {
    DataService.updateRecord('Context', currentUrl, hashUrl, this.formData);
  };
});
