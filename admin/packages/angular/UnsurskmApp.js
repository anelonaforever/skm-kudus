dntApp.controller('UnsurskmController', function($scope, $http, $location, DataService){
    var url = $location.absUrl().split('?')[0];
    var split = url.split('/dashboard#');
    var mainUrl = split[0];
    var currentUrl = mainUrl+split[1];

    $http({
        url: currentUrl+"/data", 
        method: "GET",
    }).then(function (response) {
        $scope.Records = response.data;
    })
});



