dntApp.controller('BeritaController', function($scope, $http, $location, DataService){
    var url = $location.absUrl().split('?')[0];
    var split = url.split('/dashboard#');
    var mainUrl = split[0];
    var currentUrl = mainUrl+split[1];

    $scope.paging = [];
    $scope.paging.currentPage = 1;

    $scope.getData = function(currentpage = '', cari = ''){
        $http({
            url: currentUrl+"/data", 
            method: "GET",
            params: {page: currentpage, cari: cari}
        }).then(function (response) {
            $scope.paging.totalItems = response.data.total;
            $scope.Records = response.data.data;
        })
    }

    $scope.$watch('paging.currentPage', function(newValue) {
        if($scope.pencarian == undefined || $scope.pencarian == ''){
            $scope.getData(newValue, '');
        }
    });

    $scope.caridata = function(){
        $scope.getData('', $scope.pencarian);
    }

        $scope.checkAll = function () {
        angular.forEach($scope.Records, function (record) {
            record.Selected = $scope.selectedAll;
        });
    };

    $scope.recsDelete = function(item) {
        var data = {};
        var jml=0;
        angular.forEach($scope.Records, function (record) {
            if (record.Selected == true){
                data["id["+jml+"]"] = record.id;
                jml++;
            }
        });
        if(jml > 0){
            DataService.multipleDelete(data, item, currentUrl, jml, $scope.paging.currentPage).then(function(reports){
                if(reports != undefined){
                    $scope.paging.totalItems = reports.total;
                    $scope.Records = reports.data;
                }
            });
        }
    };

    $scope.recDelete = function(item, id) {
        DataService.singleDelete(id, item, currentUrl, $scope.paging.currentPage).then(function(reports){
            $scope.paging.totalItems = reports.total;
            $scope.Records = reports.data;
        });
    }

});

dntApp.controller('BeritaControllerCreate', function ($scope, $rootScope, $http, $location, DataService, Upload) {
    var url = $location.absUrl().split('?')[0];
    var split = url.split('/dashboard#');
    var mainUrl = split[0];
    var currentUrl = mainUrl+split[1].split('/create')[0];
    var hashUrl = mainUrl+'/dashboard#'+split[1].split('/create')[0];
    $scope.currentUrl = currentUrl;
    $scope.formData = {};
    $scope.submitForm = function() {
        $scope.gambar.upload = Upload.upload({
            url: currentUrl + "/create",
            headers: {
                'optional-header': 'header-value'
            },
            data: {
                gambar: $scope.gambar,
                judul_berita: $scope.formData.judul_berita,
                isi_berita: $scope.formData.isi_berita
            }
        });

        $scope.gambar.upload.progress(function (evt) {
            $scope.gambar.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });

        $scope.gambar.upload.then(function (response) {
            if (response.data.result == 'success') {
                $scope.$broadcast($scope.hideEvent);
                swal("Sukses!", "Berita berhasil disimpan", "success");
                window.location.href = hashUrl;
            } else {
                swal("Terjadi Kesalahan!", "Berita gagal disimpan", "error");
            }
        });
    };
});

dntApp.controller('BeritaControllerEdit', function ($scope, $http, $routeParams, $location, DataService, Upload) {
    var param = $routeParams.id
    var url = $location.absUrl().split('?')[0];
    var split = url.split('/dashboard#');
    var mainUrl = split[0];
    var currentUrl = mainUrl+split[1].split('/edit')[0];
    var hashUrl = mainUrl+'/dashboard#'+split[1].split('/edit')[0];
    $scope.currentUrl = currentUrl;
    $scope.backup = [];
    $http.get(currentUrl+"/dataedit/"+param)
    .then(function (response) {
        $scope.formData = response.data;
        $scope.backup = angular.copy($scope.formData);
        $scope.moduleForm.$setPristine();
    });
    $scope.submitForm = function() {
        if($scope.gambar){
            $scope.gambar.upload = Upload.upload({
                url: currentUrl + "/edit",
                headers: {
                    'optional-header': 'header-value'
                },
                data: {
                    gambar: $scope.gambar,
                    judul_berita: $scope.formData.judul_berita,
                    isi_berita: $scope.formData.isi_berita,
                    id: $scope.formData.id
                }
            });

            $scope.gambar.upload.progress(function (evt) {
                $scope.gambar.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });

            $scope.gambar.upload.then(function (response) {
                if (response.data.result == 'success') {
                    $scope.$broadcast($scope.hideEvent);
                    swal("Sukses!", "Berita berhasil disimpan", "success");
                    window.location.href = hashUrl;
                } else {
                    swal("Terjadi Kesalahan!", "Berita gagal disimpan", "error");
                }
            });
        }else{
            DataService.updateRecord('Berita', currentUrl, hashUrl, this.formData);
        }
    };
    $scope.reset = function () {
        $scope.formData = angular.copy($scope.backup);
        $scope.moduleForm.$setPristine();
    }
});