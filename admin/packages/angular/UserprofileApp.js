dntApp.controller('UserprofileController', function($scope, $rootScope, $http, $location, $log, $routeParams) {
	var url = $location.absUrl().split('?')[0];
	var split = url.split('/dashboard#');
	var mainUrl = split[0];
	var currentUrl = mainUrl+split[1];

	$scope.profiledata = {};

	var id = $routeParams.id;

	$http({
      url: mainUrl+"/profil/profiledata", 
      method: "GET",
      params: {'id': id},
    }).then(function (response) {
      $scope.profiledata = angular.copy(response.data);
    })

});