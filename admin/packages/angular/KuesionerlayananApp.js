dntApp.controller('KuesionerlayananController', function ($scope, $http, $location, DataService, notificationService) {
    var url = $location.absUrl().split('?')[0];
    var split = url.split('/dashboard#');
    var mainUrl = split[0];
    var currentUrl = mainUrl+split[1];
    $scope.kuesioner = [];

    $scope.paging = [];
    $scope.paging.currentPage = 1;

    $http.get(currentUrl + "/datakuesioner")
    .then(function (response) {
        $scope.kuesioner = response.data;
    });

    $scope.getData = function(currentpage = '', cari = ''){
        $http({
            url: currentUrl+"/data", 
            method: "GET",
            params: {page: currentpage, cari: cari}
        }).then(function (response) {
            $scope.paging.totalItems = response.data.total;
            $scope.Records = response.data.data;
        })
    }

    $scope.$watch('paging.currentPage', function(newValue) {
        if($scope.pencarian == undefined || $scope.pencarian == ''){
            $scope.getData(newValue, '');
        }
    });

    $scope.ubahkuesioner = function(index){
        $http({
            url: currentUrl + "/ubahkuesioner",
            method: "POST",
            data: {
                id_layanan: $scope.Records[index].id,
                id_kuesioner: $scope.Records[index].id_kuesioner
            }
        }).then(function (response) {
            if (response.data.result == 'success') {
                notificationService.notif("Data telah berhasil diupdate", "success", "fa-refresh", 3000, "top", "right");
            } else {
                notificationService.notif("Data gagal diupdate", "danger", "fa-times", 3000, "top", "right");
            }
        })
    }
});



