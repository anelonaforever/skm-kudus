dntApp.controller('RentangikmController', function($scope, $http, $location, DataService){
    var url = $location.absUrl().split('?')[0];
    var split = url.split('/dashboard#');
    var mainUrl = split[0];
    var currentUrl = mainUrl+split[1];

    $scope.paging = [];
    $scope.paging.currentPage = 1;

    $scope.getData = function(currentpage = '', cari = ''){
        $http({
            url: currentUrl+"/data", 
            method: "GET",
            params: {page: currentpage, cari: cari}
        }).then(function (response) {
            $scope.paging.totalItems = response.data.total;
            $scope.Records = response.data.data;
        })
    }

    $scope.$watch('paging.currentPage', function(newValue) {
        if($scope.pencarian == undefined || $scope.pencarian == ''){
            $scope.getData(newValue, '');
        }
    });

    $scope.caridata = function(){
        $scope.getData('', $scope.pencarian);
    }

    

});



dntApp.controller('RentangikmControllerEdit', function($scope, $http, $routeParams, $location, DataService){
    var param = $routeParams.id
    var url = $location.absUrl().split('?')[0];
    var split = url.split('/dashboard#');
    var mainUrl = split[0];
    var currentUrl = mainUrl+split[1].split('/edit')[0];
    var hashUrl = mainUrl+'/dashboard#'+split[1].split('/edit')[0];
    $scope.currentUrl = currentUrl;
    $scope.backup = [];
    $http.get(currentUrl+"/dataedit/"+param)
    .then(function (response) {
        $scope.formData = response.data;
        $scope.backup = angular.copy($scope.formData);
        $scope.moduleForm.$setPristine();
    });
    $scope.submitForm = function() {
        DataService.updateRecord('Rentang IKM', currentUrl, hashUrl, this.formData);
    };
    $scope.reset = function () {
        $scope.formData = angular.copy($scope.backup);
        $scope.moduleForm.$setPristine();
    }
});