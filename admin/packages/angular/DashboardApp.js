dntApp.controller('DashboardController', function ($scope, $http, $location, $log) {
	var url = $location.absUrl().split('?')[0];
	var split = url.split('/dashboard#');
	var mainUrl = split[0];
	var currentUrl = mainUrl + split[1];

	$scope.mainUrl = mainUrl;

	$http.get(mainUrl + "/datadashboard")
		.then(function (response) {
			$scope.data = response.data;
		});
});