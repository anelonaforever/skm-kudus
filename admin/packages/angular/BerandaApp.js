dntApp.controller('BerandaController', function ($scope, $http, $location, $log, notificationService) {
    var url = $location.absUrl().split('?')[0];
    var split = url.split('/dashboard#');
    var mainUrl = split[0];
    var currentUrl = mainUrl+split[1];

    $http.get(currentUrl + "/konten")
    .then(function (response) {
        $scope.isiberanda = response.data.isi;
    });

    $scope.simpankonten = function(){
        $http({
            url: currentUrl + "/simpankonten",
            method: "POST",
            data: {
                konten: $scope.isiberanda
            }
        }).then(function (response) {
            if (response.data.result == 'success') {
                notificationService.notif("Data telah berhasil disimpan", "success", "fa-floppy-o", 3000, "top", "right");
            } else {
                notificationService.notif("Data gagal disimpan", "danger", "fa-times", 3000, "top", "right");
            }
        })
    }
});



