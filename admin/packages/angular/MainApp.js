dntApp.controller('mainController', function($scope, $rootScope, $timeout, Cropper, $http, $location, $log, notificationService) {
    var url = $location.absUrl().split('?')[0];
    var split = url.split('/dashboard#');
    var mainUrl = split[0];
    $rootScope.menus = {};
    $rootScope.userdata = null;

    $rootScope.refreshmenu = function() {
        $http({
            url: mainUrl.replace('/dashboard', '') + "/menu",
            method: "GET",
        }).then(function(response) {
            $rootScope.menus = angular.copy(response.data);
        })
    }

    $http({
        url: mainUrl.replace('/dashboard', '') + "/appdata",
        method: "GET",
    }).then(function(response) {
        $rootScope.userdata = response.data.profil;
        $rootScope.menus = angular.copy(response.data.menu);
        $rootScope.lastlogin = response.data.lastlogin;
        //hiding preloader
        $timeout(function(){
            $('#window_preloader').removeClass().addClass('slideOutDown animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                $(this).hide();
                notificationService.once("Selamat datang kembali <br>" + response.data.profil.name + "!", "rose");
            });
        },3000);
    })

    $rootScope.signout = function() {
        var href = mainUrl.replace('/dashboard', '') + "/logout";
        swal({
            title: 'Anda ingin keluar?',
            text: "Yakin pekerjaan anda sudah selesai?",
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Saya ingin keluar',
            cancelButtonText: 'Batal'
        }).then(function(isConfirm) {
            if (isConfirm.value) {
                setTimeout(function() {
                    window.location.href = href;
                }, 3000);
                swal({
                    title: 'Sampai jumpa lagi!',
                    text: 'Semoga harimu menyenangkan.',
                    imageUrl: mainUrl.replace('/dashboard', '') + '/packages/tugumuda/images/cat.gif',
                    imageHeight: 200,
                }).then(function(isConfirm1) {
                    if (isConfirm.value) {
                        window.location.href = href;
                    }
                });
            }
        })
    }

    $rootScope.fullscreen = function() {
        if (window.innerHeight == screen.height) {
            var docElement, request;
            docElement = document;
            request = docElement.cancelFullScreen || docElement.webkitCancelFullScreen || docElement.mozCancelFullScreen || docElement.msCancelFullScreen || docElement.exitFullscreen;
            if (typeof request != "undefined" && request) {
                request.call(docElement);
            }
        } else {
            var docElement, request;
            docElement = document.documentElement;
            request = docElement.requestFullScreen || docElement.webkitRequestFullScreen || docElement.mozRequestFullScreen || docElement.msRequestFullScreen;

            if (typeof request != "undefined" && request) {
                request.call(docElement);
            }
        }
    }

    $scope.tinymceOptions = {
        height: 300,
        skin: "black",
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools responsivefilemanager'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify',
        toolbar2: 'responsivefilemanager | print preview media | forecolor backcolor emoticons | bullist numlist outdent indent | link image',
        image_advtab: true,
        templates: [{
                title: 'Test template 1',
                content: 'Test 1'
            },
            {
                title: 'Test template 2',
                content: 'Test 2'
            }
        ],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ],
        image_dimensions: false,
        image_class_list: [{
            title: 'Responsive',
            value: 'img-responsive'
        }],
        filemanager_crossdomain: true,
        external_filemanager_path: "http://localhost/skm-kudus/admin/filemanager/",
        filemanager_title: "Responsive Filemanager",
        filemanager_access_key: "4y4mb4k4rp4n4asb4ng3t",
        external_plugins: {
            "filemanager": "http://localhost/skm-kudus/admin/filemanager/plugin.min.js"
        }
    };
});

//filter begin
dntApp.filter('Filesize', function() {
    return function(size) {
        if (isNaN(size))
            size = 0;

        if (size < 1024)
            return size + ' Bytes';

        size /= 1024;

        if (size < 1024)
            return size.toFixed(2) + ' Kb';

        size /= 1024;

        if (size < 1024)
            return size.toFixed(2) + ' Mb';

        size /= 1024;

        if (size < 1024)
            return size.toFixed(2) + ' Gb';

        size /= 1024;

        return size.toFixed(2) + ' Tb';
    };
});

dntApp.filter('notNull', function() {
    return function(text) {
        if (text == '' || text == null || text == '0000-00-00') {
            return '-';
        } else {
            return text;
        }
    };
});

dntApp.filter('propsFilter', function() {
    return function(items, props) {
        var out = [];
        if (angular.isArray(items)) {
            var keys = Object.keys(props);
            items.forEach(function(item) {
                var itemMatches = false;
                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }
                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }
        return out;
    };
});

//directives begin
dntApp.directive('tableTools', function($window) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.find('#searchbutton').on('click', function() {
                $('#tablesearchbar').fadeIn(200);
                if($window.innerWidth < 767){
                    $('#tombolbuat').css("margin-bottom", "-100px");
                }
                $('#kotakpencarian').focus();
            });

            element.find('#kotakpencarian').on('blur', function() {
                $('#tablesearchbar').fadeOut(200);
                if($window.innerWidth < 767){
                    $('#tombolbuat').css("margin-bottom", "0px");
                }
            });
        }
    };
});

//tabulasi
dntApp.directive('showtab', function() {
    return {
        link: function(scope, element, attrs) {
            element.click(function(e) {
                e.preventDefault();
                $(element).tab('show');
            });
        }
    };
});

dntApp.directive('materialDatepicker', function() {
  return {
    restrict: 'A',
    scope: {
      'model': '='
    },
    link: function(scope, element, attrs) {
      $(element).bootstrapMaterialDatePicker({ format : 'DD-MM-YYYY', time: false, clearButton: true, nowButton: true });
    }
  };
});

//factory & service begin
dntApp.service('notificationService', function ($http,$rootScope) {
    this.once = function (msg, type, sessionStorageName) {
        if ((typeof sessionStorage !== 'undefined') && (sessionStorage.getItem(sessionStorageName) === null)) {
            $.notify({
                // options
                message: msg,
                icon: 'fa fa-bell',
            },{
                // settings
                type: type,
                allow_dismiss: true,
                mouse_over: 'pause',
                offset: {
                    x: 20,
                    y: 80
                },
                delay: 5000,
                newest_on_top: true,
                placement: {
                    from: 'top',
                    align: 'center'
                },
                icon_type: 'class',
                template: '<div class="col-xs-11 col-sm-3 alert alert-{0} alert-with-icon" data-notify="container">'+
                    '<i data-notify="icon"></i>'+
                    '<button aria-hidden="true" class="close" type="button" data-notify="dismiss">'+
                        '<i class="material-icons">close</i>'+
                    '</button>'+
                    '<span data-notify="message">{2}</span>'+
                '</div>',
            });
            sessionStorage.setItem(sessionStorageName, true);
        }
    };
    this.notif = function (msg, type, icon = 'fa-bell', delay = 3000, from = 'top', align = 'right') {
        $.notify({
            // options
            message: msg,
            icon: 'fa '+icon,
        },{
            // settings
            type: type,
            allow_dismiss: true,
            mouse_over: 'pause',
            offset: {
                x: 20,
                y: 80
            },
            delay: delay,
            newest_on_top: true,
            placement: {
                from: from,
                align: align
            },
            icon_type: 'class',
            template: '<div class="col-xs-11 col-sm-3 alert alert-{0} alert-with-icon" data-notify="container">'+
                '<i data-notify="icon"></i>'+
                '<button aria-hidden="true" class="close" type="button" data-notify="dismiss">'+
                    '<i class="material-icons">close</i>'+
                '</button>'+
                '<span data-notify="message">{2}</span>'+
            '</div>',
        });
    };
});

dntApp.factory("DataService", function($http, $rootScope, notificationService){
    return {
        singleDelete: function(id, item, url, currentpage = '') {
            return swal({
                title: "Ingin menghapus " + item + "?",
                text: item + " akan dihapus secara permanen",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#f44336',
                cancelButtonColor: '#ff9800',
                confirmButtonText: '<i class="ion-ios-trash"></i> Hapus',
                cancelButtonText: 'Batal'
            }).then(function(isConfirm) {
                if (isConfirm.value) {
                    return $http({
                        url: url + "/delete",
                        method: "POST",
                        params: {id: parseInt(id)}
                    }).then(function(response) {
                        if (response.data.result == 'success') {
                            notificationService.notif("Data " + item + " telah berhasil dihapus", "success", "fa-trash", 3000, "top", "right");
                        } else {
                            notificationService.notif("Data " + item + " gagal dihapus", "danger", "fa-times", 3000, "top", "right");
                        }
                        return $http({
                            url: url + "/data", 
                            method: "GET",
                            params: {page: currentpage}
                        }).then(function (response) {
                            return response.data;
                        })
                    })
                }
            })
        },
        multipleDelete: function(data, item, url, jml, currentpage = '') {
            if (jml == 1) {
                var title = 'Hapus ' + item + ' yang dipilih?';
            } else {
                var title = 'Hapus beberapa ' + item + ' sekaligus?';
            }
            return swal({
                title: title,
                text: item + " akan dihapus secara permanen",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#f44336',
                cancelButtonColor: '#ff9800',
                confirmButtonText: '<i class="ion-ios-trash"></i> Hapus',
                cancelButtonText: 'Batal'
            }).then(function(isConfirm) {
                if (isConfirm.value) {
                    return $http({
                        url: url + "/delete",
                        method: "POST",
                        params: data
                    }).then(function(response) {
                        if (response.data.result == 'success') {
                            notificationService.notif("Data " + item + " telah berhasil dihapus", "success", "fa-trash", 3000, "top", "right");
                        } else {
                            notificationService.notif("Data " + item + " gagal dihapus", "danger", "fa-times", 3000, "top", "right");
                        }
                        return $http({
                            url: url + "/data", 
                            method: "GET",
                            params: {page: currentpage}
                        }).then(function (response) {
                            return response.data;
                        })
                    })
                }
            })
        },
        saveRecord: function(item, url, hashUrl, object) {
            swal({
                title: "Simpan " + item + "?",
                type: "question",
                showCancelButton: true,
                confirmButtonColor: '#4caf50',
                cancelButtonColor: '#ff9800',
                confirmButtonText: '<i class="ion-checkmark"></i> Simpan',
                cancelButtonText: 'Batal'
            }).then(function(isConfirm) {
                if (isConfirm.value) {
                    $http({
                        url: url + "/create",
                        method: "POST",
                        data: object
                    }).then(function(response) {
                        if (response.data.result == 'success') {
                            notificationService.notif("Data " + item + " telah berhasil disimpan", "success", "fa-floppy-o", 3000, "top", "right");
                            window.location.href = hashUrl;
                            if (item == 'Context' || item == 'Module') {
                                $rootScope.refreshmenu();
                            }
                        } else {
                            notificationService.notif("Data " + item + " gagal disimpan", "danger", "fa-times", 3000, "top", "right");
                        }
                    })
                }
            })
        },
        updateRecord: function(item, url, hashUrl, object) {
            swal({
                title: "Update " + item + "?",
                type: "question",
                showCancelButton: true,
                confirmButtonColor: '#4caf50',
                cancelButtonColor: '#ff9800',
                confirmButtonText: '<i class="ion-checkmark"></i> Update',
                cancelButtonText: 'Batal'
            }).then(function(isConfirm) {
                if (isConfirm.value) {
                    $http({
                        url: url + "/edit",
                        method: "POST",
                        data: object
                    }).then(function(response) {
                        if (response.data.result == 'success') {
                            notificationService.notif("Data " + item + " telah berhasil diupdate", "success", "fa-refresh", 3000, "top", "right");
                            window.location.href = hashUrl;
                            if (item == 'Context' || item == 'Module') {
                                $rootScope.refreshmenu();
                            }
                        } else {
                            notificationService.notif("Data " + item + " gagal diupdate", "danger", "fa-times", 3000, "top", "right");
                        }
                    })
                }
            })
        }
    }
});