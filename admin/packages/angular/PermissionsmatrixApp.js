dntApp.controller('PermissionMatrixController', function($scope, $http, $location, notificationService){
  var url = $location.absUrl().split('?')[0];
  var split = url.split('/dashboard#');
  var mainUrl = split[0];
  var currentUrl = mainUrl+split[1];
  $http.get(currentUrl+"/data")
  .then(function (response) {
    $scope.Records = response.data;
  });
  $scope.stateChanged = function (status, permissionId, roleId) {
    if(!status){
      var flag = 1;
    }else{
      var flag = 0;
    }
    var data = {flag: flag, permission_id: permissionId, role_id: roleId};
    $http({
      url: currentUrl+"/setpermissionsmatrix", 
      method: "POST",
      params: data
    }).then(function (response) {
      notificationService.notif("Permissions matrix berhasil diperbaharui","success","fa-check",3000);
    })
  }
});